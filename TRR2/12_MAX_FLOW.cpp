#include <iostream>
#include <fstream>
using namespace std;
/*
    Viết chương trình hoàn chỉnh tìm luồng cực đại f[][] trên mạng G = <n, E>
    biểu diễn dưới dạng ma trận trọng số c[][]
    với đỉnh phát s và đỉnh thu t sử dụng thuật toán Ford-Fulkerson:
    Yêu cầu :
        (1) Nhập ma trận trọng số biểu diễn G từ tệp DT.INP ; với s= 1 ; t= n ;
        (2) Tìm luồng cực đại f ;
        (3) Xuất kết quả ra tệp DT.OUT :
            - Dòng đầu ghi Val(f) ;
            - N dòng sau ghi f[i][j] ;
*/

const int maxint = (unsigned int)-1 >> 1;
int n, a[100][100], vs[100];
int q[10000], d[1000];
int f[100][100];
void printMatrix(int a[][100]);

bool BFS(int s, int t) {
    int u, v, dq = 1, cq = 0;
    for(v = 1; v <= n; ++v)
        vs[v] = 0;
    cq++;
    q[cq] = s;
    vs[s] = 1;
    d[s] = -1;
    while (dq <= cq) {
        u = q[dq];
        dq++;
        for (v = 1; v <= n; v++) {
            if (vs[v] == 0 && f[u][v] > 0) {
                if (v == t) {
                    d[v] = u;
                    return true;
                }
                cq++;
                q[cq] = v;
                d[v] = u;
                vs[v] = 1;
            }
        }
    }
    return false;
}

int max_fow(int s, int t) {
    int u, v;
    int maxfow = 0;
    for (u = 1; u <= n; u++)
        for (v = 1; v <= n; v++)
            f[u][v] = a[u][v];
    while (BFS(s, t)) {
        int mind = maxint;
        for (v = t; v != s; v = d[v]) {
            u = d[v];
            mind = min(mind, f[u][v]);
        }
        for (v = t; v != s; v = d[v]) {
            u = d[v];
            f[u][v] -= mind;
            f[v][u] += mind;
        }
        maxfow += mind;
    }
    return maxfow;
}

int main() {
    ifstream cin ("DT.IN");
    cin >> n;
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= n; ++j)
            cin >> a[i][j];
    }
    cin.close();

    ofstream cout ("DT.OUT");
    cout << max_fow(1, n) << endl;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            cout << f[i][j] << " ";
        } cout << endl;
    }
    cout << endl;
    cout.close();
    
    return 0;
}