#include <iostream>
using namespace std;
/*
    Viết hàm có tên là DFS(int u) trên C/C++ mô tả
    thuật toán duyệt theo chiều sâu các đỉnh của đồ thị G = <V, E>
    được biểu diễn dưới dạng ma trận kề a[][].
*/

int a[100][100], n, vs[100];
int e[100];

void DFS(int u) {
    cout << u << " ";
    vs[u] = 1;
    for (int v = 1; v <= n; v++) {
        if (vs[v] == 0 && a[u][v] == 1) {
            e[v] = u;
            DFS(v);
        }
    }
}

int main() {
    cin >> n;
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= n; ++j)
            cin >> a[i][j];
    }
    DFS(3);
    cout << endl;
    return 0;
}
