#include <iostream>
using namespace std;
/*
    Viết hàm có tên là int TPLT_BFS (int a [] []) trên C/C++
    tìm số thành phần liên thông của đồ thị G = <V, E>
    được biểu diễn dưới dạng ma trận kề a [] []
    bằng cách sử dụng hàm BFS (int u) đã biết
    mô tả thuật toán duyệt theo chiều rộng các đỉnh của đồ thị G.
*/

int a[100][100], n, vs[100];
int e[100];
int q[100];

void BFS(int u);

int ThanhPhanLT(int a[][100]) {
    int u, k = 0;
    for (u = 1; u <= n; u++)
        if (vs[u] == 0) {
            k++;
            BFS(u);
        }
    return (k);
}

int main() {
    cin >> n;
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= n; ++j)
            cin >> a[i][j];
    }
    cout << ThanhPhanLT(a) << endl;
    return 0;
}

void BFS(int u) {
    int v, dq = 1, cq = 0;
    cq++;
    q[cq] = u;
    vs[u] = 1;
    while (dq <= cq) {
        v = q[dq];
        dq++;
        //cout << v << " ";
        for (int i = 1; i <= n; i++) {
            if (vs[i] == 0 && a[v][i] == 1) {
                cq++;
                q[cq] = i;
                vs[i] = 1;
                e[i] = v;
            }
        }
    }
}