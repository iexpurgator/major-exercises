#include <iostream>
using namespace std;
/*
    Viết hàm có tên là int TPLT_DFS(int a[][]) trên C/C++
    tìm số thành phần liên thông của đồ thị G = <V, E>
    được biểu diễn dưới dạng ma trận kề a[][]
    bằng cách sử dụng hàm DFS (int u) đã biết
    mô tả thuật toán duyệt theo chiều sâu các đỉnh của đồ thị G.
*/

int a[100][100], n, vs[100];
int e[100];

void DFS(int u);

int TPLT_DFS(int a[][100]) {
    int u, k = 0;
    for (u = 1; u <= n; u++)
        if (vs[u] == 0) {
            k++;
            DFS(u);
        }
    return (k);
}

int main() {
    cin >> n;
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= n; ++j)
            cin >> a[i][j];
    }
    cout << TPLT_DFS(a) << endl;
    return 0;
}

void DFS(int u) {
    //cout << u << " ";
    vs[u] = 1;
    for (int v = 1; v <= n; v++) {
        if (vs[v] == 0 && a[u][v] == 1) {
            e[v] = u;
            DFS(v);
        }
    }
}