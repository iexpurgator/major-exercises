#include <iostream>
using namespace std;
/*
    Viết hàm có tên là EULER(int a[ ] [ ]) trên C/C++
    tìm chu trình/đường đi Euler CE[ ] của đồ thị G = <V, E>
    được biểu diễn dưới dạng ma trận kề a[ ] [ ],
    biết rằng G là đồ thị Euler/nửa Euler.
*/

int a[100][100], n, vs[100];
int e[100], s[10000], ce[10000];
int u;

void EULER(int arr[][100]) {
    int top = 0, v, k = 0;
    top++;
    s[top] = u;
    while (top > 0) {
        int v = s[top];
        int ok = 1;
        for (int x = 1; x <= n; x++) {
            if (arr[v][x] == 1) {
                top++;
                s[top] = x;
                ok = 0;
                arr[v][x] = 0;
                arr[x][v] = 0;
                break;
            }
        }
        if (ok == 1) {
            k++;
            ce[k] = v;
            top--;
        }
    }
    for (v = k; v > 0; v--)
        cout << ce[v] << " ";
}

int main() {
    cin >> n;
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= n; ++j)
            cin >> a[i][j];
    }
    u = 3;
    EULER(a);
    return 0;
}
