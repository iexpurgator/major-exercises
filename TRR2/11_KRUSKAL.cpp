#include <iostream>
using namespace std;
/*
    Viết hàm có tên là KRUSKAL(int a[ ] [ ]) trên C/C++
    tìm cây khung T[ ] nhỏ nhất của đồ thị G = <V, E>
    được biểu diễn dưới dạng ma trận trọng số a[ ] [ ] bằng cách sử dụng thuật toán Kruskal.
*/

int a[100][100];
int n, m, d[10000], c[10000], ts[10000];
int vs[100], t[100];

void KRUSKAL(int a[][100]) {
  int k = 0;
  // chuyển ma trận trọng số thành danh sách cạnh
  for (int i = 1; i <= m; ++i) {
    for (int j = 1; j <= i; ++j) {
      k++;
      if (a[i][j]) {
        d[k] = i;
        c[k] = j;
        ts[k] = a[i][j];
      }
    }
  }
  for (int i = 1; i <= m - 1; i++)
    for (int j = i + 1; j <= m; j++)
      if (ts[i] > ts[j]) {
        int tg = ts[i];
        ts[i] = ts[j];
        ts[j] = tg;
        tg = d[i];
        d[i] = d[j];

        d[j] = tg;
        tg = c[i];
        c[i] = c[j];
        c[j] = tg;
      }
  int wt = 0;
  k = 0;
  for (int i = 1; i <= n; i++)
    vs[i] = 0;
  for (int i = 1; i <= m; i++)
    if (!(vs[d[i]] != 0 && vs[d[i]] == vs[c[i]])) {
      k++;
      t[k] = i;
      wt = wt + ts[i];
      if (k == n - 1) {
        cout << wt << endl;
        cout << d[t[i]] << " " << c[t[i]] << endl;
        return;
      }
    }
}

int main() {
    cin >> n;
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= n; ++j)
            cin >> a[i][j];
    }
    KRUSKAL(a);
    cout << endl;
    return 0;
}