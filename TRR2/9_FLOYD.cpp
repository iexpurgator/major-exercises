#include <iostream>
using namespace std;
/*
    Viết hàm có tên là FLOYD(int a[ ] [ ]) trên C/C++
    tìm đường đi ngắn nhất d[ ] [ ] giữa các cặp đỉnh của đồ thị G = <V, E>
    được biểu diễn dưới dạng ma trận trọng số a[ ] [ ].
*/

int a[100][100], n, u, vs[100];
int d[100][100], e[100][100];

void FLOYD(int a[][100]) {
    int i, j, k;
    for (i = 1; i <= n; i++) {
        for (j = 1; j <= n; j++) {
            d[i][j] = a[i][j];
            e[i][j] = i;
        }
    }
    for (k = 1; k <= n; k++) {
        for (i = 1; i <= n; i++) {
            for (j = 1; j <= n; j++) {
                if (d[i][j] > d[i][k] + d[k][j]) {
                    d[i][j] = d[i][k] + d[k][j];
                    e[i][j] = k;
                }
            }
        }
    }
}

void printMatrix(int a[][100]);

int main(){
    cin >> n;
    for (int i = 1; i <= n; ++i){
        for (int j = 1; j <= n; ++j)
        cin >> a[i][j];
    }
    FLOYD(a);
    cout << endl;
    return 0;
}

void printMatrix(int matrix[][100]) {
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            if (matrix[i][j] == 32767)
                printf("%4s", "inf");
            else
                printf("%4d", matrix[i][j]);
        }
        printf("\n");
    }
    printf("\r\n");
}