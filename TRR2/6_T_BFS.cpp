#include <iostream>
using namespace std;
/*
    Viết hàm có tên là T_BFS (int a [] []) trên C/C++ tìm cây khung T []
    của đồ thị G = <V, E> được biểu diễn dưới dạng ma trận kề a [] []
    bằng cách sử dụng hàm BFS (int u) đã biết
    mô tả thuật toán duyệt theo chiều rộng các đỉnh của đồ thị G.
*/

int a[100][100], n, vs[100];
int T[100];
int q[100];
int u;
void BFS(int u);

void T_BFS(int a[][100]) {
    int v;
    for (v = 1; v <= n; v++)
        vs[v] = 0;
    BFS(u);
    int dem = 0;
    for (v = 1; v <= n; v++)
        if (vs[v] == 1) dem++;
    if (dem == n) {
        for (v = 1; v <= n; v++)
            if (T[v] != 0)
                cout << v << " " << T[v] << endl;
    } else
        cout << "Khong co Cay khung" << endl;
}

int main() {
    cin >> n;
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= n; ++j)
            cin >> a[i][j];
    }
    u = 1;
    T_BFS(a);
    return 0;
}

void BFS(int u) {
    int v, dq = 1, cq = 0;
    cq++;
    q[cq] = u;
    vs[u] = 1;
    while (dq <= cq) {
        v = q[dq];
        dq++;
        //cout << v << " ";
        for (int i = 1; i <= n; i++) {
            if (vs[i] == 0 && a[v][i] != 0) {
                cq++;
                q[cq] = i;
                vs[i] = 1;
                T[i] = v;
            }
        }
    }
}