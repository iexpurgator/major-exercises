#include <iostream>
using namespace std;
/*
    Viết hàm có tên là T_DFS (int a [] []) trên C/C++ tìm cây khung T []
    của đồ thị G = <V, E> được biểu diễn dưới dạng ma trận kề a [] []
    bằng cách sử dụng hàm DFS (int u) đã biết
    mô tả thuật toán duyệt theo chiều sâu các đỉnh của đồ thị G.
*/

int a[100][100], n, vs[100];
int T[100];
int u;
void DFS(int u);

void T_DFS(int a[][100]) {
    int v;
    for (v = 1; v <= n; v++)
        vs[v] = 0;
    DFS(u);
    int dem = 0;
    for (v = 1; v <= n; v++)
        if (vs[v] == 1) dem++;
    if (dem == n) {
        for (v = 1; v <= n; v++)
            if (T[v] != 0)
                cout << v << " " << T[v] << endl;
    } else
        cout << "Khong co Cay khung" << endl;
}

int main() {
    cin >> n;
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= n; ++j)
            cin >> a[i][j];
    }
    u = 1;
    T_DFS(a);
    return 0;
}

void DFS(int u) {
    //cout << u << " ";
    vs[u] = 1;
    for (int v = 1; v <= n; v++) {
        if (vs[v] == 0 && a[u][v] != 0) {
            T[v] = u;
            DFS(v);
        }
    }
}