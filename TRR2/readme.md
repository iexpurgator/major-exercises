# Toán rời rạc 2


| stt | name                   | check |
| ----- | ------------------------ | ------- |
| 1   | DFS(int u)             | done  |
| 2   | BFS(int u)             | done  |
| 3   | TPLT_DFS(int a[][])    | done  |
| 4   | TPLT_BFS(int a[][])    | done  |
| 5   | T_DFS(int a[][])       | done  |
| 6   | T_BFS(int a[][])       | done  |
| 7   | EULER(int a[][])       | done  |
| 8   | DIJKSTRA(int u)        | done  |
| 9   | FLOYD(int a[][])       | done  |
| 10  | PRIM(int a[][], int u) | done  |
| 11  | KRUSKAL(int a[][])     | done  |
| 12  | MAX_FLOW               | done  |
