#include <iostream>
using namespace std;
/*
    Viết hàm có tên là PRIM(int a[ ] [ ], int u) trên C/C++
    tìm cây khung T[ ] nhỏ nhất  bắt đầu tại đỉnh u của đồ thị G = <V, E>
    được biểu diễn dưới dạng ma trận trọng số a[ ] [ ] bằng cách sử dụng thuật toán Prim.
*/

int a[100][100], n, vs[100];
int e[100], d[10000];
const int maxint = (unsigned int) -1 >> 1;

void PRIM(int a[][100], int s) {
    int v;
    for (v = 1; v <= n; v++) {
        vs[v] = 0;
        d[v] = a[s][v];
        e[v] = s;
    }
    vs[s] = 1;
    d[s] = 0;
    e[s] = 0;
    int wt = 0, dem = 1;
    while (dem < n) {
        int u = 0;
        int mind = maxint;
        for (v = 1; v <= n; v++)
            if (vs[v] == 0 && d[v] < mind) {
                mind = d[v];
                u = v;
            }
        if (u == 0) {
            cout << "Khong co cay khung\r\n";
            return;
        }
        vs[u] = 1;
        wt = wt + a[u][e[u]];
        dem++;
        for (v = 1; v <= n; v++)
            if (vs[v] == 0 && d[v] > a[u][v]) {
                d[v] = a[u][v];
                e[v] = u;
            }
    }
    cout << wt << endl;
    for (v = 1; v <= n; v++)
        if (e[v] != 0)
            cout << v << " " << e[v] << endl;
            // cout << e[v] << " " << v << endl;
    return;
}

int main() {
    cin >> n;
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= n; ++j)
            cin >> a[i][j];
    }
    PRIM(a, 1);
    cout << endl;
    return 0;
}