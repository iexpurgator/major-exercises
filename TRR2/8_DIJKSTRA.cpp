#include <iostream>
using namespace std;
/*
    Viết hàm có tên là DIJKSTRA(int u) trên C/C++
    tìm đường đi ngắn nhất d[v] xuất phát từ đỉnh u đến các đỉnh v của đồ thị G = <V, E>
    được biểu diễn dưới dạng ma trận trọng số a[ ] [ ].
*/
// đồ thị có trọng số (vô cùng)

int a[100][100], n, u, vs[100];
int e[100], d[10000];
const int maxint = (unsigned int)-1 >> 1;

void DIJKSTRA(int s) {
    int u, v;
    for (v = 1; v <= n; v++) {
        d[v] = a[s][v];
        e[v] = s;
        vs[v] = 0;
    }
    d[s] = 0;
    e[s] = 0;
    u = s;
    while (1) {
        int mind = maxint;
        vs[u] = 1;
        cout << u;
        u = 0;
        for (v = 1; v <= n; v++) {
            if (vs[v] == 0 && d[v] < mind) {
                u = v;
                mind = d[v];
            }
        }
        if (u == 0)
            return;
        cout << " -> ";
        for (v = 1; v <= n; v++) {
            if (vs[v] == 0 && d[v] > d[u] + a[u][v]) {
                d[v] = d[u] + a[u][v];
                e[v] = u;
            }
        }
    }
}

int main() {
    cin >> n;
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= n; ++j)
            cin >> a[i][j];
    }
    u = 2;
    DIJKSTRA(u);
    cout << endl;
    return 0;
}
