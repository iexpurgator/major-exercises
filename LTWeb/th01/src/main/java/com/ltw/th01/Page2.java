package com.ltw.th01;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class Page2 extends HttpServlet {
//    @Override
//    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        resp.setContentType("text/html; charset=UTF-8");
//        req.getRequestDispatcher("page2.html").forward(req, resp);
//    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        resp.setContentType("text/html; charset=UTF-8");
        resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");
        String name = req.getParameter("name");
        int age = Integer.parseInt(req.getParameter("age"));
        String msg = "Bạn %s %s độ tuổi lao động";
        String s;
        PrintWriter out = resp.getWriter();

        if (age < 15) {
            s = "đang dưới";
        } else if (age < 65) {
            s = "đang trong";
        } else {
            s = "đã ngoài";
        }
//        resp.setHeader("message", String.format(msg, name, s));

        out.println("<html lang=\"vi\"><body>");
        out.printf(String.format(msg, name, s));
        out.println("</body></html>");
        out.close();
    }
}
