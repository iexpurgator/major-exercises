package ltw.bookstore.model;

import ltw.bookstore.entity.Product;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProductInfo {
    private String code;
    private String name;
    private String author;
    private double price;

    public ProductInfo(Product product) {
        this.code = product.getCode();
        this.name = product.getName();
        this.price = product.getPrice();
        this.author = product.getAuthor();
    }
}
