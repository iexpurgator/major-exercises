package ltw.bookstore.model;

import lombok.*;

@Getter
@Setter
public class CartLineInfo {

    private ProductInfo productInfo;
    private int quantity;

    public CartLineInfo() {
        this.quantity = 1;
    }

    public double getAmount() {
        return this.productInfo.getPrice() * this.quantity;
    }

}
