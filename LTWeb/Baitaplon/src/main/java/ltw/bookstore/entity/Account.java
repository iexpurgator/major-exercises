package ltw.bookstore.entity;

import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;

@Entity
@Table(name = "tbl_account")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class Account implements Serializable {
    private static final long serialVersionUID = -2054386655979281969L;

    public static final String ROLE_MANAGER = "ROLE_MANAGER";
    public static final String ROLE_EMPLOYEE = "ROLE_EMPLOYEE";

    @Id
    @Column(name = "user_name", length = 20, unique = true, nullable = false)
    private String userName;

    @Column(name = "encryted_password", length = 128, nullable = false)
    private String encrytedPassword;

    @Email
    @Column(name = "email", unique = true, nullable = false)
    private String email;

    @Column(name = "active", length = 1, nullable = false)
    private boolean active;

    @Column(name = "user_role", length = 20, nullable = false)
    private String userRole;
}
