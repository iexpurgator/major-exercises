package ltw.bookstore.entity;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "tbl_products")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class Product implements Serializable {
    private static final long serialVersionUID = -1000119078147252957L;

    @Id
    @Column(name = "code", length = 20, nullable = false, unique = true)
    private String code;

    @Column(name = "name", length = 1024, nullable = false)
    private String name;

    @Column(name = "price", nullable = false)
    private double price;

    @Lob
    @Column(name = "image", length = 10485760, nullable = true)
    private byte[] image;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date", nullable = false)
    private Date createDate;

    @Column(name = "author", length = 1024, nullable = false)
    private String author;

    @Column(name = "description", length = 10485760, nullable = true)
    private String description;

    @Column(name = "public_date", nullable = false)
    @DateTimeFormat(pattern = "yyyy-mm-dd")
    private Date publicDate;

    @Column(name = "page_number", nullable = false)
    private int pageNumber;

    public static final String GENRE_0 = "Romance";
    public static final String GENRE_1 = "Mystery";
    public static final String GENRE_2 = "Fantasy and science fiction";
    public static final String GENRE_3 = "Thrillers and horror";
    public static final String GENRE_4 = "Young adult";
    public static final String GENRE_5 = "Children’s fiction";
    public static final String GENRE_6 = "Inspirational, self-help, and religious books";
    public static final String GENRE_7 = "Biography, autobiography, and memoir";
    @Column(name = "genre", length = 64, nullable = false)
    private String genre;
}
