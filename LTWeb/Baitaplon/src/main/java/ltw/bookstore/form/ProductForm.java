package ltw.bookstore.form;

import lombok.*;
import ltw.bookstore.entity.Product;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class ProductForm {
    private String code;
    private String name;
    private double price;
    private String author;
    private String description;
    @DateTimeFormat(pattern = "yyyy-mm-dd")
    private Date publicDate;
    @DateTimeFormat(pattern = "yyyy-mm-dd")
    private Date createDate;
    private int pageNumber;
    private String genre;
    private byte[] image;
    public static final List<String> genres = Arrays.asList(Product.GENRE_0, Product.GENRE_1, Product.GENRE_2, Product.GENRE_3,
            Product.GENRE_4, Product.GENRE_5, Product.GENRE_6, Product.GENRE_7);

    private boolean newProduct = false;

    // Upload file.
    private MultipartFile fileData;

    public ProductForm() {
        this.code = RandomStringUtils.randomAlphanumeric(20);
        this.createDate = new Date();
        this.newProduct = true;
        this.image = null;
        this.fileData = null;
    }

    public ProductForm(Product product) {
        this.code = product.getCode();
        this.name = product.getName();
        this.price = product.getPrice();
        this.author = product.getAuthor();
        this.description = product.getDescription();
        this.publicDate = product.getPublicDate();
        this.pageNumber = product.getPageNumber();
        this.genre = product.getGenre();
        this.createDate = product.getCreateDate();
        this.image = product.getImage();
        this.newProduct = false;
        this.fileData = null;
    }
}
