package ltw.bookstore.form;

import lombok.*;
import ltw.bookstore.model.CustomerInfo;

@NoArgsConstructor
@Getter
@Setter
public class CustomerForm {

    private String name;
    private String address;
    private String email;
    private String phone;

    private boolean valid;

    public CustomerForm(CustomerInfo customerInfo) {
        if (customerInfo != null) {
            this.name = customerInfo.getName();
            this.address = customerInfo.getAddress();
            this.email = customerInfo.getEmail();
            this.phone = customerInfo.getPhone();
        }
    }
}
