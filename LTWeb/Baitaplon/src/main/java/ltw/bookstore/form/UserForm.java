package ltw.bookstore.form;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserForm {
    private String username;
    private String password;
    private String repassword;
    private String email;

    public boolean checkpass(){
        return password.equals(repassword);
    }
}
