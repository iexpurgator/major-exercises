package ltw.bookstore.dao;

import ltw.bookstore.entity.Product;
import ltw.bookstore.form.ProductForm;
import ltw.bookstore.model.ProductInfo;
import ltw.bookstore.pagination.PaginationResult;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.NoResultException;
import java.io.IOException;
import java.util.Date;

@Transactional
@Repository
public class ProductDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public Product findProduct(String code) {
        try {
            String sql = "Select e from " + Product.class.getName() + " e Where e.code = :code ";

            Session session = this.sessionFactory.getCurrentSession();
            Query<Product> query = session.createQuery(sql, Product.class);
            query.setParameter("code", code);
            return (Product) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public ProductInfo findProductInfo(String code) {
        Product product = this.findProduct(code);
        if (product == null) {
            return null;
        }
        return new ProductInfo(product);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void save(ProductForm productForm) {

        Session session = this.sessionFactory.getCurrentSession();

        Product product = new Product();
        boolean isNew = productForm.isNewProduct();

        product.setCode(productForm.getCode());
        product.setName(productForm.getName());
        product.setPrice(productForm.getPrice());
        product.setAuthor(productForm.getAuthor());
        product.setDescription(productForm.getDescription());
        product.setPublicDate(productForm.getPublicDate());
        product.setPageNumber(productForm.getPageNumber());
        product.setGenre(productForm.getGenre());
        product.setCreateDate(productForm.getCreateDate());

        byte[] image = productForm.getImage();
        if (productForm.getFileData() != null) {
            byte[] newimage = null;
            try {
                newimage = productForm.getFileData().getBytes();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (newimage != null && newimage.length > 0){
                image = newimage;
            }
        }
        if (image != null && image.length > 0) {
            product.setImage(image);
        }

        if (isNew) {
            product.setCreateDate(new Date());
            session.persist(product);
        } else {
            session.update(product);
        }
        // If error in DB, Exceptions will be thrown out immediately
        session.flush();
    }

    public PaginationResult<ProductInfo> queryProducts(int page, int maxResult, int maxNavigationPage, String likeName) {
        String sql = "Select new " + ProductInfo.class.getName() + "(p.code, p.name, p.author, p.price) " + " from " + Product.class.getName() + " p ";
        if (likeName != null && likeName.length() > 0) {
            sql += " Where lower(p.name) like :likeName ";
        }
        sql += " order by p.createDate desc ";

        Session session = this.sessionFactory.getCurrentSession();
        Query<ProductInfo> query = session.createQuery(sql, ProductInfo.class);

        if (likeName != null && likeName.length() > 0) {
            query.setParameter("likeName", "%" + likeName.toLowerCase() + "%");
        }
        return new PaginationResult<ProductInfo>(query, page, maxResult, maxNavigationPage);
    }

    public PaginationResult<ProductInfo> queryProducts(int page, int maxResult, int maxNavigationPage) {
        return queryProducts(page, maxResult, maxNavigationPage, null);
    }

}
