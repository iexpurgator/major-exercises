package ltw.bookstore.dao;

import ltw.bookstore.entity.Account;
import ltw.bookstore.entity.Product;
import ltw.bookstore.form.UserForm;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Transactional
@Repository
public class AccountDAO {
    @Autowired
    private SessionFactory sessionFactory;

    public Account findAccount(String userName) {
        Session session = this.sessionFactory.getCurrentSession();
        return session.find(Account.class, userName);
    }

    public void initDATA() {
        CharSequence plainPassword = "123";
        Session session = this.sessionFactory.getCurrentSession();
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String encrytedPassword = bCryptPasswordEncoder.encode(plainPassword);
        Account em1 = new Account("employee1", encrytedPassword, "employee1@e.mail", true, Account.ROLE_EMPLOYEE);
        Account man1 = new Account("manager1", encrytedPassword,"admin@e.mail", true, Account.ROLE_MANAGER);
        session.persist(em1);
        session.persist(man1);
    }

    public Account findAccountByUserName(String userName) {
        try {
            String sql = "Select e from " + Account.class.getName() + " e Where e.userName = :username ";

            Session session = this.sessionFactory.getCurrentSession();
            Query<Account> query = session.createQuery(sql, Account.class);
            query.setParameter("username", userName);
            return (Account) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public Account findAccountByEmail(String email) {
        try {
            String sql = "Select e from " + Account.class.getName() + " e Where e.email = :email ";

            Session session = this.sessionFactory.getCurrentSession();
            Query<Account> query = session.createQuery(sql, Account.class);
            query.setParameter("email", email);
            return (Account) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public Account createAccount(UserForm form) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String encrytedPassword = bCryptPasswordEncoder.encode(form.getPassword());
        Session session = this.sessionFactory.getCurrentSession();
        Account user = new Account(form.getUsername(), encrytedPassword, form.getEmail(), false, Account.ROLE_EMPLOYEE);
        session.persist(user);
        session.flush();
        return user;
    }
}
