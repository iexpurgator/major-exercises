package ltw.bookstore.validator;

import ltw.bookstore.model.CartLineInfo;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component()
public class CartFormValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return clazz == CartLineInfo.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        CartLineInfo cartLineInfo = (CartLineInfo) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "quantity", "NotEmpty.cartLineInfo.quantity");

        int count = cartLineInfo.getQuantity();
        if (count <= 0){
            errors.rejectValue("quantity", "Pattern.cartLineInfo.quantity");
        }
    }
}
