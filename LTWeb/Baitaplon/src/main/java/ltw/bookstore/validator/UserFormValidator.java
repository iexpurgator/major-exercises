package ltw.bookstore.validator;

import ltw.bookstore.dao.AccountDAO;
import ltw.bookstore.entity.Account;
import ltw.bookstore.form.UserForm;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component()
public class UserFormValidator implements Validator {

    @Autowired
    private AccountDAO accountDAO;

    private final EmailValidator emailValidator = EmailValidator.getInstance();

    private static final String USERNAME_PATTERN = "^[a-zA-Z0-9]([._](?![._])|[a-zA-Z0-9]){3,18}[a-zA-Z0-9]$";

    private static final Pattern pattern = Pattern.compile(USERNAME_PATTERN);

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz == UserForm.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserForm userForm = (UserForm) target;

        // Check the fields of ProductForm.
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty.userForm.username");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty.userForm.password");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "repassword", "NotEmpty.userForm.repassword");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty.userForm.email");

        String username = userForm.getUsername();
        if (username != null && username.length() > 0) {
            Matcher matcher = pattern.matcher(username);
            if (!matcher.matches()) {
                errors.rejectValue("username", "Pattern.userForm.username");
            } else {
                Account dbUser = accountDAO.findAccountByUserName(username);
                if (dbUser != null) {
                    errors.rejectValue("username", "Duplicate.userForm.username");
                }
            }
        }

        if (!userForm.getEmail().isEmpty()) {
            if (!this.emailValidator.isValid(userForm.getEmail())) {
                // Invalid email.
                errors.rejectValue("email", "Pattern.userForm.email");
            } else {
                Account dbUser = accountDAO.findAccountByEmail(userForm.getEmail());
                if (dbUser != null) {
                    // Email has been used by another account.
                    errors.rejectValue("email", "Duplicate.userForm.email");
                }
            }
        }

        if (!userForm.getPassword().isEmpty() && !userForm.getRepassword().isEmpty()) {
            if (!userForm.getRepassword().equals(userForm.getPassword())) {
                errors.rejectValue("repassword", "Match.userForm.repassword");
            }
        }
    }
}
