package com.library.management.book;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Controller
public class BookController {

    @GetMapping("/books")
    public String getBooks(Model model) {
        Connection connection;
        Statement statement;
        ResultSet resultSet;
        List<Book> books = new ArrayList<>();
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/ltw", "user_ltw", "1");
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM jdbc_demo.book ORDER BY bookcode;");
            while (resultSet.next()) {
                int bookcode = resultSet.getInt("bookcode");
                String title = resultSet.getString("title");
                String author = resultSet.getString("author");
                String category = resultSet.getString("category");
                int approved = resultSet.getInt("approved");
                books.add(new Book(bookcode, title, author, category, approved != 0));
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        model.addAttribute("books", books);
        return "books";
    }

    @GetMapping("/book/{bookcode}")
    public String getBook(Model model, @PathVariable String bookcode) {
        model.addAttribute("bookcode", bookcode);
        Connection connection;
        PreparedStatement ps;
        ResultSet result;
        Book book = new Book();


        if (Integer.parseInt(bookcode) < 0) {
            book.setBookcode(-1);
            book.setTitle("");
            book.setAuthor("");
            book.setCategory("");
            book.setApproved(true);
            model.addAttribute("book", book);
            return "book-detail";
        }

        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/ltw", "user_ltw", "1");
            ps = connection.prepareStatement("SELECT * FROM jdbc_demo.book WHERE bookcode = ?");
            ps.setInt(1, Integer.parseInt(bookcode));
            result = ps.executeQuery();
            while (result.next()) {
                book.setBookcode(result.getInt("bookcode"));
                book.setTitle(result.getString("title"));
                book.setAuthor(result.getString("author"));
                book.setCategory(result.getString("category"));
                book.setApproved(result.getInt("approved") != 0);
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        model.addAttribute("book", book);
        return "book-detail";
    }

    @PutMapping("/book/save/{bookcode}")
    public String updateBook(Book book, @PathVariable String bookcode) {
        Connection connection;
        PreparedStatement ps;
        int result = 0;

        if (Integer.parseInt(bookcode) < 0) return "error";
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/ltw", "user_ltw", "1");
            ps = connection.prepareStatement("UPDATE jdbc_demo.book SET title=?, author=?, category=?, approved=? WHERE bookcode=?");
            ps.setString(1, book.getTitle());
            ps.setString(2, book.getAuthor());
            ps.setString(3, book.getCategory());
            ps.setInt(4, book.isApproved() ? 1 : 0);
            ps.setInt(5, Integer.parseInt(bookcode));
            result = ps.executeUpdate();
            ps.close();
            connection.close();

            return "redirect:/books";
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return "error";
    }

    @PostMapping("/book/save/{bookcode}")
    public String addBook(Book book, @PathVariable String bookcode) {
        Connection connection;
        PreparedStatement ps;
        int result = 0;

        if (Integer.parseInt(bookcode) >= 0) {
            return "error";
        }
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/ltw", "user_ltw", "1");
            ps = connection.prepareStatement("INSERT INTO jdbc_demo.book (title, author, category, approved) VALUES (?, ?, ?, ?)");
            ps.setString(1, book.getTitle());
            ps.setString(2, book.getAuthor());
            ps.setString(3, book.getCategory());
            ps.setInt(4, book.isApproved() ? 1 : 0);
            result = ps.executeUpdate();
            ps.close();
            connection.close();

            return "redirect:/books";
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return "error";
    }

    @DeleteMapping("/book/delete/{bookcode}")
    public String deleteBook(Book book, @PathVariable String bookcode) {
        Connection connection;
        PreparedStatement ps;
        int result = 0;

        if (Integer.parseInt(bookcode) < 0) {
            return "error";
        }
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/ltw", "user_ltw", "1");
            ps = connection.prepareStatement("DELETE FROM jdbc_demo.book WHERE bookcode = ?");
            ps.setInt(1, book.getBookcode());
            result = ps.executeUpdate();
            ps.close();
            connection.close();

            return "redirect:/books";
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return "error";
    }
}
