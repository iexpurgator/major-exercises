package com.library.management;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeCtrl {
    @GetMapping("/")
    public String index() {
        return "index";
    }
}
