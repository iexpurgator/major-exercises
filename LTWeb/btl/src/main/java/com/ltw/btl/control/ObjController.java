package com.ltw.btl.control;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j(topic = "OBJECT_CONTROLLER_EXCEPTION")
public class ObjController {
    public static final Path CURRENT_FOLDER = Paths.get(System.getProperty("user.dir"));
    public static final Path staticPath = Paths.get("uploads");

    static String uploadImage(MultipartFile photo, long number) {
        if (!Files.exists(CURRENT_FOLDER.resolve(staticPath))) {
            try {
                Files.createDirectories(CURRENT_FOLDER.resolve(staticPath));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String photo_name = String.format("book_%d.png", number);
        Path file = CURRENT_FOLDER.resolve(staticPath).resolve(photo_name);
        try (OutputStream os = Files.newOutputStream(file)) {
            os.write(photo.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return photo_name;
    }

    static ByteArrayResource renderImage(String photo_name){
        Path path = ObjController.CURRENT_FOLDER.resolve(ObjController.staticPath).resolve(photo_name);
        byte[] data = {0};
        try {
            data = Files.readAllBytes(path);
        } catch (IOException e) {
            log.error(String.format("Not found Image has Path = %s", photo_name));
        }
        return new ByteArrayResource(data);
    }
}
