package com.ltw.btl.exception;

public class ElementExistsException extends RuntimeException {
    public ElementExistsException(String code) {
        super(String.format("Has exists object with code = %s", code));
    }
}
