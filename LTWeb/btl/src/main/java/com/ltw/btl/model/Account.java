package com.ltw.btl.model;

import lombok.*;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Account {
    @NotBlank
    String username;
    @NotBlank
    String password;
}

