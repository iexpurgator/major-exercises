package com.ltw.btl.service;

import java.util.List;
import java.util.Optional;

public interface IGenerate<T> {
    List<T> findAll();
    T selectById(int id);
    void save(T t);
    void remove(int id);
}
