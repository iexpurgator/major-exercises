package com.ltw.btl.service.User;

import com.ltw.btl.entity.User;
import com.ltw.btl.service.IGenerate;

public interface IUserService extends IGenerate<User> {
    User findByUsername(String username);

    User findByEmail(String email);

    boolean existsByUsername(String username);

    boolean existsByEmail(String username);

    boolean checkPassword(String p1, String p2);

    boolean checkLogin(String username, String password);
}
