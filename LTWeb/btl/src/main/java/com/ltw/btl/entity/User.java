package com.ltw.btl.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "webuser")
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Size(min = 4,max = 15, message = "More than 4 characters and small 15 characters")
    @NotBlank(message = "Not Empty")
    private String username;

    @Size(min = 8, max = 30, message = "More than 8 characters and small 30 characters")
    @NotBlank(message = "Not Empty")
    private String password;

    @Email
    @NotBlank(message = "Not Empty")
    @Column(unique = true)
    private String email;

    @Transient
    @Size(message = "Not Empty")
    @Size(min = 8, max = 30, message = "More than 8 characters and small 30 characters")
    private String passwordcf;
}
