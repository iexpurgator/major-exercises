package com.ltw.btl.exception;

public class NoSuchElementFoundException extends RuntimeException {

    public NoSuchElementFoundException(String field, String id) {
        super(String.format("Not found %s has value %s", field, id));
    }

}
