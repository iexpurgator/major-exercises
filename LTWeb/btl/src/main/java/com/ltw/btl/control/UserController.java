package com.ltw.btl.control;

import com.ltw.btl.model.Account;
import com.ltw.btl.entity.User;
import com.ltw.btl.service.User.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@AllArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("userForm", new User());
        return "register";
    }

    @PostMapping("/register")
    public String save(@ModelAttribute("userForm") @Valid User userForm, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "register";
        }
        if (!userService.checkPassword(userForm.getPassword(), userForm.getPasswordcf())) {
            model.addAttribute("errMsg", "password not match");
            return "register";
        }
        if (userService.existsByUsername(userForm.getUsername())) {
            model.addAttribute("errMsg", "User exists!");
            return "register";
        }
        redirectAttributes.addFlashAttribute("succMsg", "Register User Success!");
        userService.save(userForm);
        return "redirect:/login";
    }

    @GetMapping("/login")
    public String login(Model model, HttpSession session) {
        if (session.getAttribute("USER") != null) {
            return "redirect:/";
        }
        model.addAttribute("userForm", new Account());
        return "login";
    }

    @PostMapping("/login")
    public String postLogin(@Valid @ModelAttribute("userForm") Account userForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model, HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "login";
        }
        if (userService.checkLogin(userForm.getUsername(), userForm.getPassword())) {
            session.setAttribute("USER", userForm.getUsername());
            return "redirect:/";
        }
        model.addAttribute("msgError", "Invalid username or password!");
        return "login";
    }

    @GetMapping("/logout")
    public String logout(HttpSession session) {
        session.removeAttribute("USER");
        return "redirect:/login";
    }
}
