package com.ltw.btl.service.User;

import com.ltw.btl.entity.User;
import com.ltw.btl.exception.NoSuchElementFoundException;
import com.ltw.btl.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserService implements IUserService {
    private final UserRepository userRepository;

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User selectById(int id) {
        return userRepository.findById(id).orElseThrow(() -> new NoSuchElementFoundException("User_Id", Integer.toString(id)));
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public void remove(int id) {
        userRepository.deleteById(id);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public boolean existsByUsername(String username) {
        Optional<User> user = Optional.ofNullable(findByUsername(username));
        return user.isPresent() && user.get().getUsername().equals(username);
    }

    @Override
    public boolean existsByEmail(String email) {
        Optional<User> user = Optional.ofNullable(findByEmail(email));
        return user.isPresent() && user.get().getEmail().equals(email);
    }

    @Override
    public boolean checkPassword(String p1, String p2) {
        return p1.equals(p2);
    }

    @Override
    public boolean checkLogin(String username, String password) {
        Optional<User> user = Optional.ofNullable(findByUsername(username));
        System.out.println(user);
        return user.isPresent() && user.get().getUsername().equals(username) && user.get().getPassword().equals(password);
    }
}
