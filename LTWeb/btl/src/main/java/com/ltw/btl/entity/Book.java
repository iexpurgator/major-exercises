package com.ltw.btl.entity;

import com.ltw.btl.model.Category;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Entity
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int bookcode;

    @NotBlank(message = "Title not empty!")
    private String title;

    @NotBlank(message = "Author not empty!")
    private String author;
    private String description;

    @DateTimeFormat(pattern = "yyyy-mm-dd")
    private Date datepublic;

    @Min(value = 1)
    @Max(value = 100_000)
    private int pagenumber;

    @Enumerated(EnumType.STRING)
    private Category category;
    private String imagepath;
}
