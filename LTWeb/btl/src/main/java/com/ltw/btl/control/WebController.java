package com.ltw.btl.control;

import com.ltw.btl.entity.*;
import com.ltw.btl.model.Category;
import com.ltw.btl.service.Book.BookService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j(topic = "WEB_CONTROLLER")
@Controller
@AllArgsConstructor
public class WebController {
    private final BookService bookService;

    @GetMapping("/*")
    public String index(HttpSession session, Model model) {
        if (session.getAttribute("USER") != null) {
            model.addAttribute("userName", session.getAttribute("USER"));
        }
        List<Book> books = bookService.findAll();
        model.addAttribute("books", books);
        return "index";
    }

    @ModelAttribute("categoryList")
    public List<String> getCate() {
        return EnumSet.allOf(Category.class).stream().map(Category::name).collect(Collectors.toList());
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST, params = "action=Add")
    public String save(@ModelAttribute("bookForm") @Valid Book bookForm, BindingResult bindingResult, @RequestParam("filename") MultipartFile photo, RedirectAttributes redirectAttributes, Model model, HttpSession session) {
        if (session.getAttribute("USER") == null) return "redirect:/login";
        if (bindingResult.hasErrors()) {
            model.addAttribute("btnName", "Add");
            if (session.getAttribute("USER") != null) {
                model.addAttribute("userName", session.getAttribute("USER"));
            }
            return "detail";
        }
        String photo_name = ObjController.uploadImage(photo, System.currentTimeMillis());
        bookForm.setImagepath(photo_name);
        bookService.save(bookForm);
        redirectAttributes.addFlashAttribute("mess", "Create Book Success!");
        return "redirect:/";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST, params = "action=Edit")
    public String update(@RequestParam("bookcode") int id, Model model, HttpSession session) {
        if (session.getAttribute("USER") == null) return "redirect:/login";
        Book book = bookService.selectById(id);
        model.addAttribute("userName", session.getAttribute("USER"));
        model.addAttribute("bookForm", book);
        model.addAttribute("btnName", "Save");
        model.addAttribute("readonly", false);
        return "detail";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST, params = "action=Save")
    public String Update(@ModelAttribute("bookForm") @Valid Book bookForm, BindingResult bindingResult, RedirectAttributes redirectAttributes, HttpSession session, Model model, @RequestParam("filename") MultipartFile photo) {
        if (session.getAttribute("USER") == null) return "redirect:/login";
        if (bindingResult.hasErrors()) {
            return "detail";
        }
        if (!photo.isEmpty()) {
            String photo_name = ObjController.uploadImage(photo, System.currentTimeMillis());
            bookForm.setImagepath(photo_name);
        }
        bookService.save(bookForm);
        redirectAttributes.addFlashAttribute("mess", "Update Book Success!");
        return "redirect:/";
    }

    @GetMapping("/create")
    public String create(Model model, HttpSession session) {
        if (session.getAttribute("USER") == null) return "redirect:/login";
        model.addAttribute("userName", session.getAttribute("USER"));
        model.addAttribute("bookForm", new Book());
        model.addAttribute("btnName", "Add");
        model.addAttribute("method", "post");
        return "detail";
    }

    @GetMapping("/book")
    public String detail(@RequestParam("id") int id, Model model, HttpSession session) {
        if (session.getAttribute("USER") == null) return "redirect:/login";
        Book book = bookService.selectById(id);
        model.addAttribute("userName", session.getAttribute("USER"));
        model.addAttribute("bookForm", book);
        model.addAttribute("btnName", "Edit");
        model.addAttribute("readonly", true);
        model.addAttribute("method", "get");
        return "detail";
    }

    @GetMapping("uploads/{photo}")
    public ResponseEntity<ByteArrayResource> getImage(@PathVariable("photo") String photo, HttpSession session) {
        if (session.getAttribute("USER") == null) ResponseEntity.badRequest().build();
        ByteArrayResource byteArrayResource = ObjController.renderImage(photo);
        if (byteArrayResource.contentLength() != 0) {
            return ResponseEntity.ok().contentLength(byteArrayResource.contentLength()).contentType(MediaType.parseMediaType("image/png")).body(byteArrayResource);
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/bookDelete")
    public String delete(@RequestParam("id") int id, RedirectAttributes redirectAttributes, HttpSession session) {
        if (session.getAttribute("USER") == null) return "redirect:/login";
        bookService.remove(id);
        redirectAttributes.addFlashAttribute("msg", "Delete book success!");
        return "redirect:/";
    }
}
