package com.ltw.btl.service.Book;

import com.ltw.btl.entity.Book;
import com.ltw.btl.exception.NoSuchElementFoundException;
import com.ltw.btl.repository.BookRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class BookService implements IBookService {
    private final BookRepository bookRepository;

    @Override
    public List<Book> findAll() {
        return bookRepository.findAll();
    }

    @Override
    public Book selectById(int id) {
        return bookRepository.findById(id).orElseThrow(() -> new NoSuchElementFoundException("Book_Code", Integer.toString(id)));
    }

    @Override
    public void save(Book book) {
        bookRepository.save(book);
    }

    @Override
    public void remove(int id) {
        bookRepository.deleteById(id);
    }

}
