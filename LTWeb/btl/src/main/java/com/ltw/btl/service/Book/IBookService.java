package com.ltw.btl.service.Book;

import com.ltw.btl.entity.Book;
import com.ltw.btl.service.IGenerate;

import java.util.Optional;

public interface IBookService extends IGenerate<Book> {
   Book selectById(int id);
}
