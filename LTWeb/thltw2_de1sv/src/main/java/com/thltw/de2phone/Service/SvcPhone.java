package com.thltw.de2phone.Service;

import com.thltw.de2phone.Entity.Phone;
import com.thltw.de2phone.Exceprion.ElementExistsException;
import com.thltw.de2phone.Exceprion.NoSuchElementFoundException;
import com.thltw.de2phone.Model.InputPhone;
import com.thltw.de2phone.Repository.RepoPhone;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class SvcPhone {
    private final RepoPhone repo;

    public List<Phone> selectAllObjects() {
        return repo.findAllByOrderByName();
    }

    public Phone selectObject(UUID id) {
        return repo.findById(id).orElseThrow(() -> new NoSuchElementFoundException("ID", id.toString()));
    }

    public void saveObject(InputPhone inPhone, String id) {
        Phone phone = new Phone();
        if (!id.equals("")) {
            UUID uid = UUID.fromString(id);
            phone.setId(uid);
            if (!repo.findFirstById(uid).getCode().equals(inPhone.getCode())){
                throw new NoSuchElementFoundException("Code", inPhone.getCode());
            }
        } else {
            if (repo.existsByCode(inPhone.getCode())) {
                throw new ElementExistsException(inPhone.getCode());
            }
        }
        phone.setCode(inPhone.getCode());
        phone.setName(inPhone.getName());
        phone.setPrice(inPhone.getPrice());
        phone.setBrand(inPhone.getBrand());
        phone.setSold(inPhone.isSold() ? 1 : 0);
        repo.save(phone);
    }

    public void deleteObj(UUID id) {
        repo.deleteById(id);
    }
}
