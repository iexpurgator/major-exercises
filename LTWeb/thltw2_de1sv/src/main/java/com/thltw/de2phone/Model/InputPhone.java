package com.thltw.de2phone.Model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@RequiredArgsConstructor
public class InputPhone {
    private String id;
    @NotEmpty
    private String code;
    @NotNull
    private String name;
    @NotNull
    private int price;
    @NotNull
    private String brand;
    private boolean sold;
}
