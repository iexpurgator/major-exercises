package com.thltw.de2phone.Control;

import com.thltw.de2phone.Entity.Phone;
import com.thltw.de2phone.Service.SvcPhone;
import com.thltw.de2phone.Model.InputPhone;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Slf4j(topic = "PHONE_CONTROLLER")
@Controller
@AllArgsConstructor
public class CtrlPhone {
    private final SvcPhone service;

    @GetMapping("/phones")
    public String getObjects(Model model) {
        List<Phone> phones = service.selectAllObjects();
        model.addAttribute("phones", phones);
        return "phones";
    }

    @GetMapping("/phone/{id}")
    public String getObject(Model model, @PathVariable String id) {
        model.addAttribute("id", id);
        InputPhone inPhone = new InputPhone();

        if (id.equals("-1")) {
            inPhone.setId("-1");
            inPhone.setCode("");
            inPhone.setName("");
            inPhone.setPrice(0);
            inPhone.setBrand("");
            inPhone.setSold(false);
            model.addAttribute("phone", inPhone);
            return "phone-detail";
        }
        Phone phone = service.selectObject(UUID.fromString(id));
        inPhone.setId(phone.getId().toString());
        inPhone.setCode(phone.getCode());
        inPhone.setName(phone.getName());
        inPhone.setPrice(phone.getPrice());
        inPhone.setBrand(phone.getBrand());
        inPhone.setSold(phone.getSold() != 0);
        model.addAttribute("phone", inPhone);
        return "phone-detail";
    }

    @PostMapping("/phone/save/-1")
    public String addObject(InputPhone inPhone) {
        service.saveObject(inPhone, "");
        return "redirect:/phones";
    }

    @PutMapping("/phone/save/{id}")
    public String updateObject(InputPhone inPhone, @PathVariable String id) {
        service.saveObject(inPhone, id);
        return "redirect:/phones";
    }

    @DeleteMapping("/phone/delete/{id}")
    public String deleteObject(@PathVariable String id) {
        service.deleteObj(UUID.fromString(id));
        return "redirect:/phones";
    }
}
