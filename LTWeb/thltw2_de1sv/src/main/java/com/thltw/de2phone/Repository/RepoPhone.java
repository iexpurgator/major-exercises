package com.thltw.de2phone.Repository;

import com.thltw.de2phone.Entity.Phone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface RepoPhone extends JpaRepository<Phone, UUID> {
    List<Phone> findAllByOrderByName();
    Phone findFirstById(UUID id);
    boolean existsByCode(String code);
}
