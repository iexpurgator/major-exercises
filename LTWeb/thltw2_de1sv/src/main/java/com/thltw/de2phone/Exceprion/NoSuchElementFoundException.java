package com.thltw.de2phone.Exceprion;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class NoSuchElementFoundException extends RuntimeException {

    public NoSuchElementFoundException(String field, String id) {
        super(String.format("Not found Phone has %s = %s", field, id));
    }

}
