package com.thltw.de2phone.Exceprion;

public class ElementExistsException extends RuntimeException {
    public ElementExistsException(String code) {
        super(String.format("Has exists phone with code = %s", code));
    }
}
