package com.thltw.de2phone.Entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
public class Phone {
    @Id
    @GeneratedValue
    @Column(updatable = false, nullable = false, unique = true)
    @Type(type="org.hibernate.type.UUIDCharType")
    private UUID id;
    @Column(updatable = false, nullable = false, unique = true)
    private String code;
    private String name;
    private int price;
    private String brand;
    private int sold;
}
