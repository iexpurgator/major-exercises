package com.thltw.de1sv.Control;

import com.thltw.de1sv.DAO.DaoStu;
import com.thltw.de1sv.Model.Student;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.*;
import java.util.List;

@Controller
public class CtrlStu {

    private final DaoStu dao = new DaoStu();

    @GetMapping("/students")
    public String getStudents(Model model) {
        List<Student> students = dao.selectAllObjects();
        model.addAttribute("students", students);
        return "students";
    }

    @GetMapping("/student/{dbCode}")
    public String getStudent(Model model, @PathVariable int dbCode) {
        model.addAttribute("dbCode", dbCode);
        Student student = new Student();

        if (dbCode == -1) {
            student.setDbCode(-1);
            student.setId("");
            student.setName("");
            student.setDob(null);
            student.setMajor("");
            student.setVaccinated(false);
            model.addAttribute("student", student);
            return "student-detail";
        } else if (dbCode <= 0) return "error";

        student = dao.selectObject(dbCode);
        model.addAttribute("student", student);
        return "student-detail";
    }

    @PostMapping("/student/save/{dbCode}")
    public String addStudent(Student student, @PathVariable int dbCode) {
        if (dbCode != -1) return "error";
        try {
            int ret = dao.insertObject(student);
            if (ret >= 0) {
                return "redirect:/students";
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "error";
    }

    @PutMapping("/student/save/{dbCode}")
    public String updateStudent(Student student, @PathVariable int dbCode) {
        if (dbCode <= 0) return "error";
        try {
            if (dao.updateObject(student, dbCode) >= 0) {
                return "redirect:/students";
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "error";
    }

    @DeleteMapping("/student/delete/{dbCode}")
    public String deleteStudent(@PathVariable int dbCode) {
        if (dbCode <= 0) return "error";
        try {
            if (dao.deleteObj(dbCode) >= 0) {
                return "redirect:/students";
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "error";
    }
}
