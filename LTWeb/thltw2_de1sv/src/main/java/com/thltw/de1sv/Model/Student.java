package com.thltw.de1sv.Model;

import java.sql.Date;

/**
CREATE TABLE IF NOT EXISTS jdbc_demo.student (
dbCode SERIAL PRIMARY KEY,
id TEXT NOT NULL UNIQUE,
name TEXT NOT NULL,
dob DATE NOT NULL,
major TEXT NOT NULL,
vaccinated INT NOT NULL
);
*/
public class Student {
    private int dbCode;
    private String id;
    private String name;
    private Date dob;
    private String major;
    private boolean vaccinated;

    public Student() {
    }

    public Student(int dbCode, String id, String name, Date dob, String major, boolean vaccinated) {
        this.dbCode = dbCode;
        this.id = id;
        this.name = name;
        this.dob = dob;
        this.major = major;
        this.vaccinated = vaccinated;
    }

    public int getDbCode() {
        return dbCode;
    }

    public void setDbCode(int dbCode) {
        this.dbCode = dbCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public boolean getVaccinated() {
        return vaccinated;
    }

    public void setVaccinated(boolean vaccinated) {
        this.vaccinated = vaccinated;
    }
}
