package com.thltw.de1sv.DAO;

import com.thltw.de1sv.Model.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DaoStu {

    private final String dbClassname = "org.postgresql.Driver";
    private final String dbUrl = "jdbc:postgresql://localhost:5432/ltw";
    private final String dbUsrnm = "user_ltw";
    private final String dbPasswd = "1";

    private final String SELECT_ALL = "SELECT * FROM jdbc_demo.student ORDER BY dbCode;";
    private final String SELECT_OBJ_BY_DBCODE = "SELECT * FROM jdbc_demo.student WHERE dbCode = ?;";
    private final String INSERT_OBJ = "INSERT INTO jdbc_demo.student (id, name, dob, major, vaccinated) VALUES (?, ?, ?, ?, ?);";
    private final String UPDATE_OBJ = "UPDATE jdbc_demo.student SET id=?, name=?, dob=?, major=?, vaccinated=? WHERE dbCode = ?;";
    private final String DELETE_OBJ = "DELETE FROM jdbc_demo.student WHERE dbCode = ?;";

    public DaoStu() {
    }

    protected Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName(dbClassname);
            connection = DriverManager.getConnection(dbUrl, dbUsrnm, dbPasswd);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public List<Student> selectAllObjects() {
        List<Student> objects = new ArrayList<>();
        try (Connection connection = getConnection(); PreparedStatement ps = connection.prepareStatement(SELECT_ALL)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int dbCode = rs.getInt("dbCode");
                String id = rs.getString("id");
                String name = rs.getString("name");
                Date dob = rs.getDate("dob");
                String major = rs.getString("major");
                boolean vaccinated = rs.getInt("vaccinated") != 0;
                objects.add(new Student(dbCode, id, name, dob, major, vaccinated));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return objects;
    }

    public Student selectObject(int dbCode) {
        Student object = new Student();
        try (Connection connection = getConnection(); PreparedStatement ps = connection.prepareStatement(SELECT_OBJ_BY_DBCODE)) {
            ps.setInt(1, dbCode);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                object.setDbCode(rs.getInt("dbCode"));
                object.setId(rs.getString("id"));
                object.setName(rs.getString("name"));
                object.setDob(Date.valueOf(rs.getString("dob")));
                object.setMajor(rs.getString("major"));
                object.setVaccinated(rs.getInt("vaccinated") != 0);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return object;
    }

    public int insertObject(Student student) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement ps = connection.prepareStatement(INSERT_OBJ);
        ps.setString(1, student.getId());
        ps.setString(2, student.getName());
        ps.setDate(3, student.getDob());
        ps.setString(4, student.getMajor());
        ps.setInt(5, student.getVaccinated() ? 1 : 0);
        return ps.executeUpdate();
    }

    public int updateObject(Student student, int dbCode) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement ps = connection.prepareStatement(UPDATE_OBJ);
        if (dbCode != student.getDbCode()) return 1;
        ps.setString(1, student.getId());
        ps.setString(2, student.getName());
        ps.setDate(3, student.getDob());
        ps.setString(4, student.getMajor());
        ps.setInt(5, student.getVaccinated() ? 1 : 0);
        ps.setInt(6, student.getDbCode());
        return ps.executeUpdate();
    }

    public int deleteObj(int dbCode) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement ps = connection.prepareStatement(DELETE_OBJ);
        ps.setInt(1, dbCode);
        return ps.executeUpdate();
    }
}
