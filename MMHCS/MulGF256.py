def Mul(a, b):
    ai = a
    c = 0
    # Vòng lặp lấy lần lượt các bit trong b từ 0->7 (từ phải sang trái)
    for bi in list(map(int, list(bin(b)[2:])[::-1])):
        # Nếu bit ngoài cùng bên phải của b là 1, tính c bằng cách thực hiện XOR c với a,
        # tương đương với một phép cộng đa thức.
        if bi == 1:
            c = c ^ ai
        # Đặt f = a7
        f = ai >> 7
        # Dịch trái a 1 bit, loại bỏ a7
        ai = ai << 1
        # Nếu f = 1, tính a bằng cách XOR a với số hexa 0x1B (00011011)
        # Trên python phải xor với 0x11B vì biến không bị giới hạn ở 8 bit
        if f == 1:
            ai = ai ^ 0x11B
    return c


print(hex(Mul(0b01010011, 0b11001010)))
