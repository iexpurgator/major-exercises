#!/usr/bin/env python3
def otp(c, k):
    m = ''
    for i in range(len(c)):
        m += chr(((ord(c[i]) - ord(k[i % len(k)])) % 26) + ord('A'))
    return m


def brute(n, k):
    a = [i for i in range(k+1)]
    j = k
    while True:
        if a[j] == n:
            j -= 1;
            if j == 0: break
        else:
            a[j] += 1
            while j < k:
                j += 1
                a[j] = 1
            key = 'KH'
            for i in range(1, k+1): key += chr(a[i] - 1 + ord('A'))
            key += 'U'
            print(key, otp('UOOWO', key))


# brute(26, 2)

print(otp('UOOWO', 'KHGSU'))
print(otp('UOOWO', 'KHUYU'))