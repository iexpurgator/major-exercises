def EculidExt(e, phi):
    # (1)
    x1, y1 = 0, 1
    x2, y2 = 1, 0
    # (2)
    while phi > 0:
        # (2.1)
        h = e // phi
        r = e - (h * phi)  # e % phi
        x = x2 - (h * x1)
        y = y2 - (h * y1)
        # (2.2)
        e = phi
        phi = r
        x2 = x1
        x1 = x
        y2 = y1
        y1 = y
    # (3)
    d = x2
    y = y2
    return d


def toBin(x):
    if x == (0 or 1):
        return [x]
    bit = []
    while x:
        bit.append(x % 2)
        x >>= 1
    return bit


def PowerMod(m, e, n):
    # (1)
    b = 1
    if e == 0:
        return b
    # (2)
    A = m
    e = toBin(e)
    k = len(e)
    # (3)
    if e[0] == 1:
        b = m
    # (4)
    for i in range(1, k):
        #  (4.1)
        A = (A * A) % n
        #  (4.2)
        if e[i] == 1:
            b = (A * b) % n
    # (5)
    return b


if __name__ == '__main__':
    p = 2357
    q = 2551
    e = 3674911
    n = p*q
    phi = (p-1)*(q-1)
    d = EculidExt(e, phi)
    print('d =', d, f'(with e = {e})')
    print('d =', EculidExt(11, phi), f'(with e = 11)')
    m = 5234673
    c = PowerMod(m, e, n)
    print('c =', c)
    m_dec = PowerMod(c, d, n)
    print('m =', m_dec)
    print(m_dec == m)
