#!/usr/bin/env python3
cipher = 'Li_qex_Geiwev'
cipher1 = 'Gd_lzs_Bzdrzq'
lowlet = [chr(i) for i in range(ord('a'), ord('z')+1)]
upplet = [chr(i) for i in range(ord('A'), ord('Z')+1)]

def brute_ceasar(cipher):
    for k in range(1, 26):
        message = ''
        for c in cipher:
            if c not in lowlet and c not in upplet:
                message += c
            else:
                ord_a = ord('A') if c.isupper() else ord('a')
                message += chr((ord(c) - ord_a + (26 - k)) % 26 + ord_a)
        print("key = %2d: %s" % (k, message))

# for i in range(len(cipher)):
#     print((ord(cipher[i]) - ord(cipher1[i])) % 26, end=' ')

brute_ceasar(cipher)
# brute_ceasar(cipher1)