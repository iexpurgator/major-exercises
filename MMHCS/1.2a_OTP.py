#!/usr/bin/env python3
def otp(c, k):
    m = ''
    for i in range(len(c)):
        m += chr(((ord(c[i]) - ord(k[i % len(k)])) % 26) + ord('A'))
    return m


ciphertext = 'BLWP'
key_1 = 'FHWY'
key_2 = 'IESR'

print(otp(ciphertext, key_1))
print(otp(ciphertext, key_2))
