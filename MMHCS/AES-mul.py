# https://cs.uno.edu/~dbilar/10CSCI6130-Cryptography/papers/Neil%20(2002)%20very%20good%20laws%20of%20crypto%20.pdf

def FFMul(a, b):
    bb = b
    r = 0
    for aa in list(map(int, list(bin(a)[2:])[::-1])):
        if aa == 1:
            r = r ^ bb
        t = bb & 0x80 # lấy bit cuối cùng của byte
        bb = bb << 1
        if t != 0:
            bb = bb ^ 0x11B
    return r


def ByteMul(a, b):
    abin = list(map(int, list(bin(a)[2:])[::-1]))
    t = 0
    r = 0
    for i, aa in enumerate(abin):
        t = b << i
        if aa == 1:
            # print('{:014b}'.format(t))
            r ^= t
    while (len(bin(r)) - 2) > 8:
        m = 0x11B << ((len(bin(r)) - 2) - 9)
        # print('{:014b}'.format(r))
        # print('{:014b}'.format(m))
        r ^= m
    # print('{:014b}'.format(r))
    print(hex(r))


print(hex(FFMul(0xb6, 0x53)))
ByteMul(0xb6, 0x53)
