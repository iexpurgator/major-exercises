def printm(m):
    for i in range(len(m)):
        for j in range(len(m[0])):
            print('{:02x}'.format(m[i][j]), end = ' ')
        print()
    print()

def Mul(a, b):
    ai = a
    c = 0
    # Vòng lặp lấy lần lượt các bit trong b từ 0->7 (từ phải sang trái)
    for bi in list(map(int, list(bin(b)[2:])[::-1])):
        # Nếu bit ngoài cùng bên phải của b là 1, tính c bằng cách thực hiện XOR c với a,
        # tương đương với một phép cộng đa thức.
        if bi == 1:
            c = c ^ ai
        # Đặt f = a7
        f = ai >> 7
        # Dịch trái a 1 bit, loại bỏ a7
        ai = ai << 1
        # Nếu f = 1, tính a bằng cách XOR a với số hexa 0x1B (00011011)
        # Trên python phải xor với 0x11B vì biến không bị giới hạn ở 8 bit
        if f == 1:
            ai = ai ^ 0x11B
    return c

def MixCol(m, a = [[0x2, 0x3, 0x1, 0x1]]):
    for i in range(1,4): a.append(a[i-1][3:] + a[i-1][:3])
    ret = [[0]*4 for _ in range(4)]

    for i in range(4):
        for j in range(4):
            for k in range(4):
                ret[i][j] ^= Mul(a[i][k], m[k][j])
    return ret

m1 = [
    [0x97, 0x4D, 0xF2, 0x87],
    [0xEC, 0x90, 0x4C, 0x6E],
    [0xC3, 0x4A, 0xE7, 0x46],
    [0x95, 0xD8, 0x8C, 0xA6]
]

mixM1 = MixCol(m1)

printm(mixM1)

invMixM1 = MixCol(mixM1, a = [[0xe, 0xb, 0xd, 0x9]])

print(invMixM1 == m1)