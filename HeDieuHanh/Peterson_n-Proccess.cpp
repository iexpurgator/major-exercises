void critical() {}
void noncritical() {}

#define N 3

int flag[N];
int turn[N - 1];

bool sameOrHigher(int i, int j) {
    for (int k = 0; k < N; k++)                   // kiểm tra giá trị của các flag[k] khác flag[i]
        if (k != i && flag[k] >= j) return true;  // nếu giá trị của flag[k] >= j trả về đúng
    return false;
}

void P(int threadId) {
    for (;;) {
        for (int j = 1; j < N; ++j) {
            flag[threadId] = j;      // gán flag của tiến trình i bằng j
            turn[j - 1] = threadId;  // gán turn của tiến trình j bằng i
            while (sameOrHigher(threadId, j) && turn[j - 1] == threadId) (void)0;
        }
        flag[threadId] = N;
        critical();
        flag[threadId] = 0;
        noncritical();
    }
}

void main() {
    for (int i = 0; i < N; ++i) flag[i] = 0;
    for (int i = 0; i < N - 1; ++i) flag[i] = 0;
    StartProcess(P(0));
    StartProcess(P(1));
    StartProcess(P(2));
}
