# Hệ điều hành

## Bài toán tiến trình

Sử dụng tool [schedule.py](schedule.py)

**chú ý** đề bài có thể thêm yêu cầu ưu tiên nên phải thêm vào phần sort giá trị 3 trước 0

## Bài toán chuyển địa chỉ logic thành địa chỉ vật lý

Sử dụng tool [la2pa.py](la2pa.py)

## Tìm kích thước tối thiểu để biểu diễn địa chỉ

Sử dụng tool [minSize.py](minSize.py)

## Cấp phát bộ nhớ từ danh sách truy cập trang

Sử dụng tool [memAlloc.py](memAlloc.py)