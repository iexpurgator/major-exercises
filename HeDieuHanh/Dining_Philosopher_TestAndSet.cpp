void thinks() {}
void eat() {}

bool Test_and_Set(bool& val) {
    bool temp = val;
    val = true;
    return temp;
}

#define N 5
bool chopstick[N] = {false, false, false, false};

void Philosopher(int i) {                                 // triết gia thứ i
    for (;;) {                                            // lặp vô hạn
        if(i % 2 == 0){                                   // triết gia ở vị trí chẵn sẽ lấy đũa bên trái trước
            while (Test_and_Set(chopstick[i]));           // thử lấy chiếc đũa bên trái
            while (Test_and_Set(chopstick[(i + 1) % N])); // thử lấy chiếc đũa bên phải
        } else {                                          // triết gia ở vị trí lẻ sẽ lấy đũa bên phải trước
            while (Test_and_Set(chopstick[(i + 1) % N])); // thử lấy chiếc đũa bên phải
            while (Test_and_Set(chopstick[i]));           // thử lấy chiếc đũa bên trái
        }                                                 //
        eat();                                            // ăn cơm
        chopstick[i] = false;                             // bỏ đũa bên trái xuống
        chopstick[(i + 1) % N] = false;                   // bỏ đũa bên phải xuống
        thinks();                                         // suy nghĩ
    }
}

void main() {
    StartProcess(Philosopher(0));
    StartProcess(Philosopher(1));
    StartProcess(Philosopher(2));
    StartProcess(Philosopher(3));
    StartProcess(Philosopher(4));
}