#!/usr/bin/env python3
import sys
from math import ceil, floor, log2


def findSize():
    num_pages = int(input("Number of pages: "), 10)
    sz_page = int(input("Size of page:    "), 10)
    num_block = int(input("Number of block: "), 10)
    print()
    bit_page = log2(num_pages)
    bit_block = log2(num_block)
    offset = log2(sz_page)
    if bit_page == ceil(bit_page):
        print(
            f"{num_pages} pages need {ceil(bit_page)} bits ({num_pages} = 2^{floor(bit_page)})"
        )
    else:
        print(
            f"{num_pages} pages need {ceil(bit_page)} bits (2^{floor(bit_page)} < {num_pages} < 2^{ceil(bit_page)})"
        )
    if bit_block == ceil(bit_block):
        print(
            f"{num_block} blocks need {ceil(bit_block)} bits ({num_block} = 2^{floor(bit_block)})"
        )
    else:
        print(
            f"{num_block} blocks need {ceil(bit_block)} bits (2^{floor(bit_block)} < {num_block} < 2^{ceil(bit_block)})"
        )
    # độ lệch
    print(
        f"With {sz_page} bit/page need {ceil(offset)} bits for offset ({sz_page} = {offset})"
    )
    # số lượng bits cho 1 trang
    print(
        f"With {num_pages} pages need total {ceil(bit_page)} + {offset} = {ceil(bit_page + offset)} (logic)"
    )
    # số lượng bits cho 1 khung
    print(
        f"With {num_block} blocks need total {ceil(bit_block)} + {offset} = {ceil(bit_block + offset)} (physic)"
    )
    return


if __name__ == "__main__":
    findSize()