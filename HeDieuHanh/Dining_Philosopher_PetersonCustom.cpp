void eat() {}
void think() {}

#define N 5
bool l[N] = {false, false, false, false, false}; // left
bool r[N] = {false, false, false, false, false}; // right
int c[N] = {0, 0, 0, 0, 0};                      // claming
int m[N] = {};                                   // mark

void Philosopher(int i) {
  for (;;) {
    for (;;) {                                     // lặp cho đến khi có thể lấy đủ 2 đũa
      for (int k = 1; k <= N; ++k) {               //
        c[i] = k;                                  // đánh dấu rằng đang chờ
        m[k] = i;                                  //
        while (((c[(i + 1) % 5] >= k) ||           // lặp cho đến khi có người
                (c[(i + 2) % 5] >= k) ||           // trong bốn người còn lại
                (c[(i + 3) % 5] >= k) ||           // đang ăn và chiếc đũa thứ k
                (c[(i + 4) % 5] >= k)) &&          // không được gán giá trị i
               (m[k] == i));                       // thì thoát
      }                                            //
      c[i] = 5;                                    // đánh dấu chuẩn bị ăn NẾU không có triết gia nào bên cạnh chặn lại
      if (c[(i - 1) % 5] < 0) l[i] = true;         // triết gia [i-1] đang ăn thì đánh dấu chặn bên trái [i]
      else if (c[(i + 1) % 5] < 0) r[i] = true;    // triết gia [i+1] đang ăn thì đánh dấu chặn bên phải [i]
      else if (!r[(i - 1) % 5] && !l[(i + 1) % 5]) // NẾU như bên phải [i-1] và bên trái [i+1] không bị chặn
        break;                                     // thì thoát khỏi vòng lặp và bắt đầu lấy đũa ăn
    }                                              //
    c[i] = -1;                                     // đánh dấu rằng đang ăn
    l[i] = false;                                  // bỏ chặn phải [i] (được chặn bởi [i+1])
    r[i] = false;                                  // bỏ chặn trái [i] (được chặn bới [i-1])
    eat();
    c[i] = 0;
    think();
  }
}

void main() {
    StartProcess(Philosopher(0));
    StartProcess(Philosopher(1));
    StartProcess(Philosopher(2));
    StartProcess(Philosopher(3));
    StartProcess(Philosopher(4));
}

/*
Let us first recall E. W. Dijkstra’s invention of the Dining Philosophers that is illustrated in Figure 1.
The original formulation of the problem was this:
  “Five philosophers sit around a circular table.
  Each philosopher is alternately thinking and eating.
  In the centre of the table is a large plate of noodles.
  A philosopher needs two chopsticks to eat a helping of noodles.
  Unfortunately, only five chopsticks are available.
  One chopstick is placed between each pair of philosophers,
  and each agrees only to use the two chopsticks on their immediate right and left side”.
Because each adjacent pair of philosophers is forced to share one chopstick but requires two of them in order to eat,
appropriate synchronization of the philosophers’ access to them is necessary.
Therefore, at a fundamental level, we have a restricted mutual exclusion problem,
and so we now try to adapt Peterson’s filter algorithm to solve it.

Obviously, the thinking phase and eating phase of a philosopher’s process correspond to the noncritical and the critical section of a process in the abstract setting above.
The key idea of how to apply Peterson’s algorithm to the philosophers problem is now straightforward:
reinterpret the permission of entrance into the critical section as a mere chance to enter,
and have the applicant restart his entry protocol in the case when at least one of his two neighbors is critically engaged (that is eating).
Keeping in mind that (i – 1) (mod 5) and (i + 1) mod 5 are the numbers of philosopher i's neighbors (due to a linear array being used to represent the circular table)
and using abbreviations c for claiming and m for mark, we deduce the following attempt to solve the Dining Philosophers problem for five diners:

// Code

According to this algorithm, each philosopher i is continuously cycling through the states
c[i] = 0, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5, …, 1, 2, 3, 4, 5, -1, 0, 1, 2, …
whose semantics are described in:

        State c         Semantics
           0            thinking
           1            hungry, starting entry protocol
           2            progressing in entry protocol
           3            progressing in entry protocol
           4            progressing in entry protocol
           5            chance to eat if no neighbor eats
          -1            eating

From our above discussion of the filter algorithm we know that the statements in state c[i] = 5 (in bold type face) run under mutual exclusion,
which means that c[i] < 0 invariantly implies c[(i  1) mod 5] >= 0 and c[(i + 1) mod 5] >= 0 or, in other words, that, whenever philosopher i is in his critical section,
none of his two neighbors (i  1) mod 5 and (i + 1) mod 5 are in their critical section.

However, the above solution is not free from potential starvation, as the following scenario demonstrates:
an applicant P detects that one of his neighbors, say Q, is busy in his critical section and therefore immediately restarts the entry protocol.
At roughly the same time, Q exits the critical section and, because he is still hungry, immediately requests entrance to the critical section again.
This leads to a race between P and Q that might be won by Q because, as we know, the filter algorithm does not prevent one algorithm from passing another.
Because this situation may recur any arbitrary number of times, P may finally starve due to the “race hazard”.

Therefore, the algorithm needs further refinement. One way to remedy the race hazard is adding a variant of the
“bakery algorithm” by introducing a ticket numbering system that (roughly) indicates the order of the processes starting the entry protocol.
However, ticket numbering may have a transitive (global) effect that prevents an otherwise unblocked philosopher from starting to eat merely because of his high ticket number.
A better refinement of Program 2 is based on a mechanism that allows a hungry but blocked philosopher in the state c[i] = 5 to raise a flag.
For this purpose, Boolean arrays l and r are added with the following semantics:

•   l[i]  ⇔ philosopher i would be allowed to eat but is blocked by his left neighbor 
•   r[i]  ⇔ philosopher i would be allowed to eat but is blocked by his right neighbor

An additional guard at the end of the filter loop is now used to request each philosopher to yield to any of his neighbors who was previously blocked.
The following argument shows that the resulting algorithm is free from starvation: assume that some philosopher process i cannot proceed from state 5 to the critical section due to one or both neighbors who are in their critical section.
Then, after some finite amount of time, these neighbors will leave their critical section and will not be able to enter again before philosopher i has removed his flags l[i] and r[i] and has passed the critical section himself.

We have demonstrated the approach of adapting a wellproved generic mutual exclusion algorithm to a restricted mutual exclusion problem, with the benefit of automatically inheriting its correctness and other qualities. This approach contrasts with the usual approach of handcrafting an algorithm that solves a singular concurrency problem but inherently carries the dangers of errors due to overlooked scenarios. The net result is an elegant, symmetric, and starvationfree solution to the Dining Philosophers’ problem that does neither rely on synchronization constructs nor on hardware support for atomic memory updates.

*/