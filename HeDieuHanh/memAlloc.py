#!/usr/bin/env python3
import sys, re


class color:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'


def printMatrix(matrix):
    for row in matrix:
        for i in row:
            if i == None: print("{v:<3s}".format(v='-'), end=' ')
            else: print("{v:<3d}".format(v=i), end=' ')
        print()


def printClock(matrix, pclock, pointer):
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            cur_point = '>' if pointer[j] == i else ' '
            if matrix[i][j] == None:
                print(cur_point + "{v:<2s}".format(v='-') + pclock[i][j],
                      end=' ')
                # print(cur_point + '-' + pclock[i][j], end=' ')
            else:
                print(cur_point + "{v:<2d}".format(v=matrix[i][j]) +
                      pclock[i][j],
                      end=' ')
                # print(cur_point + str(matrix[i][j]) + pclock[i][j], end=' ')
        print()


""" example: memalloc.txt
3
2 3 2 1 5 2 4 5 3 2 5 2

4
1, 2, 3, 4, 2, 1, 5, 6, 2, 1, 2, 3, 7, 6, 3, 2, 1, 2, 3, 6

4
0 3 2 4 7 3 6 6 4 8 7 4 6 8 8
"""


def inp(file_name='memalloc.txt'):
    file_inp = open(file_name, 'r')
    num_blocks = file_inp.readline().split()[0]
    if not num_blocks:
        print(f'file "{file_name}" is empty')
        file_inp.close()
        exit(1)
    line = file_inp.readline()
    file_inp.close()
    data = re.split(r'\D+', line)
    return int(num_blocks, 10), [int(i, 10) for i in data]


def best():
    print("\nBEST:")
    num_blocks, data = inp()
    table = [[None] * len(data) for _ in range(num_blocks)]
    print(color.UNDERLINE + ''.join(["{v:<4d}".format(v=i)
                                     for i in data]) + " " + color.END)
    is_change = [' '] * len(data)  # đánh dấu sự kiện thiếu trang
    cur = 0
    freqs = {}  # dict đếm tần xuất xuất hiện của các trang
    for val in data:
        freqs[val] = freqs.get(val, 0) + 1
    for i, val in enumerate(data):
        is_dublicate = False
        # tần xuất và địa chỉ của giá trị có tần xuất thấp nhất
        idx, min_freqs = cur, 99
        if i > 0:  # lặp lại cột trước đó từ cột 1 trở đi
            for j in range(num_blocks):
                table[j][i] = table[j][i - 1]
                # lấy địa chỉ của phần tử có tần xuất thấp nhất
                if cur >= num_blocks and freqs[table[j][i - 1]] < min_freqs:
                    min_freqs, idx = freqs[table[j][i - 1]], j
                if val == table[j][i - 1]: is_dublicate = True
        freqs[val] = freqs[val] - 1  # giảm tần xuất của giá trị hiện tại
        if is_dublicate: continue  # nếu vẫn trang còn trong MEM thì bỏ qua
        table[idx][i] = val  # cập nhật trang mới
        cur += 1
        if cur > num_blocks: is_change[i] = 'F'  # đánh dấu sự kiện thiếu trang
    printMatrix(table)
    print(''.join(["{val:<4s}".format(val=elem) for elem in is_change]))
    return


def opt():
    print("\nOPT: (some time not work right)")
    num_blocks, data = inp()
    table = [[None] * len(data) for _ in range(num_blocks)]
    print(color.UNDERLINE + ''.join(["{v:<4d}".format(v=i)
                                     for i in data]) + " " + color.END)
    is_change = [' '] * len(data)  # đánh dấu sự kiện thiếu trang
    cur = 0
    queue = []
    for i, val in enumerate(data):
        is_dublicate = False
        queue.clear()
        for j in data[i + 1:]:
            # thêm những trang đầu tiên ở sau và khác trang đang sét
            if j not in queue and val != j: queue.append(j)
            if len(queue) == num_blocks: break
        index = cur
        in_queue = True
        if i > 0:  # lặp lại cột trước đó từ cột 1 trở đi
            for j in range(num_blocks):
                # tạo bản sao của cột phía trước
                table[j][i] = table[j][i - 1]
                if val == table[j][i - 1]: is_dublicate = True
                elif table[j][i] not in queue and in_queue:
                    # nếu trang không tồn tại trong queue
                    # và chưa có giá trị index nào khác
                    index = j
                    in_queue = False
                else:  # các trường hợp còn lại
                    for k in queue[::-1]:
                        # tìm trang có trong queue nhưng không được dùng tới
                        # trong khoảng thời gian lâu nhất
                        if k == table[j][i] and in_queue and index == cur:
                            index = j
                            break
            # nếu MEM chưa đầy thì index luôn bằng số lượng trang trong MEM
            if cur < num_blocks: index = cur
        if is_dublicate: continue  # nếu vẫn trang còn trong MEM thì bỏ qua
        table[index % num_blocks][i] = val  # cập nhật trang mới
        cur += 1
        if cur > num_blocks: is_change[i] = 'F'  # đánh dấu sự kiện thiếu trang
    printMatrix(table)
    print(''.join(["{val:<4s}".format(val=elem) for elem in is_change]))
    return


def fifo():
    print("\nFIFO:")
    num_blocks, data = inp()
    table = [[None] * len(data) for _ in range(num_blocks)]
    # print(color.UNDERLINE + ' '.join(map(str, data)) + color.END)
    print(color.UNDERLINE + ''.join(["{v:<4d}".format(v=i)
                                     for i in data]) + " " + color.END)
    is_change = [' '] * len(data)  # đánh dấu sự kiện thiếu trang
    cur = 0  # đánh dấu trang lâu nhất chưa được sửa đổi
    for i in range(len(data)):
        is_dublicate = False
        if i > 0:  # lặp lại cột trước đó từ cột 1 trở đi
            for j in range(num_blocks):
                table[j][i] = table[j][i - 1]
                if data[i] == table[j][i - 1]: is_dublicate = True
        if is_dublicate: continue  # nếu vẫn trang còn trong MEM thì bỏ qua
        table[cur % num_blocks][i] = data[i]  # cập nhật trang mới
        cur += 1
        if cur > num_blocks: is_change[i] = 'F'  # đánh dấu sự kiện thiếu trang
    printMatrix(table)
    # print(' '.join(map(str, is_change)))
    print(''.join(["{val:<4s}".format(val=elem) for elem in is_change]))
    return


def lru():
    print("\nLRU:")
    num_blocks, data = inp()
    table = [[None] * len(data) for _ in range(num_blocks)]
    print(color.UNDERLINE + ''.join(["{v:<4d}".format(v=i)
                                     for i in data]) + " " + color.END)
    is_change = [' '] * len(data)  # đánh dấu sự kiện thiếu trang
    cur = 0
    queue = []
    for i, val in enumerate(data):
        is_dublicate = False
        # lấy phần tử lâu nhất không bị thay đổi
        last_val = -1 if len(queue) == 0 else queue[-1]
        # nếu phần tử định đưa vào MEM có trong queue thì loại bỏ khỏi queue
        if val in queue: queue.remove(val)
        # thêm phần tử hiện tại vào queue
        queue.insert(0, val)
        # loại bỏ phần tử cuối cùng nếu queue lớn hơn kích thước MEM
        if len(queue) > num_blocks: queue.pop()
        index = cur
        if i > 0:  # lặp lại cột trước đó từ cột 1 trở đi
            for j in range(num_blocks):
                table[j][i] = table[j][i - 1]
                if val == table[j][i - 1]: is_dublicate = True
                # lấy địa chỉ của phần tử lâu nhất không bị thay đổi
                if cur >= num_blocks and table[j][i] == last_val: index = j
        if is_dublicate: continue  # nếu vẫn trang còn trong MEM thì bỏ qua
        table[index][i] = val  # cập nhật trang mới
        cur += 1
        if cur > num_blocks: is_change[i] = 'F'  # đánh dấu sự kiện thiếu trang
    printMatrix(table)
    print(''.join(["{val:<4s}".format(val=elem) for elem in is_change]))
    return


def clock():
    print("\nCLOCK:")
    num_blocks, data = inp()
    table = [[None] * len(data) for _ in range(num_blocks)]
    print(color.UNDERLINE + " " +
          ''.join(["{v:<5d}".format(v=i) for i in data]) + " " + color.END)
    is_change = [' '] * len(data)  # đánh dấu sự kiện thiếu trang
    cur = 0  # đánh dấu trang lâu nhất chưa được sửa đổi
    pointer = [-1] * len(data)
    pclock = [[' '] * len(data) for _ in range(num_blocks)]
    for i in range(len(data)):
        is_dublicate = False
        cnt_pclock = 0  # biến đếm số lượng bit U = 1
        if i > 0:  # lặp lại cột trước đó từ cột 1 trở đi
            for j in range(num_blocks):
                table[j][i] = table[j][i - 1]
                pclock[j][i] = pclock[j][i - 1]
                if data[i] == table[j][i - 1]:
                    # nếu gặp số đã có trong MEM thì U = 1
                    pclock[j][i] = '*'
                    is_dublicate = True
                    pointer[i] = cur % num_blocks
                if pclock[j][i] != ' ': cnt_pclock += 1
        # nếu trang vẫn còn trong MEM thì bỏ qua
        if is_dublicate:
            continue
        # nếu không bị lặp trong MEM nhưng số U = 1 đã tối đa
        elif not is_dublicate and cnt_pclock == num_blocks:
            # đi 1 vòng đặt lại U = 0
            for j in range(cur, cur + num_blocks):
                pclock[j % num_blocks][i] = ' '
        # TH còn lại (không bị lặp, số lượng U = 1 chưa đạt MAX)
        else:
            tmp = cur
            # tìm vị trí có U = 0 để thay vào đó
            for j in range(cur, cur + num_blocks):
                if pclock[j % num_blocks][i] != ' ':
                    pclock[j % num_blocks][i] = ' '
                else:
                    tmp = j
                    break
            cur = tmp
        table[cur % num_blocks][i] = data[i]  # cập nhật trang mới
        pclock[cur % num_blocks][i] = '*'
        cur += 1
        pointer[i] = cur % num_blocks
        if cur > num_blocks: is_change[i] = 'F'  # đánh dấu sự kiện thiếu trang
    printClock(table, pclock, pointer)
    print(" " + ''.join(["{val:<5s}".format(val=elem) for elem in is_change]))


if __name__ == "__main__":
    best()
    opt()
    fifo()
    lru()
    clock()
