void critical() {}
void noncritical() {}

#define N 3
bool flag[N];
int turn[N];

int max(int *arr) {
    int vmax = arr[0];
    for (int i = 1; i < N; ++i)
        vmax = vmax > arr[i] ? vmax : arr[i];
    return vmax;
}

void P(int i) {
    for (;;) {
        flag[i] = true;                          // Pi muốn vào vùng nguy hiểm
        turn[i] = max(turn) + 1;                 // đưa tiến trình i có turn i nhỏ nhất vào vùng nguy hiểm
        for (int j = 0; j < N; ++j) {            //
            while (i != j && flag[j]);           // nếu có Pj với (i != j) muốn vào vùng nguy hiểm thì Pi sẽ chờ
            while ((turn[j] != 0) &&             //
                   ((turn[j] == turn[i])         // nếu Pj và Pi có cùng turn
                        ? (j < i)                // thì xét nếu j < i thì i chờ
                        : (turn[j] < turn[i]))); // nếu không thì xét turn[i] > turn[j] thì i chờ
        }
        critical();
        flag[i] = false;
        turn[i] = 0; // đặt lại turn[i] để tránh giá trị max(turn) quá lớn
        noncritical();
    }
}

void main() {
    for (int i = 0; i < N; ++i) {
        flag[i] = false;
        turn[i] = 0;
    }
    StartProcess(P(0));
    StartProcess(P(1));
    StartProcess(P(2));
}