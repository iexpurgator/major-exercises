#include <stdbool.h>

bool flag[3] = {0, 0, 0};
int turn;

void P(int i) {
    for (;;) {
        flag[i] = 1;
        turn = i;
        while ((flag[i + 1] % 2 || flag[i + 2] % 2) && turn == i);
        critical();
        flag[i] = 0;
        noncritical();
    }
}

void main() {
    StartProcess(P(0));
    StartProcess(P(1));
    StartProcess(P(2));
}