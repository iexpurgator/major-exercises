#!/usr/bin/env python3
import sys
from operator import itemgetter
""" test case (schedule.txt)
nal
P1 2 12
P2 0 11
P3 2 7
P4 3 15
P5 4 8

nlp
P1 8 3
P2 2 1
P3 1 2
P4 4 1

nal
P1 2 12
P2 0 11
P3 2 7
P4 3 15
P5 4 8

nalp
P1 0 10 1
P2 2 8 3
P3 4 5 2
P4 5 3 2
"""


def inp(file_name='schedule.txt'):
    file_inp = open(file_name, 'r')
    type_inp = file_inp.readline().split()[0]
    if not type_inp:
        print(f'file "{file_name}" is empty')
        file_inp.close()
        exit(1)
    data = []
    while True:
        line = file_inp.readline()
        if not line:
            break
        readl = line.split()
        # nlp: tiến trình - độ dài thời gian - số ưu tiên
        # nal: tiến trình - thời điểm xuất hiện - độ dài
        # nalp: tiến trình - thời điểm xuất hiện - độ dài - số ưu tiên
        if type_inp == "nal":
            data.append([readl[0], int(readl[1], 10), int(readl[2], 10), 0, 0])
        elif type_inp == "nlp":
            data.append([readl[0], 0, int(readl[1], 10), int(readl[2], 10), 0])
        elif type_inp == "nalp":
            data.append([
                readl[0],
                int(readl[1], 10),
                int(readl[2], 10),
                int(readl[3], 10), 0
            ])
        """ index in data
        [0] name: tên tiến trình
        [1] arrival time: thời gian tiến trình xuất hiện
        [2] burst: thời gian chạy của tiến trình
        [3] priority: mức ưu tiên
        [4] previous time: giá trị thời gian trước đó
        """
    return data


def fcfs():  # ưu tiên xuất hiện trước
    data = sorted(inp(), key=itemgetter(1, 0))
    number_proc = len(data)
    total_wait = 0
    cur_time = 0
    for i in range(number_proc):
        print(f"{data[i][0]} {data[i][2]} {cur_time}->{cur_time + data[i][2]}")
        # tổng thời gian chờ sau tiến trình thứ i
        total_wait += (cur_time - data[i][1])
        cur_time += data[i][2]  # thời gian sau khi chạy tiến trình i
    print(f"Total time wait = {total_wait}")
    print(f"Average = {total_wait / number_proc}")
    return


def rr(t: int):  # chia nhỏ thời gian, ưu tiên xuất hiện trước
    # t: là khoảng thời gian cần chia nhỏ
    data = sorted(inp(), key=itemgetter(1, 0))
    number_proc = len(data)
    cnt = 0  # đếm số tiến trình đã hoàn thành
    total_wait = 0
    cur_time = 0
    queue = [data[0]]  # hàng đợi các tiến trình
    data.pop(0)
    # lặp đến khi hết tiến trình
    while cnt != number_proc:
        # nếu thời gian chạy của tiến trình đầu trong hàng đợi > 0
        if queue[0][2] > 0:
            # tổng thời gian chờ sau tiến trình hiện tại
            total_wait += (cur_time - queue[0][4])
            # thời gian chạy theo khoảng t nếu t < thời gian còn lại của tiến trình
            # nếu không thì thời gian chạy bằng thời gian còn lại của tiến trình
            cur_t = t if queue[0][2] > t else queue[0][2]
            print(f"{queue[0][0]} {cur_t} {cur_time}->{cur_time + cur_t}")
            # thời gian sau khi chạy tiến trình
            cur_time += cur_t
            # thời gian còn lại sau khi chạy với khoảng thời gian t
            queue[0][2] -= cur_t
            # thời gian sau khi kết thúc lần chạy với khoảng t,
            # được lưu lại để tính thời gian chờ
            queue[0][4] = cur_time
            # thêm các tiến trình mới xuất hiện sau thời gian chạy
            while data != [] and data[0][1] <= cur_time:
                queue.append(data[0])
                data.pop(0)
            # vẫn còn tiến trình mới chạy nên đặt lại biến đếm
            cnt = 0
        else:  # tăng số lượng tiến trình đã hoàn thành
            cnt += 1
        # đưa tiến trình vừa chạy xuống cuối hàng đợi
        queue.append(queue[0])
        # loại bỏ tiến trình mới chạy khỏi đầu hàng đợi
        queue.pop(0)
    # tổng thời gian chờ, cần loại bỏ thời điểm tiến trình chưa xuất hiện
    for i in range(number_proc):
        total_wait -= queue[i][1]
    print(f"Total time wait = {total_wait}")
    print(f"Average = {total_wait / number_proc}")
    return


def spf():
    """
    ưu tiên tiến trình ngắn nhất chạy trước
    """
    data = sorted(inp(), key=itemgetter(1, 2, 0))
    number_proc = len(data)
    total_wait = 0
    cur_time = 0
    time_arrival = 0
    # tổng thời gian tính từ 0 đến khi các tiến trình xuất hiện
    # tính trước để lấy [1] lưu thời gian chờ giữa các tiến trình
    for i in range(number_proc):
        time_arrival += data[i][1]
    for i in range(number_proc):
        print(f"{data[i][0]} {data[i][2]} {cur_time}->{cur_time + data[i][2]}")
        # tổng thời gian chờ sau tiến trình thứ i
        total_wait += cur_time
        # thời gian sau khi chạy tiến trình thừ i
        cur_time += data[i][2]
        data[i][2] = 0
        for j in range(number_proc):
            # nếu như sau khi chạy tiến trình thứ i mà tiến trình j chưa xuất hiện
            # thì tiến trình j cần đợi thêm thời gian: thời gian xuất hiện - thời điểm hiện tại
            # nếu không thì không cần chờ nữa
            cur_t_4wait = cur_time if cur_time < data[j][1] else data[j][1]
            if data[j][1] > 0: data[j][1] -= cur_t_4wait
        # sắp xếp lại các tiến trình với ưu tiên: [1], trùng [1] ss [2], trùng [2] ss [0]
        data = sorted(data, key=itemgetter(1, 2, 0))
    # tổng thời gian chờ trừ đi thời gian tiến trình chưa bắt đầu
    total_wait -= time_arrival
    print(f"Total time wait = {total_wait}")
    print(f"Average = {total_wait / number_proc}")
    return


def srtf():
    """
    tại thời điểm có tiến trình mới
        -> sắp xếp lại độ ưu tiên theo thời gian còn lại
    (luôn kiểm tra sau 1 đơn vị thời gian)
    """
    data = sorted(inp(), key=itemgetter(1, 2, 0))
    number_proc = len(data)
    total_wait = 0
    cur_time = 0
    cur_t = 0
    time_arrival = 0
    # tổng thời gian tính từ 0 đến khi các tiến trình xuất hiện
    # tính trước để lấy [1] lưu thời gian chờ giữa các tiến trình
    for i in range(number_proc):
        time_arrival += data[i][1]
    idx = 0
    while idx != number_proc:
        if data[idx][2] > 0:  # nếu tiến trình chưa chạy xong
            prev_i = data[idx]
            # biến thời gian tạm lưu thời gian
            # tiến trình chạy trước khi bị ngắt
            cur_t += 1
            data[idx][2] -= 1
            # giảm dần giá trị đến 0 (khi tiến trình xuất hiện)
            for j in range(number_proc):
                data[j][1] = data[j][1] - 1 if data[j][1] > 0 else 0
            # sắp xếp lại dữ liệu
            data = sorted(data, key=itemgetter(1, 2, 0))
            # nếu sau khi sắp xếp có tiến trình
            # được ưu tiên hơn hơn tiến trình trước đó
            if prev_i != data[idx]:
                # lấy vị trí hiện tại của trang dữ liệu trước khi bị thay đổi
                j = data.index(prev_i)
                print(f"{data[j][0]} {cur_t} {cur_time}->{cur_time + cur_t}")
                # tổng thời gian chờ sau tiến trình bị ngắt
                total_wait += (cur_time - data[j][4])
                # thời gian hiện tại sau khi tiến trình bị ngắt
                cur_time += cur_t
                # lưu thời gian hiện tại
                data[j][4] = cur_time
                # đặt lại biến tạm
                cur_t = 0
        elif (cur_t > 0):  # nếu biến tạm không bị đặt lại
            # -> tiến trình đã hoàn thành
            print(f"{data[idx][0]} {cur_t} {cur_time}->{cur_time + cur_t}")
            # tổng thời gian chờ sau tiến trình hoàn thành
            total_wait += (cur_time - data[idx][4])
            # thời gian hiện tại sau khi tiến trình hoàn thành
            cur_time += cur_t
            # đặt lại biến tạm
            cur_t = 0
        else:  # đếm số tiến trình đã hoàn thành
            idx += 1
    # tổng thời gian chờ trừ đi thời gian tiến trình chưa bắt đầu
    total_wait -= time_arrival
    print(f"Total time wait = {total_wait}")
    print(f"Average = {total_wait / number_proc}")
    return


def ps():
    """
    theo mức ưu tiên nếu ưu tiên
    nếu mức ưu tiên bằng nhau thì fcfs
    """
    data = sorted(inp(), key=itemgetter(3, 0))
    number_proc = len(data)
    total_wait = 0
    cur_time = 0
    for i in range(number_proc):
        print(f"{data[i][0]} {data[i][2]} {cur_time}->{cur_time + data[i][2]}")
        # tổng thời gian chờ sau tiến trình thứ i
        total_wait += (cur_time - data[i][1])
        cur_time += data[i][2]  # thời gian sau khi chạy tiến trình i
    print(f"Total time wait = {total_wait}")
    print(f"Average = {total_wait / number_proc}")
    return


if __name__ == "__main__":
    if len(sys.argv) > 1:
        if sys.argv[1] == "-fcfs":
            fcfs()
        elif sys.argv[1] == "-rr":
            t = input("t = ")
            rr(int(t, 10))
        elif sys.argv[1] == "-spf":
            spf()
        elif sys.argv[1] == "-srtf":
            srtf()
        elif sys.argv[1] == "-ps":
            ps()
        elif sys.argv[1] in ("-?", "-h", "--help"):
            print("Usage:\n\tpython3 schedule.py [Option]")
            print("Option:")
            print("\t-fcfs\tFirst Come First Serve")
            print("\t-rr  \tRound Robin")
            print("\t-spf \tShortest Proc First")
            print("\t-srtf\tShortest Remaining Time First")
            print("\t-ps  \tPriority Scheduling")
        else:
            print("\nFirst Come First Serve")
            fcfs()
            print("\nRound Robin")
            t = input("t = ")
            rr(int(t, 10))
            print("\nShortest Proc First")
            spf()
            print("\nShortest Remaining Time First")
            srtf()
            print("\nPriority Scheduling")
            ps()
            sys.exit(2)
    else:
        print("")
        print("\tpython3 schedule.py --help")
        print("")
        print("\tnlp : tiến trình -      thời gian chạy - số ưu tiên")
        print("\tnal : tiến trình - thời điểm xuất hiện - thời gian chạy")
        print(
            "\tnalp: tiến trình - thời điểm xuất hiện - thời gian chạy - số ưu tiên"
        )
