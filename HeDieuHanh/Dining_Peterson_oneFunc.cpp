#define R 0
#define L 1

extern "C" {int printf(const char *format, ...);}
void critical() {}
void uncritical() {}

bool flag[5][2];
int turn[5];

void P(int i) {
    int stick_1 = (i - 1) % 5;
    int stick_2 = (i - 2 + 5) % 5;
    // i    stick_1     stick_2
    // 1       0          4
    // 2       1          0
    // 3       2          1
    // 4       3          2
    // 5       4          3

    if(i%2 == 1){ // lấy bên phải trước bên trái sau
        flag[stick_1][R] = true;
        turn[stick_1] = i;
        while (flag[stick_1][L] && turn[stick_1] == i);

        flag[stick_2][L] = true;
        turn[stick_2] = i;
        while (flag[stick_2][R] && turn[stick_2] == i);
    } else { // lấy bên trái trước bên phải sau
        flag[stick_2][L] = true;
        turn[stick_2] = i;
        while (flag[stick_2][R] && turn[stick_2] == i);

        flag[stick_1][R] = true;
        turn[stick_1] = i;
        while (flag[stick_1][L] && turn[stick_1] == i);
    }

    critical();
    // printf("[i = %d]: stick_1 = %d, stick_2 = %d (1st %d | 2nd %d)\n", i, stick_1, stick_2, first, second);
    flag[stick_2][L] = false;
    flag[stick_1][R] = false;
    uncritical();
}

int main() {
    for (int i = 0; i < 5; ++i) {
        turn[i] = 0;
        flag[i][R] = flag[i][L] = false;
    }
    // P(1); P(2); P(3); P(4); P(5);
    StartProcess(P(1));
    StartProcess(P(2));
    StartProcess(P(3));
    StartProcess(P(4));
    StartProcess(P(5));
}