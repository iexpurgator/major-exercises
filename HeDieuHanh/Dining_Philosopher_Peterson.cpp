#define R 0
#define L 1

void critical(){}
void uncritical(){}

bool flag1[2], flag2[2], flag3[2], flag4[2], flag5[2];
bool flag[5][2];
int turn1, turn2, turn3, turn4, turn5;

void P1(){
    flag1[R] = true;
    turn1 = 1;
    while (flag1[L] && turn1 == 1);

    flag5[L] = true;
    turn5 = 1;
    while (flag5[R] && turn5 == 1);

    critical();
    flag5[L] = false;
    flag1[R] = false;
    uncritical();
}

void P2(){
    flag2[R] = true;
    turn2 = 2;
    while (flag2[L] && turn2 == 2);

    flag1[L] = true;
    turn1 = 2;
    while (flag1[R] && turn1 == 2);

    critical();
    flag1[L] = false;
    flag2[R] = false;
    uncritical();
}

void P3(){
    flag3[R] = true;
    turn3 = 3;
    while (flag3[L] && turn3 == 3);

    flag2[L] = true;
    turn2 = 3;
    while (flag2[R] && turn2 == 3);

    critical();
    flag2[L] = false;
    flag3[R] = false;
    uncritical();
}

void P4(){
    flag4[R] = true;
    turn4 = 4;
    while (flag4[L] && turn4 == 4);

    flag3[L] = true;
    turn3 = 4;
    while (flag3[R] && turn3 == 4);

    critical();
    flag3[L] = false;
    flag4[R] = false;
    uncritical();
}

void P5(){
    flag4[L] = true;
    turn4 = 5;
    while (flag4[R] && turn4 == 5);

    flag5[R] = true;
    turn5 = 5;
    while (flag5[L] && turn5 == 5);

    critical();
    flag5[R] = false;
    flag4[L] = false;
    uncritical();
}

int main(){
    turn1 = turn2 = turn3 = turn4 = turn5 = 0;
    flag1[R] = flag2[R] = flag3[R] = flag4[R] = flag5[R] = false;
    flag1[L] = flag2[L] = flag3[L] = flag4[L] = flag5[L] = false;
    StartProcess(P1());
    StartProcess(P2());
    StartProcess(P3());
    StartProcess(P4());
    StartProcess(P5());
}