#!/usr/bin/env python3
import sys


def inp(file_name='logic_address-to-physic_address.txt'):
    """ example ogic_address-to-physic_address.txt
    4096    << {sz_block}
    0 13    << line 1 of page table with 0 is page number and 13 is block number
    1 15    << line 2 of page table
    2 10    << line 3 of page table
    3 3     << line 4 of page table
    4 22    << line 5 of page table
    5 7     << line 6 of page table
    """
    file_inp = open(file_name, 'r')
    sz_block = file_inp.readline().split()[0]
    if not sz_block:
        print(f'file "{file_name}" is empty')
        file_inp.close()
        exit(1)
    data = {}
    while True:
        line = file_inp.readline()
        if not line:
            break
        readl = line.split()
        data[int(readl[0], 10)] = int(readl[1], 10)
    #
    print("Size a block: " + sz_block + "(bytes)")
    print("Page\tBlock")
    for k, v in data.items():
        print(f"{k}\t{v}")
    return int(sz_block, 10), data


def logicAddr2physicAddr():
    sz_block, data = inp()
    while True:
        print()
        logic_addr = input("Logical address:  ")
        if (logic_addr == 'e'): break
        logic_addr = int(logic_addr, 10)
        page_number = logic_addr // sz_block
        offset = logic_addr % sz_block
        physic_addr = data[page_number] * sz_block + offset
        print(
            f"Page number:      {logic_addr} / {sz_block} = {page_number} (Block number {data[page_number]})"
        )
        print(f"Offset:           {logic_addr} % {sz_block} = {offset}")
        print(
            f"Physical address: {data[page_number]} * {sz_block} + {offset} = {physic_addr}"
        )
    return


if __name__ == "__main__":
    logicAddr2physicAddr()
