# Server OpenAFS

## Step 0 - Cài đặt những công cụ cần thiết

```sh
sudo apt -y install ssh ntp ntpdate xinetd nmap
```

![](./image/server/step-0.png)

## Step 1 - Thiết lập tên miền đầy đủ (FQDN)

Thay đổi FQDN của máy chủ Kerberos bằng lệnh tương tự như sau:

```sh
hostnamectl set-hostname afs1.athdh.io
```
> với `afs1.athdh.io` là FQDN của bạn

Thay đổi địa chỉ IP và FQDN và lưu lại, ví dụ file sau khi cài đặt sẽ như sau:

![](./image/server/step-1.png)

## Step 2 - Cài đặt máy chủ KDC Kerberos

### Cài đặt Kerberos Server

Cài đặt Kerberos bằng lệnh:

```sh
sudo apt install -y krb5-{admin-server,user}
```

![](./image/server/step-2_install-krb.png)

### Tạo Realm

Để tạo một Realm mới chạy lệnh sau:

```sh
sudo krb5_newrealm
```

![](./image/server/step-2_newrealm.png)

### File cấu hình Realm

File cấu hình Realm được khởi tạo bởi bộ cài có đường dẫn `/etc/krb5.conf`, sửa lại file cấu hình theo FQDN và Kerberos Realm,ví dụ file cấu hình:

![](./image/server/step-2_conf-krb.png)

## Step 3 - AFS Server

### Cài đặt mô đun AFS Kernel

Cài đặt mô đun bằng các lệnh:

```sh
sudo su
apt install -y module-assistant
m-a prepare openafs ; m-a a-i openafs
modprobe openafs
```

### Cài đặt OpenAFS Server

Cài đặt gói openafs bằng lệnh:

```sh
sudo apt install -y openafs-{dbserver,krb5}
```

![](./image/server/step-3_openafs-client-conf.png)

## Step 4 - Thiết lập khoá AFS

Truy cập vào giao diện dòng lệnh quản trị của Kerberos với câu lệnh:

```sh
sudo kadmin.local
```

Ví dụ, tạo một người quản trị mới với tên là root:

![](./image/server/step-4_keytabafs.png)

Với `1` là \<kvno\> giá trị được trả về ở câu lệnh `ktadd` bên trên, còn `17`,`18` là số được gán bởi kiểu mã hoá sử sụng bên trên
```sh
sudo asetkey add rxkad_krb5 1 18 /tmp/afs.keytab afs/athdh.io
sudo asetkey add rxkad_krb5 1 17 /tmp/afs.keytab afs/athdh.io
```

Thêm `root` vào danh sách kiểm soát truy cập bằng cách sửa tập tin cấu hình `/etc/krb5kdc/kadm5.acl`:

```sh
sudo vi /etc/krb5kdc/kadm5.acl
```

Thêm cấu hình sau:

```conf
root/admin@ATHDH.IO * 
```

Cài đặt Kerberos Realm chỉ định cho AFS, ví dụ với câu lệnh:

```sh
echo "ATHDH.IO" | sudo tee /etc/openafs/server/krb.conf
```

Kiểm tra các key trong AFS KeyFile có sảy ra lỗi không, bằng câu lệnh:

```sh
sudo bos listkeys afs1 -localauth
```

Khởi động lại dịch vụ Kerberos:

```sh
sudo invoke-rc.d krb5-kdc restart ; sudo invoke-rc.d krb5-admin-server restart
```

![](./image/server/step-4_krbRealm-checkKeyAfs.png)

## Step 5 - Phân vùng lưu trữ AFS (vice)

Tạo một vùng nhớ lưu trữ cho AFS, ví dụ dưới đây là tạo một vùng nhớ với tên `/vicepa`:

![](./image/server/step-5_PartitionAfs.png)

## Step 6 - Tạo Cell

Now that we've installed the software components that make up the OpenAFS server and that we've taken care of the pre-configuration steps, we can create an actual AFS cell.

File cấu hình Cell được khởi tạo bởi bộ cài ở đường dẫn `/etc/openafs/CellServDB`, ta cần cấu hình lại Cell cho Server này, ví dụ về file cấu hình Cell:

![](./image/server/step-6_CellServDB.png)

Tạo Cell mới với câu lệnh:

```sh
sudo afs-newcell
```

![](./image/server/step-6_newcell.png)

Như lời khuyên ở gần cuối phiên afs-newcell, ta sẽ Nhận mã thông báo AFS với tư cách là người dùng quản trị, sau đó chạy `afs-rootvol`

![](./image/server/step-6_rootvol.png)

## Tạo tài khoản người dùng và thử nghiệm

Truy cập vào giao diện dòng lệnh quản trị của Kerberos:

```sh
sudo kadmin.local
```

Tạo tài khoản mới:

> Authenticating as principal root/admin@ATHDH.IO with password.
> kadmin.local:  **addprinc donald**
> WARNING: no policy specified for donald@ATHDH.IO; defaulting to no policy
> Enter password for principal "donald@ATHDH.IO":
> Re-enter password for principal "donald@ATHDH.IO":
> Principal "donald@ATHDH.IO" created.
> kadmin.local:  **addprinc hoantq**
> WARNING: no policy specified for hoantq@ATHDH.IO; defaulting to no policy
> Enter password for principal "hoantq@ATHDH.IO":
> Re-enter password for principal "hoantq@ATHDH.IO":
> Principal "hoantq@ATHDH.IO" created.
> kadmin.local:  **q**


Trước khi có thể tạo tài khoản AFS, hãy lấy vé Kerberos cho tài khoản quản trị, cũng như mã thông báo AFS phù hợp cho tài khoản đó:

```sh
kinit root/admin; aklog
```

Tạo tài khoản AFS mới:
```
pts createuser -name donald -id 20001
pts createuser -name hoantq -id 20002
```

Bây giờ tài khoản cho `donald`, `hoantq` của người dùng tồn tại trong cả Kerberos và OpenAFS, một phân vùng dữ liệu AFS tương ứng cũng phải được tạo cho nó sẽ được gắn vào vị trí của thư mục chính của người dùng trong AFS:

```sh
vos create -server afs1 -partition /vicepa -name user.donald -maxquota 100000 
vos create -server afs1 -partition /vicepa -name user.hoantq -maxquota 100000 
```
Kiểm tra thông tin về ổ đĩa mới:
```sh
vos exam -id user.donald
vos exam -id user.hoantq
```

Tạo cấu trúc thư mục hai cấp độ lại cần thiết để chứa phân vùng người dùng mới:

```sh
mkdir -p /afs/athdh.io/user/d/do
mkdir -p /afs/athdh.io/user/h/ho
```

Bây giờ, tạo điểm gắn kết cho phân vùng người dùng mới:
```sh
fs mkmount -dir /afs/athdh.io/user/d/do/donald -vol user.donald -rw
fs mkmount -dir /afs/athdh.io/user/h/ho/hoantq -vol user.hoantq -rw
```

Cấp quyền cho tài khoản đối với phân vùng trùng tên của nó bằng lệnh này:
```sh
fs setacl -dir /afs/athdh.io/user/d/do/donald -acl donald all
fs setacl -dir /afs/athdh.io/user/h/ho/hoantq -acl hoantq all
```

Thoát tài khoản quản trị
```sh
unlog; kdestroy
```

Để kiểm tra xem tài khoản có hoạt động hay không, trước tiên hãy đăng xuất AFS và Kerberos và đăng nhập với tư cách người dùng mới:

```sh
kinit hoantq; aklog
```

Bây giờ, với tư cách là `hoantq`, hãy cố gắng ghi thông tin vào thư mục chính mới của tài khoản:

```sh
date > /afs/athdh.io/user/h/ho/hoantq/date.txt
cat /afs/athdh.io/user/h/ho/hoantq/date.txt
```

```sh
```

---

# Client version

```sh
sudo apt update
```

```sh
sudo rm /etc/resolv.conf
sudo vi /etc/resolv.conf
```

```conf
nameserver 172.16.208.191
search athdh.io
```


## Step 0 - Cài đặt các dịch vụ thiết yếu

```sh
sudo apt install -y ssh ntp ntpdate nmap
```

![](./image/client/step-0.png)

## Step 1 - Thiết lập tên miền đầy đủ (FQDN)

```sh
hostnamectl set-hostname c1.athdh.io
```

```sh
sudo vi /etc/hosts
```

```conf
172.16.208.191  afs1.athdh.io afs1
172.16.208.182  c1.athdh.io c1 vm
```

![](./image/client/step-1.png)

## Step 2 - Cài đặt và cầu hình Kerberos máy khách

```sh
sudo apt -y install krb5-{config,user} libpam-krb5
```

![](./image/client/step-2_kbr-first-conf.png)

```sh
sudo vi /etc/krb5.conf
```

```conf
[libdefaults]
	default_realm = ATHDH.IO
	forwardable = true
	proxiable = true

[realms]
	ATHDH.IO = {
		default_domain = athdh.io
		admin_server = afs1.athdh.io
		kdc = afs1.athdh.io
	}

[domain_realm]
	.athdh.io = ATHDH.IO
	athdh.io = ATHDH.IO
```

![](./image/client/step-2_krb-conf.png)

```sh
sudo dpkg-reconfigure krb5-config
```

### Host princ & keytab

```sh
sudo su
kadmin -p root/admin@ATHDH.IO
```

> Authenticating as principal root/admin@ATHDH.IO with password.
> Password for root/admin@ATHDH.IO: **YourPassword**
> kadmin:  **addprinc -randkey host/c1.athdh.io**
> WARNING: no policy specified for host/c1.athdh.io@ATHDH.IO; defaulting to no policy
> Principal "host/c1.athdh.io@ATHDH.IO" created.
> kadmin:  **ktadd host/c1.athdh.io**
> Entry for principal host/c1.athdh.io with kvno 3, encryption type aes256-cts-hmac-sha1-96 added to keytab FILE:/etc/krb5.keytab.
> Entry for principal host/c1.athdh.io with kvno 3, encryption type aes128-cts-hmac-sha1-96 added to keytab FILE:/etc/krb5.keytab.
> kadmin: **q**

![](./image/client/step-2_krb-key.png)

## Step 3 - Cài đặt AFS máy khách

### Cài đặt mô đun AFS kernel (lấy từ AFS server)

```sh
sudo scp hoantq@afs1:/usr/src/openafs-modules*.deb /usr/src/
```

```sh
sudo dpkg -i /usr/src/openafs-modules*.deb
```

```sh
modprobe openafs
lsmod | grep afs
```

![](./image/client/step-3_afs-module.png)

### Debconf reconfig
```
dpkg-reconfigure debconf
```
Answer the questions as follows:

> Interface to use: **Dialog**
> Ignore questions with a priority less than: **low**

### Cài đặt các thư viện cho AFS

```sh
sudo apt install -y  libafs*
```

### Cài đặt OpenAFS

```sh
sudo apt install -y openafs-{client,krb5}
```

> AFS cell this workstation belongs to: **athdh.io**
> Size of AFS cache in kB: **50000**
> Run Openafs client now and at boot? **Yes**
> Look up AFS cells in DNS? **Yes**
> Encrypt authenticated traffic with AFS fileserver? **No**
> Dynamically generate the contents of /afs? **Yes**
> Use fakestat to avoid hangs when listing /afs? **Yes**

![](./image/client/step-3_afs-conf.png)

```sh
echo "athdh.io" | sudo tee /etc/openafs/ThisCell
```

```sh
sudo vi /etc/openafs/CellServDB
```

```conf
>athdh.io
172.16.208.191  #afs1
```

```sh
sudo systemctl restart openafs-client.service
sudo dpkg-reconfigure openafs-client
```

### Cấu hình nsswitch

```sh
sudo apt install -y nscd
```

Chỉnh sửa các trường trong file `/etc/nscd.conf` thành các giá trị sau:

```conf
	...
	persistent              passwd          no
	...
	persistent              group           no
	...
	persistent              hosts           no
	...
```

![](./image/client/step-3_nscd-conf.png)

```sh
sudo /etc/init.d/nscd restart
```

### Cấu hình PAM

```sh
sudo apt install -y libpam-afs-session
```

Edit `/etc/pam.d/common-auth` and change it to:

```conf
auth        sufficient    pam_unix.so          nullok_secure
auth        sufficient    pam_krb5.so          use_first_pass
auth        optional      pam_afs_session.so   program=/usr/bin/aklog
auth        required      pam_deny.so
```
![](./image/client/step-3_PAM-auth.png)

Edit `/etc/pam.d/common-account` and change it to:

```conf
account     sufficient    pam_unix.so
account     sufficient    pam_krb5.so
account     required      pam_deny.so
```
![](./image/client/step-3_PAM-account.png)

Edit `/etc/pam.d/common-password` and change it to:

```conf
password    sufficient    pam_unix.so          nullok obscure md5
password    sufficient    pam_krb5.so          use_first_pass
password    required      pam_deny.so
```
![](./image/client/step-3_PAM-passwd.png)

Edit `/etc/pam.d/common-session` and change it to:

```conf
session     required      pam_unix.so
session     optional      pam_krb5.so
session     optional      pam_afs_session.so   program=/usr/bin/aklog
```
![](./image/client/step-3_PAM-session.png)

## Kết quả

```sh
kinit donald; aklog
cd /afs/athdh.io/user/d/do/donald
cat date.txt
```

![](./image/client/demo.png)