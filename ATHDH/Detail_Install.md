# Server version

```sh
sudo apt update; sudo apt install -y kitty
```

Setting > Keyboard Shortcut

`kitty sh -c byobu` : `super` + `enter`

config starship: `~/.config/starship.toml`
```toml
[time]
disabled = false
```

[fix hangs(freeze) command](https://www.mail-archive.com/openafs-devel@openafs.org/msg15181.html)
```sh
sudo rm /etc/resolv.conf
sudo vi /etc/resolv.conf
```

```conf
nameserver 172.16.208.2
search athdh.io
```

## Step 0 - Cài đặt những công cụ cần thiết

```sh
sudo apt -y install ssh ntp ntpdate xinetd nmap
```

![](./image/server/step-0.png)

## Step 1 - Thiết lập tên miền đầy đủ (FQDN)

Thay đổi FQDN của máy chủ Kerberos bằng lệnh tương tự như sau:

```sh
hostnamectl set-hostname afs1.athdh.io
```
> với `afs1.athdh.io` là FQDN của bạn

Sau đó sửa tệp `/etc/hosts`, ví dụ mở tệp và sửa bằng `vi`
```sh
sudo vi /etc/hosts
```

Thay đổi địa chỉ IP và FQDN của bạn và lưu lại, ví dụ file sau khi cài đặt sẽ như sau:

```conf
127.0.0.1 localhost
172.16.208.191 afs1.athdh.io afs1
#127.0.1.1 b19at077

# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
```

![](./image/server/step-1.png)

## Step 2 - Cài đặt máy chủ KDC Kerberos

### Cài đặt Server

Cài đặt Kerberos bằng lệnh:

```sh
sudo apt install -y krb5-{admin-server,user}
```

Trong quá trình cài đặt, sẽ có những câu hỏi về Kerberos Realm, ví dụ về câu hỏi và cách trả lời:

> By default, the Kerberos will use the Kerberos server domain name as a REALM, **`ATHDH.IO`**
>
> The Kerberos server is **`afs1.athdh.io`**
>
> And the Admin server same as the Kerberos server **`afs1.athdh.io`**

![](./image/server/step-2_install-krb.png)

Sau khi cài đặt xong sẽ có thông báo không chạy được, vấn đề này sẽ được sửa ở bước tiếp theo.

> _**Phần cài đặt dự phòng** tránh sự cố trên KDC chính bằng cách sử thiết lập một KDC nô lệ, bằng cách tạo file với tên `/etc/xinetd.d/krb_prop`, với nội dung như sau:_
> ```conf
> service krb_prop
> {
> 	disable		= no
> 	socket_type	= stream
> 	protocol	= tcp
> 	user		= root
> 	wait		= no
> 	server		= /usr/sbin/kpropd
> }
> ```
> 
> ![](./image/server/step-2_krb-prop.png)
> 
> _Sau khi lưu file lại thì cần khởi động lại xinetd:_
> 
> ```sh
> /etc/init.d/xinetd restart
> ```
> 


### Tạo Realm

Để tạo một Realm mới chạy lệnh sau:

```sh
sudo krb5_newrealm
```

Có các câu hỏi dạng như sau:

> This script should be run on the master KDC/admin server to initialize
> a Kerberos realm.  It will ask you to type in a master key password.
> This password will be used to generate a key that is stored in
> /etc/krb5kdc/stash.  You should try to remember this password, but it
> is much more important that it be a strong password than that it be
> remembered.  However, if you lose the password and /etc/krb5kdc/stash,
> you cannot decrypt your Kerberos database.
> Loading random data
> Initializing database '/var/lib/krb5kdc/principal' for realm 'ATHDH.IO',
> master key name 'K/M@ATHDH.IO'
> You will be prompted for the database Master Password.
> It is important that you NOT FORGET this password.
> Enter KDC database master key: **YourPassword**
> Re-enter KDC database master key to verify: **YourPassword**
> ...

![](./image/server/step-2_newrealm.png)

### File cấu hình Realm

File cấu hình Realm được khởi tạo bởi bộ cài có đường dẫn `/etc/krb5.conf`, ví dụ file sau khi cấu hình:

```conf
[libdefaults]
	default_realm = ATHDH.IO
	forwardable = true
	proxiable = true

[realms]
	ATHDH.IO = {
		kdc = afs1.athdh.io
		admin_server = afs1.athdh.io
	}

[domain_realm]
	.athdh.io = ATHDH.IO
	athdh.io = ATHDH.IO
```

![](./image/server/step-2_conf-krb.png)

### Cấu hình ssh

Chỉnh sửa file cấu hình ssh `/etc/ssh/sshd_config`:
```conf
# ...
GSSAPIAuthentication yes
GSSAPICleanupCredentials yes
# ...
```

Khởi động lại dịch vụ ssh
```sh
sudo systemctl restart sshd
```

## Step 3 - AFS Server

### Debconf reconfig
```sh
dpkg-reconfigure debconf
```

> Interface to use: Dialog
> Ignore questions with a priority less than: low

![](./image/server/step-3_debconf.png)

### AFS kernel module

```sh
sudo su
apt install -y module-assistant
m-a prepare openafs ; m-a a-i openafs
modprobe openafs
```

kiểm tra module
```sh
lsmod | grep afs
```

### AFS
Cài đặt gói afs

```sh
sudo apt install -y openafs-{dbserver,krb5}
```

Khi cài đặt sẽ có những câu hỏi, ví dụ câu trả lời:

> Cell this server serves files for: **`athdh.io`**
> Size of AFS cache in kB: **400000**
> Run Openafs client now and at boot? **No**
> Look up AFS cells in DNS? **Yes**
> Encrypt authenticated traffic with AFS fileserver? **No**
> Dynamically generate the contents of /afs? **Yes**
> Use fakestat to avoid hangs when listing /afs? **Yes**

![](./image/server/step-3_openafs-client-conf.png)

Khi cài đặt bộ cài có thể sẽ tự động cài và chạy `openafs-client`, nếu trong lúc thiết lập không đúng server với server hiện tại thì cần phải dừng chương trình lại để trỏ đến máy chủ hiện tại, dừng bằng câu lệnh:

```sh
sudo /etc/init.d/openafs-client stop
```

Nếu cần thiết lập lại cài đặt của trương trình `openafs-client` sử dụng câu lệnh sau:
```sh
sudo dpkg-reconfigure openafs-client
```

## Step 4 - Thiết lập khoá AFS

Xoá khoá tạm thời

```sh
sudo rm -f /tmp/afs.keytab
```

Chạy câu lệnh sau dưới quyền root để truy cập vào giao diện dòng lệnh quản trị của Kerberos:

```sh
sudo kadmin.local
```

Ví dụ, tạo một người quản trị mới với tên là root:
> Authenticating as principal root/admin@ATHDH.IO with password.kadmin.local:  **addprinc root/admin**
> WARNING: no policy specified for root/admin@ATHDH.IO; defaulting to no policy
> Enter password for principal "root/admin@ATHDH.IO": **YourPassword**
> Re-enter password for principal "root/admin@ATHDH.IO": **YourPassword**
> Principal "root/admin@ATHDH.IO" created.
> kadmin.local:  **addprinc -policy service -randkey afs/athdh.io**
> WARNING: policy "service" does not exist
> Principal "afs/athdh.io@ATHDH.IO" created.
> kadmin.local:  **ktadd -k /tmp/afs.keytab -norandkey afs/athdh.io**
> Entry for principal afs/athdh.io with kvno _**1**_, encryption type aes256-cts-hmac-sha1-96 added to keytab WRFILE:/tmp/afs.keytab.
> Entry for principal afs/athdh.io with kvno _**1**_, encryption type aes128-cts-hmac-sha1-96 added to keytab WRFILE:/tmp/afs.keytab.
> kadmin.local:  **q**

![](./image/server/step-4_keytabafs.png)

Với `1` là \<kvno\> giá trị được trả về ở câu lệnh `ktadd` bên trên, còn `17`,`18` là số được gán bởi kiểu mã hoá sử sụng bên trên
```sh
sudo asetkey add rxkad_krb5 1 18 /tmp/afs.keytab afs/athdh.io
sudo asetkey add rxkad_krb5 1 17 /tmp/afs.keytab afs/athdh.io
```

Thêm `root` vào danh sách kiểm soát truy cập bằng cách sửa tập tin cấu hình `/etc/krb5kdc/kadm5.acl`:

```sh
sudo vi /etc/krb5kdc/kadm5.acl
```

Thêm cấu hình sau:

```conf
root/admin@ATHDH.IO * 
```

Cài đặt Kerberos Realm chỉ định cho AFS, ví dụ với câu lệnh:

```sh
echo "ATHDH.IO" | sudo tee /etc/openafs/server/krb.conf
```

Kiểm tra các key trong AFS KeyFile có sảy ra lỗi không, bằng câu lệnh:

```sh
sudo bos listkeys afs1 -localauth
```

Nếu trả về như sau thì không có lỗi sảy ra:

> ...
> All done.

Khởi động lại dịch vụ Kerberos:

```sh
sudo invoke-rc.d krb5-kdc restart ; sudo invoke-rc.d krb5-admin-server restart
```

![](./image/server/step-4_krbRealm-checkKeyAfs.png)

## Step 5 - Phân vùng lưu trữ AFS (vice)

Tạo một vùng nhớ lưu trữ cho AFS, ví dụ dưới đây là tạo một vùng nhớ với tên `/vicepa`:

```sh
sudo su
dd if=/dev/zero of=/var/lib/openafs/vicepa bs=1024k count=100
mke2fs /var/lib/openafs/vicepa
mkdir /vicepa
mount -oloop /var/lib/openafs/vicepa /vicepa
```

![](./image/server/step-5_PartitionAfs.png)

## Step 6 - Tạo Cell

Now that we've installed the software components that make up the OpenAFS server and that we've taken care of the pre-configuration steps, we can create an actual AFS cell.

File cấu hình Cell được khởi tạo bởi bộ cài ở đường dẫn `/etc/openafs/CellServDB`, ta cần cấu hình lại Cell cho Server này, ví dụ về file cấu hình Cell:

```conf
>athdh.io
172.16.208.191   #afs1
```

![](./image/server/step-6_CellServDB.png)

Tạo Cell mới với câu lệnh:

```sh
sudo afs-newcell
```
Khi tạo Cell mới sẽ có những câu hỏi như sau:

>                             Prerequisites
> 
> In order to set up a new AFS cell, you must meet the following:
> 
> 1) You need a working Kerberos realm with Kerberos4 support.  You
>    should install Heimdal with KTH Kerberos compatibility or MIT
>    Kerberos 5.
> 
> 2) You need to create the single-DES AFS key and load it into
>    /etc/openafs/server/KeyFile.  If your cell's name is the same as
>    your Kerberos realm then create a principal called afs.  Otherwise,
>    create a principal called afs/cellname in your realm.  The cell
>    name should be all lower case, unlike Kerberos realms which are all
>    upper case.  You can use asetkey from the openafs-krb5 package, or
>    if you used AFS3 salt to create the key, the bos addkey command.
> 
> 3) This machine should have a filesystem mounted on /vicepa.  If you
>    do not have a free partition, then create a large file by using dd
>    to extract bytes from /dev/zero.  Create a filesystem on this file
>    and mount it using -oloop.
> 
> 4) You will need an administrative principal created in a Kerberos
>    realm.  This principal will be added to susers and
>    system:administrators and thus will be able to run administrative
>    commands.  Generally the user is a root or admin instance of some
>    administrative user.  For example if jruser is an administrator then
>    it would be reasonable to create jruser/admin (or jruser/root) and
>    specify that as the user to be added in this script.
> 
> 5) The AFS client must not be running on this workstation.  It will be
>    at the end of this script.
> 
> Do you meet these requirements? [y/n] **y**
> If the fileserver is not running, this may hang for 30 seconds.                                                  
> service openafs-fileserver stop
> What administrative principal should be used? **root/admin**
>
> /etc/openafs/server/CellServDB already exists, renaming to .old                                                  
> service openafs-fileserver start
> bos adduser afs1.athdh.io root.admin -localauth
>
> Creating initial protection database.  This will print some errors
> about an id already existing and a bad ubik magic.  These errors can
> be safely ignored.
>
> pt_util: /var/lib/openafs/db/prdb.DB0: Bad UBIK_MAGIC. Is 0 should be 354545                                     
> Ubik Version is: 2.0
> Error while creating system:administrators: Entry for id already exists
>
> bos create afs1.athdh.io ptserver simple /usr/lib/openafs/ptserver -localauth
> bos create afs1.athdh.io vlserver simple /usr/lib/openafs/vlserver -localauth
> bos create afs1.athdh.io dafs dafs -cmd '/usr/lib/openafs/dafileserver -p 23 -busyat 600 -rxpck 400 -s 1200 -l 1200 -cb 65535 -b 240 -vc 1200' -cmd /usr/lib/openafs/davolserver -cmd /usr/lib/openafs/salvageserver -cmd /usr/lib
> /openafs/dasalvager -localauth
> bos setrestart afs1.athdh.io -time never -general -localauth
> Waiting for database elections: done.
> vos create afs1.athdh.io a root.afs -localauth
> Volume 536870912 created on partition /vicepa of afs1.athdh.io
> service openafs-client force-start
> Starting AFS services: openafs afsd.
> afsd: All AFS daemons started.
>
> Now, get tokens as root/admin in the athdh.io cell.
> Then, run afs-rootvol.

![](./image/server/step-6_newcell.png)

Nhận mã thông báo AFS với tư cách là người dùng quản trị, như lời khuyên ở gần cuối phiên afs-newcell:

```sh
sudo su
kinit root/admin; aklog
```
> Password for root/admin@ATHDH.IO: **YourPassword**

```sh
klist -5f
```
> Ticket cache: FILE:/tmp/krb5cc_1000
> Default principal: root/admin@ATHDH.IO
> 
> Valid starting       Expires              Service principal
> 03/10/2022 11:27:51  03/10/2022 21:27:51  krbtgt/ATHDH.IO@ATHDH.IO
>         renew until 03/11/2022 11:27:39, Flags: FPRIA
> 03/10/2022 11:28:02  03/10/2022 21:27:51  afs/athdh.io@ATHDH.IO
>         renew until 03/11/2022 11:27:39, Flags: FPRAT
>

```sh
tokens
```
> Tokens held by the Cache Manager:
> 
> User's (AFS ID 1) rxkad tokens for athdh.io [Expires Mar 10 21:27]
>    --End of list--
> 

Sau khi nhận mã thông báo có thể chạy `afs-rootvol`:

>                             Prerequisites
>
> In order to set up the root.afs volume, you must meet the following
> pre-conditions:
>
> 1) The cell must be configured, running a database server with a
>    volume location and protection server.  The afs-newcell script will
>    set up these services.
>
> 2) You must be logged into the cell with tokens in for a user in
>    system:administrators and with a principal that is in the UserList
>    file of the servers in the cell.
>
> 3) You need a fileserver in the cell with partitions mounted and a
>    root.afs volume created.  Presumably, it has no volumes on it,
>    although the script will work so long as nothing besides root.afs
>    exists.  The afs-newcell script will set up the file server.
>
> 4) The AFS client must be running pointed at the new cell.
> Do you meet these conditions? (y/n) **`y`**
> 
> You will need to select a server (hostname) and AFS partition on which to
> create the root volumes.
> 
> What AFS Server should volumes be placed on? **`afs1`**
> What partition? [a] **`a`**
> 
> vos create afs1 a root.cell -localauth
> Volume 536870915 created on partition /vicepa of afs1
> fs mkm /afs/athdh.io/.root.afs root.afs -rw
> fs sa /afs/athdh.io/.root.afs system:anyuser rl
> fs mkm /afs/athdh.io/.root.afs/athdh.io root.cell -cell athdh.io -fast || true
> fs sa /afs/athdh.io system:anyuser rl
> fs mkm /afs/athdh.io/.root.afs/.athdh.io root.cell -cell athdh.io -rw
> fs mkm /afs/athdh.io/.root.afs/.root.afs root.afs -rw
> vos create afs1 a user -localauth
> Volume 536870918 created on partition /vicepa of afs1
> fs mkm /afs/athdh.io/user user 
> fs sa /afs/athdh.io/user system:anyuser rl
> vos create afs1 a service -localauth
> Volume 536870921 created on partition /vicepa of afs1
> fs mkm /afs/athdh.io/service service 
> fs sa /afs/athdh.io/service system:anyuser rl
> ln -s athdh.io /afs/athdh.io/.root.afs/athdh
> ln -s .athdh.io /afs/athdh.io/.root.afs/.athdh
> fs rmm /afs/athdh.io/.root.afs
> vos addsite afs1 a root.afs -localauth
> Added replication site afs1 /vicepa for volume root.afs
> vos addsite afs1 a root.cell -localauth
> Added replication site afs1 /vicepa for volume root.cell 
> vos release root.afs -localauth
> Released volume root.afs successfully
> vos release root.cell -localauth
> Released volume root.cell successfully

![](./image/server/step-6_rootvol.png)

## Tạo tài khoản người dùng và thử nghiệm

Truy cập vào giao diện dòng lệnh quản trị của Kerberos:

```sh
sudo kadmin.local
```

Tạo tài khoản mới:

> Authenticating as principal root/admin@ATHDH.IO with password.
> kadmin.local:  **addprinc donald**
> WARNING: no policy specified for donald@ATHDH.IO; defaulting to no policy
> Enter password for principal "donald@ATHDH.IO":
> Re-enter password for principal "donald@ATHDH.IO":
> Principal "donald@ATHDH.IO" created.
> kadmin.local:  **addprinc hoantq**
> WARNING: no policy specified for hoantq@ATHDH.IO; defaulting to no policy
> Enter password for principal "hoantq@ATHDH.IO":
> Re-enter password for principal "hoantq@ATHDH.IO":
> Principal "hoantq@ATHDH.IO" created.
> kadmin.local:  **q**


Trước khi có thể tạo tài khoản AFS, hãy lấy vé Kerberos cho tài khoản quản trị, cũng như mã thông báo AFS phù hợp cho tài khoản đó:

```sh
kinit root/admin; aklog
```

Tạo tài khoản AFS mới:
```
pts createuser -name donald -id 20001
pts createuser -name hoantq -id 20002
```

Bây giờ tài khoản cho `donald`, `hoantq` của người dùng tồn tại trong cả Kerberos và OpenAFS, một phân vùng dữ liệu AFS tương ứng cũng phải được tạo cho nó sẽ được gắn vào vị trí của thư mục chính của người dùng trong AFS:

```sh
vos create -server afs1 -partition /vicepa -name user.donald -maxquota 100000 
vos create -server afs1 -partition /vicepa -name user.hoantq -maxquota 100000 
```
Kiểm tra thông tin về ổ đĩa mới:
```sh
vos exam -id user.donald
vos exam -id user.hoantq
```

Tạo cấu trúc thư mục hai cấp độ lại cần thiết để chứa phân vùng người dùng mới:

```sh
mkdir -p /afs/athdh.io/user/d/do
mkdir -p /afs/athdh.io/user/h/ho
```

Bây giờ, tạo điểm gắn kết cho phân vùng người dùng mới:
```sh
fs mkmount -dir /afs/athdh.io/user/d/do/donald -vol user.donald -rw
fs mkmount -dir /afs/athdh.io/user/h/ho/hoantq -vol user.hoantq -rw
```

Cấp quyền cho tài khoản đối với phân vùng trùng tên của nó bằng lệnh này:
```sh
fs setacl -dir /afs/athdh.io/user/d/do/donald -acl donald all
fs setacl -dir /afs/athdh.io/user/h/ho/hoantq -acl hoantq all
```

Thoát tài khoản quản trị
```sh
unlog; kdestroy
```

Để kiểm tra xem tài khoản có hoạt động hay không, trước tiên hãy đăng xuất AFS và Kerberos và đăng nhập với tư cách người dùng mới:

```sh
kinit hoantq; aklog
```

Bây giờ, với tư cách là `hoantq`, hãy cố gắng ghi thông tin vào thư mục chính mới của tài khoản:

```sh
date > /afs/athdh.io/user/h/ho/hoantq/date.txt
cat /afs/athdh.io/user/h/ho/hoantq/date.txt
```

```sh
```

---

# Client version

```sh
sudo apt update
```

```sh
sudo rm /etc/resolv.conf
sudo vi /etc/resolv.conf
```

```conf
nameserver 172.16.208.191
search athdh.io
```


## Step 0 - Cài đặt các dịch vụ thiết yếu

```sh
sudo apt install -y ssh ntp ntpdate nmap
```

![](./image/client/step-0.png)

## Step 1 - Thiết lập tên miền đầy đủ (FQDN)

```sh
hostnamectl set-hostname c1.athdh.io
```

```sh
sudo vi /etc/hosts
```

```conf
172.16.208.191  afs1.athdh.io afs1
172.16.208.182  c1.athdh.io c1 vm
```

![](./image/client/step-1.png)

## Step 2 - Cài đặt và cầu hình Kerberos máy khách

```sh
sudo apt -y install krb5-{config,user} libpam-krb5
```

![](./image/client/step-2_kbr-first-conf.png)

```sh
sudo vi /etc/krb5.conf
```

```conf
[libdefaults]
	default_realm = ATHDH.IO
	forwardable = true
	proxiable = true

[realms]
	ATHDH.IO = {
		default_domain = athdh.io
		admin_server = afs1.athdh.io
		kdc = afs1.athdh.io
	}

[domain_realm]
	.athdh.io = ATHDH.IO
	athdh.io = ATHDH.IO
```

![](./image/client/step-2_krb-conf.png)

```sh
sudo dpkg-reconfigure krb5-config
```

### Host princ & keytab

```sh
sudo su
kadmin -p root/admin@ATHDH.IO
```

> Authenticating as principal root/admin@ATHDH.IO with password.
> Password for root/admin@ATHDH.IO: **YourPassword**
> kadmin:  **addprinc -randkey host/c1.athdh.io**
> WARNING: no policy specified for host/c1.athdh.io@ATHDH.IO; defaulting to no policy
> Principal "host/c1.athdh.io@ATHDH.IO" created.
> kadmin:  **ktadd host/c1.athdh.io**
> Entry for principal host/c1.athdh.io with kvno 3, encryption type aes256-cts-hmac-sha1-96 added to keytab FILE:/etc/krb5.keytab.
> Entry for principal host/c1.athdh.io with kvno 3, encryption type aes128-cts-hmac-sha1-96 added to keytab FILE:/etc/krb5.keytab.
> kadmin: **q**

![](./image/client/step-2_krb-key.png)

## Step 3 - Cài đặt AFS máy khách

### Cài đặt mô đun AFS kernel (lấy từ AFS server)

```sh
sudo scp hoantq@afs1:/usr/src/openafs-modules*.deb /usr/src/
```

```sh
sudo dpkg -i /usr/src/openafs-modules*.deb
```

```sh
modprobe openafs
lsmod | grep afs
```

![](./image/client/step-3_afs-module.png)

### Debconf reconfig
```
dpkg-reconfigure debconf
```
Answer the questions as follows:

> Interface to use: **Dialog**
> Ignore questions with a priority less than: **low**

### Cài đặt các thư viện cho AFS

```sh
sudo apt install -y  libafs*
```

### Cài đặt OpenAFS

```sh
sudo apt install -y openafs-{client,krb5}
```

> AFS cell this workstation belongs to: **athdh.io**
> Size of AFS cache in kB: **50000**
> Run Openafs client now and at boot? **Yes**
> Look up AFS cells in DNS? **Yes**
> Encrypt authenticated traffic with AFS fileserver? **No**
> Dynamically generate the contents of /afs? **Yes**
> Use fakestat to avoid hangs when listing /afs? **Yes**

![](./image/client/step-3_afs-conf.png)

```sh
echo "athdh.io" | sudo tee /etc/openafs/ThisCell
```

```sh
sudo vi /etc/openafs/CellServDB
```

```conf
>athdh.io
172.16.208.191  #afs1
```

```sh
sudo systemctl restart openafs-client.service
sudo dpkg-reconfigure openafs-client
```

### Cấu hình nsswitch

```sh
sudo apt install -y nscd
```

Chỉnh sửa các trường trong file `/etc/nscd.conf` thành các giá trị sau:

```conf
	...
	persistent              passwd          no
	...
	persistent              group           no
	...
	persistent              hosts           no
	...
```

![](./image/client/step-3_nscd-conf.png)

```sh
sudo /etc/init.d/nscd restart
```

### Cấu hình PAM

```sh
sudo apt install -y libpam-afs-session
```

Edit `/etc/pam.d/common-auth` and change it to:

```conf
auth        sufficient    pam_unix.so          nullok_secure
auth        sufficient    pam_krb5.so          use_first_pass
auth        optional      pam_afs_session.so   program=/usr/bin/aklog
auth        required      pam_deny.so
```
![](./image/client/step-3_PAM-auth.png)

Edit `/etc/pam.d/common-account` and change it to:

```conf
account     sufficient    pam_unix.so
account     sufficient    pam_krb5.so
account     required      pam_deny.so
```
![](./image/client/step-3_PAM-account.png)

Edit `/etc/pam.d/common-password` and change it to:

```conf
password    sufficient    pam_unix.so          nullok obscure md5
password    sufficient    pam_krb5.so          use_first_pass
password    required      pam_deny.so
```
![](./image/client/step-3_PAM-passwd.png)

Edit `/etc/pam.d/common-session` and change it to:

```conf
session     required      pam_unix.so
session     optional      pam_krb5.so
session     optional      pam_afs_session.so   program=/usr/bin/aklog
```
![](./image/client/step-3_PAM-session.png)

## Kết quả

```sh
kinit donald; aklog
cd /afs/athdh.io/user/d/do/donald
cat date.txt
```

![](./image/client/demo.png)