#include <iostream>

#include "bigint.h"

using namespace std;

// g++ -std=c++11 bignum.cpp bigint.cpp -o bignum

int main() {
    BigInteger BigNumber;
    BigInteger::BigNum a, b, res;
    BigInteger::DivResult divRes;
    string inputA, inputB, strRes;

    cout << "Input 2 big integers" << endl;

    cout << "A = ";
    cin >> inputA;
    a = BigNumber.StringToBigNum(inputA);

    cout << "B = ";
    cin >> inputB;
    b = BigNumber.StringToBigNum(inputB);

    res = BigNumber.Add(a, b);
    strRes = BigNumber.BigNumToString(res);
    cout << "A + B = " << strRes << endl;

    res = BigNumber.Sub(a, b);
    strRes = BigNumber.BigNumToString(res);
    cout << "A - B = " << strRes << endl;

    res = BigNumber.Mul(a, b);
    strRes = BigNumber.BigNumToString(res);
    cout << "A * B = " << strRes << endl;

    divRes = BigNumber.DivLarge(a, b);
    strRes = BigNumber.BigNumToString(divRes.Result);
    cout << "A / B = " << strRes << endl;

    strRes = BigNumber.BigNumToString(divRes.Remainder);
    cout << "A % B = " << strRes << endl;
}