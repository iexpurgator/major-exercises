#include <cstring>
#include <iostream>

#include "bigint.h"

using namespace std;

bool isNumber(char s[]) {
    int i = 0;
    while (s[i] != '\0') {
        if (isdigit(s[i]) == 0) return false;
        i++;
    }
    return true;
}

// ./RSA -P 95512812792825622166087376161191 -Q 13713029228044835176644627922133 -E 65537 --PrintN --PrintPhi --PrintD
// ./RSA -N 1309769993480792399304232474055086052458841054767926512404540403 -E 65537 --Encrypt 'I am B19DCAT077'
// ./RSA -N 1309769993480792399304232474055086052458841054767926512404540403 -D 334432321755765140454354429113874333835938003940572638070422033 --Decrypt 735802533649957974262414991490094567650161120787684663183244076

// g++ --std=c++11 rsa.cpp bigint.cpp -o RSA

int main(int argc, char* argv[]) {
    BigInteger BigNumber;
    BigInteger::BigNum PArray, QArray, EArray, DArray, NArray, PhiArray, One;
    string strPrint;
    BigInteger::DivResult DR;
    One.Num[0] = 1;
    bool hadP = false, hadQ = false, hadE = false, hadN = false, hadD = false, hadPhi = false;

    if (argc < 2) {
    notFoundCommand:
        cout << "Usage: " << argv[0] << " [option] <option argument>" << endl;
        cout << "Options: " << endl;
        cout << "\t-P   <An Integer>" << endl;
        cout << "\t-Q   <An Integer>" << endl;
        cout << "\t-E   <An Integer>" << endl;
        cout << "\t-N   <An Integer>" << endl;
        cout << "\t-D   <An Integer>" << endl;
        cout << "\t-Phi <An Integer>" << endl;
        cout << "\t--PIsPrime check P is prime" << endl;
        cout << "\t--QIsPrime check Q is prime" << endl;
        cout << "\t--PrintN   print N" << endl;
        cout << "\t--PrintPhi print Phi" << endl;
        cout << "\t--PrintD   print D" << endl;
        cout << "\t--EncryptPublic <String/An Integer>  Encrypt RSA" << endl;
        cout << "\t--EncryptPrivate <String/An Integer> Decrypt RSA" << endl;
        return 0;
    }

    int i = 1;
    while (i < argc) {
        if (hadP && hadQ) {
            NArray = BigNumber.Mul(PArray, QArray);
            BigInteger::BigNum PMinusOne = BigNumber.Sub(PArray, One);
            BigInteger::BigNum QMinusOne = BigNumber.Sub(QArray, One);
            PhiArray = BigNumber.Mul(PMinusOne, QMinusOne);
            hadPhi = true;
            hadN = true;
        }
        if (hadE && hadPhi) {
            DArray = BigNumber.Inverse(EArray, PhiArray);
            hadD = true;
        }

        if (!strcmp(argv[i], "-N")) {
            if (!isNumber(argv[i + 1])) {
                cerr << "Invalid argument" << endl;
                return 1;
            }
            NArray = BigNumber.StringToBigNum(string(argv[i + 1]));
            hadN = true;
            i += 2;
        } else if (!strcmp(argv[i], "-Phi")) {
            if (!isNumber(argv[i + 1])) {
                cerr << "Invalid argument" << endl;
                return 1;
            }
            PhiArray = BigNumber.StringToBigNum(string(argv[i + 1]));
            hadPhi = true;
            i += 2;
        } else if (!strcmp(argv[i], "-D")) {
            if (!isNumber(argv[i + 1])) {
                cerr << "Invalid argument" << endl;
                return 1;
            }
            DArray = BigNumber.StringToBigNum(string(argv[i + 1]));
            hadD = true;
            i += 2;
        } else if (!strcmp(argv[i], "-P")) {
            if (!isNumber(argv[i + 1])) {
                cerr << "Invalid argument" << endl;
                return 1;
            }
            PArray = BigNumber.StringToBigNum(string(argv[i + 1]));
            hadP = true;
            i += 2;
        } else if (!strcmp(argv[i], "-Q")) {
            if (!isNumber(argv[i + 1])) {
                cerr << "Invalid argument" << endl;
                return 1;
            }
            QArray = BigNumber.StringToBigNum(string(argv[i + 1]));
            hadQ = true;
            i += 2;
        } else if (!strcmp(argv[i], "-E")) {
            if (!isNumber(argv[i + 1])) {
                cerr << "Invalid argument" << endl;
                return 1;
            }
            EArray = BigNumber.StringToBigNum(string(argv[i + 1]));
            hadE = true;
            i += 2;
        } else if (!strcmp(argv[i], "--PIsPrime")) {
            if (!hadP) {
                cerr << "Invalid argument" << endl;
                return 1;
            }
            cout << "P is Prime: " << (BigNumber.IsPrime(PArray) ? "Yes" : "No") << endl;
            i += 1;
        } else if (!strcmp(argv[i], "--QIsPrime")) {
            if (!hadQ) {
                cerr << "Invalid argument" << endl;
                return 1;
            }
            cout << "Q is Prime: " << (BigNumber.IsPrime(QArray) ? "Yes" : "No") << endl;
            i += 1;
        } else if (!strcmp(argv[i], "--PrintN")) {
            if (!hadN) {
                cerr << "Invalid argument" << endl;
                return 1;
            }
            strPrint = BigNumber.BigNumToString(NArray);
            cout << "N = " << strPrint << endl;
            i += 1;
        } else if (!strcmp(argv[i], "--PrintPhi")) {
            if (!hadPhi) {
                cerr << "Invalid argument" << endl;
                return 1;
            }
            strPrint = BigNumber.BigNumToString(PhiArray);
            cout << "Phi = " << strPrint << endl;
            i += 1;
        } else if (!strcmp(argv[i], "--PrintD")) {
            if (!hadD) {
                cerr << "Invalid argument" << endl;
                return 1;
            }
            strPrint = BigNumber.BigNumToString(DArray);
            cout << "D = " << strPrint << endl;
            i += 1;
        } else if (!strcmp(argv[i], "--Encrypt")) {
            BigInteger::BigNum msgArray;
            if (!isNumber(argv[i + 1])) {
                msgArray = BigNumber.EncodeString(string(argv[i + 1]));
            } else {
                msgArray = BigNumber.StringToBigNum(string(argv[i + 1]));
            }
            BigInteger::BigNum outArray;

            if (!hadE && !hadN) {
                cerr << "Invalid argument" << endl;
                return 1;
            }
            outArray = BigNumber.PwrMod(msgArray, EArray, NArray);

            strPrint = BigNumber.BigNumToString(outArray);
            cout << "Cipher Text: " << strPrint << endl;
            i += 2;
        } else if (!strcmp(argv[i], "--Decrypt")) {
            BigInteger::BigNum msgArray;
            if (!isNumber(argv[i + 1])) {
                msgArray = BigNumber.EncodeString(string(argv[i + 1]));
            } else {
                msgArray = BigNumber.StringToBigNum(string(argv[i + 1]));
            }
            BigInteger::BigNum outArray;

            msgArray = BigNumber.StringToBigNum(string(argv[i + 1]));
            if (!hadD && !hadN) {
                cerr << "Invalid argument" << endl;
                return 1;
            }

            outArray = BigNumber.PwrMod(msgArray, DArray, NArray);

            cout << "Plaint Text: " << BigNumber.DecodeString(outArray) << endl;

            i += 2;
        } else {
            goto notFoundCommand;
        }
    }
    return 0;
}