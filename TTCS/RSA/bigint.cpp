#include "bigint.h"

#include <algorithm>
#include <stack>
#include <string>

BigInteger::BigNum BigInteger::StringToBigNum(std::string input) {
    BigNum final;
    bool neg = false;

    if (input[0] == 'P' || input[0] == 'p' || input[0] == 'Q' || input[0] == 'q' || input[0] == 'E' || input[0] == 'e') {
        input.erase(0, 2);
    }

    if (input[0] == '-') {
        neg = true;
        input.erase(0, 1);
    }

    std::reverse(std::begin(input), std::end(input));

    int val = 0, itr = 1, Out = 0;
    for (int i = 0; i < input.size(); i++) {
        val += (input[i] - '0') * itr;
        itr *= 10;
        if (itr == 100) {
            final.Num[Out] = val;
            Out++;
            itr = 1;
            val = 0;
        }
    }

    if (val != 0)
        final.Num[Out] = val;

    final.negative = neg;
    return final;
}

BigInteger::BigNum BigInteger::EncodeString(std::string input) {
    BigNum final;
    BigNum b256, tmp;

    b256.Num[0] = 56;
    b256.Num[1] = 2;

    final.Num[0] = input[0];
    for (int i = 1; i < input.size(); i++) {
        tmp.Num[0] = input[i];
        final = BigInteger::Add(BigInteger::Mul(final, b256), tmp);
    }
    final.negative = false;
    return final;
}

std::string BigInteger::DecodeString(BigNum input) {
    DivResult tmp;
    BigNum b256;
    std::string final = "";

    b256.Num[0] = 56;
    b256.Num[1] = 2;

    while (!EqualZero(input)) {
        tmp = BigInteger::DivLarge(input, b256);
        input = tmp.Result;
        final = (char)(tmp.Remainder.Num[1] * 100 + tmp.Remainder.Num[0]) + final;
    }
    return final;
}

BigInteger::BigNum BigInteger::CopyOf(BigNum input) {
    BigNum Result;
    Result.negative = input.negative;
    for (int i = 0; i < maxSize; i++)
        Result.Num[i] = input.Num[i];
    return Result;
}

BigInteger::BigNum BigInteger::AddFront(BigNum input, int val) {
    BigNum Result;
    Result.negative = input.negative;
    for (int i = 0; i < (maxSize - 1); i++)
        Result.Num[i + 1] = input.Num[i];

    Result.Num[0] = val;
    return Result;
}

std::string BigInteger::BigNumToString(BigNum input) {
    std::string out;
    bool Entered = false;
    if (input.negative == true)
        out += "-";

    std::reverse(std::begin(input.Num), std::end(input.Num));
    int i;
    for (i = 0; i < maxSize; i++) {
        if (input.Num[i] != 0)
            break;
    }
    int itr = 10, val = 0;
    for (int j = 0; j < 2; j++) {
        val = input.Num[i] / itr;
        if (val != 0) {
            input.Num[i] = input.Num[i] - val * itr;
            out += (val + '0');
            Entered = true;

        } else {
            if (Entered) {
                input.Num[i] = input.Num[i] - val * itr;
                out += (val + '0');
            }
        }
        itr /= 10;
    }

    i++;
    for (i = i; i < maxSize; i++) {
        int itr = 10, val = 0;
        for (int j = 0; j < 2; j++) {
            val = input.Num[i] / itr;
            input.Num[i] = input.Num[i] - val * itr;
            out += (val + '0');
            itr /= 10;
        }
    }
    return out;
}

bool BigInteger::EqualZero(BigNum input) {
    for (int i = 0; i < maxSize; i++) {
        if (input.Num[i] != 0)
            return false;
    }
    return true;
}

BigInteger::BigNum BigInteger::Add(BigNum first, BigNum second) {
    if (EqualZero(first))
        return second;

    if (EqualZero(second))
        return first;

    BigNum Result;
    int val = 0, carry = 0;
    bool BothNegative = false;

    if (first.negative && second.negative)
        BothNegative = true;

    else if (first.negative) {
        first.negative = false;
        Result = Sub(second, first);
        return Result;
    } else if (second.negative) {
        second.negative = false;
        Result = Sub(first, second);
        return Result;
    }
    int i;
    for (i = 0; i < maxSize; i++) {
        val = (first.Num[i] + second.Num[i] + carry) % 100;
        carry = (first.Num[i] + second.Num[i] + carry) / 100;
        Result.Num[i] = val;
    }

    if (carry != 0)
        Result.Num[i] = carry;

    Result.negative = BothNegative;
    return Result;
}

BigInteger::BigNum BigInteger::Sub(BigNum firstOriginal, BigNum second) {
    // Op1 - Op2 .. first - second
    if (EqualZero(second)) {
        return firstOriginal;
    }
    if (EqualZero(firstOriginal)) {
        second.negative = true;
        return second;
    }

    BigNum Result, tempResult, first;
    first = CopyOf(firstOriginal);
    int val = 0, NextToMe = 0;
    bool LastNum = false;

    if (second.negative) {
        if (first.negative) {
            first.negative = false;
            second.negative = false;
            Result = Sub(second, first);
            return Result;
        } else {
            second.negative = false;
            Result = Add(first, second);
            return Result;
        }
    } else {
        if (first.negative) {
            first.negative = false;
            second.negative = false;
            Result = Add(first, second);
            Result.negative = true;
            return Result;
        }
    }

    int i;
    for (i = 0; i < maxSize; i++) {
        if (LastNum)
            break;
        if (first.Num[i] >= second.Num[i]) {
            val = first.Num[i] - second.Num[i];
            Result.Num[i] = val;
        } else {
            if (i == maxSize)
                LastNum = true;

            int temp = i;
            while (temp < (maxSize - 1)) {
                temp++;
                NextToMe++;
                if (first.Num[temp] != 0) {
                    first.Num[temp] -= 1;
                    first.Num[i] = first.Num[i] + 100;

                    NextToMe--;

                    temp = i + 1;
                    i--;
                    while (NextToMe != 0) {
                        first.Num[temp] = 99;
                        NextToMe--;
                        temp++;
                    }
                    break;
                } else if (first.Num[temp] == 0 && temp == (maxSize - 1))
                    LastNum = true;
            }
        }
    }

    if (LastNum == true) {
        Result = Sub(second, firstOriginal);
        Result.negative = true;
        return Result;
    }

    return Result;
}

BigInteger::BigNum BigInteger::Mul(BigNum first, BigNum second) {
    BigNum Result;

    if (EqualZero(first))
        return first;
    if (EqualZero(second))
        return second;

    int val = 0, carry = 0;
    bool neg = false;

    if (first.negative && second.negative) {
        first.negative = false;
        second.negative = false;
    } else if (first.negative) {
        first.negative = false;
        neg = true;
    } else if (second.negative) {
        second.negative = false;
        neg = true;
    }

    int i, j;
    for (i = 0; i < maxSize; i++) {
        BigNum temp;
        for (j = 0; j < maxSize; j++) {
            val = ((first.Num[i] * second.Num[j]) + carry) % 100;
            carry = ((first.Num[i] * second.Num[j]) + carry) / 100;

            temp.Num[j] += val;
        }

        if (i != 0) {
            for (int k = (maxSize - 1); k != 0; k--) {
                temp.Num[k] = temp.Num[k - i];
            }
            for (int k = 0; k < i; k++) {
                temp.Num[k] = 0;
            }
        }
        Result = Add(Result, temp);
    }

    Result.negative = neg;
    return Result;
}

BigInteger::DivResult BigInteger::DivSmall(BigNum first, BigNum second) {
    DivResult Result;

    if (EqualZero(first)) {
        Result.Result = first;
        Result.Remainder = first;
        return Result;
    }

    BigNum countArray, One, tempResult = first;
    One.Num[0] = 1;

    bool CheckNeg = tempResult.negative;
    do {
        tempResult = Sub(tempResult, second);
        CheckNeg = tempResult.negative;
        if (countArray.Num[0] != 99)
            countArray.Num[0] = countArray.Num[0] + 1;
        else
            countArray = Add(countArray, One);
    } while (!CheckNeg);

    if (countArray.Num[0] != 0) {
        countArray.Num[0] = countArray.Num[0] - 1;
        Result.Result = countArray;
    } else
        Result.Result = Sub(countArray, One);

    Result.Remainder = Add(tempResult, second);

    return Result;
}

BigInteger::DivResult BigInteger::DivLarge(BigNum first, BigNum second) {
    DivResult Result, tempResult;
    BigNum partOffirst, test;

    if (EqualZero(first)) {
        Result.Result = first;
        Result.Remainder = first;
    }

    bool neg = false;

    if (first.negative && second.negative) {
        first.negative = false;
        second.negative = false;
    } else if (first.negative) {
        first.negative = false;
        neg = true;
    } else if (second.negative) {
        second.negative = false;
        neg = true;
    }

    std::reverse(std::begin(first.Num), std::end(first.Num));

    ///////
    int i = 0;
    while (i != maxSize) {
        do {
            partOffirst = AddFront(partOffirst, first.Num[i]);
            test = Sub(partOffirst, second);
            i++;
        } while (test.negative && i != maxSize);

        tempResult = DivSmall(partOffirst, second);
        Result.Result.Num[maxSize - i] = tempResult.Result.Num[0];

        partOffirst = tempResult.Remainder;
    }
    ///////

    Result.Remainder = partOffirst;

    Result.Remainder.negative = neg;
    Result.Result.negative = neg;

    return Result;
}

BigInteger::BigNum BigInteger::PwrMod(BigNum firstOriginal, BigNum secondOriginal, BigNum Mod) {
    if (EqualZero(firstOriginal))
        return firstOriginal;

    if (EqualZero(secondOriginal)) {
        BigNum hop;
        hop.Num[0] = 1;
        return hop;
    }

    BigNum first = CopyOf(firstOriginal);
    BigNum second = CopyOf(secondOriginal);
    std::stack<ArrayOfArray> memory;
    ArrayOfArray AoA;

    BigNum Result, tempResult, Count, tempCount, check, Two;
    DivResult DR;
    Two.Num[0] = 2;
    tempCount.Num[0] = 1;
    tempResult = first;
    Result = first;

    do {
        DR = DivLarge(tempResult, Mod);
        Result = DR.Remainder;
        Count = tempCount;
        AoA.Result = Result;
        AoA.Count = Count;
        memory.push(AoA);

        tempCount = Mul(Count, Two);
        tempResult = Mul(Result, Result);

        check = Sub(second, tempCount);
    } while (!check.negative);

    Result = *new BigNum;
    Result.Num[0] = 1;

    while (!EqualZero(second)) {
        AoA = memory.top();

        if (!Sub(second, AoA.Count).negative) {
            Result = Mul(Result, AoA.Result);
            DR = DivLarge(Result, Mod);
            Result = DR.Remainder;

            second = Sub(second, AoA.Count);
        }

        memory.pop();
    }
    return Result;
}

bool BigInteger::IsPrime(BigNum input) {
    bool Result = false;
    DivResult DR;
    BigNum n = CopyOf(input);
    BigNum One, Two, a, NMinusOne, K, tempK, Q, test, Calc, power;
    One.Num[0] = 1;
    Two.Num[0] = 2;
    a.Num[0] = 2;  // a = 2

    // check if even num
    DR = DivLarge(n, Two);
    if (EqualZero(DR.Remainder))
        return false;

    NMinusOne = Sub(n, One);

    do {
        Q = NMinusOne;
        K = tempK;

        DR = DivLarge(NMinusOne, Two);
        NMinusOne = DR.Result;

        if (K.Num[0] != 99)
            tempK.Num[0] = K.Num[0] + 1;
        else
            tempK = Add(K, One);

    } while (EqualZero(DR.Remainder));

    NMinusOne = Sub(n, One);

    Calc = PwrMod(a, Q, n);
    DR = DivLarge(Calc, n);

    if (EqualZero(Sub(DR.Remainder, One)) || EqualZero(Sub(DR.Remainder, NMinusOne)))
        return true;

    if (test.Num[0] != 99)
        test.Num[0] = test.Num[0] + 1;
    else
        test = Add(test, One);

    while (!EqualZero(Sub(K, test))) {
        Calc = PwrMod(Calc, Two, n);
        DR = DivLarge(Calc, n);

        if (EqualZero(Sub(DR.Remainder, One))) {
            Result = false;
            break;
        } else if (EqualZero(Sub(DR.Remainder, NMinusOne))) {
            Result = true;
            break;
        }

        if (test.Num[0] != 99)
            test.Num[0] = test.Num[0] + 1;
        else
            test = Add(test, One);
    }

    return Result;
}

BigInteger::BigNum BigInteger::Inverse(BigNum input, BigNum mod) {
    BigNum Result, Q, A2, A3, B2, B3, T2, T3, Zero, One, temp;
    One.Num[0] = 1;

    A2 = CopyOf(Zero);
    A3 = CopyOf(mod);
    B2 = CopyOf(One);
    B3 = CopyOf(input);

    while (!EqualZero(B3) && !EqualZero(Sub(B3, One))) {
        Q = DivLarge(A3, B3).Result;
        T2 = Sub(A2, Mul(Q, B2));
        T3 = Sub(A3, Mul(Q, B3));

        A2 = CopyOf(B2);
        A3 = CopyOf(B3);

        B2 = CopyOf(T2);
        B3 = CopyOf(T3);
    }

    if (EqualZero(B3)) {
        // no inverse
        // return 0
    }

    if (EqualZero(Sub(B3, One))) {
        while (B2.negative) {
            B2 = Add(B2, mod);
        }
        Result = B2;

        temp = Sub(Result, mod);
        while (!temp.negative) {
            Result = temp;
            temp = Sub(Result, mod);
        }
    }

    return Result;
}