#ifndef BIGINT_H
#define BIGINT_H
#include <string>

// 4096 bits equals 617 digits
// 2 digits per element makes them 618
class BigInteger {
   public:
    static const int maxSize = 618;

    struct BigNum {
        unsigned char Num[maxSize] = {0};
        bool negative = false;
    };

    struct DivResult {
        BigNum Result;
        BigNum Remainder;
    };

    struct ArrayOfArray {
        BigNum Result;
        BigNum Count;
    };

    BigNum StringToBigNum(std::string input);

    BigNum EncodeString(std::string input);

    std::string DecodeString(BigNum input);

    BigNum CopyOf(BigNum input);

    BigNum AddFront(BigNum input, int val);

    std::string BigNumToString(BigNum input);

    bool EqualZero(BigNum input);

    BigNum Add(BigNum first, BigNum second);

    BigNum Sub(BigNum firstOriginal, BigNum second);

    BigNum Mul(BigNum first, BigNum second);

    DivResult DivSmall(BigNum first, BigNum second);

    DivResult DivLarge(BigNum first, BigNum second);

    BigNum PwrMod(BigNum firstOriginal, BigNum secondOriginal, BigNum Mod);

    bool IsPrime(BigNum input);

    BigNum Inverse(BigNum input, BigNum mod);
};

#endif  // BIGINT_H