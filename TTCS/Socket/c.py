from socket import *

serverName = '127.0.0.1'
serverPort = 12000
# (2) create socket, connection to `serverName` with port `serverPort`
# TCP connection setup
clientSocket = socket(AF_INET, SOCK_STREAM)
clientSocket.connect((serverName, serverPort))

abytes = b'Hello, I am B19DCAT077 client.'
print(f'To server: {abytes}')
# (3) send request to server
clientSocket.send(abytes)

# (6) read reply from server
server_sent = clientSocket.recv(1024)
print(f'From server: {server_sent}')

# close socket
clientSocket.close()
