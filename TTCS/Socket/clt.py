from socket import *
import hashlib


key = b"C47hCy2Tr6XN2E6e"
hashsize = 160 // 8  # sha1 has 160 bits

serverName = '127.0.0.1'
serverPort = 12000
clientSocket = socket(AF_INET, SOCK_STREAM)
clientSocket.connect((serverName, serverPort))

# Send
abytes = b'Hello, I am B19DCAT077 client.'

signature = hashlib.sha1(abytes+key).digest()
message = signature + abytes

print(f'To server: {abytes}')
clientSocket.send(message)

# Receive
server_sent = clientSocket.recv(1024)

if len(server_sent) < hashsize:
    clientSocket.close()
    print("The received message has lost its integrity.")
    exit(0)

signature = server_sent[:hashsize]
message = server_sent[hashsize:]
recv_signature = hashlib.sha1(message+key).digest()

if signature != recv_signature:
    print("The received message has lost its integrity.")
else:
    print(f'From server: {message}')

clientSocket.close()
