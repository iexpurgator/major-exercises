from socket import *

serverPort = 12000
serverSocket = socket(AF_INET, SOCK_STREAM)
# (0) create socket, port `serverPort` for incoming request
serverSocket.bind(('', serverPort))
serverSocket.listen(1)
serverSocket.settimeout(10)
print('[The server is ready]')

while True:
    try:
        # (1) waiting connection request
        # TCP connection setup
        connectionSocket, addr = serverSocket.accept()
        
        # (4) read request from client
        client_sent = connectionSocket.recv(1024)
        print(f'From client: {client_sent}')
        
        abytes = client_sent.decode().replace('client', 'server').encode()
        print(f'To client: {client_sent}')
        # (5) write reply to client
        connectionSocket.send(abytes)
        
        # close connection
        connectionSocket.close()
    except:
        print('[The server is closed]')
        exit(0)
