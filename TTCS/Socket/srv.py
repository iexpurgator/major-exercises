from socket import *
import hashlib


key = b"C47hCy2Tr6XN2E6e"
hashsize = 160 // 8  # sha1 has 160 bits

serverPort = 12000
serverSocket = socket(AF_INET, SOCK_STREAM)
serverSocket.bind(('', serverPort))
serverSocket.listen(1)
serverSocket.settimeout(10)
print('[The server is ready]')

while True:
    try:
        connectionSocket, addr = serverSocket.accept()
        
        # Receive
        client_sent = connectionSocket.recv(1024)

        if len(client_sent) < hashsize:
            raise Exception("The received message has lost its integrity.")

        signature = client_sent[:hashsize]
        message = client_sent[hashsize:]
        recv_signature = hashlib.sha1(message+key).digest()

        if signature != recv_signature:
            connectionSocket.close()
            raise Exception("The received message has lost its integrity.")

        print(f'From client: {message}')
        
        # Send
        abytes = message.decode().replace('client', 'server').encode()
        signature = hashlib.sha1(abytes+key).digest()
        message = signature + abytes
        
        print(f'To client: {abytes}')
        connectionSocket.send(message)
        
        connectionSocket.close()
    except Exception as e:
        if type(e).__name__ == 'timeout':
            print('[The server is closed]')
            serverSocket.close()
            exit(0)
        else:
            print(e)
