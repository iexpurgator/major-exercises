from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip("\n")
readint = lambda: int(stdin.readline().rstrip("\n"))
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + "\n")
writelist = lambda l: stdout.write(" ".join(map(str, l)) + "\n")
writeyn = lambda b: stdout.write("YES\n" if b else "NO\n")


def sdiv(s:str) -> list:
    l = len(s) // 2
    return s[:l], s[l:]


def srot(s:str) -> str:
    s = splitstr(s, ord)
    tsum = sum(s) - (ord('A') * len(s))
    s = [chr(((i - ord('A') + tsum) % 26) + ord('A')) for i in s]
    return ''.join(s)


def smerge(s1:str, s2:str):
    s = splitstr(s1, ord)
    for i, c in enumerate(s2):
        s[i] = chr(ord('A') + (ord(c) - ord('A') + s[i] - ord('A')) % 26)
    return ''.join(s)


def main():
    t = readint()
    for _ in range(t):
        inp = readstr()
        d1, d2 = sdiv(inp)
        write(smerge(srot(d1), srot(d2)))
    return


if __name__ == "__main__":
    main()