from sys import stdin, stdout

s = stdin.readline()
l, u = 0, 0
for i in s:
    if (i >= 'a' and i <= 'z'):
        l = l + 1
    if (i >= 'A' and i <= 'Z'):
        u = u + 1

stdout.write(s.lower() if l >= u else s.upper())