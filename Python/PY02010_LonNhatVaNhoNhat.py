while True:
    n = int(input())
    if n == 0: break
    a = []
    for _ in range(n):
        inp = int(input())
        if inp not in a:
            a.append(inp)
    a.sort()
    if len(a) == 1:
        print('BANG NHAU')
    else:
        print(a[0], a[-1])