from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip()
readint = lambda: int(stdin.readline().rstrip())
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + '\n')
writelist = lambda l: stdout.write(' '.join(map(str, l)) + '\n')
writeyn = lambda b: stdout.write('YES\n' if b else 'NO\n')


def main():
    while True:
        a = readarr(int)
        if a == [0, 0, 0, 0]: break
        cnt = 0
        while True:
            if (a[0] == a[1]
            and a[1] == a[2]
            and a[2] == a[3]
            and a[3] == a[0]):
                break
            a = [
                abs(a[0] - a[1]),
                abs(a[1] - a[2]),
                abs(a[2] - a[3]),
                abs(a[3] - a[0])
            ]
            cnt += 1
        write(cnt)

    return


if __name__ == "__main__":
    main()