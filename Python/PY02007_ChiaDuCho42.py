from re import X
from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip()
readint = lambda: int(stdin.readline().rstrip())
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + '\n')
writelist = lambda l: stdout.write(' '.join(map(str, l)) + '\n')
writeyn = lambda b: stdout.write('YES\n' if b else 'NO\n')


def main():
    s = []
    count = 10
    while True:
        inp = ''
        try:
            inp = input()
        except EOFError:
            s = list(set(s))
            write(len(s))
            break
        a = list(map(int, inp.split()))
        for i in a:
            if count > 0:
                x = i % 42
                if x == 0:
                    s.append(0)
                else:
                    s.append(i%42)
                count -= 1
    return


if __name__ == "__main__":
    main()