from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip()
readint = lambda: int(stdin.readline().rstrip())
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + '\n')
writelist = lambda l: stdout.write(' '.join(map(str, l)) + '\n')
writeyn = lambda b: stdout.write('YES\n' if b else 'NO\n')


def gcd(a:int, b:int) -> int:
    while b:
        a, b = b, a % b
    return a


def main():
    n = readint()
    a = readarr(int)
    a.sort()
    for i in range(n-1):
        for j in a[i+1:]:
            if gcd(a[i], j) == 1:
                write(f'{a[i]} {j}')
    return


if __name__ == "__main__":
    main()