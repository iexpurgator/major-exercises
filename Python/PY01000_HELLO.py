from sys import stdin, stdout

name = stdin.readline().rstrip("\n")
stdout.write('Hello ' + name + ' !\n')