from sys import stdin, stdout
import re

t = int(stdin.readline().rstrip('\n'))
for _ in range(t):
    inp = stdin.readline().rstrip('\n')
    l = [m.group() for m in re.finditer(r'(.)\1*', inp)]
    res = ''
    for i in l:
        res += str(len(i)) + i[0]
    stdout.write(res + '\n')
