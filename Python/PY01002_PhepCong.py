from sys import stdin, stdout

input = stdin.readline().rstrip("\n")
a = int(input[0])
b = int(input[4])
c = int(input[8])
s = 'YES' if a+b==c else 'NO'
stdout.write(s + '\n')