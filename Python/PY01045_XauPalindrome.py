from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip("\n")
write = lambda o: stdout.write(str(o) + "\n")


def main():
    write(len(readstr()) - 1)
    return


if __name__ == "__main__":
    main()