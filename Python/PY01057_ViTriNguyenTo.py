from sys import stdin, stdout
import re

readstr = lambda: stdin.readline().rstrip("\n")
readint = lambda: int(stdin.readline().rstrip("\n"))
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + "\n")
writelist = lambda l: stdout.write(" ".join(map(str, l)) + "\n")
writeyn = lambda b: stdout.write("YES\n" if b else "NO\n")


def main():
    n = 600
    prime = [True for i in range(n + 1)]
    p = 2
    while (p * p <= n):
         
        # If prime[p] is not changed, then it is a prime
        if (prime[p] == True):
             
            # Update all multiples of p
            for i in range(p ** 2, n + 1, p):
                prime[i] = False
        p += 1
    prime[0]= False
    prime[1]= False

    t = readint()
    for _ in range(t):
        inp = splitstr(readstr(), int)
        r = True
        for i in range(len(inp)):
            if prime[i]:
                r = r and prime[inp[i]]
            else:
                r = r and (not prime[inp[i]])
            if not r: break
        writeyn(r)
    return


if __name__ == "__main__":
    main()