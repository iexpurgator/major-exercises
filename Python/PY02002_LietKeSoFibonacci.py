from re import I
from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip("\n")
readint = lambda: int(stdin.readline().rstrip("\n"))
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + "\n")
writelist = lambda l: stdout.write(" ".join(map(str, l)) + "\n")
writeyn = lambda b: stdout.write("YES\n" if b else "NO\n")


def fiborange(b:int, e:int):
    i, j = 1, 0
    f = []
    for x in range(e):
        if x + 1 >= b:
            f.append(str(i))
        t = i
        i = j + i
        j = t
    return ' '.join(f)

def main():
    t = readint()
    for _ in range(t):
        b, e = readarr(int)
        write(fiborange(b, e))
    return


if __name__ == "__main__":
    main()