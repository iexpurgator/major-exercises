from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip()
readint = lambda: int(stdin.readline().rstrip())
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + '\n')
writelist = lambda l: stdout.write(' '.join(map(str, l)) + '\n')
writeyn = lambda b: stdout.write('YES\n' if b else 'NO\n')


def main():
    t = readint()
    for _ in range(t):
        n = readint()
        a = readarr(int)
        b = readarr(int)
        a.sort()
        b.sort()
        res = True
        for i in range(n):
            if not res: break
            res = a[i] <= b[i]
        writeyn(res)
    return


if __name__ == "__main__":
    main()