from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip()
readint = lambda: int(stdin.readline().rstrip())
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + '\n')
writelist = lambda l: stdout.write(' '.join(map(str, l)) + '\n')
writeyn = lambda b: stdout.write('Yes\n' if b else 'No\n')


def isPrime(n: int) -> bool:
    if n == 2: return True
    elif n < 2 or n & 1 == 0: return False
    i = 3
    while i * i <= n:
        if n % i == 0: return False
        i += 2
    return True


def main():
    t = readint()
    for _ in range(t):
        n = readstr()
        sp = splitstr(n, int)
        if all(x in [2, 3, 5, 7] for x in sp):
            rn = int(n[::-1])
            n = int(n)
            writeyn(isPrime(sum(sp)) and isPrime(rn) and isPrime(n))
        else:
            write('No')
    return


if __name__ == "__main__":
    main()