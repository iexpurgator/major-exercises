from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip()
readint = lambda: int(stdin.readline().rstrip())
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + '\n')
writelist = lambda l: stdout.write(' '.join(map(str, l)) + '\n')
writeyn = lambda b: stdout.write('YES\n' if b else 'NO\n')


def main():
    while True:
        n = readint()
        if n == 0: break
        cnt = 1
        while n != 1:
            if n % 2 == 1:
                n = n * 3 + 1
                cnt += 1
            else:
                n //= 2
                cnt += 1
        write(cnt)
    return


if __name__ == "__main__":
    main()