from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip("\n")
readint = lambda: int(stdin.readline().rstrip("\n"))
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + "\n")
writelist = lambda l: stdout.write("".join(map(str, l)) + "\n")
writeyn = lambda b: stdout.write("YES\n" if b else "NO\n")


def permute(s, answer):
    if len(s) == 0:
        write(answer)
        return
    for i in range(len(s)):
        ch = s[i]
        left_substr = s[0:i]
        right_substr = s[i + 1 :]
        rest = left_substr + right_substr
        permute(rest, answer + ch)


def main():
    string = readstr()
    res = ""
    permute(string, res)
    return


if __name__ == "__main__":
    main()