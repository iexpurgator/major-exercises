from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip("\n")
readint = lambda: int(stdin.readline().rstrip("\n"))
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + "\n")
writelist = lambda l: stdout.write(" ".join(map(str, l)) + "\n")
writeyn = lambda b: stdout.write("YES\n" if b else "NO\n")

def Union(lst1, lst2):
    res = list(set(lst1) | set(lst2))
    res.sort()
    return res


def Diff(lst1, lst2):
    res = list(set(lst1) & set(lst2))
    res.sort()
    return res


def main():
    s1 = list(map(str, stdin.readline().lower().split()))
    s2 = list(map(str, stdin.readline().lower().split()))
    s1.sort()
    s2.sort()
    writelist(Union(s1, s2))
    writelist(Diff(s1, s2))
    return


if __name__ == "__main__":
    main()