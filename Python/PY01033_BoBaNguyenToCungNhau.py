from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip("\n")
readint = lambda: int(stdin.readline().rstrip("\n"))
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + "\n")
writelist = lambda s, l: stdout.write((s).join(map(str, l)) + "\n")
writeyn = lambda b: stdout.write("YES\n" if b else "NO\n")


def gcd(a: int, b: int) -> int:
    while b:
        a, b = b, a % b
    return a


if __name__ == "__main__":
    b, e = readarr(int)
    a = list(range(b, e + 1))
    m = []
    for idx, i in enumerate(a):
        if idx == len(a) - 1: break # remove last row not use
        tmp = [0] * (idx + 1)
        for j in a[idx + 1 :]:
            tmp.append(gcd(i, j))
        m.append(tmp)
    l = []
    for i in range(e - b - 1):
        for j in range(i + 1, e - b):
            if m[i][j] == 1:
                for k in range(j + 1, e - b + 1):
                    if m[i][k] == 1 and m[j][k] == 1:
                        l.append((a[i], a[j], a[k]))
    writelist("\n", l)