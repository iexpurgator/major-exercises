from sys import stdin, stdout

t = int(stdin.readline().rstrip('\n'))
for _ in range(t):
    enc = list(stdin.readline().rstrip('\n'))
    dec = ''
    for i in range(0,len(enc),2):
        dec += enc[i] * int(enc[i+1])
    stdout.write(dec + '\n')
