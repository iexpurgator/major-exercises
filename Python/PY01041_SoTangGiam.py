from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip("\n")
readint = lambda: int(stdin.readline().rstrip("\n"))
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + "\n")
writelist = lambda l: stdout.write(" ".join(map(str, l)) + "\n")
writeyn = lambda b: stdout.write("YES\n" if b else "NO\n")


def main():
    t = readint()
    for _ in range(t):
        s = splitstr(readstr(), int)
        k = 1
        for i in range(1, len(s)):
            if k == 1:
                if s[i-1] >= s[i]: k = 0
            if k == 0:
                if s[i - 1] <= s[i]:
                    k = -1
                    break
        writeyn(k == 0)
    return


if __name__ == "__main__":
    main()