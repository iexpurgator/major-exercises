from sys import stdin, stdout

def gcd(a:int, b:int) -> int:
    while b:
        a, b = b, a % b
    return a

input = stdin.readline().split()
n = int(input[0])
k = int(input[1])
cnt = 1
for i in range(10**(k-1), 10**(k)):
    if gcd(i, n) != 1: continue
    stdout.write(str(i) + (' ' if cnt % 10 != 0  else '\n'))
    cnt += 1