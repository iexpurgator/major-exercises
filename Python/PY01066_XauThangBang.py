from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip()
readint = lambda: int(stdin.readline().rstrip())
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + "\n")
writelist = lambda l: stdout.write(" ".join(map(str, l)) + "\n")
writeyn = lambda b: stdout.write("YES\n" if b else "NO\n")


def isbalance(s:str) -> bool:
    n = len(s)
    s = splitstr(s, ord)
    r = s[::-1]
    for i in range(1, n):
        if abs(s[i] - s[i-1]) != abs(r[i] - r[i-1]):
            return False
    return True


def main():
    t = readint()
    for i in range(t):
        writeyn(isbalance(readstr()))
    return


if __name__ == "__main__":
    main()