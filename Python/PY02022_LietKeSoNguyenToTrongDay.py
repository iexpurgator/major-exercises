from sys import stdin, stdout
from collections import Counter

readstr = lambda: stdin.readline().rstrip()
readint = lambda: int(stdin.readline().rstrip())
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + '\n')
writelist = lambda l: stdout.write(' '.join(map(str, l)) + '\n')
writeyn = lambda b: stdout.write('YES\n' if b else 'NO\n')


def isPrime(n: int) -> bool:
    if n == 2: return True
    elif n < 2 or n & 1 == 0: return False
    i = 3
    while i * i <= n:
        if n % i == 0: return False
        i += 2
    return True


def main():
    n = readint()
    a = readarr(int)
    m = dict(Counter(a))
    s = sorted(set(a), key=a.index)
    for i in s:
        if isPrime(i):
            print(f"{i} {m.get(i)}")
    return


if __name__ == "__main__":
    main()