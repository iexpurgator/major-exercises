from sys import stdin, stdout

t = int(stdin.readline().rstrip('\n'))
for _ in range(t):
    inp = stdin.readline().rstrip('\n')
    if len(inp) == 1:
        stdout.write(inp + '\n')
    else:
        l = len(inp) - 1
        inp = int(inp)
        p = int('4' * l)
        x = 10**l
        f = round(inp / x)
        i = inp // x
        if inp > (i * x + p) and f <= i: f += 1
        stdout.write(str(f * x) + '\n')