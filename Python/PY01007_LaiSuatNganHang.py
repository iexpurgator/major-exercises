from sys import stdin, stdout

t = int(stdin.readline().rstrip("\n"))
for _ in range(t):
    input = list(map(float, stdin.readline().split()))
    p = input[1] / 100
    i = 1
    res = input[0] * (1 + p)
    while res < input[2]:
        i += 1
        res = (input[0] * ((1 + p) ** i))
    stdout.write(str(i) + '\n')