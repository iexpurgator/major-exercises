from sys import stdin, stdout


def isPrime(n: int) -> bool:
    if n == 2: return True
    elif n < 2 or n & 1 == 0: return False
    i = 3
    while i * i <= n:
        if n % i == 0: return False
        i += 2
    return True


def gcd(a:int, b:int) -> int:
    while b:
        a, b = b, a % b
    return a


n = int(stdin.readline().strip('\n'))
for _ in range(n):
    input = int(stdin.readline().rstrip('\n'))
    count = 0
    for i in range(1, input):
        if gcd(i, input) == 1: count += 1
    stdout.write('YES\n' if isPrime(count) else 'NO\n')