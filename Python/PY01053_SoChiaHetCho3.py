from sys import stdin, stdout

t = int(stdin.readline().rstrip('\n'))
for _ in range(t):
    s = list(map(int, list(stdin.readline().rstrip('\n'))))
    stdout.write("YES\n" if sum(s) % 3 == 0 else "NO\n")