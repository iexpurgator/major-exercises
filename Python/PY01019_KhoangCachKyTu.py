from sys import stdin, stdout

t = int(stdin.readline().rstrip('\n'))
for _ in range(t):
    s1 = list(map(ord,list(stdin.readline().rstrip('\n'))))
    s2 = s1[::-1]
    res = 'YES\n'
    for i in range(1, len(s1)):
        if abs(s1[i] - s1[i-1]) != abs(s2[i] - s2[i-1]):
            res = 'NO\n'
            break
    stdout.write(res)