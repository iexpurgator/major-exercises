from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip("\n")
readint = lambda: int(stdin.readline().rstrip("\n"))
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + "\n")
writelist = lambda l: stdout.write(" ".join(map(str, l)) + "\n")
writeyn = lambda b: stdout.write("YES\n" if b else "NO\n")


def main():
    t = readint()
    for _ in range(t):
        s = splitstr(readstr(), int)
        s = str(sum(s))
        writeyn(len(s) > 1 and s == s[::-1])
    return


if __name__ == "__main__":
    main()