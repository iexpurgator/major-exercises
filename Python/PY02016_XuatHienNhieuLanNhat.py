from sys import stdin, stdout
from collections import Counter, OrderedDict

readstr = lambda: stdin.readline().rstrip()
readint = lambda: int(stdin.readline().rstrip())
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + '\n')
writelist = lambda l: stdout.write(' '.join(map(str, l)) + '\n')
writeyn = lambda b: stdout.write('YES\n' if b else 'NO\n')


def main():
    t = readint()
    for _ in range(t):
        n = readint()
        a = dict(Counter(readarr(int)))
        a = sorted(a.items(), key=lambda x: x[1], reverse=True)
        if a[0][1] > n//2:
            write(a[0][0])
        else:
            write('NO')
    return


if __name__ == "__main__":
    main()