from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip("\n")
readint = lambda: int(stdin.readline().rstrip("\n"))
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + "\n")
writelist = lambda l: stdout.write(" ".join(map(str, l)) + "\n")
writeyn = lambda b: stdout.write("YES\n" if b else "NO\n")


def isPrime(n: int) -> bool:
    if n == 2:
        return True
    elif n < 2 or n & 1 == 0:
        return False
    i = 3
    while i * i <= n:
        if n % i == 0:
            return False
        i += 2
    return True


def main():
    t = readint()
    for _ in range(t):
        s = splitstr(readstr(), int)
        r = True
        for i in s[::2]:
            r = r and (i%2 == 0)
        for i in s[1::2]:
            r = r and (i%2 == 1)
        writeyn(r and isPrime(sum(s)))
    return


if __name__ == "__main__":
    main()