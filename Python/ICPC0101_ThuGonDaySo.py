from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip()
readint = lambda: int(stdin.readline().rstrip())
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + '\n')
writelist = lambda l: stdout.write(' '.join(map(str, l)) + '\n')
writeyn = lambda b: stdout.write('YES\n' if b else 'NO\n')


def main():
    n = readint()
    a = readarr(int)
    cnt = 1
    while cnt != 0:
        cnt = 0
        i = 1
        n = len(a)
        while i < n:
            if (a[i-1] + a[i]) % 2 == 0:
                a.pop(i-1)
                a.pop(i-1)
                cnt += 1
            i += 1
            n = len(a)
    write(len(a))
    return


if __name__ == "__main__":
    main()