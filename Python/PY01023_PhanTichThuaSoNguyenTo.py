from sys import stdin, stdout

def isPrime(n: int) -> bool:
    if n == 2: return True
    elif n < 2 or n & 1 == 0: return False
    i = 3
    while i * i <= n:
        if n % i == 0: return False
        i += 2
    return True

def gcd(a:int, b:int) -> int:
    while b:
        a, b = b, a % b
    return a

t = int(stdin.readline().rstrip('\n'))
for _ in range(t):
    n = int(stdin.readline().rstrip('\n'))
    i = 2
    l = []
    cnt = 0
    while True:
        if n % i == 0:
            n //= i
            cnt += 1
        else:
            if cnt != 0: l.append([i, cnt])
            i += 1
            cnt = 0
        if n == 1:
            l.append([i, cnt])
            break
    v = l[0]
    if len(l) == 0:
        stdout.write('1\n')
    else:
        stdout.write('1 * ' + ' * '.join(f'{v[0]}^{v[1]}' for v in l) + '\n')
    # stdout.write('YES\n' if 0 == 0 else 'NO\n')