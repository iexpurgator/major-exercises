from sys import stdin, stdout
import re

readstr = lambda: stdin.readline().rstrip("\n")
readint = lambda: int(stdin.readline().rstrip("\n"))
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + "\n")
writelist = lambda l: stdout.write(" ".join(map(str, l)) + "\n")
writeyn = lambda b: stdout.write("YES\n" if b else "NO\n")


def main():
    t = readint()
    for _ in range(t):
        inp = readstr()
        s = sum(list(map(int, re.findall(r"\d", inp))))
        w = re.findall(r"\D", inp)
        w.sort()
        write(''.join(w) + str(s))
    return


if __name__ == "__main__":
    main()