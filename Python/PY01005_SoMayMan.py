from sys import stdin, stdout
import collections

input = collections.Counter(stdin.readline().rstrip("\n"))
count = input['4'] + input['7']
stdout.write('YES\n' if count == 4 or count == 7 else 'NO\n')