class LuongMua:
    id = 0
    name = None
    time = 0.0
    rain = 0

    def __init__(self, id,  name,  time,  rain):
        self.id = id
        self.name = name
        self.time = time
        self.rain = rain

    def getId(self):
        return self.id

    def getName(self):
        return self.name

    def getTime(self):
        return self.time

    def setTime(self, time):
        self.time = time

    def getRain(self):
        return self.rain

    def setRain(self, rain):
        self.rain = rain

    def __str__(self) -> str:
        return f"T{self.id:02d} {self.name} {self.rain / self.time:.02f}"


def minusTime(t1,  t2):
    tt1 = t1.split(":")
    tt2 = t2.split(":")
    h = int(tt2[0]) - int(tt1[0])
    m = int(tt2[1]) - int(tt1[1])
    return h + (m / 60.0)


if __name__ == "__main__":
    listlked = []
    T = int(input())
    id = 1
    i = 0
    while T > 0:
        name = None
        t1 = None
        t2 = None
        rain = 0
        name = input()
        t1 = input()
        t2 = input()
        rain = int(input())
        time = minusTime(t1, t2)
        i = 0
        while (i < len(listlked)):
            if (listlked[i].getName() == name):
                break
            i += 1
        if (i == len(listlked)):
            listlked.append(LuongMua(id, name, time, rain))
            id += 1
        else:
            ti = listlked[i].getTime()
            ri = listlked[i].getRain()
            listlked[i].setTime(time + ti)
            listlked[i].setRain(rain + ri)
        T -= 1
    for i in listlked:
        print(i)
