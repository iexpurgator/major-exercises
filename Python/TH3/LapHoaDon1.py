import math


class KHDungNuoc:
    id = None
    name = None
    valOld = 0
    valNew = 0
    map = {100: 0.02, 150:0.03, 200:0.05}

    def __init__(self, id,  name,  valOld,  valNew):
        self.id = f"KH{id:02d}"
        self.name = name
        self.valOld = valOld
        self.valNew = valNew

    def getDiff(self):
        return int((self.valNew - self.valOld))

    def toMoney(self):
        used = self.getDiff()
        donGia = 0
        tien = [0] * (3)
        if (used > 100):
            tien[0] = 50 * 100
            tien[1] = 50 * 150
            tien[2] = (used - 100) * 200
            donGia = 200
        elif(used > 50):
            tien[0] = 50 * 100
            tien[1] = (used - 50) * 150
            tien[2] = 0
            donGia = 150
        else:
            tien[0] = used * 100
            tien[1] = 0
            tien[2] = 0
            donGia = 100
        tt = tien[0] + tien[1] + tien[2]
        return tt + int(math.ceil(tt * self.map.get(donGia)))

    def __str__(self) -> str:
        return self.id + " " + self.name + " " + str(self.toMoney())


if __name__ == "__main__":
    listKH = []
    T = int(input())
    id = 1
    while id <= T:
        name = None
        valNew = 0
        valOld = 0
        name = input()
        valOld = int(input())
        valNew = int(input())
        listKH.append(KHDungNuoc(id, name, valOld, valNew))
        id += 1

    listKH.sort(key=lambda x: x.getDiff(), reverse=True)
    for i in listKH:
        print(i)
