from math import gcd


class PhanSo:
    def __init__(self, tu, mau) -> None:
        self.tu = tu
        self.mau = mau

    def simple(self):
        g = gcd(self.tu, self.mau)
        self.tu //= g
        self.mau //= g

    def __str__(self) -> str:
        return f"{self.tu}/{self.mau}"


if __name__ == '__main__':
    tu, mau = input().split()
    p = PhanSo(int(tu), int(mau))
    p.simple()
    print(p)
