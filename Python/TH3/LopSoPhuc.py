import math


class ComplexNumber:
    a = 0
    b = 0

    def __init__(self, a,  b):
        self.a = a
        self.b = b

    @staticmethod
    def add(c1,  c2):
        return c1.add(c2)

    @staticmethod
    def mul(c1,  c2):
        return c1.mul(c2)

    @staticmethod
    def sqr(c):
        return ComplexNumber.mul(c, c)

    def add(self, c):
        a = self.a + c.a
        b = self.b + c.b
        return ComplexNumber(a, b)

    def mul(self, c):
        a = (self.a * c.a) - (self.b * c.b)
        b = (self.a * c.b) + (self.b * c.a)
        return ComplexNumber(a, b)

    def __str__(self) -> str:
        if (self.b > 0):
            return str(self.a) + " + " + str(self.b) + "i"
        else:
            return str(self.a) + " - " + str(abs(self.b)) + "i"


if __name__ == "__main__":
    T = int(input())
    i = 0
    s = [None] * (4)
    while i < T:
        s = input().split()
        A = ComplexNumber(int(s[0]), int(s[1]))
        B = ComplexNumber(int(s[2]), int(s[3]))
        C = ComplexNumber.add(A, B).mul(A)
        D = ComplexNumber.sqr(ComplexNumber.add(A, B))
        print(str(C) + ", " + str(D))
        i += 1
