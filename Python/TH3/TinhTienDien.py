import math


class KhachHangDien:
    dm = {'A': 100,
          'B': 500,
          'C': 200}
    ma = None
    ten = None
    loai = None
    chisoDau = 0
    chisoCuoi = 0

    def __init__(self, ma,  ten: str,  linein):
        self.ma = f"KH{ma:02d}"
        self.ten = ' '.join([i.capitalize() for i in ten.split()])
        line = linein.split()
        self.loai = line[0][0]
        self.chisoDau = int(line[1])
        self.chisoCuoi = int(line[2])

    def getTienTrongDM(self):
        chiso = self.chisoCuoi - self.chisoDau
        if (chiso > self.dm[self.loai]):
            return self.dm[self.loai] * 450
        else:
            return chiso * 450

    def getTienVuotDM(self):
        chiso = self.chisoCuoi - self.chisoDau - self.dm[self.loai]
        if (chiso > 0):
            return chiso * 1000
        else:
            return 0

    def getVAT(self):
        return int(math.ceil(self.getTienVuotDM() * 0.05))

    def getTongTien(self):
        return self.getTienTrongDM() + self.getTienVuotDM() + self.getVAT()

    def __str__(self) -> str:
        return f"{self.ma} {self.ten} {self.getTienTrongDM()} {self.getTienVuotDM()} {self.getVAT()} {self.getTongTien()}"

    def __lt__(self, o):
        return o.getTongTien() < self.getTongTien()

    def __gt__(self, o):
        return o.getTongTien() > self.getTongTien()


if __name__ == "__main__":
    T = int(input())
    listKH = []
    for i in range(1, T + 1):
        listKH.append(KhachHangDien(i, input(), input()))
    listKH.sort()
    for i in listKH:
        print(i)
