import math


class Bikecircle:
    name = None
    team = None
    time = None

    def __init__(self, name,  team,  time):
        self.name = name
        self.team = team
        self.time = time

    def getTime(self):
        t = self.time.split(":")
        return int(t[0]) * 60 + int(t[1])

    def getVantoc(self):
        t = self.time.split(":")
        ti = (int(t[0]) - 6) + (float(t[1]) / 60.0)
        return int(round(120.0 / ti))

    def convertID(self):
        id = ""
        tmp = self.team + " " + self.name
        iid = tmp.split()
        for string in iid:
            id += string[0]
        return id

    def __str__(self) -> str:
        return self.convertID() + " " + self.name + " " + self.team + " " + str(self.getVantoc()) + " Km/h"


if __name__ == "__main__":
    listVDV = []
    T = int(input())
    while (T > 0):
        name = input()
        team = input()
        time = input()
        listVDV.append(Bikecircle(name, team, time))
        T -= 1

    listVDV.sort(key=lambda x: x.getTime())
    for i in listVDV:
        print(i)
