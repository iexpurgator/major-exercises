while True:
    try:
        str = input().lower().split()
    except:
        break
    str[0] = str[0].capitalize()
    last_str = len(str) - 1
    if str[last_str] in ['!', '?', '.']:
        str[last_str - 1] += str[last_str]
        str.remove(str[last_str])
    elif str[last_str][::-1][0] not in ['!', '?', '.']:
        str[last_str] += '.'
    print(' '.join(str))
