class NhanVien13:
    def __init__(self, id,  name,  diemLT,  diemTH):
        self.id = "TS%02d" % id
        self.name = name
        self.diemLT = diemLT
        self.diemTH = diemTH

    def diemTB(self):
        return (self.diemLT + self.diemTH) / 2.0

    def xepLoai(self):
        tb = self.diemTB()
        xl = None
        if (tb > 9.5):
            xl = "XUAT SAC"
        elif(tb >= 8):
            xl = "DAT"
        elif(tb > 5):
            xl = "CAN NHAC"
        else:
            xl = "TRUOT"
        return f"{tb:.02f} {xl}"

    def __str__(self) -> str:
        return self.id + " " + self.name + " " + self.xepLoai()


def fmDiem(diem):
    return diem / 10.0 if diem > 10 else diem


if __name__ == "__main__":
    listTD = []
    T = int(input())
    id = 0
    while (id < T):
        name = input()
        diemLT = fmDiem(float(input()))
        diemTH = fmDiem(float(input()))
        id += 1
        listTD.append(NhanVien13(id, name, diemLT, diemTH))
    listTD.sort(key=lambda x: (x.diemTB() * 100), reverse=True)
    for i in listTD:
        print(i)
