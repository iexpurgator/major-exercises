class Point:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    def distance(self, p2):
        return ((self.x - p2.x)**2 + (self.y - p2.y)**2) ** (1/2)

    def Perimeter(self, p2, p3):
        self.d12 = self.distance(p2)
        self.d13 = self.distance(p3)
        self.d23 = p2.distance(p3)
        if (self.d12 + self.d13 > self.d23 and
                self.d12 + self.d23 > self.d13 and
                self.d13 + self.d23 > self.d12):
            return "%.3f" % (self.d12 + self.d13 + self.d23)
        else:
            return "INVALID"


if __name__ == '__main__':
    t = int(input())
    while t > 0:
        arr = input().split()
        p1 = Point(float(arr[0]), float(arr[1]))
        p2 = Point(float(arr[2]), float(arr[3]))
        p3 = Point(float(arr[4]), float(arr[5]))
        for i in arr:
            if abs(int(i)) >= 1000: print("INVALID")
        else: print(p1.Perimeter(p2, p3))
        t -= 1
