from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip("\n")
readint = lambda: int(stdin.readline().rstrip("\n"))
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + "\n")
writelist = lambda l: stdout.write(" ".join(map(str, l)) + "\n")
writeyn = lambda b: stdout.write("YES\n" if b else "NO\n")


def palindrome(n:int, b:int=10) -> bool:
    if n < b:
        return str(n) == str(n)[::-1]
    res = ''
    while n:
        res = str(n % b) + res
        n //= b
    return res == res[::-1]


def main():
    a, b, m = readarr(int)
    cnt = 0
    for i in range(a, b+1):
        b2 = bin(i)[2:]
        r = b2 == b2[::-1]
        if m >= 3 and r:
            r = r and palindrome(i, 3)
        if m > 3 and r:
            r = r and palindrome(i, m)
        # for j in range(3, m+1):
        #     if not r : break
        #     r = r and palindrome(i, j)
        if r: cnt += 1
    write(cnt)
    return


if __name__ == "__main__":
    main()