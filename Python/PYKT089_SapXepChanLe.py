from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip()
readint = lambda: int(stdin.readline().rstrip())
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + ' ')
writelist = lambda l: stdout.write(' '.join(map(str, l)) + '\n')
writeyn = lambda b: stdout.write('YES\n' if b else 'NO\n')


def main():
    n = readint()
    a = []
    while True:
        inp = ''
        try:
            inp = input()
        except EOFError:
            break
        a += list(map(int, inp.split()))
    a = a[:n]
    e = []; o = []
    for i in a:
        if i % 2 == 0: e.append(i)
        else: o.append(i)
    e.sort()
    o.sort(reverse=True)
    for i in a:
        write(e.pop(0) if i % 2 == 0 else o.pop(0))
    return


if __name__ == "__main__":
    main()