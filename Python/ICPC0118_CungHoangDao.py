from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip()
readint = lambda: int(stdin.readline().rstrip())
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + '\n')
writelist = lambda l: stdout.write(' '.join(map(str, l)) + '\n')
writeyn = lambda b: stdout.write('YES\n' if b else 'NO\n')

chd = [('Bach Duong', 21, 3, 19, 4), ('Kim Nguu', 20, 4, 20, 5),
       ('Song Tu', 21, 5, 20, 6), ('Cu Giai', 21, 6, 22, 7),
       ('Su Tu', 23, 7, 22, 8), ('Xu Nu', 23, 8, 22, 9),
       ('Thien Binh', 23, 9, 22, 10), ('Thien Yet', 23, 10, 22, 11),
       ('Nhan Ma', 23, 11, 21, 12), ('Ma Ket', 22, 12, 19, 1),
       ('Bao Binh', 20, 1, 18, 2), ('Song Ngu', 19, 2, 20, 3)]


def main():
    t = readint()
    for _ in range(t):
        d, m = readarr(int)
        for v in chd:
            if (d >= v[1] and m == v[2]) or (d <= v[3] and m == v[4]):
                write(v[0])
    return


if __name__ == "__main__":
    main()