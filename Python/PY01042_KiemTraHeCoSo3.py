from sys import stdin, stdout

t = int(stdin.readline().rstrip('\n'))
for _ in range(t):
    s = stdin.readline().rstrip('\n')
    s = s.replace('0', '').replace('1', '').replace('2', '')
    stdout.write("YES\n" if s == '' else "NO\n")