from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip("\n")
readint = lambda: int(stdin.readline().rstrip("\n"))
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + "\n")
writelist = lambda l: stdout.write(" ".join(map(str, l)) + "\n")
writeyn = lambda b: stdout.write("YES\n" if b else "NO\n")


def main():
    t = readint()
    for _ in range(t):
        inp = readstr()
        l = len(inp) % 2 == 1
        b = inp[0] != inp[1]
        inp = list(map(int, list(inp.replace(inp[0], '0'))[::2]))
        writeyn(l and b and sum(inp) == 0)
    return


if __name__ == "__main__":
    main()