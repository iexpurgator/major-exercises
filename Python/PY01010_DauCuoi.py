from sys import stdin, stdout

n = int(stdin.readline().strip('\n'))
for _ in range(n):
    input = stdin.readline().rstrip('\n')
    stdout.write('YES\n' if int(input[0:2]) == int(input[len(input)-2:]) else 'NO\n')