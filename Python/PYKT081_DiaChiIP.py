from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip()
readint = lambda: int(stdin.readline().rstrip())
writeyn = lambda b: stdout.write("YES\n" if b else "NO\n")


def validate_ip_address(address):
    parts = address.split(".")
    if len(parts) != 4:
        return False
    for part in parts:
        try:
            int(part)
        except:
            return False
        if int(part) < 0 or int(part) > 255:
            return False
    return True


def main():
    t = readint()
    for _ in range(t):
        ip = readstr()
        writeyn(validate_ip_address(ip))
    return


if __name__ == "__main__":
    main()