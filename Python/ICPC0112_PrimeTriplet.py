from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip()
readint = lambda: int(stdin.readline().rstrip())
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + '\n')
writelist = lambda l: stdout.write(' '.join(map(str, l)) + '\n')
writeyn = lambda b: stdout.write('YES\n' if b else 'NO\n')


def SievePrime(n:int) -> list:
    prime = [True for i in range(n + 1)]
    p = 2
    while (p * p <= n):
        if (prime[p] == True):
            for i in range(p ** 2, n + 1, p):
                prime[i] = False
        p += 1
    prime[0]= False
    prime[1]= False
    return prime


def main():
    prime = SievePrime(1000000)
    t = readint()
    for _ in range(t):
        n = readint()
        cnt = 0
        for i in range(6, n):
            if ( (prime[i] == True and prime[i-2] == True and prime[i-6] == True) or
                 (prime[i] == True and prime[i-4] == True and prime[i-6] == True)):
                 cnt += 1
        write(cnt)
    return


if __name__ == "__main__":
    main()