from sys import stdin, stdout

t = int(stdin.readline().rstrip('\n'))
for _ in range(t):
    inp = stdin.readline().rstrip('\n')
    inp = inp[len(inp)-2:]
    stdout.write('YES\n' if inp == '86' else 'NO\n')
