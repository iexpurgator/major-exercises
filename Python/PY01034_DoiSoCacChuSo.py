from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip("\n")
readint = lambda: int(stdin.readline().rstrip("\n"))
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + "\n")
writelist = lambda l: stdout.write(" ".join(map(str, l)) + "\n")
writeyn = lambda b: stdout.write("YES\n" if b else "NO\n")


def prevPermutation(A: list):
    i = len(A) - 2
    while i >= 0 and A[i] <= A[i + 1]:
        i -= 1
    if i >= 0:
        max_ = i + 1
        # max number greater on right that less than A[i]
        for j in range(max_ + 1, len(A)):
            if A[max_] < A[j] < A[i]:
                max_ = j
        A[max_], A[i] = A[i], A[max_]
    return int(''.join(A))


def main():
    t = readint()
    for _ in range(t):
        inp = readstr()
        sol = prevPermutation(splitstr(inp, str))
        if int(inp) != sol and len(inp) == len(str(sol)):
            write(sol)
        else:
            write(-1)
    return


if __name__ == "__main__":
    main()