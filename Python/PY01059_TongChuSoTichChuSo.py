from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip("\n")
readint = lambda: int(stdin.readline().rstrip("\n"))
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + "\n")
writelist = lambda l: stdout.write(" ".join(map(str, l)) + "\n")
writeyn = lambda b: stdout.write("YES\n" if b else "NO\n")


def main():
    t = readint()
    for _ in range(t):
        s = splitstr(readstr(), int)
        even = s[::2]
        odd = s[1::2]
        m = 1
        for i in odd:
            if i != 0: m *= i
        write(f'{sum(even)} {m if sum(odd) != 0 and m != 1 else 0}')
    return


if __name__ == "__main__":
    main()