from sys import stdin, stdout
import math

readstr = lambda: stdin.readline().rstrip("\n")
readint = lambda: int(stdin.readline().rstrip("\n"))
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + "\n")
writelist = lambda l: stdout.write(" ".join(map(str, l)) + "\n")
writeyn = lambda b: stdout.write("YES\n" if b else "NO\n")


def myRound(a):
    x = math.floor(a)
    b = a - x
    if b < 0.25:
        b = 0
    elif b < .75:
        b = 0.5
    else:
        b = 1
    return float(x + b)

def convert(a: int):
    cntCA = [39, 37, 35, 33, 30, 27, 23, 20, 16, 13, 10, 7, 5, 3]
    maxBand = 9.0
    for idx, j in enumerate(cntCA):
        if a >= j:
            return maxBand - (0.5 * idx)
    return 1.0
            

def main():
    t = readint()
    for _ in range(t):
        arr = readarr(str)
        s = float(arr[2]) + float(arr[3])
        s += convert(int(arr[0]))
        s += convert(int(arr[1]))
        # print(s/4)
        write(myRound(s/4))
    return


if __name__ == "__main__":
    main()