from sys import stdin, stdout

p = "ABCDEFGHIJKLMNOPQRSTUVWXYZ_."

while True:
    inp = stdin.readline().rstrip('\n')
    if inp == '0': break
    inp = inp.split(' ')
    k = int(inp[0])
    s = inp[1]
    res = ''
    for i in range(len(s)):
        res = p[(p.index(s[i]) + k) % 28] + res
    stdout.write(res + '\n')
