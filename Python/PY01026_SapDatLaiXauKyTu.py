from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip("\n")
readint = lambda: int(stdin.readline().rstrip("\n"))
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o))
writel = lambda o: stdout.write(str(o) + "\n")
writelist = lambda l: stdout.write(" ".join(map(str, l)) + "\n")
writeyn = lambda b: stdout.write("YES\n" if b else "NO\n")


def main():
    t = readint()
    for i in range(t):
        s1 = splitstr(readstr(), ord)
        s2 = splitstr(readstr(), ord)
        s1.sort()
        s2.sort()
        write(f'Test {i+1}: ')
        writeyn(s1 == s2)
    return


if __name__ == "__main__":
    main()