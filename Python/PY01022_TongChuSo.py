from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip("\n")
readint = lambda: int(stdin.readline().rstrip("\n"))
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + "\n")
writelist = lambda l: stdout.write(" ".join(map(str, l)) + "\n")
writeyn = lambda b: stdout.write("YES\n" if b else "NO\n")


def ssum(s:str) -> str:
    res = 0
    if s[0] == '-':
        res = ord('-') - ord('0')
        s = s.replace('-', '')
    ar = splitstr(s, int)
    return str(res + sum(ar))


def main():
    inp = readstr()
    cnt = 0
    while len(inp) != 1:
        inp = ssum(inp)
        cnt += 1
    write(cnt)
    return


if __name__ == "__main__":
    main()