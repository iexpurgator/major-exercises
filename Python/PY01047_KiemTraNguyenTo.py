from sys import stdin, stdout

def isPrime(n: int) -> bool:
    if n == 2: return True
    elif n < 2 or n & 1 == 0: return False
    i = 3
    while i * i <= n:
        if n % i == 0: return False
        i += 2
    return True


t = int(stdin.readline().rstrip('\n'))
for _ in range(t):
    input = stdin.readline().rstrip('\n')
    stdout.write('YES\n' if isPrime(int(input[len(input) - 4:])) else 'NO\n')