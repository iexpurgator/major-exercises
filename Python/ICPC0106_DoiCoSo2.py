from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip("\n")
readint = lambda: int(stdin.readline().rstrip("\n"))
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + "\n")
writelist = lambda l: stdout.write(" ".join(map(str, l)) + "\n")
writeyn = lambda b: stdout.write("YES\n" if b else "NO\n")


def base_convert(i, b):
    result = []
    while i > 0:
            result.insert(0, i % b)
            i = i // b
    return ''.join(list(map(str, result)))


def main():
    t = readint()
    for _ in range(t):
        b = readint()
        inp = int(readstr(), 2)
        if b == 4:
            inp = base_convert(inp, 4)
        elif b == 8:
            inp = oct(inp)[2:]
        elif b == 16:
            inp = hex(inp)[2:].upper()
        elif b == 2:
            inp = bin(inp)[2:]
        write(inp)
    return


if __name__ == "__main__":
    main()