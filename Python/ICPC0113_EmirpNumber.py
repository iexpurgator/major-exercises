from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip()
readint = lambda: int(stdin.readline().rstrip())
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o))
writelist = lambda l: stdout.write(' '.join(map(str, l)) + '\n')
writeyn = lambda b: stdout.write('YES\n' if b else 'NO\n')


def SievePrime(n:int) -> list:
    prime = [True for i in range(n + 1)]
    p = 2
    while (p * p <= n):
        if (prime[p] == True):
            for i in range(p ** 2, n + 1, p):
                prime[i] = False
        p += 1
    prime[0]= False
    prime[1]= False
    return prime


def main():
    prime = SievePrime(1000000)
    t = readint()
    for _ in range(t):
        n = readint()
        cprime = prime[:n+1]
        for i in range(n):
            ir = int(str(i)[::-1])
            if i != ir and ir < n and cprime[ir] and cprime[i]:
                write(f'{i} {ir} ')
                cprime[ir] = False
        write('\n')
    return


if __name__ == "__main__":
    main()