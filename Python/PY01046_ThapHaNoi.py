from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip("\n")
readint = lambda: int(stdin.readline().rstrip("\n"))
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + "\n")
writelist = lambda l: stdout.write(" ".join(map(str, l)) + "\n")
writeyn = lambda b: stdout.write("YES\n" if b else "NO\n")


def TowerOfHanoi(n, from_rod, to_rod, aux_rod):
    if n == 0:
        return
    TowerOfHanoi(n - 1, from_rod, aux_rod, to_rod)
    # print("Move disk",n,"from rod",from_rod,"to rod",to_rod)
    write(from_rod + ' -> ' + to_rod)
    TowerOfHanoi(n - 1, aux_rod, to_rod, from_rod)


def main():
    n = readint()
    TowerOfHanoi(n, 'A', 'C', 'B')


if __name__ == "__main__":
    main()
