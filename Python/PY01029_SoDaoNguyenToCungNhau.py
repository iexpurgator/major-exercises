from sys import stdin, stdout

def gcd(a:int, b:int) -> int:
    while b:
        a, b = b, a % b
    return a


t = int(stdin.readline().rstrip('\n'))
for _ in range(t):
    input = stdin.readline().rstrip('\n')
    stdout.write('YES\n' if gcd(int(input), int(input[::-1])) == 1 else 'NO\n')