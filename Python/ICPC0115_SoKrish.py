from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip("\n")
readint = lambda: int(stdin.readline().rstrip("\n"))
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + "\n")
writelist = lambda l: stdout.write(" ".join(map(str, l)) + "\n")
writeyn = lambda b: stdout.write("Yes\n" if b else "No\n")


def main():
    g = [1, 1, 2]
    for i in range(3, 10):
        g.append(g[i-1]*i)
    t = readint()
    for _ in range(t):
        inp = readstr()
        a = splitstr(inp, int)
        s = 0
        for i in a:
            s += g[i]
        writeyn(s == int(inp))
    return


if __name__ == "__main__":
    main()