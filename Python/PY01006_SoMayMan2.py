from sys import stdin, stdout
import re

n = int(stdin.readline().rstrip("\n"))
for _ in range(n):
    input = stdin.readline().rstrip("\n")
    stdout.write('YES\n' if re.match('^[47]*$',input) else 'NO\n')