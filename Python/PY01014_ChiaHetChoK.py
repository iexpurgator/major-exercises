from sys import stdin, stdout

input = list(map(int, stdin.readline().split()))
# a, K, N = input[0], input[1], input[2]

i = (input[0] // input[1]) + 1

if input[1] * i > input[2]:
    stdout.write('-1')
else:
    stdout.write(' '.join(map(str, range((input[1] * i) - input[0], input[2] - input[0] + 1, input[1]))))
