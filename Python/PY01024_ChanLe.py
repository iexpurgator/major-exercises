from sys import stdin, stdout

t = int(stdin.readline().rstrip('\n'))
for _ in range(t):
    a = list(map(int, list(stdin.readline().rstrip('\n'))))
    s = sum(a)
    res = 'YES\n'
    if s % 10 != 0:
        res = 'NO\n'
    else:
        for i in range(1, len(a)):
            if abs(a[i] - a[i-1]) != 2:
                res = 'NO\n'
                break
    stdout.write(res)
