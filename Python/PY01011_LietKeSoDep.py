from sys import stdin, stdout

arr = '02468'
dt = []


def permutation(prefix, k):
    if k == 0:
        s = prefix + prefix[::-1]
        if len(s) == len(str(int(s))):
            dt.append(s)
        return

    for i in range(len(arr)):
        new_prefix = prefix + arr[i]
        permutation(new_prefix, k - 1)


for r in range(1, 4):
    permutation("", r)

t = int(stdin.readline().rstrip('\n'))
for _ in range(t):
    N = int(stdin.readline().rstrip('\n'))
    for i in dt:
        if int(i) >= N: break
        stdout.write(i + ' ')
    stdout.write('\n')