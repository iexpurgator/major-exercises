from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip()
readint = lambda: int(stdin.readline().rstrip())
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + '\n')
writelist = lambda l: stdout.write(' '.join(map(str, l)) + '\n')
writeyn = lambda b: stdout.write('YES\n' if b else 'NO\n')


def merge(a: list, l: int, m: int, r: int) -> int:
    i, j, k = 0, 0, l
    count = 0
    nL, nR = m - l + 1, r - m
    L = a[l:l + nL]
    R = a[m + 1:m + nR + 1]
    while i < nL and j < nR:
        if L[i] <= R[j]:
            a[k] = L[i]
            k += 1
            i += 1
        else:
            a[k] = R[j]
            k += 1
            j += 1
            count += nL - i
    while i < nL:
        a[k] = L[i]
        k += 1
        i += 1
    while j < nR:
        a[k] = R[j]
        k += 1
        j += 1
    return count


def mergeSort(a: list, l: int, r: int) -> int:
    count = 0
    if l < r:
        m = (l + r) // 2
        count += mergeSort(a, l, m)
        count += mergeSort(a, m + 1, r)
        count += merge(a, l, m, r)
    return count


def main():
    n = readint()
    a = readarr(int)
    write(mergeSort(a, 0, n - 1))
    return


if __name__ == "__main__":
    main()