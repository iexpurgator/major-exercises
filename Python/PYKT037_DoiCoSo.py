from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip("\n")
readint = lambda: int(stdin.readline().rstrip("\n"))
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + "\n")
writelist = lambda l: stdout.write(" ".join(map(str, l)) + "\n")
writeyn = lambda b: stdout.write("YES\n" if b else "NO\n")


def main():
    lib = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    t = readint()
    for _ in range(t):
        res = ''
        n, b = readarr(int)
        while n:
            res = lib[n % b] + res
            n //= b
        write(res)
    return


if __name__ == "__main__":
    main()