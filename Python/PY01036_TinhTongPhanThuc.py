from sys import stdin, stdout

t = int(stdin.readline().rstrip('\n'))
for _ in range(t):
    n = int(stdin.readline().rstrip('\n'))
    stdout.write( ("%.6f" % (sum(1/n for n in range(1 if n % 2 == 1 else 2, n+1, 2)))) + '\n')