from sys import stdin, stdout

n = stdin.readline().rstrip("\n")
a = "CHAN" if int(n) & 1 == 0 else "LE"
stdout.write(a + '\n')