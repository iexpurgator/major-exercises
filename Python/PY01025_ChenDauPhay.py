from sys import stdin, stdout

s = stdin.readline().rstrip('\n')[::-1]
sp = [s[index : index + 3] for index in range(0, len(s), 3)]
stdout.write(','.join(sp)[::-1])