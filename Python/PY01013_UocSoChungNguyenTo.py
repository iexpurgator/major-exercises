from sys import stdin, stdout

def isPrime(n: int) -> bool:
    if n == 2: return True
    elif n < 2 or n & 1 == 0: return False
    i = 3
    while i * i <= n:
        if n % i == 0: return False
        i += 2
    return True

def gcd(a:int, b:int) -> int:
    while b:
        a, b = b, a % b
    return a

t = int(stdin.readline().rstrip('\n'))
for _ in range(t):
    input = stdin.readline().split()
    g = gcd(int(input[0]), int(input[1]))
    stdout.write('YES\n' if isPrime(sum(int(d) for d in str(g))) else 'NO\n')