from sys import stdin, stdout

s1 = stdin.readline().rstrip('\n')
s2 = stdin.readline().rstrip('\n')
p = int(stdin.readline().rstrip('\n')) - 1

stdout.write(s1[:p] + s2 + s1[p:])
