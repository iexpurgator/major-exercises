from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip("\n")
readint = lambda: int(stdin.readline().rstrip("\n"))
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + "\n")
writelist = lambda l: stdout.write(" ".join(map(str, l)) + "\n")
writeyn = lambda b: stdout.write("YES\n" if b else "NO\n")


def isPrime(n: int) -> bool:
    if n == 2:
        return True
    elif n < 2 or n & 1 == 0:
        return False
    i = 3
    while i * i <= n:
        if n % i == 0:
            return False
        i += 2
    return True


def main():
    t = readint()
    for _ in range(t):
        s = readstr()
        l = len(s)
        s = s.replace("2", "").replace("3", "").replace("5", "").replace("7", "")
        n = len(s)
        writeyn(isPrime(l) and l - n > n)
    return


if __name__ == "__main__":
    main()