from functools import partial

ERROR_MSG = 'ERROR'


# Tạo lớp CalcCtrl để điều khiển các chức năng
class CalcCtrl:

    def __init__(self, model, view):
        """ Khởi tạo trình điều khiển """
        self._evaluate = model
        self._view = view
        # Connect signals and slots
        self._connectSignals()

    def _calculateResult(self):
        """ Hiển thị kết quả và lưu log"""
        expression = self._view.displayText()
        result = self._evaluate(expression)
        self._view.setDisplayText(result)
        # Lưu file lịch sử tính toán
        log = open('history.log', 'a')
        log.write(f'{expression} = {result}\n')
        log.close()

    def _buildExpression(self, sub_exp):
        """ Xây dựng biểu thức """
        if self._view.displayText() == ERROR_MSG:
            self._view.clearDisplay()

        expression = self._view.displayText() + sub_exp
        self._view.setDisplayText(expression)

    def _connectSignals(self):
        """ Nhận tín hiệu và thực hiện chức năng từ các phím bấm """
        for btnText, btn in self._view.buttons.items():
            if btnText not in {'=', 'C'}:
                btn.clicked.connect(partial(self._buildExpression, btnText))
        # Thực hiện các tình năng khi bấm các phím
        self._view.buttons['='].clicked.connect(self._calculateResult)
        self._view.display.returnPressed.connect(self._calculateResult)
        self._view.buttons['C'].clicked.connect(self._view.clearDisplay)


def evaluateExpression(expression):
    """ Kiểm tra biểu thức """
    try:
        result = str(eval(expression, {}, {}))
    except Exception:
        result = ERROR_MSG
    return result