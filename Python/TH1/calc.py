import sys

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget
from PyQt5.QtWidgets import QGridLayout, QLineEdit, QPushButton, QVBoxLayout

from calc_ctrl import CalcCtrl, evaluateExpression


# Tạo GUI với lớp CalcUI được kế thừa từ QMainWindow
class CalcUI(QMainWindow):

    def __init__(self):
        """ Khởi tạo class """
        super().__init__()
        self.setWindowTitle("Simple Calculator")
        # Thiết lập thông số cửa sổ mặc định
        self.setFixedSize(265, 235)
        self.generalLayout = QVBoxLayout()
        # Tạo một QWidget chính cho cửa sổ
        self._centralWidget = QWidget(self)
        self.setCentralWidget(self._centralWidget)
        self._centralWidget.setLayout(self.generalLayout)
        # Tạo màn hình hiển thị các phép tính
        self._initDisplay()
        # Tạo các phím bấm
        self._initButtons()

    def _initDisplay(self):
        """ Khởi tạo màn hình hiển thị """
        # Tạo tiện ích hiển thị sử dụng QLineEdit
        self.display = QLineEdit()
        # Đặt cố định chiều cao
        self.display.setFixedHeight(35)
        # Căn trái văn bản hiển thị
        self.display.setAlignment(Qt.AlignRight)
        # Chỉ cho phép màn hình đọc
        self.display.setReadOnly(True)
        # Thêm Màn hình vào generalLayout
        self.generalLayout.addWidget(self.display)

    def _initButtons(self):
        """ Khởi tạo các nút bấm """
        self.buttons = {}
        buttonsLayout = QGridLayout()
        # Tạo kí tự các nút bấm cho QGridLayout
        buttons = {
            '7': (0, 0), '8':   (0, 1), '9': (0, 2), '/': (0, 3), 'C': (0, 4),
            '4': (1, 0), '5':   (1, 1), '6': (1, 2), '*': (1, 3), '(': (1, 4),
            '1': (2, 0), '2':   (2, 1), '3': (2, 2), '-': (2, 3), ')': (2, 4),
            '0': (3, 0), '000': (3, 1), '.': (3, 2), '+': (3, 3), '=': (3, 4),
        }
        # Tạo kí tự các nút bấm và thêm vào QGridLayout
        for btnText, pos in buttons.items():
            self.buttons[btnText] = QPushButton(btnText)
            self.buttons[btnText].setFixedSize(40, 40)
            buttonsLayout.addWidget(self.buttons[btnText], pos[0], pos[1])
        # Thêm nút bấm vào generalLayout
        self.generalLayout.addLayout(buttonsLayout)

    def setDisplayText(self, text):
        """ Đặt text lên màn hình """
        self.display.setText(text)
        self.display.setFocus()

    def displayText(self):
        """ Lấy text trên màn hình """
        return self.display.text()

    def clearDisplay(self):
        """ Xóa text trên màn hình """
        self.setDisplayText('')


def main():
    # Tạo một QApllication
    pycalc = QApplication(sys.argv)
    view = CalcUI()
    # Hiển thị GUI
    view.show()
    CalcCtrl(model=evaluateExpression, view=view)
    # Vòng lặp sự kiện
    sys.exit(pycalc.exec_())


if __name__ == '__main__':
    main()