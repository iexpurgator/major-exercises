from sys import stdin, stdout

t = int(stdin.readline().rstrip('\n'))
for _ in range(t):
    s = stdin.readline().rstrip('\n')
    res = 'YES\n'
    for i in range(1, len(s)):
        if s[i-1] > s[i]:
            res = 'NO\n'
            break
    stdout.write(res)
