from sys import stdin, stdout

readstr = lambda: stdin.readline().rstrip("\n")
readint = lambda: int(stdin.readline().rstrip("\n"))
readarr = lambda t: list(map(t, stdin.readline().split()))
splitstr = lambda s, t: list(map(t, list(s)))
write = lambda o: stdout.write(str(o) + "\n")
writelist = lambda l: stdout.write(" ".join(map(str, l)) + "\n")
writeyn = lambda b: stdout.write("YES\n" if b else "NO\n")


def main():
    t = readint()
    for _ in range(t):
        s = readstr()
        if len(s) % 2 == 1:
            s = s[1:]
        r = s[:2]
        s = s.replace(r, '')
        writeyn(s == '')
    return


if __name__ == "__main__":
    main()