#include <winsock2.h>
#include <winsock.h>
#include <iostream>
#include <algorithm>
#pragma comment(lib, "WS2_32.lib")

using namespace std;
int main(int argument, char const * argv[]) {
    cout << "\n----TCP Server----\n" << endl;
    WSADATA Winsockdata;
    int iWsaStartup = WSAStartup(MAKEWORD(2,2),&Winsockdata);
    if(iWsaStartup != 0){
        cout << "WSA startup failed" <<endl;
    }
    SOCKET TCPServerSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(TCPServerSocket == INVALID_SOCKET){
        cout << "TCP creat failed" << WSAGetLastError() << endl;
    }
//----------------------------------------------------------
    int port = 7749;
    char* ip_addr = "192.168.56.2";
//----------------------------------------------------------
    struct sockaddr_in TCPServerAdd;
    TCPServerAdd.sin_family = AF_INET;
    TCPServerAdd.sin_addr.s_addr = inet_addr(ip_addr);
    TCPServerAdd.sin_port = htons(port);
    int iBind = bind(TCPServerSocket, (SOCKADDR *) &TCPServerAdd, sizeof(TCPServerAdd));
    if (iBind == SOCKET_ERROR){
        cout << "Bind fail & ERROR no -> " << WSAGetLastError() << endl;
    }
    int iListen = listen(TCPServerSocket, 2);
    if (iListen == SOCKET_ERROR) {
        cout << "Listen fail & ERROR no -> " << WSAGetLastError() << endl;
    }
    cout << "Client connecting..." << endl;
    struct sockaddr_in TCPClientAdd;
    int iTCPClientAdd = sizeof(TCPClientAdd);
    SOCKET sAcceptSocket = accept(TCPServerSocket, (SOCKADDR *)&TCPClientAdd, &iTCPClientAdd);
    if (sAcceptSocket == INVALID_SOCKET) {
        cout << "Accept fail & ERROR no -> " << WSAGetLastError() << endl;
    }
    cout << "Client Accepted" << endl;
//----------------------------------------------------------
    char* RecvBuffer = new char[2048];
    int iRecvBuffer = 2049;
    int iRecv = recv(sAcceptSocket, RecvBuffer, iRecvBuffer, 0);
    if(iRecv == SOCKET_ERROR){
        cout << "Receive fail & ERROR no -> " << WSAGetLastError() << endl;
    }
    cout << "[Client] >> " << RecvBuffer << endl;
//----------------------------------------------------------
    char* SenderBuffer = new char[2049];
    fill(SenderBuffer, SenderBuffer+2049, 0);
    int iSenderBuffer = strlen(RecvBuffer);
    transform(RecvBuffer, RecvBuffer+iSenderBuffer, SenderBuffer, ::toupper);
    cout << "[Server] << " << SenderBuffer << endl;
    int iSend = send(sAcceptSocket, SenderBuffer, iSenderBuffer, 0);
    if(iSend == SOCKET_ERROR){
        cout << "Send fail & ERROR no -> " << WSAGetLastError() << endl;
    }
//----------------------------------------------------------
    int iCloseSocket = closesocket(TCPServerSocket);
    if(iCloseSocket == SOCKET_ERROR){
        cout << "Close fail & ERROR no -> " << WSAGetLastError() << endl;
    }
    int iWsaCleanup = WSACleanup();
    if(iWsaCleanup == SOCKET_ERROR){
        cout << "Cleanup fail & ERROR no -> " << WSAGetLastError() << endl;
    }
    system("PAUSE");
    return 0;
}
