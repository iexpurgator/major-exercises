#include <winsock2.h>
#include <winsock.h>
#include <iostream>
#pragma comment(lib, "WS2_32.lib")

using namespace std;
int main(int argument, char const * argv[]) {
    cout << "\n----TCP Client----\n" << endl;
    WSADATA Winsockdata;
    int iWsaStartup = WSAStartup(MAKEWORD(2,2),&Winsockdata);
    if(iWsaStartup != 0){
        cout << "WSA startup failed" <<endl;
    }
    SOCKET TCPClientSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(TCPClientSocket == INVALID_SOCKET){
        cout << "TCP creat failed" << WSAGetLastError() << endl;
    }
//----------------------------------------------------------
    int port = 7749;
    char* ip_addr = "192.168.56.2";
//----------------------------------------------------------
    struct sockaddr_in TCPServerAdd;
    TCPServerAdd.sin_family = AF_INET;
    TCPServerAdd.sin_addr.s_addr = inet_addr(ip_addr);
    TCPServerAdd.sin_port = htons(port);
    int iConnect = connect(TCPClientSocket, (SOCKADDR *) &TCPServerAdd, sizeof(TCPServerAdd));
    if (iConnect == SOCKET_ERROR){
        cout << "Connect fail & ERROR no -> " << WSAGetLastError() << endl;
    }
    cout << "Connected" << endl;
//----------------------------------------------------------
    printf("[Client] << ");
    char* SenderBuffer = new char[2048];
    scanf("%[^\n]s",SenderBuffer);
    int iSenderBuffer = strlen(SenderBuffer)+1;
    int iSend = send(TCPClientSocket, SenderBuffer, iSenderBuffer, 0);
    if(iSend == SOCKET_ERROR){
        cout << "Send fail & ERROR no -> " << WSAGetLastError() << endl;
    }
//----------------------------------------------------------
    char* RecvBuffer = new char[2048];
    int iRecvBuffer = 2049;
    int iRecv = recv(TCPClientSocket, RecvBuffer, iRecvBuffer, 0);
    if(iRecv == SOCKET_ERROR){
        cout << "Receive fail & ERROR no -> " << WSAGetLastError() << endl;
    }
    cout << "[Server] >> " << RecvBuffer << endl;
//----------------------------------------------------------
    int iCloseSocket = closesocket(TCPClientSocket);
    if(iCloseSocket == SOCKET_ERROR){
        cout << "Close fail & ERROR no -> " << WSAGetLastError() << endl;
    }
    int iWsaCleanup = WSACleanup();
    if(iWsaCleanup == SOCKET_ERROR){
        cout << "Cleanup fail & ERROR no -> " << WSAGetLastError() << endl;
    }
    system("PAUSE");
    return 0;
}
