package com.ptudtbdd.chuong_04_12;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SimpleCaculator extends AppCompatActivity {
    EditText tn1, tn2;
    TextView tv1, tv2;
    Button btn;
    Spinner spTinh;
    ArrayAdapter<String> adapterTinh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.simple_caculator);
        initGUI();

        spTinh.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String txtn1 = tn1.getText().toString();
                String txtn2 = tn2.getText().toString();

                double n1, n2;
                try {
                    n1 = Double.parseDouble(txtn1);
                    n2 = Double.parseDouble(txtn2);
                    String o = adapterTinh.getItem(position);
                    tv2.setText(tinh(n1, n2, o));
                } catch (NumberFormatException e) {
//                    Toast.makeText(getApplicationContext(), "de nghi nhap so", Toast.LENGTH_LONG).show(); // sometime error
                    Toast.makeText(SimpleCaculator.this, "de nghi nhap so", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public Double tong(Double d1, Double d2) {
        return d1+d2;
    }

    private void initGUI() {
        tn1 = findViewById(R.id.inpTn1);
        tn2 = findViewById(R.id.inpTn2);
        tv1 = findViewById(R.id.titleView);
        tv2 = findViewById(R.id.txtKQ);
        btn = findViewById(R.id.submit_btn);
        spTinh = findViewById(R.id.spinnerTinh);
        String[] listTinh = getResources().getStringArray(R.array.listPhepTinh);
        adapterTinh = new ArrayAdapter<>(this, R.layout.item, listTinh);
        spTinh.setAdapter(adapterTinh);
    }

    public void tinhToan(View v) {
        String txtn1 = tn1.getText().toString();
        String txtn2 = tn2.getText().toString();

        double n1, n2;
        try {
            n1 = Double.parseDouble(txtn1);
            n2 = Double.parseDouble(txtn2);
            tv2.setText(String.format(java.util.Locale.US, "Ket qua: %f", tong(n1, n2)));
        } catch (NumberFormatException e) {
            Toast.makeText(this, "de nghi nhap so", Toast.LENGTH_LONG).show();
        }
    }

    private String tinh(double x, double y, String o) {
        String t = "";
        switch (o) {
            case "+":
                t = "Tong: " + (x + y);
                break;
            case "-":
                t = "Hieu: " + (x - y);
                break;
            case "*":
                t = "Tich: " + (x * y);
                break;
            case "/":
                if (y == 0) {
                    t = "Khong chia cho 0";
                }
                else {
                    t = "Thuong: " + (x / y);
                }
                break;
        }
        return t;
    }
}