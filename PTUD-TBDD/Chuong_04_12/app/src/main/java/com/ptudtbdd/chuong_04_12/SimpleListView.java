package com.ptudtbdd.chuong_04_12;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class SimpleListView extends AppCompatActivity {
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_list_view);

        String[] listTech = getResources().getStringArray(R.array.technology);
        ArrayAdapter<String> adapterTech = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, listTech);
        lv = findViewById(R.id.listView);
        lv.setAdapter(adapterTech);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String o = adapterTech.getItem(position);
                Toast.makeText((SimpleListView.this), ("Tech '" + o + "' has selected!"), Toast.LENGTH_LONG).show();
            }
        });
    }
}