package com.ptudtbdd.chuong_04_12;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class DateTimePicker extends AppCompatActivity {
    EditText txtDate, txtTime;
    Button btDate, btTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_time_picker);
        initUI();
        btDate.setOnClickListener(v -> {
            Calendar c = Calendar.getInstance();
            int cy = c.get(Calendar.YEAR);
            int cm = c.get(Calendar.MONTH);
            int cd = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog = new DatePickerDialog(DateTimePicker.this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    txtDate.setText((dayOfMonth + "/" + (month + 1) + "/" + year));
                }
            }, cy, cm, cd);
            dialog.show();
        });

        btTime.setOnClickListener(v -> {
            TimePickerDialog dialog = new TimePickerDialog(DateTimePicker.this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    txtTime.setText((hourOfDay + ":" + minute));
                }
            }, 6, 53, false);
            dialog.show();
        });

    }

    private void initUI() {
        txtDate = findViewById(R.id.txtDate);
        txtTime = findViewById(R.id.txtTime);
        btDate = findViewById(R.id.btDate);
        btTime = findViewById(R.id.btTime);
    }
}