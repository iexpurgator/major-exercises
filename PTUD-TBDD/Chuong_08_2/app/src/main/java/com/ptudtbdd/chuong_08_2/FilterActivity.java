package com.ptudtbdd.chuong_08_2;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.widget.TextView;

public class FilterActivity extends AppCompatActivity {
    TextView txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        txt = findViewById(R.id.txt);
        Uri url = getIntent().getData();
        String st = "Scheme:" + url.getScheme() + "\n host:" + url.getHost();
        int k = 1;
        for (String i : url.getPathSegments()) {
            st += "\n" + k + ": " + i;
            k++;
        }
        txt.setText(st);
    }
}