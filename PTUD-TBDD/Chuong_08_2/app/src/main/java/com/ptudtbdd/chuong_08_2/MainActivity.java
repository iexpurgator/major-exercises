package com.ptudtbdd.chuong_08_2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    ImageView bt1, bt2, bt3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bt1 = findViewById(R.id.btBrowser);
        bt2 = findViewById(R.id.btSms);
        bt3 = findViewById(R.id.btPhone);

        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent t = new Intent(Intent.ACTION_VIEW);
                t.setData(Uri.parse("https://plugins.jetbrains.com/plugin/17718-github-copilot/versions/stable"));
                startActivity(t);
            }
        });
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent t = new Intent(Intent.ACTION_VIEW);
                t.setData(Uri.parse("sms:" + "0912004866"));
                t.putExtra("sms_body", "SMS");
                startActivity(t);
            }
        });
        bt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent t = new Intent(Intent.ACTION_DIAL);
                t.setData(Uri.parse("tel:0912004866"));
                startActivity(t);
            }
        });

    }
}