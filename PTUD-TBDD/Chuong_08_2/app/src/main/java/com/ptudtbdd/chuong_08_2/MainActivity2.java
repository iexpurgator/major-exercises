package com.ptudtbdd.chuong_08_2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity2 extends AppCompatActivity {
    Intent t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        t = new Intent(this, MyService.class);
    }

    public void start(View view){
        t.putExtra("r", 12.0);
        startService(t);
    }

    public void stop(View view){
        stopService(t);
    }
}