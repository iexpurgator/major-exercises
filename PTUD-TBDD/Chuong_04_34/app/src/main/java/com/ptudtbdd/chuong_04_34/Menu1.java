package com.ptudtbdd.chuong_04_34;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class Menu1 extends AppCompatActivity {
    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu1);
        tv = findViewById(R.id.item_text);
        registerForContextMenu(tv);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case (R.id.mFile):
                Toast.makeText(Menu1.this, ("Selected File"), Toast.LENGTH_SHORT).show();
                break;
            case (R.id.mExit):
                System.exit((0));
                break;
            case (R.id.mEmail):
                Toast.makeText(Menu1.this, ("Selected Email"), Toast.LENGTH_SHORT).show();
                break;
            case (R.id.mPhone):
                Toast.makeText(Menu1.this, ("Selected Phone"), Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.context_menu, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case (R.id.mRed):
                tv.setTextColor(getResources().getColor(R.color.red));
                break;
            case (R.id.mGreen):
                tv.setTextColor(getResources().getColor(R.color.green));
                break;
            case (R.id.mBlue):
                tv.setTextColor(getResources().getColor(R.color.blue));
                break;

        }
        return super.onContextItemSelected(item);
    }
}