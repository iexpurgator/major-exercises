package com.ptudtbdd.chuong_04_34.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ptudtbdd.chuong_04_34.R;
import com.ptudtbdd.chuong_04_34.model.OperatingSystem;

import java.util.List;

public class AdapterOS extends RecyclerView.Adapter<AdapterOS.ViewHolderOS> {
    private List<OperatingSystem> mList;
    private OSItemListener listener;
//    private Context context; // need able for simple click event

//    public AdapterOS(Context context, List<OperatingSystem> mList) {
//        this.mList = mList;
//        this.context = context;
//    }

    public AdapterOS(List<OperatingSystem> mList, OSItemListener listener) {
        this.mList = mList;
        this.listener = listener;
    }

    public AdapterOS(List<OperatingSystem> mList) {
        this.mList = mList;
    }

    public void setListener(OSItemListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolderOS onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_kt1, parent, (false));
        return new ViewHolderOS(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderOS holder, int position) {
        OperatingSystem os = mList.get(position);
        if (os == null) {
            return;
        }
        holder.img.setImageResource(os.getImg());
        holder.tv.setText(os.getName());
        // fo simple ClickEvent
//        holder.cview.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(context.getApplicationContext(), (os.getName() + " " + os.getVersion()), Toast.LENGTH_SHORT).show();
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public OperatingSystem getItemAt(int position){
        return mList.get(position);
    }

    public void addOS(OperatingSystem os){
        mList.add(os);
        notifyDataSetChanged();
    }

    class ViewHolderOS extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView img;
        TextView tv;
//        CardView cview;

        public ViewHolderOS(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.item_image);
            tv = itemView.findViewById(R.id.item_text);
            itemView.setOnClickListener(this);
//            cview = itemView.findViewById(R.id.cardView); // for simple ClickEvent
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                listener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    class ViewHolderOS_0 extends RecyclerView.ViewHolder {
        ImageView img;
        TextView tv;
//        CardView cview;

        public ViewHolderOS_0(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.item_image);
            tv = itemView.findViewById(R.id.item_text);
//            cview = itemView.findViewById(R.id.cardView); // for simple ClickEvent
        }
    }

    public interface OSItemListener {
        public void onItemClick(View view, int position);
    }
}
