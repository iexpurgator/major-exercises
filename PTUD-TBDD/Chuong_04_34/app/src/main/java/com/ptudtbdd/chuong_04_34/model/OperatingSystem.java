package com.ptudtbdd.chuong_04_34.model;

public class OperatingSystem {
    private int img;
    private String name;
    private String version;

    public OperatingSystem() {
    }

    public OperatingSystem(int img, String name, String version) {
        this.img = img;
        this.name = name;
        this.version = version;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
