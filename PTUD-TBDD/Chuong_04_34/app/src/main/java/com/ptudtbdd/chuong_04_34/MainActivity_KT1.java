package com.ptudtbdd.chuong_04_34;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ptudtbdd.chuong_04_34.adapter.AdapterOS;
import com.ptudtbdd.chuong_04_34.model.OperatingSystem;

import java.util.ArrayList;
import java.util.List;

//public class MainActivity_KT1 extends AppCompatActivity {
public class MainActivity_KT1 extends AppCompatActivity implements AdapterOS.OSItemListener {
    private RecyclerView recyclerView;
    private AdapterOS adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_kt1);
        recyclerView = findViewById(R.id.recyclerView);
        adapter = new AdapterOS(getList());
        adapter.setListener((MainActivity_KT1.this));
//        adapter = new AdapterOS((MainActivity_KT1.this), getList()); // simple click event
//        LinearLayoutManager linearManager = new LinearLayoutManager((MainActivity_KT1.this), RecyclerView.VERTICAL, false);
        GridLayoutManager manager = new GridLayoutManager((MainActivity_KT1.this), (3));
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
    }

    private List<OperatingSystem> getList() {
        List<OperatingSystem> list = new ArrayList<>();
        list.add(new OperatingSystem(R.drawable.pop, ("Pop OS!"), ("20")));
        list.add(new OperatingSystem(R.drawable.windows, ("Windows"), ("11")));
        list.add(new OperatingSystem(R.drawable.apple, ("iOS"), ("16.3.1")));
        list.add(new OperatingSystem(R.drawable.android, ("Android"), ("13")));
        return list;
    }

    @Override
    public void onItemClick(View view, int position) {
//        OperatingSystem os = getList().get(position);
        OperatingSystem os = adapter.getItemAt(position);
        Toast.makeText((MainActivity_KT1.this), (os.getName() + " " + os.getVersion()), Toast.LENGTH_SHORT).show();
    }
}