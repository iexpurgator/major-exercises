package com.ptudtbdd.chuong_06_1.model;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.ptudtbdd.chuong_06_1.R;

public class Fragment_a extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_a, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Button bt = view.findViewById(R.id.btSubmit_a);
        EditText tName = view.findViewById(R.id.editTxt_a);
        TextView textView = view.findViewById(R.id.txtView_a);

        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = tName.getText().toString();
                textView.setText(("Hello " + name + "!"));
//                tName.setBackgroundColor(Color.WHITE);
            }
        });
        tName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                tName.setBackgroundColor(Color.TRANSPARENT);
                textView.setText("");
            }
        });
    }
}
