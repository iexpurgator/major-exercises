package com.ptudtbdd.chuong_06_1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.ptudtbdd.chuong_06_1.model.Fragment_b;
import com.ptudtbdd.chuong_06_1.model.Fragment_c;

public class MainActivity extends AppCompatActivity {
    FragmentManager manager;
    Button btB, btC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btB = findViewById(R.id.show_frag_b);
        btC = findViewById(R.id.show_frag_c);
        manager = getSupportFragmentManager();

        btB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment_b fb = new Fragment_b();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.add(R.id.frame_dynamic_frag, fb);
                transaction.addToBackStack(null);
                transaction.commit();

            }
        });
        btC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment_c fc = new Fragment_c();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.add(R.id.frame_dynamic_frag, fc);
                transaction.addToBackStack(null);
                transaction.commit();

            }
        });
    }

    @Override
    public void onBackPressed() {
        if (manager.getBackStackEntryCount() > 0) {
            manager.popBackStack();
        } else {
            super.onBackPressed();
        }
    }
}