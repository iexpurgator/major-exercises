package com.ptudtbdd.chuong_06_1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.View;

import com.ptudtbdd.chuong_06_1.model.Fragment_a;
import com.ptudtbdd.chuong_06_1.model.Fragment_b;
import com.ptudtbdd.chuong_06_1.model.Fragment_c;

public class FrgmntBackStack extends AppCompatActivity {
    private FragmentManager manager;
    private FragmentTransaction transaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frgmnt_back_stack);
        manager = getSupportFragmentManager();

    }

    private void add(Fragment fragment, String tag, String name) {
        transaction = manager.beginTransaction();
        transaction.add(R.id.frame_dynamic_frag2, fragment, tag);
        transaction.addToBackStack(name);
        transaction.commit();
    }

    private void remove(Fragment fragment, String tag) {
        transaction = manager.beginTransaction();
        fragment = manager.findFragmentByTag(tag);
        if (fragment != null)
            transaction.remove(fragment);
        transaction.commit();
    }

    public void addA(View v) {
        Fragment_a fragment = new Fragment_a();
        add(fragment, "fraga", "fa");
    }

    public void addB(View v) {
        Fragment_b fragment = new Fragment_b();
        add(fragment, "fragb", "fb");
    }

    public void addC(View v) {
        Fragment_c fragment = new Fragment_c();
        add(fragment, "fragc", "fc");
    }

    public void delA(View v) {
        Fragment_a fragment = new Fragment_a();
        remove(fragment, "fraga");
    }

    public void delB(View v) {
        Fragment_b fragment = new Fragment_b();
        remove(fragment, "fragb");
    }

    public void delC(View v) {
        Fragment_c fragment = new Fragment_c();
        remove(fragment, "fragc");
    }

    public void back(View v) {
        if (manager.getBackStackEntryCount() > 0)
            manager.popBackStack();
    }

    @Override
    public void onBackPressed() {
        if (manager.getBackStackEntryCount() > 0)
            manager.popBackStack();
        else
            super.onBackPressed();
    }

    public void popA(View v) {
        manager.popBackStack("fa", 0);
    }
}