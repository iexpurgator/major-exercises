package com.ptudtbdd.chuong_09_1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.ptudtbdd.chuong_09_1.dal.DatabaseLite;
import com.ptudtbdd.chuong_09_1.model.Item;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class AddActivity extends AppCompatActivity implements View.OnClickListener {
    public Spinner sp;
    private EditText edtTitle, edtPrice, edtDate;
    private Button btnUpdate, btnCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        initView();
        btnCancel.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);
        edtDate.setOnClickListener(this);
    }

    // init view
    private void initView() {
        sp = findViewById(R.id.spCategory);
        edtTitle = findViewById(R.id.edtTitle);
        edtPrice = findViewById(R.id.edtPrice);
        edtDate = findViewById(R.id.edtDate);
        btnUpdate = findViewById(R.id.btnUpdate);
        btnCancel = findViewById(R.id.btnCancel);
        sp.setAdapter(new ArrayAdapter<String>(this, R.layout.item_spinner,
                getResources().getStringArray(R.array.category)));
    }

    @Override
    public void onClick(View v) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

        switch (v.getId()) {
            case (R.id.edtDate):
                final Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        calendar.set(year, month, dayOfMonth);
                        edtDate.setText(formatter.format(calendar.getTime()));
                    }
                }, year, month, day);
                dialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                dialog.show();
                break;
            case (R.id.btnUpdate):
                String title = edtTitle.getText().toString();
                String price = edtPrice.getText().toString();
                String date = edtDate.getText().toString();
                String category = sp.getSelectedItem().toString();
                if (title.isEmpty() || price.isEmpty() || date.isEmpty()) {
                    Toast.makeText(this, "Vui lòng nhập đầy đủ thông tin", Toast.LENGTH_SHORT).show();
                    break;
                }
                if (!price.matches("\\d+")) {
                    Toast.makeText(this, "Price: Vui lòng nhập số", Toast.LENGTH_SHORT).show();
                    break;
                }
                Item item = new Item(title, category, price, date);
                DatabaseLite db = new DatabaseLite(this);
                db.addItem(item);
                finish();
                break;
            case (R.id.btnCancel):
                finish();
                break;
        }
    }

}