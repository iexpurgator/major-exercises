package com.ptudtbdd.chuong_09_1.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ptudtbdd.chuong_09_1.R;
import com.ptudtbdd.chuong_09_1.UpdateDeleteActivity;
import com.ptudtbdd.chuong_09_1.adapter.RecyclerViewAdapter;
import com.ptudtbdd.chuong_09_1.dal.DatabaseLite;
import com.ptudtbdd.chuong_09_1.model.Item;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class FragmentHome extends Fragment implements RecyclerViewAdapter.ItemListener {
    private RecyclerView recyclerView;
    private RecyclerViewAdapter adapter;
    private DatabaseLite db;
    private TextView tvTong;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    // create recycle view
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recycleView);
        tvTong = view.findViewById(R.id.tvTongHome);
        db = new DatabaseLite(getContext());
//        db.cleanDatabase();
//        db.addItem(new Item("Mua quan", "Mua sam", "400", "07/04/2023"));
//        db.addItem(new Item("Mua ao", "Mua sam", "200", "13/04/2023"));
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        adapter = new RecyclerViewAdapter(db.getItemByDate(formatter.format(date)));
        adapter.setItemListener(this);
        LinearLayoutManager manager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        tvTong.setText("Tong tien: " + getTong());
    }

    // tinh tong
    public int getTong() {
        int tong = 0;
        for (Item item : adapter.getList()) {
            tong += Integer.parseInt(item.getPrice());
        }
        return tong;
    }

    // call layout update delete
    @Override
    public void onItemClick(View view, int position) {
        Item item = adapter.getItem(position);
        Intent intent = new Intent(getContext(), UpdateDeleteActivity.class);
        intent.putExtra("item", item);
        startActivity(intent);
    }

    // refresh data
    @Override
    public void onResume() {
        super.onResume();
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        List<Item> list = db.getItemByDate(formatter.format(date));
        adapter.setList(list);
        tvTong.setText("Tong tien: " + getTong());
    }
}
