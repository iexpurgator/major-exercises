package com.ptudtbdd.chuong_09_1.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ptudtbdd.chuong_09_1.R;
import com.ptudtbdd.chuong_09_1.UpdateDeleteActivity;
import com.ptudtbdd.chuong_09_1.adapter.RecyclerViewAdapter;
import com.ptudtbdd.chuong_09_1.dal.DatabaseLite;
import com.ptudtbdd.chuong_09_1.model.Item;

import java.util.List;

public class FragmentHistory extends Fragment implements RecyclerViewAdapter.ItemListener {
    private RecyclerViewAdapter adapter;
    private RecyclerView recyclerView;
    private DatabaseLite db;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_history, container, false);
    }

    // create recycle view
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recycleView);
        db = new DatabaseLite(getContext());
        List<Item> list = db.getAllItems();
        adapter = new RecyclerViewAdapter(list);
        adapter.setItemListener(this);
        LinearLayoutManager manager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
    }

    // go to update delete activity
    @Override
    public void onItemClick(View view, int position) {
        Item item = adapter.getItem(position);
        Intent intent = new Intent(getContext(), UpdateDeleteActivity.class);
        intent.putExtra("item", item);
        startActivity(intent);
    }

    // refresh data
    @Override
    public void onResume() {
        super.onResume();
        adapter.setList(db.getAllItems());
    }
}
