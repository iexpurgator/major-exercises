package com.ptudtbdd.chuong_09_1.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.widget.SearchView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ptudtbdd.chuong_09_1.R;
import com.ptudtbdd.chuong_09_1.adapter.RecyclerViewAdapter;
import com.ptudtbdd.chuong_09_1.dal.DatabaseLite;
import com.ptudtbdd.chuong_09_1.model.Item;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

public class FragmentSearch extends Fragment implements View.OnClickListener {
    private RecyclerViewAdapter adapter;
    private RecyclerView recyclerView;
    private TextView tvTongSearch;
    private SearchView searchView;
    private EditText eFrom, eTo;
    private Button btnSearch;
    private Spinner spCategorySearch;
    private DatabaseLite db;
    private String sCategory, sFrom, sTo, sTitle;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // init view
        recyclerView = view.findViewById(R.id.recycleViewSearch);
        tvTongSearch = view.findViewById(R.id.tvTongSearch);
        searchView = view.findViewById(R.id.searchBar);
        eFrom = view.findViewById(R.id.eFrom);
        eTo = view.findViewById(R.id.eTo);
        btnSearch = view.findViewById(R.id.btnSearch);
        spCategorySearch = view.findViewById(R.id.spCategorySearch);
        String arr[] = getResources().getStringArray(R.array.category);
        arr = Arrays.copyOf(arr, arr.length + 1);
        for (int i = arr.length - 1; i > 0; i--) {
            arr[i] = arr[i - 1];
        }
        arr[0] = "All";
        spCategorySearch.setAdapter(new ArrayAdapter<String>(getContext(), R.layout.item_spinner, arr));
        // set string
        sCategory = null;
        sFrom = null;
        sTo = null;
        sTitle = null;
        // set adapter
        db = new DatabaseLite(getContext());
        adapter = new RecyclerViewAdapter(db.getAllItems());
        LinearLayoutManager manager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        tvTongSearch.setText("Tong tien: " + getTong());
        // set event
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                sCategory = spCategorySearch.getSelectedItem().toString();
                sFrom = eFrom.getText().toString();
                sTo = eTo.getText().toString();
                sTitle = query;
                refresh();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                sCategory = spCategorySearch.getSelectedItem().toString();
                sFrom = eFrom.getText().toString();
                sTo = eTo.getText().toString();
                sTitle = newText;
                refresh();
                return false;
            }
        });
        spCategorySearch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sCategory = spCategorySearch.getItemAtPosition(position).toString();
                sFrom = eFrom.getText().toString();
                sTo = eTo.getText().toString();
                sTitle = searchView.getQuery().toString();
                refresh();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                sFrom = eFrom.getText().toString();
                sTo = eTo.getText().toString();
                sTitle = searchView.getQuery().toString();
                adapter = new RecyclerViewAdapter(db.searchItem(sTitle, null, sFrom, sTo));
                recyclerView.setAdapter(adapter);
                tvTongSearch.setText("Tong tien: " + getTong());
            }
        });
        // set click
        eFrom.setOnClickListener(this);
        eTo.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
    }

    // tinh tong
    public int getTong() {
        int tong = 0;
        for (Item item : adapter.getList()) {
            tong += Integer.parseInt(item.getPrice());
        }
        return tong;
    }

    @Override
    public void onClick(View v) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        int year, month, day;
        switch (v.getId()) {
            case (R.id.eFrom):
                final Calendar fromCalendar = Calendar.getInstance();
                year = fromCalendar.get(Calendar.YEAR);
                month = fromCalendar.get(Calendar.MONTH);
                day = fromCalendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        fromCalendar.set(year, month, dayOfMonth);
                        eFrom.setText(formatter.format(fromCalendar.getTime()));
                    }
                }, year, month, day);
                dialog.getDatePicker().setMaxDate(fromCalendar.getTimeInMillis());
                dialog.show();
                break;
            case (R.id.eTo):
                final Calendar toCalendar = Calendar.getInstance();
                year = toCalendar.get(Calendar.YEAR);
                month = toCalendar.get(Calendar.MONTH);
                day = toCalendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog1 = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        toCalendar.set(year, month, dayOfMonth);
                        eTo.setText(formatter.format(toCalendar.getTime()));
                    }
                }, year, month, day);
                dialog1.getDatePicker().setMaxDate(toCalendar.getTimeInMillis());
                dialog1.show();
                break;
            case (R.id.btnSearch):
                sCategory = spCategorySearch.getSelectedItem().toString();
                sTitle = searchView.getQuery().toString();
                sFrom = eFrom.getText().toString();
                sTo = eTo.getText().toString();
                refresh();
                break;
        }
    }

    // refresh recyclerview
    public void refresh() {
        if(sCategory.equals("All")){
            sCategory = null;
        }
        adapter = new RecyclerViewAdapter(db.searchItem(sTitle, sCategory, sFrom, sTo));
        recyclerView.setAdapter(adapter);
        tvTongSearch.setText("Tong tien: " + getTong());
    }

}
