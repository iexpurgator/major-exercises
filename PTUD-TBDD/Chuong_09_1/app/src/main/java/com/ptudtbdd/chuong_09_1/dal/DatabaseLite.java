package com.ptudtbdd.chuong_09_1.dal;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ptudtbdd.chuong_09_1.model.Item;

import java.util.ArrayList;
import java.util.List;

public class DatabaseLite extends SQLiteOpenHelper {
    private final static String DATABASE_NAME = "ChiTieu.db";
    private final static int DATABASE_VERSION = 1;

    public DatabaseLite(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE IF NOT EXISTS items (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "title TEXT, " +
                "category TEXT, " +
                "price TEXT, " +
                "date TEXT" +
                ");";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    // get all items order by date desc
    public List<Item> getAllItems() {
        List<Item> list = new ArrayList<>();
        Cursor cursor = getReadableDatabase().query("items", null, null,
                null, null, null, "date DESC");
        while (cursor != null && cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndexOrThrow("id"));
            String title = cursor.getString(cursor.getColumnIndexOrThrow("title"));
            String category = cursor.getString(cursor.getColumnIndexOrThrow("category"));
            String price = cursor.getString(cursor.getColumnIndexOrThrow("price"));
            String date = cursor.getString(cursor.getColumnIndexOrThrow("date"));
            Item item = new Item(id, title, category, price, date);
            list.add(item);
        }
        if (cursor != null) {
            cursor.close();
        }
        return list;
    }

    // add item
    public long addItem(Item item) {
        ContentValues values = new ContentValues();
        values.put("title", item.getTitle());
        values.put("category", item.getCategory());
        values.put("price", item.getPrice());
        values.put("date", item.getDate());
        return getWritableDatabase().insert("items", null, values);
    }

    // update item
    public long updateItem(Item item) {
        ContentValues values = new ContentValues();
        values.put("title", item.getTitle());
        values.put("category", item.getCategory());
        values.put("price", item.getPrice());
        values.put("date", item.getDate());
        return getWritableDatabase().update("items", values, "id = ?",
                new String[]{String.valueOf(item.getId())});
    }

    // delete item
    public long deleteItem(int id) {
        return getWritableDatabase().delete("items", "id = ?",
                new String[]{String.valueOf(id)});
    }

    // clean database
    public void cleanDatabase() {
        getWritableDatabase().execSQL("DELETE FROM items;");
        getWritableDatabase().execSQL("DROP TABLE IF EXISTS items");
        String query = "CREATE TABLE IF NOT EXISTS items (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "title TEXT, " +
                "category TEXT, " +
                "price TEXT, " +
                "date TEXT" +
                ");";
        getWritableDatabase().execSQL(query);
    }

    // get item by date
    public List<Item> getItemByDate(String date) {
        List<Item> list = new ArrayList<>();
        Cursor cursor = getReadableDatabase().query("items", null, "date = ?",
                new String[]{date}, null, null, "date DESC");
        while (cursor != null && cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndexOrThrow("id"));
            String title = cursor.getString(cursor.getColumnIndexOrThrow("title"));
            String category = cursor.getString(cursor.getColumnIndexOrThrow("category"));
            String price = cursor.getString(cursor.getColumnIndexOrThrow("price"));
            String date1 = cursor.getString(cursor.getColumnIndexOrThrow("date"));
            Item item = new Item(id, title, category, price, date1);
            list.add(item);
        }
        if (cursor != null) {
            cursor.close();
        }
        return list;
    }

    // search item by title like %title% and category and range date
    public List<Item> searchItem(String title, String category, String date1, String date2) {
        List<Item> list = new ArrayList<>();
        String selection = null;
        List<String> args = new ArrayList<>();
        if (title != null && !title.isEmpty()) {
            selection = "title LIKE ?";
            args.add("%" + title + "%");
        }
        if (category != null) {
            selection = (selection != null ? selection + " AND " : "") + "category = ?";
            args.add(category);
        }
        if (date1 != null && date2 != null && !date1.isEmpty() && !date2.isEmpty()) {
            selection = (selection != null ? selection + " AND " : "") + "date BETWEEN ? AND ?";
            args.add(date1);
            args.add(date2);
        }
        String[] selectionArgs = args.toArray(new String[args.size()]);

        Cursor cursor = getReadableDatabase().query("items", null, selection,
                selectionArgs, null, null, "date DESC");
        while (cursor != null && cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndexOrThrow("id"));
            String title1 = cursor.getString(cursor.getColumnIndexOrThrow("title"));
            String category1 = cursor.getString(cursor.getColumnIndexOrThrow("category"));
            String price = cursor.getString(cursor.getColumnIndexOrThrow("price"));
            String date = cursor.getString(cursor.getColumnIndexOrThrow("date"));
            Item item = new Item(id, title1, category1, price, date);
            list.add(item);
        }
        if (cursor != null) {
            cursor.close();
        }
        return list;
    }
}
