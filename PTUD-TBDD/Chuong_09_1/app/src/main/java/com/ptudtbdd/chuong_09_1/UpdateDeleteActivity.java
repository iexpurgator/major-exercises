package com.ptudtbdd.chuong_09_1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.ptudtbdd.chuong_09_1.dal.DatabaseLite;
import com.ptudtbdd.chuong_09_1.model.Item;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class UpdateDeleteActivity extends AppCompatActivity implements View.OnClickListener {
    public Spinner sp;
    private EditText edtTitle, edtPrice, edtDate;
    private Button btnUpdate, btnRemove, btnBack;
    private Item item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_delete);
        initView();
        btnUpdate.setOnClickListener(this);
        btnRemove.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        edtDate.setOnClickListener(this);
        Intent intent = getIntent();
        item = (Item) intent.getSerializableExtra("item");
        // set data
        edtTitle.setText(item.getTitle());
        edtPrice.setText(item.getPrice());
        edtDate.setText(item.getDate());
        for (int i = 0; i < sp.getCount(); i++) {
            if (sp.getItemAtPosition(i).toString().equalsIgnoreCase(item.getCategory())) {
                sp.setSelection(i);
                break;
            }
        }
    }

    // init view
    private void initView() {
        sp = findViewById(R.id.spCategoryUD);
        edtTitle = findViewById(R.id.edtTitleUD);
        edtPrice = findViewById(R.id.edtPriceUD);
        edtDate = findViewById(R.id.edtDateUD);
        btnUpdate = findViewById(R.id.btnUpdateUD);
        btnRemove = findViewById(R.id.btnRemoveUD);
        btnBack = findViewById(R.id.btnBackUD);
        sp.setAdapter(new ArrayAdapter<String>(this, R.layout.item_spinner,
                getResources().getStringArray(R.array.category)));
    }

    @Override
    public void onClick(View v) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        DatabaseLite db = new DatabaseLite(this);

        switch (v.getId()) {
            case (R.id.edtDate):
                final Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        calendar.set(year, month, dayOfMonth);
                        edtDate.setText(formatter.format(calendar.getTime()));
                    }
                }, year, month, day);
                dialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                dialog.show();
                break;
            case (R.id.btnUpdate):
                String title = edtTitle.getText().toString();
                String price = edtPrice.getText().toString();
                String date = edtDate.getText().toString();
                String category = sp.getSelectedItem().toString();
                if (title.isEmpty() || price.isEmpty() || date.isEmpty()) {
                    Toast.makeText(this, "Vui lòng nhập đầy đủ thông tin", Toast.LENGTH_SHORT).show();
                    break;
                }
                if (!price.matches("\\d+")) {
                    Toast.makeText(this, "Price: Vui lòng nhập số", Toast.LENGTH_SHORT).show();
                    break;
                }
                item.setTitle(title);
                item.setPrice(price);
                item.setDate(date);
                item.setCategory(category);
                db.updateItem(item);
                finish();
                break;
            case (R.id.btnRemoveUD):
                int id = item.getId();
                // confirm delete
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Xác nhận");
                builder.setMessage("Bạn có muốn xóa " + id + " không?");
                builder.setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        db.deleteItem(id);
                        finish();
                    }
                });
                builder.setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog aDialog = builder.create();
                aDialog.show();
                break;
            case (R.id.btnBackUD):
                finish();
                break;
        }
    }
}