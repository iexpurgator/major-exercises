package com.ptudtbdd.chuong_03;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class EventHandleActivity extends AppCompatActivity implements View.OnClickListener {
//    CheckBox chb01, chb02, chb03, chb11, chb12, chb13, chb14, chb15;
    RadioButton rab_male, rab_female;
    RatingBar rtb;
    Spinner spinner_country, spinner_university;
    TextView result;
    Button btn;

    ArrayAdapter<String> aaCountry, aaUniversity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_03);
        initUI();
        btn.setOnClickListener(this);
    }

    private void initUI() {
//        chb01 = findViewById(R.id.cb01);
//        chb02 = findViewById(R.id.cb02);
//        chb03 = findViewById(R.id.cb03);
//        chb11 = findViewById(R.id.cb11);
//        chb12 = findViewById(R.id.cb12);
//        chb13 = findViewById(R.id.cb13);
//        chb14 = findViewById(R.id.cb14);
//        chb15 = findViewById(R.id.cb15);
        rab_male = findViewById(R.id.male);
        rab_female = findViewById(R.id.female);
        rtb = findViewById(R.id.ratingbar);
        spinner_country = findViewById(R.id.spinner_country);
        spinner_university = findViewById(R.id.spinner_university);
        btn = findViewById(R.id.btnSubmit);
        result = findViewById(R.id.txtOutput);
        String[] listCountry = getResources().getStringArray(R.array.country);
        aaCountry = new ArrayAdapter<>(EventHandleActivity.this, R.layout.support_simple_spinner_dropdown_item, listCountry);
        spinner_country.setAdapter(aaCountry);
        String[] listUniversity = getResources().getStringArray(R.array.university);
        aaUniversity = new ArrayAdapter<>(EventHandleActivity.this, R.layout.support_simple_spinner_dropdown_item, listUniversity);
        spinner_university.setAdapter(aaUniversity);
    }

    @Override
    public void onClick(View v) {
        StringBuilder platform, favorite;
        String gender, rate, country, university;

        platform = new StringBuilder();
        int[] listPlatformId = {R.id.cb01, R.id.cb02, R.id.cb03};
        for (int platformId : listPlatformId) {
            CheckBox cbi = findViewById(platformId);
            if (!cbi.isChecked()) continue;
            if (platform.length() > 0) platform.append(", ");
            platform.append(cbi.getText());
        }
        platform = new StringBuilder("Platform: " + platform + "\n");
        //
        gender = "Gender: ";
        if (rab_female.isChecked()) {
            gender += rab_female.getText();
        }
        if (rab_male.isChecked()) {
            gender += rab_male.getText();
        }
        gender += "\n";
        //
        rate = "Rate: " + rtb.getRating() + "\n";
        country = "Country: " + spinner_country.getSelectedItem() + "\n";
        university = "University: " + spinner_university.getSelectedItem() + "\n";
        //
        favorite = new StringBuilder();
        int[] listFavoriteId = {R.id.cb11, R.id.cb12, R.id.cb13, R.id.cb14, R.id.cb15};
        for (int favoriteId : listFavoriteId) {
            CheckBox cbi = findViewById(favoriteId);
            if (!cbi.isChecked()) continue;
            if (favorite.length() > 0) favorite.append(", ");
            favorite.append(cbi.getText());
        }
        favorite = new StringBuilder("Favorite: " + favorite + "\n");
        result.setText((platform.toString() + gender + rate + country + university + favorite.toString()));
    }
}