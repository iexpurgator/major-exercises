package com.ptudtbdd.chuong_03;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    EditText username, password;
    Button bLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_05_1);
        initUI();
        bLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inUsername = username.getText().toString();
                String inPassword = password.getText().toString();
                String status = "";
                if (inUsername.equals("hoantq") && inPassword.equals("12345678")) {
                    status = String.format("Dang nhap thanh cong: %s", inUsername);
                } else {
                    status = "Sai thong tin dang nhap";
                }
                Toast.makeText(LoginActivity.this, status, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void initUI() {
        username = findViewById(R.id.inpUsername);
        password = findViewById(R.id.inpPassword);
        bLogin = findViewById(R.id.btnLogin);
    }
}