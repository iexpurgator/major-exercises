package com.ptudtbdd.chuong_09_1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.ptudtbdd.chuong_09_1.database.Database;
import com.ptudtbdd.chuong_09_1.database.Item;
import com.ptudtbdd.chuong_09_1.model.Category;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    Spinner spCate;
    TextView tvItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spCate = findViewById(R.id.spCate);
        tvItem = findViewById(R.id.tvItem);
        Database db = new Database(this);
//        db.insertCategory(new Category("Khoa hoc"));
//        db.insertCategory(new Category("Van hoc"));
//        db.insertCategory(new Category("Lich su"));
        List<Category> categoryList = db.getCategory();
        String[] st = new String[categoryList.size()];
        int k = 0;
        for (Category c : categoryList) {
            st[k++] = c.getId() + ". " + c.getName();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.item, st);
        spCate.setAdapter(adapter);
//        db.insertItem(new Item("Khoa hoc 1", 200, "2020-05-01", categoryList.get(0)));
//        db.insertItem(new Item("Khoa hoc 2", 300, "2020-02-01", categoryList.get(0)));
//        db.insertItem(new Item("Lich su 1", 400, "2020-01-03", categoryList.get(2)));
//        db.insertItem(new Item("Van hoc 1", 500, "2020-01-01", categoryList.get(1)));
        String st2 = "Book:";
        k = 0;
        for (Item i : db.getItem()) {
            st2 += "\n" + i.getName() + " - " + i.getCategory().getName();
        }
        tvItem.setText(st2);
    }
}