package com.ptudtbdd.chuong_09_1.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ptudtbdd.chuong_09_1.model.Category;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

public class Database extends SQLiteOpenHelper {
    private final static String DATABASE_NAME = "nhom_7.db";
    private final static int DATABASE_VERSION = 1;

    public Database(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE IF NOT EXISTS CATEGORY(" +
                "ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                "NAME TEXT NOT NULL" +
                ")";
        db.execSQL(sql);
        sql = "CREATE TABLE IF NOT EXISTS ITEMS(" +
                "ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                "NAME TEXT NOT NULL," +
                "PRICE REAL NOT NULL," +
                "DATE_UPDATE TEXT NOT NULL," +
                "CATEGORY_ID INTEGER NOT NULL," +
                "FOREIGN KEY(CATEGORY_ID) REFERENCES CATEGORY(ID)" +
                ")";
        db.execSQL(sql);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void insertCategory(Category category) {
        String sql = "INSERT INTO CATEGORY(NAME) VALUES(?)";
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(sql, new String[]{category.getName()});
    }

    public long insertItem(Item item) {
        ContentValues values = new ContentValues();
        values.put("NAME", item.getName());
        values.put("PRICE", item.getPrice());
        values.put("DATE_UPDATE", item.getDateUpdate());
        values.put("CATEGORY_ID", item.getCategory().getId());
        SQLiteDatabase db = getWritableDatabase();
        return db.insert("ITEMS", null, values);
    }

    public List<Category> getCategory() {
        List<Category> list = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query("CATEGORY", null, null, null, null, null, null);
        while (cursor != null && cursor.moveToNext()) {
            int id = cursor.getInt(0);
            String name = cursor.getString(1);
            list.add(new Category(id, name));
        }
        return list;
    }

    public List<Item> getItem() {
        List<Item> list = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT I.ID, I.NAME, I.PRICE, I.DATE_UPDATE, C.ID, C.NAME " +
                "FROM CATEGORY C INNER JOIN ITEMS I ON (C.ID=I.CATEGORY_ID)" +
                " ORDER BY I.DATE_UPDATE DESC;";
        Cursor cursor = db.rawQuery(sql, null);
        while (cursor != null && cursor.moveToNext()) {
            int id = cursor.getInt(0);
            String name = cursor.getString(1);
            double price = cursor.getDouble(2);
            String dateUpdate = cursor.getString(3);
            Category cate = new Category(cursor.getInt(4), cursor.getString(5));
            list.add(new Item(id, name, price, dateUpdate, cate));
        }
        return list;
    }
}
