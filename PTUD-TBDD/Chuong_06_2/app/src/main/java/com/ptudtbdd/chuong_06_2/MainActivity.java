package com.ptudtbdd.chuong_06_2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;
import com.ptudtbdd.chuong_06_2.adapter.AdapterViewPager;
import com.ptudtbdd.chuong_06_2.model.HorizontalFlipTransformation;

public class MainActivity extends AppCompatActivity {
    private ViewPager vPager;
    private TabLayout tab;
    private AdapterViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tab = findViewById(R.id.tab_layout);
        vPager = findViewById(R.id.view_pager);
        pager = new AdapterViewPager(getSupportFragmentManager(), 3);
        vPager.setPageTransformer(true, new HorizontalFlipTransformation());
        vPager.setAdapter(pager);
        tab.setupWithViewPager(vPager);
    }
}