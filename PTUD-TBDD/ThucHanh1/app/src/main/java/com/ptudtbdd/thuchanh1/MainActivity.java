package com.ptudtbdd.thuchanh1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.ptudtbdd.thuchanh1.model.ObjAdapter;
import com.ptudtbdd.thuchanh1.model.Task;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, ObjAdapter.ObjItemListener, SearchView.OnQueryTextListener {
    private Button btBeginDate, btEndDate;
    private EditText eName, eBeginDate, eEndDate;
    private CheckBox cbDone;
    private RecyclerView recyclerView;
    private Button btAdd, btUpdate;
    private ObjAdapter adapter;
    private SearchView search;
    private int current_pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        adapter = new ObjAdapter((this));
        LinearLayoutManager manager = new LinearLayoutManager((this), (RecyclerView.VERTICAL), (false));
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        adapter.setClickListenner(this);
        btBeginDate.setOnClickListener(this);
        btEndDate.setOnClickListener(this);
        btAdd.setOnClickListener(this);
        btUpdate.setOnClickListener(this);
        search.setOnQueryTextListener(this);

        initData();
    }

    private void initView() {
        btBeginDate = findViewById(R.id.btBeginDate);
        btEndDate = findViewById(R.id.btEndDate);
        eName = findViewById(R.id.inputName);
        eBeginDate = findViewById(R.id.beginDateEdit);
        eEndDate = findViewById(R.id.endDateEdit);
        cbDone = findViewById(R.id.cbStatusTask);
        recyclerView = findViewById(R.id.recyclerView);
        btAdd = findViewById(R.id.btAdd);
        btUpdate = findViewById(R.id.btUpdate);
        search = findViewById(R.id.search);
    }

    private void initData() {
        adapter.addItem(new Task("Thuc hanh LT Android", "10/03/2023", "11/03/2023", true));
        adapter.addItem(new Task("Thuc hanh Pentest", "15/03/2023", "16/03/2023", false));
    }

    @Override
    public void onClick(View v) {
        // Chose date
        if (v.getId() == R.id.btBeginDate || v.getId() == R.id.btEndDate) {
            Calendar c = Calendar.getInstance();
            int cy = c.get(Calendar.YEAR);
            int cm = c.get(Calendar.MONTH);
            int cd = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    String str_date = (dayOfMonth + "/" + (month + 1) + "/" + year);
                    if (v.getId() == R.id.btBeginDate) {
                        eBeginDate.setText(str_date);
                    }
                    if (v.getId() == R.id.btEndDate) {
                        eEndDate.setText(str_date);
                    }
                }
            }, cy, cm, cd);
            dialog.show();
        }

        // Add and Update
        if (v.getId() == R.id.btAdd || v.getId() == R.id.btUpdate) {
            Task obj = new Task();
            String name = eName.getText().toString();
            String begin = eBeginDate.getText().toString();
            String end = eEndDate.getText().toString();
            Boolean status = cbDone.isChecked();
            if (name.isEmpty() | begin.isEmpty() | end.isEmpty()) {
                Toast.makeText(getApplicationContext(), ("Khong duoc de trong"), Toast.LENGTH_SHORT).show();
                return;
            }

            try {
                obj.setName(name);
                obj.setBegin(begin);
                obj.setEnd(end);
                obj.setStatus(status);
                if (v.getId() == R.id.btAdd) {
                    adapter.addItem(obj); // add
                }
                if (v.getId() == R.id.btUpdate) {
                    adapter.updateItem(current_pos, obj); // update
                    btAdd.setEnabled(true);
                    btUpdate.setEnabled(false);
                }
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), ("Loi roi"), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onItemClick(View view, int p) {
        btAdd.setEnabled(false);
        btUpdate.setEnabled(true);
        current_pos = p;
        Task obj = adapter.getItem(current_pos);

        eName.setText(obj.getName());
        eBeginDate.setText(obj.getBegin());
        eEndDate.setText((obj.getEnd()));
        cbDone.setChecked(obj.getStatus());
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        filter(newText);
        return false;
    }

    private void filter(String s) {
        List<Task> filtered = new ArrayList<>();
        for (Task obj : adapter.getDataList()) {
            if (obj.getName().toLowerCase(Locale.ROOT).contains(s.toLowerCase())) {
                filtered.add(obj);
            }
        }
        if (filtered.isEmpty()) {
            Toast.makeText((this), ("Data not found"), Toast.LENGTH_SHORT).show();
        } else {
            adapter.filterList((filtered));
        }
    }
}