package com.ptudtbdd.thuchanh1.model;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ptudtbdd.thuchanh1.R;

import java.util.ArrayList;
import java.util.List;

public class ObjAdapter extends RecyclerView.Adapter<ObjAdapter.ObjViewHolder> {
    private Context context;
    private List<Task> showList;
    private List<Task> dataList;
    private ObjItemListener mObjItem;

    public ObjAdapter(Context context) {
        this.context = context;
        showList = new ArrayList<>();
        dataList = new ArrayList<>();
    }

    @NonNull
    @Override
    public ObjViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item, parent, false);
        return new ObjViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ObjViewHolder holder, int position) {
        Task myobj = showList.get(position);
        if (myobj == null) return;
        holder.tvName.setText(myobj.getName());
        holder.tvStart.setText(myobj.getBegin().toString());
        holder.tvEnd.setText((myobj.getEnd().toString()));
        holder.cbStatus.setChecked(myobj.getStatus());
        holder.btRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Notify");
                builder.setMessage("Delete " + myobj.getName() + " ?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
//                        listBackup.remove(position);
                        showList.remove(holder.getAdapterPosition()); //usage position -> error
                        dataList.remove(holder.getAdapterPosition()); //usage position -> error
                        notifyDataSetChanged();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (!showList.isEmpty()) return showList.size();
        return 0;
    }

    public void addItem(Task obj) {
        showList.add(obj);
        dataList.add(obj);
        notifyDataSetChanged();
    }

    public void updateItem(int position, Task obj) {
        showList.set(position, obj);
        dataList.set(position, obj);
        notifyDataSetChanged();
    }

    public Task getItem(int position) {
        return showList.get(position);
    }

    public List<Task> getDataList() {
        return dataList;
    }

    public void filterList(List<Task> filterlist) {
        showList = filterlist;
        notifyDataSetChanged();
    }

    class ObjViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvName, tvStart, tvEnd;
        private CheckBox cbStatus;
        private Button btRemove;

        public ObjViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.itemName);
            tvStart = itemView.findViewById(R.id.itemBeginDate);
            tvEnd = itemView.findViewById(R.id.itemEndDate);
            cbStatus = itemView.findViewById(R.id.itemStatus);
            btRemove = itemView.findViewById(R.id.btRemoveItem);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mObjItem != null) {
                mObjItem.onItemClick(v, getAdapterPosition());
            }
        }
    }


    public interface ObjItemListener {
        void onItemClick(View view, int p);
    }

    public void setClickListenner(ObjItemListener mObjItem) {
        this.mObjItem = mObjItem;
    }
}
