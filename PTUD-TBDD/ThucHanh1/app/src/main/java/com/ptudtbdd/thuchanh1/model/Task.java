package com.ptudtbdd.thuchanh1.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Task {
    private String name;
    private Date begin, end;
    private Boolean status;
    private final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

    public Task() {
    }

    public Task(String name, Date begin, Date end, Boolean status) {
        this.name = name;
        this.begin = begin;
        this.end = end;
        this.status = status;
    }

    public Task(String name, String begin, String end, Boolean status) {
        this.name = name;
        try {
            this.begin = formatter.parse(begin);
            this.end = formatter.parse(end);
        } catch (ParseException e) {
        }
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBegin() {
        return formatter.format(begin);
    }

    public void setBegin(String begin) {
        try {
            this.begin = formatter.parse(begin);
        } catch (ParseException e) {
        }
    }

    public String getEnd() {
        return formatter.format(end);
    }

    public void setEnd(String end) {
        try {
            this.end = formatter.parse(end);
        } catch (ParseException e) {
        }
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
