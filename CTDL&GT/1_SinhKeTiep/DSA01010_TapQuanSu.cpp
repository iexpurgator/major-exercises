#include <bits/stdc++.h>
using namespace std;

void next_combination(int a[], int n, int k, int b[]){
    int i = k;
    int cnt = 0;
    while(i > 0 && a[i] == n-k+i) i--;
    if(i > 0){
        a[i]++;
        if(!b[a[i]]) cnt++;
        for(int j = i+1; j <= k; j++) {
            a[j] = a[i] + j - i;
            if(!b[a[j]]) cnt++;
        }
    }
    else cnt = k;
    cout << cnt << endl;
}

void solve() {
    int T; cin >> T;
    while (T--) {
        int n, k;
        cin >> n >> k;
        int a[k+5], *b = new int[n+5];
        for (int i =1; i <= k; ++i) {
            cin >> a[i];
            b[a[i]] = 1;
        }
        next_combination(a, n, k, b);
    }
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
