#include <bits/stdc++.h>
using namespace std;
int n, k, Stop;
string s = "\n\0";
vector <string> vs;

void sinhkt() {
    int i = n-1;
    while (s[i] == 'B') {
        s[i] = 'A';
        i--;
    }
    if (i < 0)
        Stop = 1;
    else s[i] = 'B';
}

int kiemtra() {
    int i = 0, solan = 0, t = 0;
    while (i < n) {
        while (s[i] == 'B') i++;
        t = 0;
        while (s[i] == 'A' && i < n) {
            t++;
            i++;
        }
        if (t == k) solan++;
    }
    if (solan == 1)
        return 1;
    return 0;
}

void solve() {
    cin >> n >> k;
    for (int i = 0; i < n; ++i)
        s = "A" + s;
    while (Stop == 0) {
        if (kiemtra() == 1)
            vs.push_back(s);
        sinhkt();
    }
    cout << vs.size() << endl;
    for (auto str : vs)
        cout << str;
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
