#include <bits/stdc++.h>
using namespace std;

int T;

void gen(int K, char str[], int n) {
    if (n == K) {
        str[n] = '\0';
        cout << "H" << str << "A\n";
        return;
    }
    if (str[n - 1] == 'H') {
        str[n] = 'A';
        gen(K, str, n + 1);
    }
    if (str[n - 1] == 'A') {
        str[n] = 'A';
        gen(K, str, n + 1);
        str[n] = 'H';
        gen(K, str, n + 1);
    }
}
void generate(int K) {
    if (K <= 0) return;
    char str[K];
    str[0] = 'A';
    gen(K, str, 1);
}

void solve() {
    cin >> T;
    while (T--) {
        int n;
        cin >> n;
        n -= 2;
        if(n == 0) cout << "HA\n";
        else generate(n);
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}