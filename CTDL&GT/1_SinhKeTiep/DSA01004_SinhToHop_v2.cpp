#include <bits/stdc++.h>
using namespace std;

void solve() {
    int T; cin >> T;
    while (T--) {
        int n, r;
        cin >> n >> r;
        bool v[n+5] = {0};
        for (int i = 0; i < r; ++i) {
            v[i] = 1;
        }
        do {
	        for (int i = 0; i < n; ++i) {
	            if (v[i]) cout << (i + 1);
	        } cout << ' ';
        } while (prev_permutation(v, v+n));
        cout << endl;
    }
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}