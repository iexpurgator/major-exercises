#include <bits/stdc++.h>
using namespace std;

vector<string> vec;
void generateNumbersUtil() {
    queue<string> q;
    q.push("9");
    for (int count = 4160; count > 0; count--) {
        string s1 = q.front();
        q.pop();
        vec.push_back(s1);
        string s2 = s1;
        q.push(s1.append("0"));
        q.push(s2.append("9"));
    }
}

string findSmallestMultiple(int n) {
    for (int i = 0; i < vec.size(); i++)
        if (stoll(vec[i])%n == 0)
            return vec[i];
    return "";
}

void solve() {
    int T; cin >> T;
    generateNumbersUtil();
    while (T--) {
        int n; cin >> n;
        cout << findSmallestMultiple(n) << endl;
    }
}
int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}