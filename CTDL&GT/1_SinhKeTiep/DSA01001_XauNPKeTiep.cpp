#include <iostream>
using namespace std;

string next_binary(string num) {
    int flag = 0;
    for (int i = num.size() - 1; i >= 0; i--) {
        if (num.at(i) == '0') {
            num.at(i) = '1';
            flag = 1;
            break;
        } else
            num.at(i) = '0';
    }
    if (flag < 0)
        num = "1" + num;
    return num;
}

int main() {
    int T;
    cin >> T;
    while (T--) {
        string bin;
        cin >> bin;
        cout << next_binary(bin) << endl;
    }
    return 0;
}