#include <bits/stdc++.h>
using namespace std;
const char* BIG = "BIGGEST\0";

void solve() {
    int T; cin >> T;
    while (T--) {
        int n;
        string s;
        cin >> n >> s;
        cout << n << ' ';
        if(next_permutation(s.begin(), s.end()))
            cout << s << endl;
        else cout << BIG << endl;
    }
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
