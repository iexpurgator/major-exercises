#include <bits/stdc++.h>
using namespace std;

void gen_binAB(string &ab) {
	int n = ab.length();
	int j = n;
	cout << ab;
	for (int i = n-1; i>=0; --i) {
		if (ab.at(i) == 'B') {
			ab.at(i) = 'A';
			j--;
		} else {
			ab.at(i) = 'B';
			break;
		}
	}
	if (j > 0) {
		cout << ' ';
		gen_binAB(ab);
	}
	else return;
}

void solve() {
	int T; cin >> T;
	while (T--) {
		int n; cin >> n;
		string a = "\0";
		for (int i = 0; i < n; ++i)
			a = "A" + a;
		gen_binAB(a);
		cout << endl;
	}
}

int main () {
	ios_base::sync_with_stdio(false);
	cin.tie(nullptr);
	solve();
	return 0;
}
