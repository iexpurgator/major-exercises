#include <bits/stdc++.h>
using namespace std;

void solve() {
    int T; cin >> T;
    while (T--) {
        string s, bin = "\0";
        cin >> s;
        bin += s[0];
        for (int i = 1; i < s.length(); ++i) {
            if (i == 0) cout << s[i];
            else {
                if (s[i] == '0') bin += bin[i-1];
                else bin += (bin[i-1] == '0' ? '1' : '0');
            }
        }
        bin += "\0";
        cout << bin << endl;
    }
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
