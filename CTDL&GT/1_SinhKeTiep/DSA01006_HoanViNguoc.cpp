#include <bits/stdc++.h>
using namespace std;
#define print for(int i=0;i<n;++i) cout << a[i];

void solve() {
    int T; cin >> T;
    while (T--) {
        int n; cin >> n;
        int a[n+5];
        for (int i = 0; i < n; ++i) {
            a[i] = n-i;
        }
        do {
            print;
            cout << ' ';
        } while (prev_permutation(a, a+n));
        cout << endl;
    }
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
