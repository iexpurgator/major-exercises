#include <iostream>
using namespace std;

void next_combination(int a[], int n, int k){
	int i = k;
	while(i > 0 && a[i] == n-k+i) i--;
	if(i > 0){
		a[i]++;
		for(int j = i+1; j <= k; j++)
			a[j] = a[i] + j - i;
	}
	else for(int i = 1; i <= k; ++i)
		a[i] = i;
}

int main() {
	int T; cin >> T;
	while (T--) {
		int n, k;
		cin >> n >> k;
		int a[k+5];
		for (int i = 1; i <= k; ++i)
			cin >> a[i];
		next_combination(a, n, k);
		for (int i = 1; i <= k; ++i)
			cout << a[i] << ' ';
		cout << endl;
	}
}
