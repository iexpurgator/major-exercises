#include <bits/stdc++.h>
using namespace std;

void solve() {
    while (1) {
        int n, k, sum, res = 0, s_over_max = 0;
        cin >> n >> k >> sum;
        if (n==0 && k == 0 && sum == 0) break;
        if (k > n) {cout << 0 << endl; continue;}
        vector <bool> a(n);
        fill(a.end() - k, a.end(), true);
        do {
            int s = 0;
            s_over_max++;
            for (int i = 0; i < a.size(); ++i){
                if (a[i]) s += (i+1);
            }
            if (s == sum) res++;
            if (s_over_max == 1 && sum > s) break;
        } while (next_permutation(a.begin(), a.end()));
        cout << res << endl;
    }
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
