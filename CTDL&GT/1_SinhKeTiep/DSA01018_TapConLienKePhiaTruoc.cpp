#include <bits/stdc++.h>
using namespace std;

void solve() {
    int T; cin >> T;
    while (T--) {
        int n, r;
        cin >> n;
        cin >> r;
        bool v[n+5] = {0};
        for (int i = 0, j; i <r; ++i) {
            cin >> j;
            v[j-1] = 1;
        }
        next_permutation(v, v+n);
        for (int i = 0; i < n; ++i) {
            if (v[i])
                cout << (i + 1) << " ";
        }
        cout << endl;
    }
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
