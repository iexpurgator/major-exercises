#include <bits/stdc++.h>
using namespace std;

void solve() {
    int T; cin >> T;
    while (T--) {
        string s;
        cin >> s;
        for (int i = 0; i < s.size(); ++i) {
            if (i == 0) cout << s[i];
            else {
                if (s[i] != s[i-1]) cout << 1;
                else cout << 0;
            }
        }
        cout << endl;
    }
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
