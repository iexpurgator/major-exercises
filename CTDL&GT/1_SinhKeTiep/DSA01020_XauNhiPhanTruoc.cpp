#include <bits/stdc++.h>
using namespace std;

int T;

void generate(int n, string &s) {
    for (int i = n-1; i >= 0; --i) {
        if (s[i] == '1') {
            s[i] = '0';
            break;
        } else
            s[i] = '1';
    }
}

void solve() {
    cin >> T;
    while (T--) {
        string s;
        cin >> s;
        int n = s.length();
        generate(n, s);
        cout << s << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
