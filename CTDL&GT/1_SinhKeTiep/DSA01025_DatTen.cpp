#include <bits/stdc++.h>
using namespace std;

int T;

void gen_combination(int *a, int n, int k) {
    for (int i = 1; i <= k; ++i) cout << (char)(a[i] + 'A' - 1);
    int i = k;
    while (i > 0 && a[i] >= n + i - k) i--;
    if (i == 0) return;
    cout << '\n';
    a[i]++;
    for (int j = i + 1; j <= n; ++j) {
        a[j] = a[i] + j - i;
    }
    gen_combination(a, n, k);
}

void solve() {
    int T;
    cin >> T;
    while (T--) {
        int n, k;
        cin >> n >> k;
        int a[n + 5];
        for (int i = 1; i <= k; ++i) {
            a[i] = i;
        }
        gen_combination(a, n, k);
        cout << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
