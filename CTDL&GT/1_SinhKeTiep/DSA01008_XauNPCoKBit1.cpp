#include <bits/stdc++.h>
using namespace std;
#define print(a,n) for(int i=0;i<(n);++i) {cout << a[i];} cout<<'\n';

void solve() {
    int T; cin >> T;
    while (T--) {
        int n, k; cin >> n >> k;
        int a[n+5];
        for (int i = 0; i < n; ++i) {
            if (i < n-k) a[i] = 0;
            else a[i] = 1;
        }
        do { print(a,n); }
        while( next_permutation(a, a+n) );
        cout << endl;
    }
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
