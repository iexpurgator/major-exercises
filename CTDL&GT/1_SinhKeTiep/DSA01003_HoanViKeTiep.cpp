#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    int T; cin >> T;
    while (T--) {
        int n;
        cin >> n;
        int a[n+1];
        for (int i = 0; i < n; ++i)
            cin >> a[i];
        next_permutation(a, a+n);
        for (int i = 0; i < n; ++i)
            cout << a[i] << ' ';
        cout << endl;
    }
    return 0;
}