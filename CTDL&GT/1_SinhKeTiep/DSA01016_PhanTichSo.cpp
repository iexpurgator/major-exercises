#include <bits/stdc++.h>
using namespace std;
int T;

bool stop;
int n, k, a[15];

void gen() {
	int i = k;
	while (i>0 && a[i] == 1) i--;
	if (i<=0) stop = true;
	else {
		a[i]--;
		int d = k-i+1;
		k = i;
		for (int j = i+1; j <= i + d/a[i]; ++j)
			a[j] = a[i];
		k += d/a[i];
		if (d%a[i])
			a[++k] = d%a[i];
	}
}

void solve() {
    cin >> T;
    while (T--) {
        cin >> n;
        k = 1;
        a[k] = n;
        stop = false;
        while (!stop) {
			cout << '(';
			for (int i=1; i<=k; ++i) {
				cout << a[i];
				if (i < k) cout << ' ';
			}
			cout << ')' << ' ';
			gen();
		} cout << endl;
    }
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
