#include <bits/stdc++.h>
using namespace std;

void to_bin (int n, long x) {
    for (long i = n-1;i >= 0; --i) {
        long k = x >> i;
        if (k & 1) cout << 1;
        else cout << 0;
    }
    cout << ' ';
}

void solve() {
    int T; cin >> T;
    while (T--) {
        long n;
        cin >> n;
        for (long i = 0; i < (1<<n); i++){
            to_bin(n, i^(i>>1));
        }
        cout << endl;
    }
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
