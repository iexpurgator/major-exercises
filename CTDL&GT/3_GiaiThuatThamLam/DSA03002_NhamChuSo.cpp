#include <bits/stdc++.h>
using namespace std;

void solve() {
    string a, b;
    cin >> a >> b;
    replace(a.begin(), a.end(), '6', '5');
    replace(b.begin(), b.end(), '6', '5');
    long smin = stol(a) + stol(b);
    replace(a.begin(), a.end(), '5', '6');
    replace(b.begin(), b.end(), '5', '6');
    long smax = stol(a) + stol(b);
    cout << smin << ' ' << smax << '\n';
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}