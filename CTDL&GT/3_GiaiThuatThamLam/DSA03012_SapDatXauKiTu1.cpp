#include <bits/stdc++.h>
using namespace std;

int T;
int n;

bool greedy(int* a) {
    sort(a, a + 256, greater<int>());
    int i = 0, x = 0;
    while (a[i]) {
        x = abs(x - a[i]);
        if (x < 2) return true;
        i++;
    }
    return x < 2;
}

void solve() {
    cin >> T;
    while (T--) {
        int a[256] = {};
        string s;
        cin >> s;
        for (int i = 0; i < s.length(); ++i) {
            n++;
            a[s[i]]++;
        }
        cout << (greedy(a) ? 1 : -1) << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}
