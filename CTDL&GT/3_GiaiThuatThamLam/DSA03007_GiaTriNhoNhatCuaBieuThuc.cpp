#include <bits/stdc++.h>
using namespace std;

int T;
long long n, sum;

void solve() {
    cin >> T;
    while (T--) {
        sum = 0;
        cin >> n;
        long long a[n], b[n];
        for (int i = 0; i < n; i++)
            cin >> a[i];
        for (int i = 0; i < n; i++)
            cin >> b[i];
        sort(a, a+n);
        sort(b, b+n);
        for (int i=0;i<n;++i){
            sum += a[i]*b[n-i-1];
        }
        cout << sum << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}