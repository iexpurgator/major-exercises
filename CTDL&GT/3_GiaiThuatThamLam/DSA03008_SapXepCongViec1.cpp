#include <bits/stdc++.h>

using namespace std;

int T;
int n, s, m;

int cmp(int * x, int * y) {
    return x[1] < y[1];
}

void greedy(int ** a, int n) {
    int cnt = 1, i = 0;
    for (int j = 1; j < n; ++j) {
        if (a[j][0] >= a[i][1]) {
            cnt++;
            i = j;
        }
    }
    cout << cnt << '\n';
    return;
}

void solve() {
    cin >> T;
    while (T--) {
        cin >> n;
        int ** a = new int * [n];
        for (int i = 0; i < n; ++i) {
            a[i] = new int[2];
            cin >> a[i][0];
        }
        for (int i = 0; i < n; ++i)
            cin >> a[i][1];
        sort(a, a + n, cmp);
        greedy(a, n);
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}