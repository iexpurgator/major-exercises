#include <bits/stdc++.h>

using namespace std;

int T;
int n, s, m;

int cmp(int * x, int * y) {
    return x[2] > y[2];
}

void greedy(int ** a, int n) {
    bool f[n] = {};
    int cnt = 0, profit = 0;
    for (int i = 0; i < n; ++i) {
        for (int j = min(n, a[i][1]) - 1; j >= 0; --j) {
            if (!f[j]) {
                cnt++;
                profit += a[i][2];
                f[j] = true;
                break;
            }
        }
    }
    cout << cnt << ' ' << profit << '\n';
    return;
}

void solve() {
    cin >> T;
    while (T--) {
        cin >> n;
        int ** a = new int * [n];
        for (int i = 0; i < n; ++i) {
            a[i] = new int[3];
            cin >> a[i][0] >> a[i][1] >> a[i][2];
        }
        sort(a, a + n, cmp);
        greedy(a, n);
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}