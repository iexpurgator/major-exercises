#include <bits/stdc++.h>
using namespace std;

int dp[100001];
int res;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int n;
    cin >> n;
    vector<int> arr(n);
    for (int &i : arr) cin >> i;
    for (int &i : arr) {
        dp[i] = max(dp[i], dp[i - 1] + 1);
        res = max(res, dp[i]);
    }
    cout << n - res << endl;
    return 0;
}