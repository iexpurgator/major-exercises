#include <bits/stdc++.h>
using namespace std;
typedef long long int ll;
int T;

void solve() {
    T = 1;
    cin >> T;
    while (T--) {
        int n, m, k;
        cin >> n >> m >> k;
        ll a[n], b[m], c[k];
        for (int i = 0; i < n; ++i) cin >> a[i];
        for (int i = 0; i < m; ++i) cin >> b[i];
        for (int i = 0; i < k; ++i) cin >> c[i];

        long i, j, h;
        i = j = h = 0;
        bool notOk = 1;
        while (i < n && j < m && h < k) {
            if (a[i] == b[j] && a[i] == c[h]) {
                notOk = 0;
                cout << a[i] << " ";
                // cout << i << ' ' << j << ' ' << h << '\n';
                i++, j++, h++;
                continue;
            }
            if (a[i] < b[j])
                i++;
            else if (b[j] < c[h])
                j++;
            else
                h++;
        }
        if (notOk) cout << "NO\n";
        else cout << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}