#include <bits/stdc++.h>
using namespace std;

int T, cnt, n;
const int c[] = {1, 2, 5, 10, 20, 50, 100, 200, 500, 1000};

int greedy(int n) {
	int i = 9, d = 0;
	while(n != 0) {
		while(n >= c[i]) {
			d++;
			n = n -c[i];
		}
		i--;
	}
	return d;
}

void solve() {
    cin >> T;
    while (T--) {
		cin >> n;
		cout << greedy(n) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}
