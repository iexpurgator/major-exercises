#include <bits/stdc++.h>
using namespace std;

int T;

bool check(string sou, string tar) {
    int nt = tar.size();
    int ns = 0;
    for (int i = 0; i < nt; i++) {
        if (tar[i] == sou[ns])
            ns++;
        if (ns == sou.size())
            return true;
    }
    return false;
}

string greedy(long long n) {
    string tar = to_string(n);
    long long x = pow(n, 1.0 / 3.0);
    for (; x > 0; x--) {
        string sou = to_string(x * x * x);
        if (check(sou, tar))
            return sou;
    }
    return "-1";
}

void solve() {
    cin >> T;
    while (T--) {
        long long n;
        cin >> n;
        cout << greedy(n) << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}