#include <bits/stdc++.h>
using namespace std;
typedef long long int ll;
int T;

void solve() {
    T = 1;
    // cin >> T;
    while (T--) {
        int n;
        cin >> n;
        int a[n];
        for (int i = 0; i < n; ++i)
            cin >> a[i];
        sort(a, a + n);
        int sum2, sum3;
        sum2 = a[0] * a[1];
        sum3 = a[0] * a[1] * a[n-1];
        int tmp = a[n-1] * a[n-2];
        if(sum2 < tmp) sum2 = tmp;
        tmp = a[n-1] * a[n-2] * a[n-3];
        if(sum3 < tmp) sum3 = tmp;
        cout << (sum3 > sum2 ? sum3 : sum2) << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}