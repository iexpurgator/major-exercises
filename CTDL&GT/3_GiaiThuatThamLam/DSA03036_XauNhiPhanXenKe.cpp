#include <bits/stdc++.h>
using namespace std;

void solve() {
    int n;
    cin >> n;
    for (int i = 0; i < n; ++i) {
        if (i & 1)
            cout << 1 << ' ';
        else
            cout << 0 << ' ';
    }
    cout << '\n';
    for (int i = 0; i < n; ++i) {
        if (i & 1)
            cout << 0 << ' ';
        else
            cout << 1 << ' ';
    }
    cout << '\n';
}

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
    return 0;
}