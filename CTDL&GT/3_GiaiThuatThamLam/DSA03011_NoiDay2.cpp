#include <bits/stdc++.h>
using namespace std;
#define ll long long int
#define MODULO 1000000007
int T;
long long int n;
ll res;

void solve() {
    cin >> T;
    while (T--) {
        res = 0;
        cin >> n;
        ll k;
        priority_queue<ll, vector<ll>, greater<ll>> a;
        for (int i = 0; i < n; ++i) {
            cin >> k;
            a.push(k);
        }
        while (a.size() > 1) {
            ll b = a.top();
            a.pop();
            ll c = (a.top() + b) % MODULO;
            a.pop();
            res = (res + c) % MODULO;
            a.push(c);
        }
        cout << res << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}
