#include <bits/stdc++.h>
using namespace std;

int T;
int n, s, m;

void solve() {
    cin >> T;
    while (T--) {
        cin >> n >> s >> m;
        int res = s*m > (s - s/7)*n ? -1 : ceil((m*s)/(n*1.0));
        cout << res << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}