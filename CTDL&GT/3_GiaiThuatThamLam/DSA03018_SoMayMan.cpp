#include <bits/stdc++.h>
using namespace std;

int T;
int n, sum, m;

void greedy(int n) {
	int x = n / 7;
	int n7 = 0, n4 = 0;
	int l = INT_MAX;
	for(int i = 0; i <= x; i++) {
		int j = (n - i*7) / 4;
		if(i*7 + j*4 == n && i + j < l) {
			n7 = i;
			n4 = j;
			l = n7 + n4;
		}
	}
	string r4(n4, '4');
	string r7(n7, '7');
	if(r4 + r7 == "") cout << "-1" << endl;
	else cout << r4 + r7 << endl;
}

void solve() {
    cin >> T;
    while(T--){
		cin >> n;
		greedy(n);
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}