#include <bits/stdc++.h>
using namespace std;

int T;
int n;

void solve() {
    cin >> T;
    while (T--) {
        int a[256] = {};
        string s;
        cin >> n >> s;
        for (int i = 0; i < s.length(); ++i)
            a[s[i]]++;
        sort(a, a + 256, greater<int>());
        cout << ((a[0]-1)*(n-1) <= s.length()-a[0] ? 1 : -1) << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}
