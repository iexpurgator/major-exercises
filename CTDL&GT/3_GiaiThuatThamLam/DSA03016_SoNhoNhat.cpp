#include <bits/stdc++.h>
using namespace std;

int T;
int n, sum, m;

void solve() {
    cin >> T;
    while(T--){
        cin >> sum >> n;
        int a[n] = {};
        a[0] = 1; sum--;
        for (int i=n-1; i>0;--i){
            if(sum == 0) break;
            if(sum >= 9) a[i] = 9;
            else a[i] = sum;
            sum -= a[i];
        }
        if(sum > 0 && a[0]+sum < 9){
            a[0] += sum;
            sum = 0;
        }
        if(a[1] > 9 || sum > 0) cout << -1;
        else for(int i : a) cout << i;
        cout << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}