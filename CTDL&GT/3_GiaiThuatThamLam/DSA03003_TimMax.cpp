#include <bits/stdc++.h>
#define MODULO 1000000007L
using namespace std;

int T;
long long sum;

void solve() {
    cin >> T;
    while (T--) {
        sum = 0L;
        long n;
        cin >> n;
        long *a = new long[n + 5];
        for (int i = 0; i < n; ++i)
            cin >> a[i];
        sort(a, a + n);
        for (int i = 1; i < n; ++i) {
            sum = (sum + (a[i] * i) % MODULO) % MODULO;
        }
        cout << sum << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}