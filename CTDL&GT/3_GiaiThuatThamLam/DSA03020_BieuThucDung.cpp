#include <bits/stdc++.h>
using namespace std;

int T;
long long n, m;

int greedy(string s) {
	int n = 0, d = 0, i = 0;
	while(i < s.size()) {
		if(s[i] == '[') d++;
		else d--;
		if(d < 0) {
			while(s[i] == s[i+1] && i < s.size())
				i = i + 1;
			swap(s[i], s[i+1]);
			n++;
			i = -1;
			d = 0;
		}
		i++;
	}
	return n;
}

void solve() {
    cin >> T;
    while(T--){
		string s; cin >> s;
		cout << greedy(s) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}