#include <bits/stdc++.h>
using namespace std;

int T;
int n;

bool greedy(int n, int *a) {
    vector<int> b(vector<int>(a, a + n));
    sort(b.begin(), b.end());
    for (int i = 0; i < n; i++)
        if (!(a[i] == b[i]) && !(a[n - 1 - i] == b[i]))
            return false;
    return true;
}

void solve() {
    cin >> T;
    while (T--) {
        cin >> n;
        int a[n];
        for (int i = 0; i < n; i++)
            cin >> a[i];
        cout << (greedy(n, a) ? "Yes" : "No") << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}