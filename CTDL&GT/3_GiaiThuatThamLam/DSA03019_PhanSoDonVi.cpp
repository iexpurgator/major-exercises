#include <bits/stdc++.h>
using namespace std;

int T;
long long n, m;

void greedy(long long p, long long q) {
	while(q%p != 0) {
		long long x = q/p + 1;
		p = x*p-q;
		q = x*q;
		cout << "1/" << x << " + ";
	}
	cout << "1/" << q/p << endl;
}

void solve() {
    cin >> T;
    while(T--){
		cin >> n >> m;
		greedy(n, m);
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}