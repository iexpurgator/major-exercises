#include <iostream>
using namespace std;

typedef long long ll;
#define MOD 1000000007L;
#define ln '\n'
#define dbg(x) cout << #x << " = " << x << '\n'
#define all(x) (x).begin(), (x).end()
#define sz(x) ((ll)(x).size())

int solve(int *a, int n, int k) {
    int s[k][n + 1] = {};
    for (int j = 1; j <= n; j++) {
        for (int i = 0; i < k; i++) {
            if (a[j - 1] % k == i && s[i][j - 1] == 0)
                s[i][j] = 1;
            else {
                int x = (k + i - (a[j - 1] % k)) % k;
                if (s[x][j - 1] != 0)
                    s[i][j] = max(s[x][j - 1] + 1, s[i][j - 1]);
                else
                    s[i][j] = s[i][j - 1];
            }
        }
    }
    return s[0][n];
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int n, k;
        cin >> n >> k;
        int *a = new int[n];
        for (int i = 0; i < n; i++)
            cin >> a[i];
        cout << solve(a, n, k) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}