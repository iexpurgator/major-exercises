#include <bits/stdc++.h>
using namespace std;
#define MODULO 1000000007L
int T;
// DSA05025_ConEch

long long dp(int n){
    long long a[105] = {0};
    a[0] = a[1] = 1LL;
    a[2] = 2LL;
    if(n < 3) return a[n];
    for(int i = 3; i <= n; ++i)
        a[i] = a[i-1] + a[i-2] + a[i-3];
    return a[n];
}

void solve() {
    cin >> T;
    while (T--) {
        int n;
        cin >> n;
        cout << dp(n) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}