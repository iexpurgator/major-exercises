#include <iostream>
#include <algorithm>
using namespace std;

typedef long long ll;
#define MOD 1000000007L;
#define ln '\n'
#define dbg(x) cout << #x << " = " << x << '\n'
#define all(x) (x).begin(), (x).end()
#define sz(x) ((ll)(x).size())

struct Pair {
    int t;
    int s;
};

bool Less(Pair x, Pair y) {
    return x.t < y.t;
}

int solve(Pair *a, int n) {
    int c[n] = {1};
    int m = 0;
    sort(a, a + n, Less);
    for (int i = 1; i < n; i++) {
        c[i] = 1;
        for (int j = 0; j < i; j++)
            if (a[i].t > a[j].s && c[i] < c[j] + 1)
                c[i] = c[j] + 1;
        if (c[i] > m)
            m = c[i];
    }
    return m;
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int n;
        cin >> n;
        Pair *a = new Pair[n];
        for (int i = 0; i < n; i++)
            cin >> a[i].t >> a[i].s;
        cout << solve(a, n) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}