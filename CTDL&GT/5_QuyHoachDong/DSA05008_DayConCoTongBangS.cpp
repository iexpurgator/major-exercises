#include <bits/stdc++.h>
using namespace std;

int T;

bool isSubsetSum(int set[], int n, int sum) {
    bool subset[n + 1][sum + 1];
    for (int i = 0; i <= n; i++)
        subset[i][0] = true;
    for (int i = 1; i <= sum; i++)
        subset[0][i] = false;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= sum; j++) {
            subset[i][j] = subset[i - 1][j];
            if (j >= set[i - 1])
                subset[i][j] = subset[i][j] || subset[i - 1][j - set[i - 1]];
        }
    }
    return subset[n][sum];
}

void solve() {
    cin >> T;
    while(T--){
        int n, sum;
        cin >> n >> sum;
        int a[n];
        for (int i=0; i < n; ++i)
            cin >> a[i];
        if (isSubsetSum(a, n, sum) == true)
            cout << "YES\n";
        else
            cout << "NO\n";
    }
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}