#include <iostream>
#include <vector>
using namespace std;

typedef long long ll;
#define MOD 1000000007L;
#define ln '\n'
#define dbg(x) cout << #x << " = " << x << '\n'
#define all(x) (x).begin(), (x).end()
#define sz(x) ((ll)(x).size())

vector<int> split(long long n) {
    vector<int> a;
    while (n) {
        a.insert(a.begin(), n % 10);
        n = n / 10;
    }
    return a;
}

long long combine(vector<int> a, int l, int r) {
    long long res = 0;
    for (int i = l; i <= r; i++)
        res = res * 10 + a[i];
    return res;
}

long long solve(long long n) {
    vector<int> a = split(n);
    int na = a.size();
    long long s[na + 1] = {};
    for (int i = 1; i <= na; i++) {
        s[i] = s[i - 1];
        for (int j = i; j >= 1; j--)
            s[i] = s[i] + combine(a, j - 1, i - 1);
    }
    return s[na];
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        long long n;
        cin >> n;
        cout << solve(n) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}