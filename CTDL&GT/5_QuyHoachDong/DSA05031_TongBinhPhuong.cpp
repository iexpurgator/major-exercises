#include <iostream>
using namespace std;

typedef long long ll;
#define MOD 1000000007L;
#define ln '\n'
#define dbg(x) cout << #x << " = " << x << '\n'
#define all(x) (x).begin(), (x).end()
#define sz(x) ((ll)(x).size())

int solve(int n) {
    int a[n + 1] = {0, 1};
    for (int i = 2; i <= n; i++) {
        a[i] = 1 + a[i - 1];
        for (int j = 2; j * j <= i; j++)
            a[i] = min(a[i], a[i - j * j] + 1);
    }
    return a[n];
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int n;
        cin >> n;
        cout << solve(n) << ln;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}