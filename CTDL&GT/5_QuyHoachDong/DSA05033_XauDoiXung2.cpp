#include <bits/stdc++.h>
using namespace std;
#define ll long long int
#define fr(i, k, n) for (int i = (k); i <= (n); ++i)
#define fd(i, k, n) for (int i = (n); i >= (k); --i)
#define ifr(i, n) for (int i = 0; i < (n); ++i)

int T;

int lps(string str) {
    int n = str.size();
    int L[n][n];
    ifr(i, n) L[i][i] = 1;
    fr(cl, 2, n) fr(i, 0, n - cl) {
        int j = i + cl - 1;
        if (str[i] == str[j] && cl == 2)
            L[i][j] = 2;
        else if (str[i] == str[j])
            L[i][j] = L[i + 1][j - 1] + 2;
        else
            L[i][j] = max(L[i][j - 1], L[i + 1][j]);
    }
    return n - L[0][n - 1];
}

void solve() {
    cin >> T;
    while (T--) {
        string s;
        cin >> s;
        cout << lps(s) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
