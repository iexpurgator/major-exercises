#include <bits/stdc++.h>
using namespace std;
#define MODULO 1000000007L
int T;
// DSA05002_DayConLapLaiDaiNhat

long dp(int n, string s){
    int a[105][105] = {0};
    for(int i = 1; i <= n; ++i){
        for(int j = 1; j <= n; ++j){
            if(s[j-1] == s[i-1] && i!= j)
                a[i][j] = a[i-1][j-1] + 1;
            else
                a[i][j] = max(a[i-1][j], a[i][j-1]);
        }
    }
    return a[n][n];
}

void solve() {
    cin >> T;
    while (T--) {
        int n, k;
        string s;
        cin >> n >> s;
        cout << dp(n, s) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
