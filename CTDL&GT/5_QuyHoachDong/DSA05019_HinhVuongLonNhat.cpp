#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define MOD 1000000007L;
#define ln '\n'
#define dbg(x) cout << #x << " = " << x << '\n'
#define all(x) (x).begin(), (x).end()
#define sz(x) ((ll)(x).size())

int solve(int m, int n, int **a) {
    int s[m][n] = {}, res = 0;
    for (int i = 0; i < n; i++) {
        s[0][i] = a[0][i];
        if (s[0][i] > res)
            res = s[0][i];
    }

    for (int i = 0; i < m; i++) {
        s[i][0] = a[i][0];
        if (s[0][i] > res)
            res = s[0][i];
    }

    for (int i = 1; i < m; i++) {
        for (int j = 1; j < n; j++) {
            if (a[i][j] != 0)
                s[i][j] = min(min(s[i - 1][j], s[i][j - 1]), s[i - 1][j - 1]) + 1;
            else
                s[i][j] = 0;
            if (s[i][j] > res)
                res = s[i][j];
        }
    }
    return res;
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int m, n;
        cin >> m >> n;
        int **a = new int *[m];
        for (int i = 0; i < m; i++) {
            a[i] = new int[n];
            for (int j = 0; j < n; j++)
                cin >> a[i][j];
        }
        cout << solve(m, n, a) << ln;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}