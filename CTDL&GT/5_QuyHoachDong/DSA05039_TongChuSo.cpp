#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define MOD 1000000007L;
#define ln '\n'
#define dbg(x) cout << #x << " = " << x << '\n'
#define all(x) (x).begin(), (x).end()
#define sz(x) ((ll)(x).size())

int dp[901][8101]; // can change to [101][1001] -> fast

int minimumNumberOfDigits(int a, int b) {
    if (a > b || a < 0 || b < 0 || a > 900 || b > 8100)
        return -1;
    if (a == 0 && b == 0)
        return 0;
    if (dp[a][b] != -1)
        return dp[a][b];
    int ans = 101;
    for (int i = 9; i >= 1; i--) {
        int k = minimumNumberOfDigits(a - i, b - (i * i));
        if (k != -1)
            ans = min(ans, k + 1);
    }
    return dp[a][b] = ans;
}
void printSmallestNumber(int a, int b) {
    memset(dp, -1, sizeof(dp));
    dp[0][0] = 0;
    int k = minimumNumberOfDigits(a, b);
    if (k == -1 || k > 100)
        cout << -1;
    else {
        while (a > 0 && b > 0) {
            for (int i = 1; i <= 9; i++) {
                if (a >= i && b >= i * i && 1 + dp[a - i][b - i * i] == dp[a][b]) {
                    cout << i;
                    a -= i;
                    b -= i * i;
                    break;
                }
            }
        }
    }
    cout << ln;
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int a, b;
        cin >> a >> b;
        printSmallestNumber(a, b);
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}
