#include <iostream>
using namespace std;

typedef long long ll;
#define MOD 1000000007L;
#define ln '\n'
#define dbg(x) cout << #x << " = " << x << '\n'
#define all(x) (x).begin(), (x).end()
#define sz(x) ((ll)(x).size())

int solve(int n) {
    int *a = new int[n];
    int n2 = 0, n3 = 0, n5 = 0;
    int s2 = 2, s3 = 3, s5 = 5;
    a[0] = 1;
    for (int i = 1; i < n; i++) {
        int c = min(s2, min(s3, s5));
        a[i] = c;
        if (c == s2) {
            n2 = n2 + 1;
            s2 = a[n2] * 2;
        }
        if (c == s3) {
            n3 = n3 + 1;
            s3 = a[n3] * 3;
        }
        if (c == s5) {
            n5 = n5 + 1;
            s5 = a[n5] * 5;
        }
    }
    return a[n - 1];
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int n;
        cin >> n;
        cout << solve(n) << ln;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}
