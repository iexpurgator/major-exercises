#include <iostream>
using namespace std;

typedef long long ll;
#define MOD 1000000007L;
#define ln '\n'
#define dbg(x) cout << #x << " = " << x << '\n'
#define all(x) (x).begin(), (x).end()
#define sz(x) ((ll)(x).size())

int solve(int n, int k) {
	int a[n+1][k+5] = {};
	for(int i = 1; i <= 9; i++)
			a[1][i] = 1;
	for(int i = 1; i <= n; i++) 
		for(int so = 0; so <= 9; so++) 
			for(int j = so; j <= k; j++)
				a[i][j] =(a[i][j] + a[i-1][j-so])%(1000000000+7);
	return a[n][k];
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
		int n, k;
		cin >> n >> k;
		cout << solve(n, k) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}