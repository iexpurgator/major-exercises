#include <bits/stdc++.h>
using namespace std;
#define ll long long

int T;

bool valid (int *a, int n){
    int sum = 0;
    sum = accumulate(a, a+n,sum);
    if(sum&1) return false;
    bool dp[(sum/2)+1][n+1] = {0};
    fill(dp[0], dp[0]+n+1, true);
    for (int i=1;i<=sum/2; ++i){
        for (int j=1;j<=n;++j){
            dp[i][j] = dp[i][j-1];
            if (i>= a[j-1])
                dp[i][j] = dp[i][j] || dp[i-a[j-1]][j-1];
    }
    return dp[sum/2][n];
}

void solve(){
    cin >> T;
    while (T--) {
        int n; cin >> n;
        int a[n];
        for (int i=0; i < n; ++i)
            cin >> a[i];
        cout << (valid(a, n) ? "YES" : "NO") << "\n";
    }
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
