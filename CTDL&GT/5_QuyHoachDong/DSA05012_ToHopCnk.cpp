#include <bits/stdc++.h>
using namespace std;
#define MODULO 1000000007
int T;
// DSA05012

long BinomialCoefficient(int n, int k){
    long C[k + 1];
    memset(C, 0, sizeof(C));
    C[0] = 1L;
    for (int i = 1; i <= n; i++) {
        for (int j = min(i, k); j > 0; j--)
            C[j] = ((C[j - 1] % MODULO) + C[j]) % MODULO;
    }
    return C[k] % MODULO;
}

void solve() {
    cin >> T;
    while (T--) {
       	int n, k;
       	cin >> n >> k;
        cout << BinomialCoefficient(n, k) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}