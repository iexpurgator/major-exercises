#include <bits/stdc++.h>
using namespace std;
#define ll long long int
#define fr(i, k, n) for (int i = (k); i <= (n); ++i)
#define fd(i, k, n) for (int i = (n); i >= (k); --i)
#define ifr(i, n) for (int i = 0; i < (n); ++i)

int T;

int lps(string s) {
    int n = s.size();
    int res = 0;
    int L[n][n];
    memset(L, 0, sizeof(L));
    ifr(i, n) L[i][i] = 1;
    fd(i, 0, n - 1) { // lui
        fr(j, i, n - 1) { // tien
            if (s[i] == s[j]) {
                if (i + 1 > j - 1) L[i][j] = 1;
                else L[i][j] = L[i + 1][j - 1];
            }
            if (L[i][j])
                res = max(j - i + 1, res);
        }
    }
    return res;
}

void solve() {
    cin >> T;
    while (T--) {
        string s;
        cin >> s;
        cout << lps(s) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
