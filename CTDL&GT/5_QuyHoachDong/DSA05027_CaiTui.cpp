#include <bits/stdc++.h>
using namespace std;
#define MODULO 1000000007L
int T;
// DSA05027_CaiTui

long dp(int n, int* w, int* v, int k){
    long a[n+5][k+5];
    fill((long*)a, (long*)a + sizeof(a)/sizeof(a[0][0]), 0L);
    for(int i = 1; i <= n; ++i){
        for(int j = 1; j <= k; ++j){
            if(j < w[i-1]){
                a[i][j] += a[i-1][j];
            } else {
                a[i][j] = max(v[i-1] + a[i-1][j - w[i-1]], a[i-1][j]);
            }
        }
    }
    return a[n][k];
}

void solve() {
    cin >> T;
    // T=1;
    while (T--) {
        int k, n;
        cin >> n >> k;
        int a[n], b[n];
        for (int i=0;i<n;++i)
            cin >> a[i];
        for (int i=0;i<n;++i)
            cin >> b[i];
        cout << dp(n, a, b, k) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
