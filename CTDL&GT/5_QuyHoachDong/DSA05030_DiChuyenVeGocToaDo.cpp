#include <iostream>
using namespace std;

typedef long long ll;
#define MOD 1000000007L;
#define ln '\n'
#define dbg(x) cout << #x << " = " << x << '\n'
#define all(x) (x).begin(), (x).end()
#define sz(x) ((ll)(x).size())

ll solve(int n, int m) {
    ll c[n + 1][m + 1] = {};
    for (int i = 0; i <= m; i++)
        c[n][i] = 1;
    for (int i = 0; i <= n; i++)
        c[i][m] = 1;
    for (int i = n - 1; i >= 0; i--)
        for (int j = m - 1; j >= 0; j--)
            c[i][j] = c[i + 1][j] + c[i][j + 1];

    return c[0][0];
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int m, n;
        cin >> n >> m;
        cout << solve(n, m) << ln;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}