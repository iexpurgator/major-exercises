#include <iostream>
using namespace std;

typedef long long ll;
#define MOD 1000000007L;
#define ln '\n'
#define dbg(x) cout << #x << " = " << x << '\n'
#define all(x) (x).begin(), (x).end()
#define sz(x) ((ll)(x).size())

int ssl(int *a, int n) {
    int s[n] = {};
    int m = 0;
    for (int i = 0; i < n; i++) {
        s[i] = a[i];
        for (int j = 0; j < i; j++)
            if (a[i] > a[j])
                s[i] = max(s[i], s[j] + a[i]);
        if (m < s[i])
            m = s[i];
    }
    return m;
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int n;
        cin >> n;
        int *a = new int[n];
        for (int i = 0; i < n; i++)
            cin >> a[i];
        cout << ssl(a, n) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}