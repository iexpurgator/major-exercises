#include <bits/stdc++.h>
using namespace std;

int T;

int dynamic(int *a, int n){
    vector <int> lis(n, 1);
    for (int i = 1; i < n; ++i){
        // lis[i] = 1;
        for (int j = 0; j < i; ++j){
            if(a[i] > a[j] && lis[i] < lis[j] + 1) lis[i] = lis[j] + 1;
        }
    }
    return *max_element(lis.begin(), lis.end());
}

void solve(){
	//cin >> T;
	T=1;
	while(T--){
		int n;
		cin >> n;
		int a[n];
		for (int i=0;i<n;++i)
			cin >> a[i];
		cout << dynamic(a, n) << "\n";
	}
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
