#include <iostream>
using namespace std;

typedef long long ll;
#define MOD 1000000007L;
#define ln '\n'
#define dbg(x) cout << #x << " = " << x << '\n'
#define all(x) (x).begin(), (x).end()
#define sz(x) ((ll)(x).size())

int solve(int *a, int n) {
    int l[n] = {}, r[n] = {}, m;

    for (int i = 0; i < n; i++) {
        l[i] = a[i];
        for (int j = 0; j < i; j++)
            if (a[j] < a[i]) l[i] = max(l[i], l[j] + a[i]);
    }
    for (int i = n - 1; i >= 0; i--) {
        r[i] = a[i];
        for (int j = n - 1; j > i; j--)
            if (a[j] < a[i]) r[i] = max(r[i], r[j] + a[i]);
    }

    m = r[0] + l[0] - a[0];
    for (int i = 1; i < n; i++)
        if (r[i] + l[i] - a[i] > m)
            m = r[i] + l[i] - a[i];
    return m;
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int n;
        cin >> n;
        int *a = new int[n];
        for (int i = 0; i < n; i++)
            cin >> a[i];
        cout << solve(a, n) << ln;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}