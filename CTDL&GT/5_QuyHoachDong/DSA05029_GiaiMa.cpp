#include <bits/stdc++.h>
using namespace std;
#define MODULO 1000000007L
int T;
// DSA05029_GiaiMa

long dp(int n, string encode){
    if(encode[0] == '0') return 0;
    int a[105] = {0};
    a[0] = a[1] = 1;
    for(int i = 2; i <= n; ++i) {
        if (encode[i-1] > '0')
            a[i] += a[i-1];
        if (encode[i-2] == '1' ||
           (encode[i-2] == '2' && encode[i-1] < '7'))
            a[i] += a[i-2];
    }
    return a[n];
}

void solve() {
    cin >> T;
    // T=1;
    while (T--) {
        // int k, n;
        // cin >> n >> k;
        string s;
        cin >> s;
        cout << dp(s.length(), s) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
