#include <bits/stdc++.h>
using namespace std;
#define ll long long

int T;

int dp(int *a, int *b, int n) {
    int c[n+1][n+1] = {};
    sort(b, b+n);
    for(int i = 1; i <= n; i++) {
        for(int j = 1; j <= n; j++)
            if(a[i-1] == b[j-1]) c[i][j] = c[i-1][j-1]+1;
            else c[i][j] = max(c[i-1][j], c[i][j-1]);
    }

    return n - c[n][n];
}

void solve(){
    cin >> T;
    while (T--) {
        int n; cin >> n;
        int a[n+5], b[n+5];
        for(int i = 0; i < n; i++) {
            cin >> a[i];
            b[i] = a[i];
        }
        cout << dp(a, b, n) << endl;
    }
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
