#include <bits/stdc++.h>
using namespace std;

int T;

// CPP/Large Number/CPPLAN02.cpp
string LAN02(string a, string b) {
    string res = "";
    int carry = 0, len;
    while (a.size() != b.size())
        if (a.size() < b.size()) a = '0' + a;
        else b = '0' + b;
    len = a.size() - 1;
    for (int i = len; i >= 0; --i) {
        int temp = int(a[i] - '0') + int(b[i] - '0') + carry;
        res = char(temp % 10 + '0') + res;
        carry = temp / 10;
    }
    if (carry > 0) res = char(carry + '0') + res;
    return res;
}

// CPP/Large Number/CPPLAN03.cpp
string LAN03(string a, string b){
    int len_a = a.size();
    int len_b = b.size();
    if (len_a == 0 || len_b == 0) return "0";
    vector<int> result(len_a + len_b, 0);
    int i_a = 0;
    int i_b = 0;
    for (int i = len_a - 1; i >= 0; i--) {
        int carry = 0;
        int n_a = a[i] - '0';
        i_b = 0;
        for (int j = len_b - 1; j >= 0; j--) {
            int n_b = b[j] - '0';
            int sum = n_a * n_b + result[i_a + i_b] + carry;
            carry = sum / 10;
            result[i_a + i_b] = sum % 10;
            i_b++;
        }
        if (carry > 0) result[i_a + i_b] += carry;
        i_a++;
    }
    int i = result.size() - 1;
    while (i >= 0 && result[i] == 0) i--;
    if (i == -1) return "0";
    string s = "";
    while (i >= 0) s += std::to_string(result[i--]);
    return s;
}

string Catalan(int n) {
    string a[n + 1];
    a[0] = "1";
    for (int i = 1; i <= n; i++) {
        for (int j = 0; j < i; j++)
            a[i] = LAN02(a[i], LAN03(a[j], a[i - j - 1]));
    }
    return a[n];
}

void solve() {
    cin >> T;
    while (T--) {
        int n;
        cin >> n;
        cout << Catalan(n) << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
    return 0;
}