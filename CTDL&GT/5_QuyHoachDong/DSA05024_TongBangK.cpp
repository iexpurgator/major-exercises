#include <iostream>
using namespace std;

typedef long long ll;
#define MOD 1000000007L;
#define ln '\n'
#define dbg(x) cout << #x << " = " << x << '\n'
#define all(x) (x).begin(), (x).end()
#define sz(x) ((ll)(x).size())

int solve(int *a, int n, int k) {
    int c[k + 1] = {1};
    for (int i = 1; i <= k; i++) {
        for (int j = 0; j < n; j++) {
            if (a[j] <= i)
                c[i] = (c[i] + c[i - a[j]]) % MOD;
        }
    }
    return c[k];
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int n, k;
        cin >> n >> k;
        int *a = new int[n];
        for (int i = 0; i < n; i++)
            cin >> a[i];
        cout << solve(a, n, k) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}