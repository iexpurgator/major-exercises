#include <iostream>
using namespace std;

typedef long long ll;
#define MOD 1000000007L;
#define ln '\n'
#define dbg(x) cout << #x << " = " << x << '\n'
#define all(x) (x).begin(), (x).end()
#define sz(x) ((ll)(x).size())

int solve(int *a, int n) {
    long long max_end_of_here = a[0];
    long long max_so_far = 0;
    for (int i = 1; i < n; i++) {
        long long t = max_end_of_here;
        max_end_of_here = max(max_so_far + a[i], max_end_of_here);
        max_so_far = t;
    }
    return (max_end_of_here > max_so_far) ? max_end_of_here : max_so_far;
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int n;
        cin >> n;
        int *a = new int[n];
        for (int i = 0; i < n; i++)
            cin >> a[i];
        cout << solve(a, n) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}