#include <bits/stdc++.h>
using namespace std;
#define MODULO 1000000007L
int T;
// DSA05026_XemPhim

long dp(int n, int* arr, int k){
    long a[n+5][k+5];
    fill((long*)a, (long*)a + sizeof(a)/sizeof(a[0][0]), 0L);
    for(int i = 1; i <= n; ++i){
        for(int j = 1; j <= k; ++j){
            if(j < arr[i-1]){
                a[i][j] += a[i-1][j];
            } else {
                a[i][j] = max(arr[i-1] + a[i-1][j - arr[i-1]], a[i-1][j]);
            }
        }
    }
    return a[n][k];
}

void solve() {
    // cin >> T;
    T=1;
    while (T--) {
        int k, n;
        cin >> k >> n;
        int a[n];
        for (int i=0;i<n;++i)
            cin >> a[i];
        cout << dp(n, a, k) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
