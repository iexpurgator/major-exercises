#include <iostream>
using namespace std;

typedef long long ll;
#define MOD 1000000007L;
#define ln '\n'
#define dbg(x) cout << #x << " = " << x << '\n'
#define all(x) (x).begin(), (x).end()
#define sz(x) ((ll)(x).size())

int solve(int n, int x, int y, int z) {
    int c[n + 1] = {};
    c[1] = x;

    for (int i = 2; i <= n; i++) {
        int t = (i % 2 == 0) ? c[i / 2] + z : c[(i + 1) / 2] + y + z;
        c[i] = min(t, c[i - 1] + x);
    }
    return c[n];
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int n, x, y, z;
        cin >> n >> x >> y >> z;
        cout << solve(n, x, y, z) << ln;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}