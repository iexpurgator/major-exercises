#include <bits/stdc++.h>
using namespace std;

int T;

void solve(){
    cin >> T;
    while(T--){
        string s;
        cin >> s;
        int n = (int) s.length();
        vector < vector <int> > a (n, vector<int> (n, 0));
        for (int i = 1; i < n; ++i)
            for (int j = 0, k = i; k < n; ++k, ++j)
                if(s[j] ^ s[k]) a[j][k] = min(a[j][k-1], a[j+1][k]) +1;
                else a[j][k] = a[j+1][k-1];
        cout << a[0][n-1] << "\n";
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
