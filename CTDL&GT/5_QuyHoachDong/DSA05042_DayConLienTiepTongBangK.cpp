#include <iostream>
#include <unordered_map>
using namespace std;

typedef long long ll;
#define MOD 1000000007L;
#define ln '\n'
#define dbg(x) cout << #x << " = " << x << '\n'
#define all(x) (x).begin(), (x).end()
#define sz(x) ((ll)(x).size())

int solve(ll* arr, int n, ll sum) {
    ll curr_sum = arr[0];
    int start = 0, i;
    for (i = 1; i <= n; i++) {
        while (curr_sum > sum && start < i - 1) {
            curr_sum = curr_sum - arr[start];
            start++;
        }
        if (curr_sum == sum)
            return 1;
        if (i < n)
            curr_sum = curr_sum + arr[i];
    }
    return 0;
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int n;
        ll k;
        cin >> n >> k;
        ll* a = new ll[n + 5];
        for (int i = 0; i < n; ++i)
            cin >> a[i];
        if (solve(a, n, k))
            cout << "YES" << ln;
        else
            cout << "NO" << ln;
    }
}
int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}