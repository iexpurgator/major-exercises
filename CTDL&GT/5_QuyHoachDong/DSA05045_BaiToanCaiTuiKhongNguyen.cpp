#include <bits/stdc++.h>
using namespace std;
#define MODULO 1000000007L
int T;
// Fractional Knapsack
void maxProfit(vector<int> profit, vector<int> weight, int N) {
    int numOfElements = profit.size();
    int i;
    multimap<double, int> ratio;
    double max_profit = 0;
    for (i = 0; i < numOfElements; i++) {
        ratio.insert({(double)profit[i] / weight[i], i});
    }
    multimap<double, int>::reverse_iterator it;
    for (it = ratio.rbegin(); it != ratio.rend(); it++) {
        double fraction = (double)N / weight[it->second];
        if (N >= 0 && N >= weight[it->second]) {
            max_profit += profit[it->second];
            N -= weight[it->second];
        } else if (N < weight[it->second]) {
            max_profit += fraction * profit[it->second];
            break;
        }
    }
    printf("%.2lf\n", max_profit);
}

void solve() {
    cin >> T;

    while (T--) {
        int k, n;
        cin >> n >> k;
        vector<int> profit(n), weight(n);
        for (int i = 0; i < n; ++i) {
            // cin >> weight[i] >> profit[i];
            cin >> profit[i] >> weight[i];
        }
        maxProfit(profit, weight, k);
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
