#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define MOD 1000000007L;
#define ln '\n'
#define dbg(x) cout << #x << " = " << x << '\n'
#define all(x) (x).begin(), (x).end()
#define sz(x) ((ll)(x).size())

int solve(int n, int k) {
    if (k > n) return 0;
    long long res = 1;
    for (int i = n - k + 1; i <= n; i++) {
        res = (res * i) % MOD;
    }
    return res;
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int n, k;
        cin >> n >> k;
        cout << solve(n, k) << ln;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}