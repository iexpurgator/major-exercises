#include <bits/stdc++.h>
using namespace std;

int bitonic(int arr[], int n) {
    int inc[n];
    inc[0] = 1;
    for (int i = 1; i < n; i++)
        inc[i] = (arr[i] > arr[i - 1]) ? inc[i - 1] + 1 : 1;
    int dec[n];
    dec[n - 1] = 1;
    for (int i = n - 2; i >= 0; i--)
        dec[i] = (arr[i] > arr[i + 1]) ? dec[i + 1] + 1 : 1;
    int res[n];
    for (int i = 0; i < n; i++)
        res[i] = inc[i] + dec[i] - 1;
    return *max_element(res, res+n);
}

void solve() {
    int T;
    cin >> T;
    while (T--) {
        int n;
        cin >> n;
        int* a = new int[n + 5];
        for (int i = 0; i < n; ++i) cin >> a[i];
        cout << bitonic(a, n) << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
    return 0;
}