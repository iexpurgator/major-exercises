#include <bits/stdc++.h>
using namespace std;
#define MODULO 1000000007L
int T;
// DSA05013_BacThang

long dp(int n, int k){
    long a[100005] = {0};
    a[0] = a[1] = 1;
    for (int i = 2; i <= n; ++i){
        for (int j = 1; j <= min(k, i); ++j){
            a[i] += a[i-j];
            a[i] %= MODULO;
        }
    }
    return a[n];
}

void solve() {
    cin >> T;
    while (T--) {
        int n, k;
        cin >> n >> k;
        cout << dp(n, k) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
