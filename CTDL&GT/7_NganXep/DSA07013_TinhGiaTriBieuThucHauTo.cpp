#include <bits/stdc++.h>
using namespace std;

int T;

bool op(char c) {
    switch (c) {
    case '+':
    case '-':
    case '/':
    case '*':
        return true;
    }
    return false;
}

long converter(string pre_exp) {
    stack <long> s;
    for (char& c : pre_exp) {
        if (op(c)) {
            long b = s.top(); s.pop();
            long a = s.top(); s.pop();
            switch(c){
                case '+': a += b; break;
                case '-': a -= b; break;
                case '*': a *= b; break;
                case '/': a /= b; break;
            }
            s.push(a);
        } else {
            s.push((long)(c - '0'));
        }
    }
    return s.top();
}

void solve() {
    cin >> T;
    cin.ignore();
    while (T--) {
        string s = "";
        cin >> s;
        cout << converter(s) << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}