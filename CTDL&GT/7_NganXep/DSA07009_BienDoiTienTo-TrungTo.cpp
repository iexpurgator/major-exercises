#include <bits/stdc++.h>
using namespace std;

int T;

bool op(char c) {
    switch (c) {
    case '+':
    case '-':
    case '/':
    case '*': return true;
    }
    return false;
}

string converter(string pre_exp) {
    stack < string > s;
    int length = pre_exp.size();
    for (int i = length - 1; i >= 0; i--) {
        if (op(pre_exp[i])) {
            string op1 = s.top();
            s.pop();
            string op2 = s.top();
            s.pop();
            string temp = "(" + op1 + pre_exp[i] + op2 + ")";
            s.push(temp);
        } else {
            s.push(string(1, pre_exp[i]));
        }
    }
    return s.top();
}

void solve() {
    cin >> T;
    cin.ignore();
    while (T--) {
        string s = "";
        // getline(cin, s);
        cin >> s;
        cout << converter(s) << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}