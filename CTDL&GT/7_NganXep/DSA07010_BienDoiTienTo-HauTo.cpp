#include <bits/stdc++.h>
using namespace std;

int T;

bool op(char c) {
    switch (c) {
    case '+':
    case '-':
    case '/':
    case '*':
        return true;
    }
    return false;
}

string converter(string pre_exp) {
    stack <string> s;
    for (char& c : pre_exp) {
        if (op(c)) {
            string op1 = s.top();
            s.pop();
            string op2 = s.top();
            s.pop();
            string temp = op1 + op2 + c;
            s.push(temp);
        } else {
            s.push(string(1, c));
        }
    }
    return s.top();
}

void solve() {
    cin >> T;
    cin.ignore();
    while (T--) {
        string s = "";
        // getline(cin, s);
        cin >> s;
        reverse(s.begin(), s.end());
        cout << converter(s) << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}