#include <bits/stdc++.h>
using namespace std;
int T;
string str;

void print(stack<char> c) {
    string s = "";
    while (!c.empty()) {
        s = c.top() + s;
        c.pop();
    }
    cout << s << endl;
}

void solve() {
    cin >> T;
    while (T--) {
        cin >> str;
        stack<char> c, s;
        stack<char> temp;
        for (char& chr : str) {
            if (chr != ')') {
                if (chr == '(' && !c.empty())
                    s.push(c.top());
                c.push(chr);
            } else {
                while (c.top() != '(') {
                    if (c.top() != '+' && c.top() != '-')
                        temp.push(c.top());
                    else if (s.empty() || s.top() == '+' || s.top() == '(')
                        temp.push(c.top());
                    else if (c.top() == '-')
                        temp.push('+');
                    else
                        temp.push('-');
                    c.pop();
                }
                c.pop();
                s.pop();
                if (temp.top() == '+' || temp.top() == '-')
                    c.pop();
                while (!temp.empty()) {
                    c.push(temp.top());
                    temp.pop();
                }
            }
        }
        print(c);
    }
}

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
    return 0;
}