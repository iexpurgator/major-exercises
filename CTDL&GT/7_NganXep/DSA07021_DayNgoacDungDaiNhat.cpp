#include <iostream>
#include <stack>
using namespace std;

int main () {
    int T; cin >> T;
    while (T--) {
        string s;
        cin >> s;
        stack <int> st;
        st.push(-1);
        int res = 0;
        for (int i = 0; i < s.length(); ++i) {
            if(s[i] == '(') st.push(i);
            else {
                st.pop();
                if (st.empty()) st.push(i);
                else res = max(res, i - st.top());
            }
        }
        cout << res << endl;
    }
    return 0;
}