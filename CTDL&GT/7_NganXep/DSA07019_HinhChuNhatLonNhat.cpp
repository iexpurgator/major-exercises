#include <bits/stdc++.h>
using namespace std;

int T;
long res, area;
stack<int> s;

void update(long *a, int i) {
    int cur = s.top();
    s.pop();
    area = a[cur] * (s.empty() ? i : i - s.top() - 1);
    res = max(area, res);
}

void solve() {
    cin >> T;
    while (T--) {
        int n, i;
        cin >> n;
        long *a = new long[n + 5];
        res = 0L;
        for (i = 0; i < n; ++i)
            cin >> a[i];
        for (i = 0; i < n;)
            if (s.empty() || a[s.top()] <= a[i])
                s.push(i++);
            else
                update(a, i);
        while (!s.empty())
            update(a, i);
        cout << res << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}