#include <bits/stdc++.h>

#define ull unsigned long long int
#define ll long long int
using namespace std;

string decodeString(const string& s, int& i) {
    string res;
    while (i < s.length() && s[i] != ']') {
        if (!isdigit(s[i]))
            res += s[i++];
        else {
            int n = 0;
            while (i < s.length() && isdigit(s[i]))
                n = n * 10 + s[i++] - '0';
            i++;  // '['
            string t = decodeString(s, i);
            i++;  // ']'
            while (n-- > 0)
                res += t;
        }
    }
    return res;
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        string str;
        cin >> str;
        for (int i = 0; i < str.length() - 1; ++i)
            if (!isdigit(str[i]) && str[i + 1] == '[')
                str.insert(i + 1, "1");
        int k = 0;
        cout << decodeString(str, k) << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}