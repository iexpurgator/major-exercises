#include <bits/stdc++.h>
using namespace std;
int T;

void convert(int *a, int n, unordered_map<int, int> repeat) {
    int greatest[n];
    stack<int> sg;
    for (int i = n - 1; i >= 0; i--) {
        while (!sg.empty() && repeat[a[sg.top()]] <= repeat[a[i]])
            sg.pop();
        greatest[i] = (sg.empty() ? -1 : sg.top());
        sg.push(i);
    }
    for (int i = 0; i < n; i++) {
        if (greatest[i] != -1)
            cout << a[greatest[i]] << ' ';
        else
            cout << -1 << ' ';
    }
    cout << '\n';
}

void solve() {
    cin >> T;
    while (T--) {
        int n;
        cin >> n;
        int *a = new int[n];
        unordered_map<int, int> repeat;
        for (int i = 0; i < n; ++i) {
            cin >> a[i];
            repeat[a[i]]++;
        }
        convert(a, n, repeat);
    }
}

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
    return 0;
}