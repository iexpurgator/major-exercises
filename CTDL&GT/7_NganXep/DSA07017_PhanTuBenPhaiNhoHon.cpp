#include <bits/stdc++.h>
using namespace std;

int T;
int n;

void convert(int *a) {
    int greatest[n], smaller[n];
    stack<int> sg, ss;
    for (int i = n - 1; i >= 0; i--) {
        while (!sg.empty() && a[sg.top()] <= a[i])
            sg.pop();
        greatest[i] = (sg.empty() ? -1 : sg.top());
        sg.push(i);

        while (!ss.empty() && a[ss.top()] >= a[i])
            ss.pop();
        smaller[i] = (ss.empty() ? -1 : ss.top());
        ss.push(i);
    }
    for (int i = 0; i < n; i++) {
        if (greatest[i] != -1 && smaller[greatest[i]] != -1)
            cout << a[smaller[greatest[i]]] << ' ';
        else
            cout << -1 << ' ';
    }
    cout << '\n';
}

void solve() {
    cin >> T;
    while (T--) {
        cin >> n;
        int *a = new int[n + 5];
        for (int i = 0; i < n; ++i) {
            cin >> a[i];
        }
        convert(a);
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}