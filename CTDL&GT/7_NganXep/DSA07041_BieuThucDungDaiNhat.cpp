/*
Cho biểu thức P chỉ bao gồm các ký tự mở ngoặc ‘(’ hoặc đóng ngoặc ‘)’.
Biểu thức P có thể viết đúng hoặc không đúng. Nhiệm vụ của bạn là tìm độ dài lớn nhất viết đúng của P.
Chú ý: Độ dài của biểu thức đúng ngắn nhất là 2.


Input


4
(()(
()()((
((()()())))
()(())(

Output
2

4

10

6
*/
#include <bits/stdc++.h>
using namespace std;

void solve(){
    int T; cin >> T;
    while(T--){
        string s;
        cin >> s;
        stack<char> st;
        while (!st.empty())
            st.pop();
        int count = 0;
        for (char& i : s) {
            if (i == '(')
                st.push(i);
            if (i == ')') {
                if (!st.empty() && st.top() == '(') {
                    st.pop();
                    count++;
                }
            }
        }
        count *= 2;
        cout << count << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}