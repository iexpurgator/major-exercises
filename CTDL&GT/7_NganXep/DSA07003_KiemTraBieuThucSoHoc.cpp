#include <bits/stdc++.h>
using namespace std;

int T;

bool checkRedundancy(string & str) {
    stack < char > st;
    for (char& ch: str) {
        if (ch == ')') {
            bool flag = true;
            while (!st.empty() && st.top() != '(') {
                switch (st.top()){
                case '+':
                case '-':
                case '*':
                case '/':
                    flag = false;
                default:
                    st.pop();
                }
            }
            if(st.top() == '(')
                st.pop();
            if (flag)
                return true;
        } else
            st.push(ch);
    }
    return false;
}

void solve() {
    cin >> T;
    // T = 1;
    cin.ignore();
    while (T--) {
        string s = "";
        getline(cin, s);
        if (checkRedundancy(s))
            cout << "Yes\n";
        else
            cout << "No\n";
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}