#include <bits/stdc++.h>
using namespace std;

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        long n;
        cin >> n;
        vector<long> a(n, 0);
        for (long &i : a) cin >> i;
        vector<long> res(n, 1);
        stack<long> s;
        s.push(-1);
        s.push(0);
        for (int i = 1; i < n; i++) {
            while (s.top() != -1 && a[s.top()] <= a[i])
                s.pop();
            res[i] = i - s.top();
            s.push(i);
        }
        for (long &i : res)
            cout << i << ' ';
        cout << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}