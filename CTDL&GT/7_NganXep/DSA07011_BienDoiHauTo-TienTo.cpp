#include <bits/stdc++.h>
using namespace std;

int T;
stack<string> s;
string str;

int op(char c) {
    switch (c) {
        case '+':
        case '-':
            return 1;
        case '*':
        case '/':
            return 2;
        case '^':
            return 3;
    }
    return -1;
}

void convert() {
    // reverse(str.begin(), str.end());
    for (char &c : str) {
        if (op(c) > 0) {
            string op1 = s.top();
            s.pop();
            string op2 = s.top();
            s.pop();
            string res = c + op2 + op1;
            s.push(res);
        } else {
            s.push(string(1, c));
        }
    }
    while (s.size() > 2) {
        string op1 = s.top();
        s.pop();
        string op2 = s.top();
        s.pop();
        string res = op2 + op1;
        s.push(res);
    }
    cout << s.top();
    s.pop();
    cout << '\n';
}

void solve() {
    cin >> T;
    while (T--) {
        cin >> str;
        convert();
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}