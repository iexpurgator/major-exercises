#include <bits/stdc++.h>
using namespace std;

void solve(){
    int T = 1;
    cin >> T;
    cin.ignore();
    while(T--){
        string s;
        getline(cin, s);
        stringstream ss(s);
        string tkn;
        stack <string> st;
        while(ss >> tkn)
            st.push(tkn);
        while(!st.empty()){
            cout << st.top() << ' ';
            st.pop();
        }
        cout << '\n';
    }
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}