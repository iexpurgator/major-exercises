#include <bits/stdc++.h>
using namespace std;

int T;

void solve(){
    // cin >> T;
    T = 1;
    while(T--){
        int n, k;
        stack <int> st;
        string s;
        cin >> n;
        for(int i = 0; i < n; i++) {
            cin >> s;
            if(s == "PUSH") {
                cin >> k;
                st.push(k);
            } else if(s == "POP") {
                if(!st.empty())
                    st.pop();
            } else if(s == "PRINT"){
                if(!st.empty())
                    cout << st.top() << '\n';
                else
                    cout << "NONE\n";
            }
        }
    }
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}