#include <bits/stdc++.h>
using namespace std;

void solve(){
    int T = 1;
    cin >> T;
    while(T--){
        string str;
        cin >> str;
        stack <string> s;
        stack <int> o;
        for (int i = 0; i < str.length(); ++i){
            if (str[i] == '['){
                s.push("[");
                if (i == 0 || !isdigit(str[i - 1]))
                    o.push(1);
            } else if (str[i] == ']') {
                string temp = "";
                int t = 1;
                while (s.top() != "["){
                    temp = s.top() + temp;
                    s.pop();
                }
                s.pop();
                if (!o.empty()){
                    t = o.top();
                    o.pop();
                }
                for (int i = 0; i < t; i++)
                    s.push(temp);
            } else if (isdigit(str[i])) {
                int t = 0;
                while (isdigit(str[i]))
                    t = t * 10 + str[i++] - '0';
                i = i - 1;
                o.push(t);
            } else
                s.push(string(1,str[i]));
        }
        str = "";
        while(!s.empty()){
            str = s.top() + str;
            s.pop();
        }
        cout << str << '\n';
    }
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie();
    solve();
    return 0;
}
