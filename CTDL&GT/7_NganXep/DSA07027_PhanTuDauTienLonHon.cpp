#include <bits/stdc++.h>
using namespace std;

int T;
int n;

void convert(int *a) {
    int greatest[n];
    stack<int> sg;
    for (int i = n - 1; i >= 0; i--) {
        while (!sg.empty() && a[sg.top()] <= a[i])
            sg.pop();
        greatest[i] = (sg.empty() ? -1 : sg.top());
        sg.push(i);
    }
    for (int i = 0; i < n; i++) {
        if (greatest[i] != -1)
            cout << a[greatest[i]] << ' ';
        else
            cout << -1 << ' ';
    }
    cout << '\n';
}

void solve() {
    cin >> T;
    while (T--) {
        cin >> n;
        int *a = new int[n + 5];
        for (int i = 0; i < n; ++i) {
            cin >> a[i];
        }
        convert(a);
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}