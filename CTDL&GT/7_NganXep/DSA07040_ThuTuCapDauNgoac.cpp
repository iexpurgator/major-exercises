#include <bits/stdc++.h>
using namespace std;

void solve() {
    int T;
    cin >> T;
    cin.ignore();
    while (T--) {
        string s;
        getline(cin, s);
        stack<int> st;
        int cnt = 0;
        for (int i = 0; i < s.length(); ++i) {
            if (s[i] == '(') {
                cnt++;
                cout << cnt << ' ';
                st.push(cnt);
            } else if (s[i] == ')') {
                cout << st.top() << ' ';
                st.pop();
            }
        }
        cout << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}