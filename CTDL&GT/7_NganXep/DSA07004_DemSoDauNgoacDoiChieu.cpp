#include <bits/stdc++.h>
using namespace std;

int T;

int change(string & str) {
    stack < char > st;
    int cnt = 0;
    for (char& ch: str) {
        if (ch == ')') {
            if(st.empty()) // convert first ')'
                cnt++, st.push('(');
            else
                st.pop();
        } else
            st.push(ch);
    }
    while(!st.empty()) {
        if(st.top() == '(') // convert last '('
            cnt++, st.pop();
        st.pop();
    }
    return cnt;
}

void solve() {
    cin >> T;
    cin.ignore();
    while (T--) {
        string s = "";
        // getline(cin, s);
        cin >> s;
        cout << change(s) << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}