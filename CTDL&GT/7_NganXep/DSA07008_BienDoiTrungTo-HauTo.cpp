#include <bits/stdc++.h>
using namespace std;

int T;
stack<char> s;
string str;

int op(char c) {
    switch (c) {
        case '+':
        case '-':
            return 1;
        case '*':
        case '/':
            return 2;
        case '^':
            return 3;
    }
    return -1;
}

void convert() {
    for (char &c : str) {
        if (c == '(') {
            s.push(c);
        } else if (op(c) > 0) {
            while (!s.empty() && op(c) <= op(s.top())) {
                cout << s.top();
                s.pop();
            }
            s.push(c);
        } else if (c == ')') {
            while (s.top() != '(') {
                cout << s.top();
                s.pop();
            }
            s.pop();
        } else {
            cout << c;
        }
    }
    while (!s.empty()) {
        if (s.top() != '(') cout << s.top();
        s.pop();
    }
    cout << '\n';
}

void solve() {
    cin >> T;
    while (T--) {
        cin >> str;
        convert();
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}