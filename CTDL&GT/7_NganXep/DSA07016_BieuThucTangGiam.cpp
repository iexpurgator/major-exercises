#include <bits/stdc++.h>
using namespace std;

int T;
stack<int> s;
string str;

void convert() {
    int i = 1;
    s.push(i);
    for (char &c : str) {
        switch (c) {
            case 'D':
                s.push(++i);
                break;
            case 'I':
                while (!s.empty()) {
                    cout << s.top();
                    s.pop();
                }
                s.push(++i);
                break;
        }
    }
    while (!s.empty()) {
        cout << s.top();
        s.pop();
    }
    cout << '\n';
}

void solve() {
    cin >> T;
    while (T--) {
        cin >> str;
        convert();
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}