#include <stdio.h>

char key[5];
int value;
int stack[205];
int stack_top = -1;

int main() {
	while(scanf("%s", &key)) {
	    switch (key[1]){
		case 117:
			scanf("%d", &value);
			stack_top++;
			stack[stack_top] = value;
		    break;
		case 111:
			stack_top--;
			break;
		case 104:
			if(stack_top == -1){
				puts("empty");
				break;
			} else {
				for(int i = 0; i <= stack_top; i++)
					printf("%d ", stack[i]);
				printf("\n");
			}
			break;
		default:
		    return 0;
		}
		key[1] = 0;
	}
	return 0;
}