#include <iostream>
#include <vector>

int main() {
	std::string key;
	int value;
	std::vector <int> st;
	while(std::cin >> key) {
		if(key == "push"){
			std::cin >> value;
			st.push_back(value);
		}
		else if(key == "pop") st.pop_back();
		else {
			if(st.empty()){
				std::cout << "empty" << '\n';
				break;
			} else {
				for(int i : st)
					std::cout << i << ' ';
				std::cout << '\n';
			}
		}
	}
	return 0;
}