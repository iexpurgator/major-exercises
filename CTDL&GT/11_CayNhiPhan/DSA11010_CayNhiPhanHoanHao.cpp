#include <bits/stdc++.h>
using namespace std;

struct node {
    int data;
    node *left, *right;
    node(int data) {
        this->data = data;
        left = right = NULL;
    }
};

void addNode(int u, int v, char x, node **root) {
    if ((*root) != NULL && (*root)->data == u) {
        if (x == 'L')
            (*root)->left = new node(v);
        else
            (*root)->right = new node(v);
    } else {
        if ((*root)->right != NULL)
            addNode(u, v, x, &(*root)->right);
        if ((*root)->left != NULL)
            addNode(u, v, x, &(*root)->left);
    }
}

bool isLeaf(node *root) {
    if (!root)
        return false;
    if (!root->left && !root->right)
        return true;
    return false;
}

bool check(int level, int &levelt, node *root) {
    level = level + 1;
    if (isLeaf(root)) {
        if (levelt == -1)
            levelt = level;
        if (levelt != -1 && level != levelt)
            return false;
        if (levelt != -1 && level == levelt)
            return true;
    } else {
        if (!root->left || !root->right)
            return false;
        else {
            if (!check(level, levelt, root->left))
                return false;
            if (!check(level, levelt, root->right))
                return false;
            return true;
        }
    }
    return true;
}

void solve() {
    int T;
    cin >> T;
    while (T--) {
        int n, u, v;
        char x;
        cin >> n >> u >> v >> x;
        node *root = new node(u);
        addNode(u, v, x, &root);
        for (int i = 1; i < n; i++) {
            cin >> u >> v >> x;
            addNode(u, v, x, &root);
        }
        int level = -1;
        if (check(0, level, root))
            cout << "Yes\n";
        else
            cout << "No\n";
        delete root;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}