#include <bits/stdc++.h>

#define ull unsigned long long int
#define ll long long int
using namespace std;

struct node {
    int data;
    node *left, *right;
    node(int data) {
        this->data = data;
        left = right = NULL;
    }
};

node* init(int n) {
    node* ro = NULL;
    int a, b;
    char c;
    unordered_map<int, node*> m;
    node* cha;
    while (n--) {
        cin >> a >> b >> c;
        if (m.find(a) == m.end()) {
            m[a] = new node(a);
            if (ro == NULL) ro = m[a];
        }
        cha = m[a];
        m[b] = new node(b);
        if (c == 'L')
            cha->left = m[b];
        else
            cha->right = m[b];
    }
    return ro;
}

void storeInorder(node* node, int inorder[], int* index_ptr) {
    if (node == NULL) return;
    storeInorder(node->left, inorder, index_ptr);
    inorder[*index_ptr] = node->data;
    (*index_ptr)++;
    storeInorder(node->right, inorder, index_ptr);
}

int countNodes(struct node* root) {
    if (root == NULL) return 0;
    return countNodes(root->left) + countNodes(root->right) + 1;
}

void arrayToBST(int* arr, struct node* root, int* index_ptr) {
    if (root == NULL) return;
    arrayToBST(arr, root->left, index_ptr);
    root->data = arr[*index_ptr];
    (*index_ptr)++;
    arrayToBST(arr, root->right, index_ptr);
}

void binaryTreeToBST(struct node* root) {
    if (root == NULL) return;
    int n = countNodes(root);
    int* arr = new int[n];
    int i = 0;
    storeInorder(root, arr, &i);
    sort(arr, arr + n);
    i = 0;
    arrayToBST(arr, root, &i);
    delete[] arr;
}

void printInorder(node* root) {
    if (root == NULL) return;
    printInorder(root->left);
    cout << root->data << " ";
    printInorder(root->right);
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int n;
        cin >> n;
        node* root = init(n);
        binaryTreeToBST(root);
        printInorder(root);
        cout << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}