#include <bits/stdc++.h>
using namespace std;

struct node {
    int data;
    node *left, *right;
    node(int data) {
        this->data = data;
        left = right = NULL;
    }
};

int search(int* a, int begin, int end, int data) {
    int i = begin;
    for (; i <= end; ++i)
        if (a[i] == data) break;
    return i;
}

node* levelOrder(int n, int* pre, int* in, int begin, int end) {
    static int preindex = 0;
    if (begin > end) return NULL;
    node* root = new node(pre[preindex++]);
    if (begin == end) return root;
    int index = search(in, begin, end, root->data);
    root->left = levelOrder(n, pre, in, begin, index - 1);
    root->right = levelOrder(n, pre, in, index + 1, end);
    if (preindex >= n) preindex = 0;  // reset static
    return root;
}

void printPostorder(node* root) {
    if (root == NULL) return;
    printPostorder(root->left);
    printPostorder(root->right);
    cout << root->data << " ";
}

void solve() {
    int T;
    cin >> T;
    while (T--) {
        int n;
        cin >> n;
        int in[n];
        for (int i = 0; i < n; ++i)
            cin >> in[i];
        int pre[n];
        for (int i = 0; i < n; ++i)
            cin >> pre[i];
        node* post = levelOrder(n, pre, in, 0, n - 1);
        printPostorder(post);
        cout << '\n';
        delete post;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}