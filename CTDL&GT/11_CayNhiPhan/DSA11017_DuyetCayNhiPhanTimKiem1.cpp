#include <bits/stdc++.h>

#define ull unsigned long long int
#define ll long long int
using namespace std;

struct node {
    int data;
    node *left, *right;
    node(int data) {
        this->data = data;
        left = right = NULL;
    }
};

void insert_pre(node **root, int x) {
    if (*root == NULL) {
        (*root) = new node(x);
    } else {
        if (x < (*root)->data)
            insert_pre(&(*root)->left, x);
        else
            insert_pre(&(*root)->right, x);
    }
}

void postorder(node *root) {
    if (root != NULL) {
        postorder(root->left);
        postorder(root->right);
        cout << root->data << " ";
    }
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int n, t;
        cin >> n >> t;
        node *root = new node(t);
        for (int i = 1; i < n; i++) {
            cin >> t;
            insert_pre(&root, t);
        }
        postorder(root);
        cout << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}