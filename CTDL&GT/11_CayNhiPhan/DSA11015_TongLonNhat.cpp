#include <bits/stdc++.h>

#define ull unsigned long long int
#define ll long long int
using namespace std;

struct node {
    int data;
    node *left, *right;
    node(int data) {
        this->data = data;
        left = right = NULL;
    }
};

void addNode(int u, int v, char x, node **root) {
    if ((*root) != NULL && (*root)->data == u) {
        if (x == 'L')
            (*root)->left = new node(v);
        else
            (*root)->right = new node(v);
    } else {
        if ((*root)->right != NULL)
            addNode(u, v, x, &(*root)->right);
        if ((*root)->left != NULL)
            addNode(u, v, x, &(*root)->left);
    }
}

int maxSum(node *root, int &ans) {
    if (root == NULL) return 0;
    if (root->left == NULL && root->right == NULL)
        return root->data;
    int left = maxSum(root->left, ans);
    int right = maxSum(root->right, ans);
    if (root->left && root->right) {
        ans = max(ans, left + right + root->data);
        return (max(left, right) + root->data);
    }
    if (root->left == NULL)
        return right + root->data;
    return left + root->data;
}

void solve() {
    int T;
    cin >> T;
    while (T--) {
        int n, u, v;
        char x;
        cin >> n >> u >> v >> x;
        node *root = new node(u);
        addNode(u, v, x, &root);
        for (int i = 1; i < n; i++) {
            cin >> u >> v >> x;
            addNode(u, v, x, &root);
        }
        int ans = INT_MIN;
        maxSum(root, ans);
        cout << ans << "\n";
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}