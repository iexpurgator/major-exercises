#include <bits/stdc++.h>

#define ull unsigned long long int
#define ll long long int
using namespace std;

struct node {
    int data;
    node *left, *right;
    node(int data) {
        this->data = data;
        left = right = NULL;
    }
};

void addNode(int u, int v, char x, node **root) {
    if ((*root) != NULL && (*root)->data == u) {
        if (x == 'L')
            (*root)->left = new node(v);
        else
            (*root)->right = new node(v);
    } else {
        if ((*root)->right != NULL)
            addNode(u, v, x, &(*root)->right);
        if ((*root)->left != NULL)
            addNode(u, v, x, &(*root)->left);
    }
}

void levelOrder(node *root) {
    stack<node *> S;
    queue<node *> Q;
    Q.push(root);
    S.push(root);
    while (!Q.empty()) {
        node *tmp = Q.front();
        Q.pop();
        if (tmp->right) {
            Q.push(tmp->right);
            S.push(tmp->right);
        }
        if (tmp->left) {
            Q.push(tmp->left);
            S.push(tmp->left);
        }
    }
    while (!S.empty()) {
        cout << S.top()->data << ' ';
        S.pop();
    }
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int n, u, x;
        char c;
        cin >> n >> u >> x >> c;
        node *root = new node(u);
        addNode(u, x, c, &root);
        for (int i = 1; i < n; ++i) {
            cin >> u >> x >> c;
            addNode(u, x, c, &root);
        }
        levelOrder(root);
        cout << '\n';
        delete root;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}