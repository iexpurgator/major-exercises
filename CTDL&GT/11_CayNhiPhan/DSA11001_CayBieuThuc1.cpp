#include <bits/stdc++.h>

#define ull unsigned long long int
#define ll long long int
using namespace std;

struct node {
    char data;
    node *left, *right;
    node(char data) {
        this->data = data;
        left = right = NULL;
    }
};

bool isOp(char c) {
    return (c == '+' || c == '-' || c == '*' || c == '/');
}

void printInorder(node* root) {
    if (root == NULL) return;
    printInorder(root->left);
    cout << root->data;
    printInorder(root->right);
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        string s;
        cin >> s;
        stack<node *> st;
        for (char &i : s) {
            if (isOp(i)) {
                node *ro = new node(i);
                node *r = (st.top());
                st.pop();
                node *l = (st.top());
                st.pop();
                ro->left = l;
                ro->right = r;
                st.push(ro);
            } else {
                st.push(new node(i));
            }
        }
        printInorder(st.top());
        cout << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}