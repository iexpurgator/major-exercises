#include <bits/stdc++.h>

#define ull unsigned long long int
#define ll long long int
using namespace std;

struct node {
    int data;
    node *left, *right;
    node(int data) {
        this->data = data;
        left = right = NULL;
    }
};

node *insert_level(node *root, int val) {
    if (root == NULL)
        return new node(val);
    if (val < root->data)
        root->left = insert_level(root->left, val);
    else
        root->right = insert_level(root->right, val);
    return root;
}

void inorder(node *root, int *b) {
    if (root != NULL) {
        b[0] = root->data;
        inorder(root->left, b + 1);
        inorder(root->right, b + 1);
    }
}

bool isInorder(int arr[], int n) {
    if (n == 0 || n == 1)
        return true;
    for (int i = 1; i < n; i++)
        if (arr[i - 1] > arr[i])
            return false;
    return true;
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int n;
        cin >> n;
        int a[n + 5];
        for (int i = 0; i < n; ++i)
            cin >> a[i];
        cout << isInorder(a, n) << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}