#include <bits/stdc++.h>

#define ull unsigned long long int
#define ll long long int
using namespace std;

struct node {
    int data;
    node *left, *right;
    node(int data) {
        this->data = data;
        left = right = NULL;
    }
};

void addNode(int u, int v, char x, node **root) {
    if ((*root) != NULL && (*root)->data == u) {
        if (x == 'L')
            (*root)->left = new node(v);
        else
            (*root)->right = new node(v);
    } else {
        if ((*root)->right != NULL)
            addNode(u, v, x, &(*root)->right);
        if ((*root)->left != NULL)
            addNode(u, v, x, &(*root)->left);
    }
}

void printLevelOrder_spiral(node *root) {
    if (root == NULL) return;
    stack<node *> s1, s2;
    s1.push(root);
    while (!s1.empty() || !s2.empty()) {
        while (!s1.empty()) {
            node *tmp = s1.top();
            s1.pop();
            cout << tmp->data << ' ';
            if (tmp->right)
                s2.push(tmp->right);
            if (tmp->left)
                s2.push(tmp->left);
        }

        while (!s2.empty()) {
            node *tmp = s2.top();
            s2.pop();
            cout << tmp->data << ' ';
            if (tmp->left)
                s1.push(tmp->left);
            if (tmp->right)
                s1.push(tmp->right);
        }
    }
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int n, u, x;
        char c;
        cin >> n >> u >> x >> c;
        node *root = new node(u);
        addNode(u, x, c, &root);
        for (int i = 1; i < n; ++i) {
            cin >> u >> x >> c;
            addNode(u, x, c, &root);
        }
        printLevelOrder_spiral(root);
        cout << '\n';
        delete root;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}