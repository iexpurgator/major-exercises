
#include <bits/stdc++.h>
using namespace std;
/**
 Construct a tree from Inorder and Level order traversals
 **/
struct node {
    int data;
    struct node *left, *right;
    node(int data) {
        this->data = data;
        left = right = NULL;
    }
};

int search(int arr[], int begin, int end, int data) {
    for (int i = begin; i <= end; i++)
        if (arr[i] == data)
            return i;
    return -1;
}

void printarr(int* a, int n) {
    for (int i = 0; i < n; ++i)
        cout << a[i] << ' ';
    cout << '\n';
}

node* buildTree(int* in, int* level,
                int begin, int end, int n) {
    if (n <= 0)
        return NULL;
    node* root = new node(level[0]);
    int index = search(in, begin, end, root->data);
    // cout << "index: " << index << '\n';  // for debug
    unordered_set<int> s;
    for (int i = begin; i < index; i++)
        s.insert(in[i]);
    int lLevel[s.size()];
    int rLevel[end - begin - s.size()];
    int li = 0, ri = 0;
    for (int i = 1; i < n; i++) {
        if (s.find(level[i]) != s.end())
            lLevel[li++] = level[i];
        else
            rLevel[ri++] = level[i];
    }
    // cout << "left: ";                          // for debug
    // printarr(lLevel, s.size());                // for debug
    // cout << "right: ";                         // for debug
    // printarr(rLevel, end - begin - s.size());  // for debug
    root->left = buildTree(in, lLevel, begin, index - 1, index - begin);
    root->right = buildTree(in, rLevel, index + 1, end, end - index);
    return root;
}

void printPostorder(node* node) {
    if (node == NULL) return;
    printPostorder(node->left);
    printPostorder(node->right);
    cout << node->data << " ";
}

void solve() {
    int T;
    cin >> T;
    while (T--) {
        int n;
        cin >> n;
        int in[n];
        for (int i = 0; i < n; ++i)
            cin >> in[i];
        int level[n];
        for (int i = 0; i < n; ++i)
            cin >> level[i];
        node* post = buildTree(in, level, 0, n - 1, n);
        printPostorder(post);
        cout << '\n';
        delete post;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}