#include <bits/stdc++.h>
using namespace std;

struct node {
    int data;
    node *left, *right;
    node(int data) {
        this->data = data;
        left = right = NULL;
    }
};

bool isLeaf(node *root) {
    if (!root)
        return false;
    if (!root->left && !root->right)
        return true;
    return false;
}

long long SumRightLeaf(node *root) {
    long long ans = 0LL;
    if (root != NULL) {
        if (isLeaf(root->right))
            ans += root->right->data;
        else
            ans += SumRightLeaf(root->right);
        ans += SumRightLeaf(root->left);
    }
    return ans;
}

node *init(int n) {
    node *ro = NULL;
    int a, b;
    char c;
    unordered_map<int, node *> m;
    node *cha;
    while (n--) {
        cin >> a >> b >> c;
        if (m.find(a) == m.end()) {
            m[a] = new node(a);
            if (ro == NULL) ro = m[a];
        }
        cha = m[a];
        m[b] = new node(b);  // node con
        if (c == 'L')
            cha->left = m[b];  // left
        else
            cha->right = m[b];  // right
    }
    return ro;
}

void solve() {
    int T;
    cin >> T;
    while (T--) {
        int n;
        cin >> n;
        node *root = init(n);
        cout << SumRightLeaf(root) << "\n";
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}