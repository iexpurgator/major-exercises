#include <bits/stdc++.h>

#define ull unsigned long long int
#define ll long long int
using namespace std;

struct node {
    int data;
    node *left, *right;
    node(int data) {
        this->data = data;
        left = right = NULL;
    }
};

node *BalancedBST(vector<int> pre, int st, int en) {
    if (st > en) return NULL;
    int m = (st + en) / 2;
    node *root = new node(pre[m]);
    root->left = BalancedBST(pre, st, m - 1);
    root->right = BalancedBST(pre, m + 1, en);
    return root;
}

void printPostorder(node* root) {
    if (root == NULL) return;
    printPostorder(root->left);
    printPostorder(root->right);
    cout << root->data << " ";
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int n;
        cin >> n;
        vector<int> pre(n);
        for (int &i : pre) cin >> i;
        sort(pre.begin(), pre.end());
        node *root = BalancedBST(pre, 0, n - 1);
        printPostorder(root);
        cout << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}