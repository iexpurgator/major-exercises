#include <bits/stdc++.h>

#define ull unsigned long long int
#define ll long long int
using namespace std;

struct node {
    int data;
    node *left, *right;
    node(int data) {
        this->data = data;
        left = right = NULL;
    }
};

void addNode(int u, int v, char x, node **r1) {
    if ((*r1) != NULL && (*r1)->data == u) {
        if (x == 'L')
            (*r1)->left = new node(v);
        else
            (*r1)->right = new node(v);
    } else {
        if ((*r1)->right != NULL)
            addNode(u, v, x, &(*r1)->right);
        if ((*r1)->left != NULL)
            addNode(u, v, x, &(*r1)->left);
    }
}

bool equal(node *r1, node *r2) {
    if (r1 == NULL && r2 == NULL) return true;
    queue<node *> q;
    q.push(r1);
    queue<node *> q2;
    q2.push(r2);
    while (q.empty() == false && q2.empty() == false) {
        node *branch = q.front();
        q.pop();
        node *b2 = q2.front();
        q2.pop();
        // cout << branch->data << " " << b2->data << "\n";
        if (branch->data != b2->data) return false;
        if (branch->left != NULL && b2->left != NULL)
            q.push(branch->left), q2.push(b2->left);
        if (branch->right != NULL && b2->right != NULL)
            q.push(branch->right), q2.push(b2->right);
    }
    return true;
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int n, m, u, x;
        char c;
        //
        cin >> n >> u >> x >> c;
        node *r1 = new node(u);
        addNode(u, x, c, &r1);
        for (int i = 1; i < n; ++i) {
            cin >> u >> x >> c;
            addNode(u, x, c, &r1);
        }
        //
        cin >> m >> u >> x >> c;
        node *r2 = new node(u);
        addNode(u, x, c, &r2);
        for (int i = 1; i < m; ++i) {
            cin >> u >> x >> c;
            addNode(u, x, c, &r2);
        }
        if (m != n) {
            cout << 0 << '\n';
            continue;
        }
        cout << equal(r1, r2) << '\n';
        delete r1;
        delete r2;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}