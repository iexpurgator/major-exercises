#include <bits/stdc++.h>
using namespace std;

struct node {
    int data;
    node *left, *right;
    node(int data) {
        this->data = data;
        left = right = NULL;
    }
};

void addNode(int u, int v, char x, node **root) {
    if ((*root) != NULL && (*root)->data == u) {
        if (x == 'L')
            (*root)->left = new node(v);
        else
            (*root)->right = new node(v);
    } else {
        if ((*root)->right != NULL)
            addNode(u, v, x, &(*root)->right);
        if ((*root)->left != NULL)
            addNode(u, v, x, &(*root)->left);
    }
}

bool check(int level, int &tree_level, node *root) {
    level = level + 1;
    if (!root->left && !root->right) {
        if (tree_level == -1)
            tree_level = level;
        if (tree_level != -1 && level != tree_level)
            return false;
        if (tree_level != -1 && level == tree_level)
            return true;
    }
    if (root->left)
        if (!check(level, tree_level, root->left))
            return false;
    if (root->right)
        if (!check(level, tree_level, root->right))
            return false;
    return true;
}

void solve() {
    int T;
    cin >> T;
    while (T--) {
        int n, u, v;
        char x;
        cin >> n >> u >> v >> x;
        node *root = new node(u);
        addNode(u, v, x, &root);
        for (int i = 1; i < n; i++) {
            cin >> u >> v >> x;
            addNode(u, v, x, &root);
        }
        int level = -1;
        cout << check(0, level, root) << endl;
        delete root;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}