#include <bits/stdc++.h>

#define ull unsigned long long int
#define ll long long int
using namespace std;

struct node {
    int data;
    node *left, *right;
    node(int data) {
        this->data = data;
        left = right = NULL;
    }
};

node *insert(node *root, int val) {
    if (root == NULL)
        return new node(val);
    if (val < root->data)
        root->left = insert(root->left, val);
    else
        root->right = insert(root->right, val);
    return root;
}

int NodeTrungGian(node *root) {
    if (root == NULL || (root->left == NULL && root->right == NULL)) return 0;
    return 1 + NodeTrungGian(root->left) + NodeTrungGian(root->right);
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        node *root = NULL;
        int n;
        cin >> n;
        while (n--) {
            int x;
            cin >> x;
            root = insert(root, x);
        }
        cout << NodeTrungGian(root) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}