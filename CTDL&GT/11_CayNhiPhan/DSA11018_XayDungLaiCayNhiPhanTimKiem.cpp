#include <bits/stdc++.h>

#define ull unsigned long long int
#define ll long long int
using namespace std;

struct node {
    int data;
    node *left, *right;
    node(int data) {
        this->data = data;
        left = right = NULL;
    }
};

node *insert_level(node *root, int val) {
    if (root == NULL)
        return new node(val);
    if (val < root->data)
        root->left = insert_level(root->left, val);
    else
        root->right = insert_level(root->right, val);
    return root;
}

void preorder(node *root) {
    if (root != NULL) {
        cout << root->data << " ";
        preorder(root->left);
        preorder(root->right);
    }
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int n;
        cin >> n;
        node *root = NULL;
        for (int i = 0, u; i < n; i++) {
            cin >> u;
            root = insert_level(root, u);
        }
        preorder(root);
        cout << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}