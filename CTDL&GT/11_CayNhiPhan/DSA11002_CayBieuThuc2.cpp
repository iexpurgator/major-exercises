#include <bits/stdc++.h>

#define ull unsigned long long int
#define ll long long int
using namespace std;

char isOp(string s) {
    if (s == "+") return 3;
    if (s == "-") return 5;
    if (s == "*") return 7;
    if (s == "/") return 9;
    return 0;
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int n;
        cin >> n;
        string s[n];
        for (string &i : s) cin >> i;
        queue<long> st;
        for (int i = n - 1; i >= 0; --i) {
            char Op = isOp(s[i]);
            if (Op) {
                long b = st.front(); 
                st.pop();
                long a = st.front();
                st.pop();
                if (Op == 3) st.push(a + b);
                if (Op == 5) st.push(a - b);
                if (Op == 7) st.push(a * b);
                if (Op == 9) st.push(a / b);
            } else {
                st.push(stol(s[i]));
            }
        }
        cout << st.front() << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}