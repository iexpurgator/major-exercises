#include <bits/stdc++.h>

#define ull unsigned long long int
#define ll long long int
using namespace std;
// sample DSA11017
struct node {
    int data;
    node *left, *right;
    node(int data) {
        this->data = data;
        left = right = NULL;
    }
};

node *insert_pre(node *root, int val) {
    if (root == NULL)
        return new node(val);
    if (val < root->data)
        root->left = insert_pre(root->left, val);
    else
        root->right = insert_pre(root->right, val);
    return root;
}

void postorder(node *root) {
    if (root != NULL) {
        postorder(root->left);
        postorder(root->right);
        cout << root->data << " ";
    }
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        node *root = NULL;
        int n;
        cin >> n;
        while (n--) {
            int x;
            cin >> x;
            root = insert_pre(root, x);
        }
        postorder(root);
        cout << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}