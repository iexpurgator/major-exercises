#include <bits/stdc++.h>
using namespace std;

int n;
vector<int> G[100005];

int DFS(int u) {
    vector<int> depth(100005, 0);
    vector<bool> vs(100005, 0);
    stack<int> s;
    s.push(u);
    vs[u] = true;
    while (!s.empty()) {
        u = s.top();
        s.pop();
        for (int i : G[u]) {
            if (!vs[i]) {
                depth[i] = depth[u] + 1;
                vs[i] = true;
                s.push(u);
                s.push(i);
                break;
            }
        }
    }
    return max(1, *max_element(depth.begin(), depth.end()));
}

void solve() {
    int T;
    cin >> T;
    while (T--) {
        int n, a, b;
        for(int i = 0; i < 100005; ++i)
            G[i].clear();
        cin >> n;
        for (int i = 1; i < n; i++) {
            cin >> a >> b;
            G[a].push_back(b);
        }
        cout << DFS(1) << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}