#include <bits/stdc++.h>
using namespace std;

int T;

void solve() {
    cin >> T;
    while (T--) {
        long cnt = 0;
        long long sk;
        queue <string> q;
        stack <string> res;
        q.push("6");
        q.push("8");
        res.push("6");
        res.push("8");
        cin >> sk;
        while(!q.empty()){
            string k = q.front();
            q.pop();
            if(k.length() + 1 > sk) break;
            string k1 = k + "6";
            string k2 = k + "8";
            q.push(k1);
            q.push(k2);
            res.push(k1);
            res.push(k2);
        }
        cout << res.size() << '\n';
        while(!res.empty()){
            cout << res.top() << ' ';
            res.pop();
        } cout << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
} 