#include <bits/stdc++.h>
using namespace std;

int T;

void solve() {
    cin >> T;
    while (T--) {
        long cnt = 0;
        long long sk;
        queue < long long > q;
        q.push(1);
        cin >> sk;
        while(!q.empty()){
            cnt++;
            long long kk = q.front();
            if(kk % sk == 0){
                cout << kk << '\n';
                break;
            } q.pop();
            q.push(kk*10);
            q.push(kk*10+1);
        }
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
} 