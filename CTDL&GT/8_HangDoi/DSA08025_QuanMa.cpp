#include <bits/stdc++.h>
using namespace std;

int T;
int mx[8] = {1, 2, -1, 2, -2, -1, -2, 1};
int my[8] = {2, 1, 2, -1, -1, -2, 1, -2};

struct knight {
    char c;
    int i;
    int cnt;
};

bool isSafe(knight t, int vs[10][10]) {
    return (t.c <= 'h' && t.c >= 'a' &&
            t.i <= 8 && t.i >= 1 &&
            vs[t.c - 'a' + 1][t.i] == 0);
}

int bfs(knight s, knight t) {
    queue<knight> q;
    q.push(s);
    int vs[10][10] = {0};
    while (!q.empty()) {
        knight u = q.front();
        q.pop();
        if (u.c == t.c && u.i == t.i) {
            return u.cnt;
        }
        for (int i = 0; i < 8; ++i) {
            knight t = {(u.c + mx[i]), (u.i + my[i]), u.cnt + 1};
            if (isSafe(t, vs)) {
                vs[t.c - 'a' + 1][t.i] = 1;
                q.push(t);
            }
        }
    }
    return 0;
}

void solve() {
    cin >> T;
    cin.ignore();
    while (T--) {
        char sc, tc;
        int si, ti;
        string s;
        getline(cin, s);
        sc = s[0];
        si = (int)s[1] - '0';
        tc = s[3];
        ti = (int)s[4] - '0';
        knight st = {sc, si, 0};
        knight t = {tc, ti, 0};
        cout << bfs(st, t) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}