#include <bits/stdc++.h>
using namespace std;
int T;

struct st {
    int s;
    int count;
};

bool check(int t) {
    bool visited[10] = {0};
    while (t) {
        if (visited[t % 10] || t % 10 > 5) return false;
        visited[t % 10] = 1;
        t /= 10;
    }
    return true;
}

void solve() {
    cin >> T;
    while (T--) {
        st start = {0, 0};
        int t;
        queue<st> q;
        cin >> start.s >> t;
        if (check(start.s))
            start.count = 1;
        while (start.s != t) {
            if (check(++start.s))
                start.count++;
        }
        cout << start.count << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
    return 0;
}