#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define MOD 1000000007L;
#define ln '\n'
#define dbg(x) cout << #x << " = " << x << '\n'
#define all(x) (x).begin(), (x).end()
#define sz(x) ((ll)(x).size())

struct h {
    int cn[6];
    int step;
};
h r, l, a, t;
int n = 6;

void right(h cur) {
    r.cn[0] = cur.cn[0];
    r.cn[1] = cur.cn[4];
    r.cn[2] = cur.cn[1];
    r.cn[3] = cur.cn[3];
    r.cn[4] = cur.cn[5];
    r.cn[5] = cur.cn[2];
    r.step = cur.step + 1;
}

void left(h cur) {
    l.cn[0] = cur.cn[3];
    l.cn[1] = cur.cn[0];
    l.cn[2] = cur.cn[2];
    l.cn[3] = cur.cn[4];
    l.cn[4] = cur.cn[1];
    l.cn[5] = cur.cn[5];
    l.step = cur.step + 1;
}

bool check(h cur) {
    for (int i = 0; i < n; ++i)
        if (cur.cn[i] != t.cn[i]) return false;
    return true;
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        a.step = 0;
        for (int i = 0; i < n; ++i) cin >> a.cn[i];
        for (int i = 0; i < n; ++i) cin >> t.cn[i];
        // 1 2 3 4 5 6
        // 1 5 2 4 6 3
        // 4 1 3 5 2 6
        queue<h> q;
        q.push(a);
        while (true) {
            h fr = q.front();
            q.pop();
            if (check(fr)) {
                cout << fr.step << ln;
                break;
            } else {
                left(fr);
                q.push(l);
                right(fr);
                q.push(r);
            }
        }
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}