#include <bits/stdc++.h>
using namespace std;
int T;

int n;
string s, res;
set<string> dic;

int BFS() {
    queue<string> q;
    q.push(s);
    n = s.size();
    int step = 0;
    while (!q.empty()) {
        int x = q.size();
        ++step;
        for (int i = 0; i < x; i++) {
            string word = q.front();
            q.pop();
            if (word == res) return step;
            for (int pos = 0; pos < n; pos++) {
                char origin = word[pos];
                for (char c = 'A'; c <= 'Z'; c++) {
                    word[pos] = c;
                    if (word == res)
                        return step + 1;
                    if (dic.find(word) == dic.end())
                        continue;
                    q.push(word);
                    dic.erase(word);
                }
                word[pos] = origin;
            }
        }
    }
    return 0;
}

void solve() {
    // T = 1;
    cin >> T;
    while (T--) {
        cin >> n;
        string x;
        cin >> s >> res;
        for (int i = 0; i < n; ++i) {
            cin >> x;
            if (x == s) continue;
            dic.insert(x);
        }
        cout << BFS() << '\n';
        dic.clear();
    }
}

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
    return 0;
}