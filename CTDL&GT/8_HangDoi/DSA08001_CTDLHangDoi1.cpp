#include <bits/stdc++.h>
using namespace std;

int T;

void solve() {
    cin >> T;
    while (T--) {
        int n;
        queue<long> q;
        cin >> n;
        while (n--) {
            int proc;
            cin >> proc;
            switch (proc) {
                case 1: // size
                    cout << q.size() << '\n';
                    break;
                case 2: // empty?
                    cout << (q.empty() ? "YES\n" : "NO\n");
                    break;
                case 3: // push
                    int k;
                    cin >> k;
                    q.push(k);
                    break;
                case 4: // pop
                    if(!q.empty()) q.pop();
                    break;
                case 5: // begin
                    if(q.empty()) cout << -1 << '\n';
                    else cout << q.front() << '\n';
                    break;
                case 6: // end
                    if(q.empty()) cout << -1 << '\n';
                    else cout << q.back() << '\n';
                    break;
            }
        }
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}