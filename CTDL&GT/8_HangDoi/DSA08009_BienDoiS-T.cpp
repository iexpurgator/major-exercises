#include <bits/stdc++.h>
using namespace std;
int T;

struct st {
    int s;
    int count;
};

void solve() {
    cin >> T;
    while (T--) {
        int a, t;
        queue<st> q;
        bool visited[10000] = {0};

        cin >> a >> t;
        q.push({a, 0});
        visited[a] = 1;
        while (q.front().s != t && !q.empty()) {
            st u = q.front();
            q.pop();
            if (u.s - 1 > 0 && !visited[u.s - 1]) {
                q.push({u.s-1, u.count+1});
                visited[u.s-1] = true;
            }
            if (u.s * 2 < 10000 && !visited[u.s * 2]) {
                q.push({u.s*2, u.count+1});
                visited[u.s*2] = true;
            }
        }
        cout << q.front().count << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
    return 0;
}