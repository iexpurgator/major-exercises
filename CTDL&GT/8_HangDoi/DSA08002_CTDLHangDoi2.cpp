#include <bits/stdc++.h>
using namespace std;

int T;

void solve() {
    // cin >> T;
    T = 1;
    while (T--) {
        int n;
        queue<long> q;
        cin >> n;
        while (n--) {
            string s;
            cin >> s;
            if(s == "PUSH"){
                int k;
                cin >> k;
                q.push(k);
            } else if (s == "POP") {
                if(!q.empty()) q.pop();
            } else if (s == "PRINTFRONT") {
                if(q.empty()) cout << "NONE\n";
                else cout << q.front() << '\n';
            }
        }
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}