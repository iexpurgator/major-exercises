#include <bits/stdc++.h>
using namespace std;

int T;

void solve() {
    cin >> T;
    while (T--) {
        int n;
        string s = "1";
        queue < string > q;
        q.push(s);
        cin >> n;
        while(n--){
            s = q.front(); q.pop();
            cout << s << ' ';
            string s0 = s + '0';
            q.push(s0);
            string s1 = s + '1';
            q.push(s1);
        }
        cout << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}