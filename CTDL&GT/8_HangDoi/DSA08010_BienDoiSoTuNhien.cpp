#include <bits/stdc++.h>
using namespace std;
int T;

#define F first
#define S second

int solve(int n) {
    queue<pair<int, int>> a;
    a.push({n, 0});
    set<int> s;
    s.insert(n);
    while (!a.empty()) {
        pair<int, int> u = a.front();
        a.pop();
        if (u.F == 1)
            return u.S;
        else {
            for (int i = 2; i * i < u.F; i++) {
                if (u.F % i == 0) {
                    int k = u.F / i;
                    if (k == 1) return u.S + 1;
                    if (s.find(k) == s.end() && k > 1) {
                        if (k == 2) return u.S + 2;
                        a.push({k, u.S + 1});
                        s.insert(k);
                    }
                }
            }
            int k = sqrt(u.F);
            if (k * k == u.F && s.find(k) == s.end()) {
                if (k == 1) return u.S + 1;
                if (k == 2) return u.S + 2;
                a.push({k, u.S + 1});
                s.insert(k);
            }
            if (s.find(u.F - 1) == s.end()) {
                if (u.F == 2) return u.S + 1;
                if (u.F - 1 == 2) return u.S + 2;
                a.push({u.F - 1, u.S + 1});
                s.insert(u.F - 1);
            }
        }
    }
}

void solve() {
    cin >> T;
    while (T--) {
        int n;
        cin >> n;
        cout << solve(n) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
    return 0;
}