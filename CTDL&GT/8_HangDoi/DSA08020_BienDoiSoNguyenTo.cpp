#include <bits/stdc++.h>
using namespace std;
int T;

struct st {
    string s;
    int count;
};

bool isPrime(int i) {
    if (i == 2) return 1;
    if (i % 2 == 0) return 0;
    for (int j = 2; j <= sqrt(i); ++j) {
        if (i % j == 0) return false;
    }
    return true;
}

bool check(int t) {
    bool visited[10] = {0};
    while (t) {
        if (visited[t % 10] || t % 10 > 5) return false;
        visited[t % 10] = 1;
        t /= 10;
    }
    return true;
}

void solve() {
    cin >> T;
    while (T--) {
        string s;
        int t;
        bool visited[9005] = {0};  // stoi(s)-1000
        cin >> s >> t;
        queue<st> q;
        q.push({s, 0});
        visited[stoi(s) - 1000] = true;
        while (stoi(q.front().s) != t && !q.empty()) {
            st u = q.front();
            q.pop();
            for (char c = '1'; c <= '9'; c += 2) {
                string s1 = u.s;
                s1[3] = c;
                int n = stoi(s1);
                if (!visited[n - 1000] && isPrime(n)) {
                    // cout << n << '\n';
                    q.push({s1, u.count + 1});
                    visited[n - 1000] = true;
                }
            }
            for (int i = 2; i > 0; --i) {
                for (char c = '0'; c <= '9'; ++c) {
                    string s1 = u.s;
                    s1[i] = c;
                    int n = stoi(s1);
                    if (!visited[n - 1000] && isPrime(n)) {
                        // cout << n << '\n';
                        q.push({s1, u.count + 1});
                        visited[n - 1000] = true;
                    }
                }
            }
            for (char c = '1'; c <= '9'; ++c) {
                string s1 = u.s;
                s1[0] = c;
                int n = stoi(s1);
                if (!visited[n - 1000] && isPrime(n)) {
                    // cout << n << '\n';
                    q.push({s1, u.count + 1});
                    visited[n - 1000] = true;
                }
            }
        }
        cout << q.front().count << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
    return 0;
}