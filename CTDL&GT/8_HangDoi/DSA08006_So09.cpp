#include <bits/stdc++.h>
using namespace std;

int T;

void solve() {
    cin >> T;
    while (T--) {
        int n;
        string s = "9";
        queue < string > q;
        q.push(s);
        cin >> n;
        // n = T+1;
        // cout << n << " ";
        if(stoull(s) % n == 0){
            cout << s << '\n';
            continue;
        }
        while(1){
            s = q.front(); q.pop();
            string s0 = s + '0';
            if(stoull(s0) % n == 0) {
                cout << s0 << '\n';
                break;
            }
            q.push(s0);
            string s1 = s + '9';
            if(stoull(s1) % n == 0) {
                cout << s1 << '\n';
                break;
            }
            q.push(s1);
        }
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}