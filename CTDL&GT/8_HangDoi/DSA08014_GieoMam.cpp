#include <bits/stdc++.h>
using namespace std;
int T;

int a[505][505];
bool vs[505][505];
int n, m;

struct hoan {
    int x;
    int y;
    int day;
};

int bfs() {
    queue<hoan> q;
    bool ok = 1;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            if (a[i][j] == 2) q.push({i, j, 0});
            if (a[i][j] == 1 && a[i][j + 1] == 0 &&
                a[i][j - 1] == 0 && a[i + 1][j] == 0 &&
                a[i - 1][j] == 0) ok = 0;
        }
    }
    if(!ok) return -1;
    int ans = 0;
    while (!q.empty()) {
        hoan u = q.front();
        q.pop();
        if (vs[u.x][u.y + 1] == 0 && a[u.x][u.y + 1] == 1) {
            q.push({u.x, u.y + 1, u.day + 1});
            vs[u.x][u.y + 1] = 1;
        }
        if (vs[u.x][u.y - 1] == 0 && a[u.x][u.y - 1] == 1) {
            q.push({u.x, u.y - 1, u.day + 1});
            vs[u.x][u.y - 1] = 1;
        }
        if (vs[u.x + 1][u.y] == 0 && a[u.x + 1][u.y] == 1) {
            q.push({u.x + 1, u.y, u.day + 1});
            vs[u.x + 1][u.y] = 1;
        }
        if (vs[u.x - 1][u.y] == 0 && a[u.x - 1][u.y] == 1) {
            q.push({u.x - 1, u.y, u.day + 1});
            vs[u.x - 1][u.y] = 1;
        }
        ans = u.day;
    }
    return ans;
}

void solve() {
    // T = 1;
    cin >> T;
    while (T--) {
        cin >> n >> m;
        memset(vs, 0, sizeof(vs));
        memset(a, 0, sizeof(a));
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                cin >> a[i][j];
            }
        }
        cout << bfs() << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
    return 0;
}