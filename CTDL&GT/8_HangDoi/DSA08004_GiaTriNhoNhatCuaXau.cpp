#include <bits/stdc++.h>
using namespace std;
int T;

void solve() {
    cin >> T;
    while (T--) {
        int n, i = 0;
        int a[26] = {0};
        long long res = 0LL;
        string str;
        priority_queue<int> q;

        cin >> n >> str;
        for (char &c : str)
            a[c - 'A']++;
        sort(a, a + 26, greater<int>());
        while (i < 26 && a[i] != 0)
            q.push(a[i++]);
        while (n-- && q.top() != 0) {
            q.push(q.top() - 1);
            q.pop();
        }
        while (!q.empty()) {
            res += (long long)q.top() * q.top();
            q.pop();
        }
        cout << res << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
    return 0;
}