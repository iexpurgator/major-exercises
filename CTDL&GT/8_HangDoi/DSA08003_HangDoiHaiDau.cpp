#include <bits/stdc++.h>
using namespace std;

int T;

void solve() {
    // cin >> T;
    T = 1;
    while (T--) {
        int n;
        deque < int > dq;
        cin >> n;
        while(n--){
            string s;
            cin >> s;
            if(s == "PUSHFRONT"){
                int k; cin >> k;
                dq.push_front(k);
            }
            else if(s == "PRINTFRONT"){
                if(dq.empty()) cout << "NONE\n";
                else cout << dq.front() << '\n';
            }
            else if(s == "POPFRONT"){
                if(!dq.empty()) dq.pop_front();
            }
            else if(s == "PUSHBACK"){
                int k; cin >> k;
                dq.push_back(k);
            }
            else if(s == "PRINTBACK"){
                if(dq.empty()) cout << "NONE\n";
                else cout << dq.back() << '\n';
            }
            else if(s == "POPBACK"){
                if(!dq.empty()) dq.pop_back();
            }
        }
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}