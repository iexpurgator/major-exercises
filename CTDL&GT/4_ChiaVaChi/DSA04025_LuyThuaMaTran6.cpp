#include <bits/stdc++.h>
using namespace std;

#define fr(i, n) for (int i = 1; i <= n; i++)
#define ll long long
#define MOD 1000000007L
typedef vector<vector<ll> > matrix;
int T;
int K;

matrix mul(matrix A, matrix B) {
    matrix C(K + 1, vector<ll>(K + 1));
    fr(i, K) fr(j, K) fr(k, K)
        C[i][j] = ((A[i][k] * B[k][j]) % MOD + C[i][j]) % MOD;
    return C;
}

matrix pow(matrix A, int p) {
    if (p == 1) return A;
    if (p % 2) return mul(A, pow(A, p - 1));
    matrix X = pow(A, p / 2);
    return mul(X, X);
}

void solve() {
    cin >> T;
    while (T--) {
        ll n;
        cin >> K >> n;
        matrix T(K + 1, vector<ll>(K + 1));
        //
        fr(i, K) fr(j, K) cin >> T[i][j];
        T = pow(T, n);
        //
        ll sum = 0LL;
        fr(i, K)
            sum = (sum + T[i][1]) % MOD;
        cout << sum << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
    return 0;
}