#include <bits/stdc++.h>
using namespace std;
#define M_7 1000000007L
#define ll long long int
int T;
int n, k;
int *a;

int divide (int l, int r){
    if (l > r) return 0;
    int mid = (l+r)/2;
    if (a[mid] == k) return mid+1;
    if (a[mid] > k) return divide(l, mid-1);
    return divide(mid+1, r);
}

void solve() {
    cin >> T;
    while(T--){
        cin >> n >> k;
        a = new int[n];
        for (int i=0;i < n; ++i){
            cin >> a[i];
        }
        int res = divide(0, n);
        if(res) cout << res << '\n';
        else cout << "NO\n";
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}
