#include <bits/stdc++.h>
using namespace std;
#define M_7 1000000007L
#define ll long long int
int T;
long n, k;
ll *a;

int divide (int l, int r){
    if (l > r) return -1;
    int mid = (l+r)/2;
    if (a[mid] == k) return mid+1;
    if (a[mid] < k && a[mid+1] > k) return mid+1;
    if (a[mid] > k) return divide(l, mid-1);
    return divide(mid+1, r);
}

void solve() {
    cin >> T;
    while(T--){
        cin >> n >> k;
        a = new ll[n];
        for (int i=0;i < n; ++i){
            cin >> a[i];
        }
        cout << divide(0, n) << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}
