#include <bits/stdc++.h>
using namespace std;
#define M_7 1000000007L
#define ll long long int

int T;
int n;
ll k;
ll x[94];

char DaC(int n, ll k) {
    if (n == 1 && k == 1) return 'A';
    if (n == 2 && k == 1) return 'B';
    if (k <= x[n - 2]) return DaC(n - 2, k);
    return DaC(n - 1, k - x[n - 2]);
}

void solve() {
    x[1] = 1;
    for (int i = 2; i < 93; i++)
        x[i] = x[i - 2] + x[i - 1];
    cin >> T;
    while (T--) {
        cin >> n >> k;
        cout << DaC(n, k) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}
