#include <iostream>
#include <string>
using namespace std;

int main() {
    int T;
    cin >> T;
    while (T--) {
        string a, b;
        cin >> a >> b;
        cout << stol(a, 0, 2) * stol(b, 0, 2) << '\n';
    }
    return 0;
}
