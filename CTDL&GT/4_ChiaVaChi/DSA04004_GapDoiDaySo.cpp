#include <bits/stdc++.h>
using namespace std;
#define M_7 1000000007L
#define ll long long int
int T;
int n;
ll k;

int divide (int n, ll len, ll k){
    if(n==1) return 1;
    if(len == k) return n;
    if(k > len) k = len - (k-len);
    return divide(n-1, len/2, k);
}

void solve() {
    cin >> T;
    while(T--){
        cin >> n >> k;
        int res = divide(n,1LL<<(n-1), k);
        cout << res << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}
