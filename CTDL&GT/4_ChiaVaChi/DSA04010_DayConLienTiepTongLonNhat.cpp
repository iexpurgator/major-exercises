#include <bits/stdc++.h>
using namespace std;

int T, n;

int max(int a, int b, int c) {
    return max(a, max(b, c));
}

int DaC(int* a, int l, int m, int r) {
    int maxL = a[m], maxR = a[m + 1];
    int sum = maxL;
    for (int i = m - 1; i >= l; i--) {
        sum += a[i];
        if (sum > maxL)
            maxL = sum;
    }
    sum = maxR;
    for (int i = m + 2; i <= r; i++) {
        sum += a[i];
        if (sum > maxR)
            maxR = sum;
    }
    return max(maxL, maxR, maxR + maxL);
}

int DaC(int* a, int l, int r) {
    if (l == r) return a[r];
    int m = (l + r) >> 1;
    int mL = DaC(a, l, m);
    int mR = DaC(a, m + 1, r);
    int mM = DaC(a, l, m, r);
    return max(mL, mR, mM);
}


void solve() {
    cin >> T;
    while (T--) {
        cin >> n;
        int* a = new int[n];
        for (int i = 0; i < n; i++)
            cin >> a[i];
        cout << DaC(a, 0, n - 1) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}