#include <bits/stdc++.h>
using namespace std;
#define ifr(i, n) for (int i = 0; i < (n); ++i)

int T;

long long merge(long long *a, int l, int m, int r) {
    int i = 0, j = 0, k = l;
    long long count = 0;
    int nL = m - l + 1, nR = r - m;
    int *L = new int[nL];
    copy(a + l, a + l + nL, L);  // copy from a[] to L[]
    int *R = new int[nR];
    copy(a + m + 1, a + m + 1 + nR, R);

    while (i < nL && j < nR) {
        if (L[i] <= R[j])
            a[k++] = L[i++];
        else {
            a[k++] = R[j++];
            count += (long long)nL - i;
        }
    }

    while (i < nL) a[k++] = L[i++];
    while (j < nR) a[k++] = R[j++];
    return count;
}

long long mergeSort(long long *a, int l, int r) {
    long long count = 0;
    if (l < r) {
        int m = (l + r) / 2;
        count += mergeSort(a, l, m);
        count += mergeSort(a, m + 1, r);
        count += merge(a, l, m, r);
    }
    return count;
}

void solve() {
    cin >> T;
    while (T--) {
        int n;
        cin >> n;
        long long *a = new long long[n];
        ifr(i, n) cin >> a[i];
        cout << mergeSort(a, 0, n - 1) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
