#include <bits/stdc++.h>
using namespace std;
#define M_7 1000000007L
#define ll long long int
int T;
long n, m, k;
ll *a, *b;

int divide (int n, int m, int k, int l1, int l2){
    if (l1 == n) return b[l2+k-1];
    if (l2 == m) return a[l1+k-1];
    if (k == 0 || k > (n-l1) + (m-l2)) return -1;
    if (k == 1) return (a[l1] < b[l2]) ? a[l1] : b[l2];
    int curr = k/2;
    if (curr -1 >= n-l1) {
        if (a[n-1] < b[l2-curr -1])
            return b[l2+(k-(n-l1)-1)];
        else
            return divide(n, m, k-curr, l1, l2+curr);
    }
    if (curr-1>=m-l2) {
        if (b[m-1]<a[l1+curr-1])
            return a[l1+(k-(m-l2)-1)];
        else
            return divide(n, m, k-curr, l1+curr, l2);
    }
    else {
        if (a[curr + l1-1] < b[curr +l2-1])
            return divide(n, m, k-curr, l1+curr, l2);
        else
            return divide(n, m, k-curr, l1, l2+curr);
    }
}

void solve() {
    cin >> T;
    while(T--){
        cin >> n >> m >> k;
        a = new ll[n];
        b = new ll[m];
        for (int i=0;i < n; ++i){
            cin >> a[i];
        }
        for(int i=0;i<m;++i){
            cin >> b[i];
        }
        cout << divide(n, m, k, 0, 0) << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}
