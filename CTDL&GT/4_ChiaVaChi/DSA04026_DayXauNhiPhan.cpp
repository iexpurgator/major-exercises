#include <stdio.h>

int T;
long long int x[94];

bool DaC(int n, long long int k) {
    if (n == 1 && k == 1) return 0;
    if (n == 2 && k == 1) return 1;
    if (k <= x[n - 2]) return DaC(n - 2, k);
    return DaC(n - 1, k - x[n - 2]);
}

int main() {
    x[1] = 1;
    for (int i = 2; i < 93; i++)
        x[i] = x[i - 2] + x[i - 1];
    scanf("%d", &T);
    while (T--) {
        int n;
        long long int k;
        scanf("%d%lld", &n, &k);
        printf("%d\r\n", DaC(n, k));
    }
    return 0;
}
