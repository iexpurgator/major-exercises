#include <bits/stdc++.h>
using namespace std;
#define ll long long int
#define ifr(i, n) for (int i = 0; i < (n); ++i)

int T;

ll daq(ll n, ll l, ll r) {
    ll x = log(n) / log(2);                                              // độ dài của n ở dạng nhị phân
    x = (ll)(pow(2, x) - 1) * 2 + 1;                                     // độ dài của n ở khi phân tích bằng [n/2][n%2][n/2]
    ll mid = x / 2 + 1;                                                  //
    if (l == r && l == mid) return n % 2;                                // [...][n%2][...]
    if (l < mid && r < mid) return daq(n / 2, l, r);                     // [n/2][...][...]
    if (l > mid && r > mid) return daq(n / 2, l - mid, r - mid);         // [...][...][n/2]
    if (l == mid) return (ll)n % 2 + daq(n / 2, 1, r - mid);             // [n/2][n%2][...]
    if (r == mid) return (ll)n % 2 + daq(n / 2, l, mid);                 // [...][n%2][n/2]
    return (ll)daq(n / 2, l, mid - 1) + n % 2 + daq(n / 2, 1, r - mid);  // [n/2][n%2][n/2]
}

void solve() {
    cin >> T;
    while (T--) {
        ll n, l, r;
        cin >> n >> l >> r;
        cout << daq(n, l, r) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
