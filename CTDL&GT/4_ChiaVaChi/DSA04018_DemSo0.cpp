#include <bits/stdc++.h>
using namespace std;
#define M_7 1000000007L
#define ll long long int
int T;
long n;
bool *a;

int divide (int l, int r){
    if (l > r) return n*(!a[0]);
    int mid = (l+r)/2;
    if (!a[mid-1] && a[mid]) return mid;
    if (a[mid]) return divide(l, mid-1);
    return divide(mid+1, r);
}

void solve() {
    cin >> T;
    while(T--){
        cin >> n;
        a = new bool[n];
        for (int i=0;i < n; ++i){
            cin >> a[i];
        }
        cout << divide(0, n) << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}
