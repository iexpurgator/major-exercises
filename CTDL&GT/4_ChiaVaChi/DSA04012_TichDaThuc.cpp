#include <bits/stdc++.h>
using namespace std;

#define fr(i, n) for (int i = 1; i <= (n); i++)
#define ifr(i, n) for (int i = 0; i < (n); i++)
#define ll long long
#define MOD 1000000007L
typedef vector<vector<ll> > matrix;
int T;

int *mul(int *a, int *b, int n, int m) {
    int *res = new int[n + m];
    fill(res, res + n + m, 0);
    ifr(i, n) ifr(j, m)
        res[i + j] += a[i] * b[j];
    return res;
}

void solve() {
    cin >> T;
    while (T--) {
        int n, m;
        cin >> n >> m;
        int a[105], b[105];
        ifr(i, n) cin >> a[i];
        ifr(i, m) cin >> b[i];

        int *res = mul(a, b, n, m);
        ifr(i, (n + m - 1))
            printf("%d%c", res[i], (i + 2 == n + m) ? '\n' : ' ');
    }
}

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
    return 0;
}