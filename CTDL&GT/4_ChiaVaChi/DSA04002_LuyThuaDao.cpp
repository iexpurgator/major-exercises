#include <bits/stdc++.h>
using namespace std;
#define M_7 1000000007L
#define ll long long int
int T;

ll rev(ll n) {
    ll nre = 0;
    while (n) {
        nre = nre * 10 + n % 10;
        n /= 10;
    }
    return nre;
}

ll power(ll n, ll k, const long mod) {
    if (n == 0) return 0;
    if (k == 1) return n % mod;
    if (n == 1 || k == 0) return 1;
    ll sqr = power(n, k >> 1, mod);
    if (k & 1)
        return ((sqr * sqr) % mod * n) % mod;
    else
        return (sqr * sqr) % mod;
}

void solve() {
    cin >> T;
    while (T--) {
        ll n;
        cin >> n;
        cout << power(n, rev(n), M_7) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}