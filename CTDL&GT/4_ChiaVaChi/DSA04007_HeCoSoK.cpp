#include <bits/stdc++.h>
using namespace std;
int T;

int n, m, k;

string sum(string a, string b, int k) {
    string res;
    int c = 0;
    for (int i = a.size() - 1; i >= 0; i--) {
        int x = a[i] - '0';
        int y = b[i] - '0';
        res.insert(res.begin(), '0' + (x + y + c) % k);
        c = (x + y + c) / k;
    }
    if (c > 0) res.insert(res.begin(), '0' + c);
    return res;
}

void solve() {
    cin >> T;
    while (T--) {
        string a, b;
        int k;
        cin >> k >> a >> b;
        while (a.size() < b.size())
            a.insert(a.begin(), '0');
        while (b.size() < a.size())
            b.insert(b.begin(), '0');
        cout << sum(a, b, k) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
    return 0;
}