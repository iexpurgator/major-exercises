#include <bits/stdc++.h>
using namespace std;
#define M_7 1000000007L
#define ll long long int
int T;
long n, m, k;
ll *a, *b;

int divide (int l, int r){
    int mid = (l+r)/2;
    if(a[mid] != b[mid] && (mid-1 < l || a[mid-1] == b[mid-1]))
        return mid+1;
    if(a[mid] == b[mid])
        return divide(mid+1, r);
    return divide(l, mid-1);
}

void solve() {
    cin >> T;
    while(T--){
        k = 0;
        cin >> n;
        a = new ll[n];
        b = new ll[n-1];
        for (int i=0;i < n; ++i){
            cin >> a[i];
        }
        for(int i=0;i<n-1;++i){
            cin >> b[i];
        }
        cout << divide(0, n-1) << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}
