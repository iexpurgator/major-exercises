#include <bits/stdc++.h>
using namespace std;

#define fr(i, n) for (int i = 1; i <= n; i++)
#define ll long long
#define MOD 1000000007L
typedef vector<vector<ll> > matrix;
const int K = 2;
int T;

matrix mul(matrix A, matrix B) {
    matrix C(K + 1, vector<ll>(K + 1));
    fr(i, K) fr(j, K) fr(k, K)
        C[i][j] = ((A[i][k] * B[k][j]) % MOD + C[i][j]) % MOD;
    return C;
}

matrix pow(matrix A, int p) {
    if (p == 1) return A;
    if (p % 2) return mul(A, pow(A, p - 1));
    matrix X = pow(A, p / 2);
    return mul(X, X);
}

ll fib(ll N) {
    vector<ll> F1(K + 1);
    F1[1] = 1;
    F1[2] = 1;

    matrix T(K + 1, vector<ll>(K + 1));
    T[1][1] = 0, T[1][2] = 1;
    T[2][1] = 1, T[2][2] = 1;

    if (N == 1) return 1;
    T = pow(T, N - 1);

    ll res = 0;
    fr(i, K) res = (res + T[1][i] * F1[i]) % MOD;
    return res;
}

void solve() {
    cin >> T;
    while (T--) {
        ll n;
        cin >> n;
        cout << fib(n) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
    return 0;
}