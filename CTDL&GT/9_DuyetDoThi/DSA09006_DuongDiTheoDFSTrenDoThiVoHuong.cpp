#include <bits/stdc++.h>
using namespace std;

int T;

int n;
unordered_map<int, vector<int> > G;
vector<bool> vs;
vector<int> path;

void DFS(int u) {
    if(vs[u]) return;
    vs[u] = true;
    for (int v = 0; v < (int)G[u].size(); ++v) {
        if (!vs[G[u][v]]) {
            path[G[u][v]] = u;
            DFS(G[u][v]);
        }
    }
}

void solve() {
    cin >> T;
    while (T--) {
        int m, u, v, s, t;
        cin >> n >> m >> s >> t;
        G.clear();
        for (int i = 0; i < m; ++i) {
            cin >> u >> v;
            G[u].push_back(v);
            G[v].push_back(u);
        }

        path.assign(n + 5, 0);
        vs.assign(n + 5, 0);
        DFS(s);
        if (!vs[t])
            cout << -1;
        else {
            stack<int> way;
            v = t;
            while (v != s) {
                way.push(v);
                v = path[v];
            }
            way.push(s);
            while (!way.empty()) {
                cout << way.top() << " ";
                way.pop();
            }
        }
        cout << "\r\n";
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}