#include <bits/stdc++.h>
using namespace std;

int T;

int n;
unordered_map<int, vector<int> > G;
vector<bool> vs;

bool DFS(int u, vector <bool> recStack) {
    if (vs[u] == false) {
        vs[u] = true;
        recStack[u] = true;
        for (int v = 0; v < (int)G[u].size(); ++v) {
            if (!vs[G[u][v]] && DFS(G[u][v], recStack))
                return true;
            else if (recStack[G[u][v]])
                return true;
        }
    }
    recStack[u] = false;
    return false;
}

bool isCycle() {
    vector <bool> recStack(n+5, false);
    for (int i = 1; i <= n; i++)
        if (DFS(i, recStack))
            return true;
    return false;
}

void solve() {
    cin >> T;
    while (T--) {
        int m, u, v;
        cin >> n >> m;
        G.clear();
        for (int i = 0; i < m; ++i) {
            cin >> u >> v;
            G[u].push_back(v);
        }
        for (int i = 1; i <= n; ++i)
            sort(G[i].begin(), G[i].end());

        vs.assign(n + 5, 0);
        if (isCycle()) cout << "YES";
        else cout << "NO";
        cout << "\r\n";
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}