#include <bits/stdc++.h>
using namespace std;

int T;

int n;
unordered_map<int, vector<int> > G;
vector<bool> vs;
vector<int> recStack;

bool BFS(int u) {
    queue<int> q;
    q.push(u);
    while (!q.empty()) {
        u = q.front();
        q.pop();
        vs[u] = true;
        for (int v = 0; v < (int)G[u].size(); ++v) {
            if (!vs[G[u][v]]) {
                vs[G[u][v]] = true;
                q.push(G[u][v]);
                recStack[G[u][v]] = u;
            } else if (G[u][v] != recStack[u])
                return true;
        }
    }
    return false;
}

bool isCyclic() {
    for (int i = 1; i <= n; i++)
        if (!vs[i])
            if (BFS(i))
                return true;
    return false;
}

void solve() {
    cin >> T;
    while (T--) {
        int m, u, v;
        cin >> n >> m;
        G.clear();
        for (int i = 0; i < m; ++i) {
            cin >> u >> v;
            G[u].push_back(v);
            G[v].push_back(u);
        }
        // for (int i = 1; i <= n; ++i)
        //     if (G[i].size() > 0) sort(G[i].begin(), G[i].end());
        vs.assign(n + 5, 0);
        recStack.assign(n + 5, -1);
        if(isCyclic())cout << "YES";
        else cout << "NO";
        cout << "\r\n";
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}