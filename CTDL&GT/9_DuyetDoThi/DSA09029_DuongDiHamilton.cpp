#include <bits/stdc++.h>

#define ull unsigned long long int
#define ll long long int
using namespace std;
const long MODULO = 1e9L + 7L;

bool visited[20] = {};
vector<int> near[20];

bool Try(int k, int v, int *x, bool *visited, vector<int> *near) {
    if (k == v) return true;
    if (k < v) {
        int pre = x[k - 1];
        for (int i = 0; i < near[pre].size(); i++) {
            int nex = near[pre][i];
            if (visited[nex] == false) {
                visited[nex] = true;
                x[k] = nex;
                if (Try(k + 1, v, x, visited, near))
                    return true;
                visited[nex] = false;
            }
        }
    }
    return false;
}

bool check(int v, vector<int> *near) {
    int x[v + 1] = {};
    for (int i = 0; i < v; i++) {
        memset(visited, false, 20);
        visited[i] = true;
        x[0] = i;
        if (Try(1, v, x, visited, near))
            return true;
    }
    return false;
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int v, e;
        cin >> v >> e;
        for (int i = 0; i < 20; i++)
            near[i].clear();
        for (int i = 0; i < e; i++) {
            int be, en;
            cin >> be >> en;
            near[be - 1].push_back(en - 1);
            near[en - 1].push_back(be - 1);
        }
        cout << check(v, near) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}