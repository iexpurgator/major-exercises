#include <bits/stdc++.h>
using namespace std;

int T;

int n;
unordered_map<int, vector<int> > G;
vector<bool> vs;
// vector<int> path;

void DFS(int u) {
    if (vs[u]) return;
    vs[u] = true;
    for (int v = 0; v < (int)G[u].size(); ++v) {
        if (!vs[G[u][v]]) {
            DFS(G[u][v]);
        }
    }
}

int ThanhPhanLT() {
    int u, k = 0;
    for (u = 1; u <= n; u++)
        if (!vs[u]) {
            k++;
            DFS(u);
        }
    return (k);
}

void solve() {
    cin >> T;
    while (T--) {
        int m, u, v;
        cin >> n >> m;
        G.clear();
        for (int i = 0; i < m; ++i) {
            cin >> u >> v;
            G[u].push_back(v);
            G[v].push_back(u);
        }
        
        vs.assign(n + 5, 0);
        int res = ThanhPhanLT();
        for (int i = 1; i <= n; ++i) {
            fill(vs.begin(), vs.end(), 0);
            vs[i] = true;
            if (ThanhPhanLT() > res)
                cout << i << ' ';
        }
        cout << "\r\n";
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}