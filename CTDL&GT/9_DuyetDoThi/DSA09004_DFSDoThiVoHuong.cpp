#include <bits/stdc++.h>
using namespace std;
int T;

int v, e, taget, v1, v2;
unordered_map <int, vector<int>> G;
vector <bool> ok (1005);

void DFS (int u) {
	ok[u] = true;
	cout  << u << ' ';
	for (int i=0;i< (int)G[u].size();++i)
		if(!ok[G[u][i]])
			DFS(G[u][i]);
}

void solve() {
    cin >> T;
    while(T--) {
		G.clear();
		fill(ok.begin(), ok.end(), 0);
		cin >> e >> v >> taget;
		for (int i=0; i<v; ++i) {
			cin >> v1 >> v2;
			G[v1].push_back(v2);
			G[v2].push_back(v1);
		}
		DFS(taget);
		cout << endl;
    }
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
