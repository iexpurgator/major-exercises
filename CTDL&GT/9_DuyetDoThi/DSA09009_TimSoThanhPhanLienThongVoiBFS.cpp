#include <bits/stdc++.h>
using namespace std;

int T;

int n;
unordered_map<int, vector<int> > G;
vector<bool> vs;

void BFS(int u) {
    queue<int> q;
    q.push(u);
    while (!q.empty()) {
        u = q.front();
        q.pop();
        vs[u] = true;
        for (int v = 0; v < (int)G[u].size(); ++v) {
            if (!vs[G[u][v]]) {
                vs[G[u][v]] = true;
                q.push(G[u][v]);
            }
        }
    }
}

int ThanhPhanLT() {
    int u, k = 0;
    for (u = 1; u <= n; u++)
        if (!vs[u]) {
            k++;
            BFS(u);
        }
    return (k);
}

void solve() {
    cin >> T;
    while (T--) {
        int m, u, v;
        cin >> n >> m;
        G.clear();
        for (int i = 0; i < m; ++i) {
            cin >> u >> v;
            G[u].push_back(v);
            G[v].push_back(u);
        }

        vs.assign(n + 5, 0);
        cout << ThanhPhanLT() << "\r\n";
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}