#include <bits/stdc++.h>
using namespace std;

int T;

int n;
unordered_map<int, vector<int> > G;
// vector<bool> vs;
// vector<bool> recStack;

bool isCycle() {
    vector<int> in_degree(n+5, 0);
    for (int u = 1; u <= n; u++) {
        for (int v = 0; v < (int)G[u].size(); ++v)
            in_degree[G[u][v]]++;
    }
    queue<int> q;
    for (int i = 1; i <= n; i++)
        if (in_degree[i] == 0)
            q.push(i);
    int cnt = 0;
    vector<int> top_order;
    while (!q.empty()) {
        int u = q.front();
        q.pop();
        top_order.push_back(u);
        for (int v = 0; v < (int)G[u].size(); ++v)
            if (--in_degree[G[u][v]] == 0)
                q.push(G[u][v]);
        cnt++;
    }
    if (cnt != n)
        return true;
    else
        return false;
}

void solve() {
    cin >> T;
    while (T--) {
        int m, u, v;
        cin >> n >> m;
        G.clear();
        for (int i = 0; i < m; ++i) {
            cin >> u >> v;
            G[u].push_back(v);
        }

        // vs.assign(n + 5, 0);
        // recStack.assign(n + 5, false);
        if (isCycle())
            cout << "YES";
        else
            cout << "NO";
        cout << "\r\n";
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}