#include <bits/stdc++.h>
using namespace std;

int T;

int n;
unordered_map<int, vector<int> > G;
vector<bool> vs;

bool DFS(int u) {
    vs[u] = true;
    for (int v = 0; v < (int)G[u].size(); ++v) {
        if (!vs[G[u][v]] && DFS(G[u][v]))
            return true;
        // else if (G[u][v] != 0)
        //     return true;
    }
    return false;
}

bool isTree() {
    if(DFS(1))
        return true;
    for (int i = 1; i <= n; i++)
        if (!vs[i])
            return false;
    return true;
}

void solve() {
    cin >> T;
    while (T--) {
        int m, u, v;
        cin >> n;
        m = n-1;
        G.clear();
        for (int i = 0; i < m; ++i) {
            cin >> u >> v;
            G[u].push_back(v);
            G[v].push_back(u);
        }
        for (int i = 1; i <= n; ++i) {
            sort(G[i].begin(), G[i].end());
        }

        vs.assign(n + 5, 0);
        if (isTree()) cout << "YES";
        else cout << "NO";
        cout << "\r\n";
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}