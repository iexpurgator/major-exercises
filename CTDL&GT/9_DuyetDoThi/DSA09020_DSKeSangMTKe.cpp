#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define MOD 1000000007L;
#define ln '\n'
#define dbg(x) cout << #x << " = " << x << '\n'
#define all(x) (x).begin(), (x).end()
#define sz(x) ((ll)(x).size())

void solve() {
    int T = 1;
    // cin >> T;
    while (T--) {
        int n;
        cin >> n;
        cin.ignore();
        vector<int> G(n, 0);
        for (int i = 0; i < n; ++i) {
            string s, k;
            getline(cin, s);
            stringstream ss(s);
            while (ss >> k) {
                int j = stoi(k) - 1;
                G[j] = 1;
            }
            for (int &e : G) {
                cout << e << ' ';
                e = 0;
            }
            cout << ln;
        }
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}