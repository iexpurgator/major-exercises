#include <bits/stdc++.h>
using namespace std;
int T;

int e, v, v1, v2;
unordered_map <int, vector<int>> G;

void solve() {
    cin >> T;
    while(T--){
		// v: vertice
		// e: edge
		G.clear();
		cin >> v >> e;
		for (int i = 0; i < e; ++i) {
			cin >> v1 >> v2;
			G[v1].push_back(v2);
			G[v2].push_back(v1);
		}
		for (int i = 1; i <= v; ++i) {
			cout << i << ": ";
			for (auto j : G[i])
				cout << j << ' ';
			cout << endl;
		}
    }
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
