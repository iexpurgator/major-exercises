#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define MOD 1000000007L;
#define ln '\n'
#define dbg(x) cout << #x << " = " << x << ln
#define all(x) (x).begin(), (x).end()
#define sz(x) ((ll)(x).size())

void solve() {
    int T = 1;
    // cin >> T;
    while (T--) {
        int n, k;
        cin >> n;
        for (int i = 1; i <= n; ++i) {
            for (int j = 1; j <= n; ++j) {
                cin >> k;
                if (k == 1) cout << j << ' ';
            }
            cout << ln;
        }
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}