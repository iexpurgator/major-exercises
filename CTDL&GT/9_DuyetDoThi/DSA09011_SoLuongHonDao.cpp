#include <bits/stdc++.h>
using namespace std;

int T;
const int movex[8] = {1, 1, -1, -1, 0, 0, 1, -1};
const int movey[8] = {1, -1, 1, -1, 1, -1, 0, 0};
const int MAX = 1005;
int n, m;
int **G = new int *[MAX];
bool **vs = new bool *[MAX];

bool isOk(int i, int j) {
    return (i < n && i >= 0 && j < m && j >= 0 && G[i][j] == 1);
}

void BFS(int u, int v) {
    queue<pair<int, int> > queue;
    queue.push(pair<int, int>(u, v));
    vs[u][v] = true;
    while (!queue.empty()) {
        u = queue.front().first;
        v = queue.front().second;
        queue.pop();
        for (int i = 0; i < 8; i++) {
            int ni = u + movex[i];
            int nj = v + movey[i];
            if (isOk(ni, nj) && !vs[ni][nj]) {
                vs[ni][nj] = true;
                queue.push(pair<int, int>(ni, nj));
            }
        }
    }
}

int count() {
    int s = 0;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            if (!vs[i][j] && G[i][j] == 1) {
                s = s + 1;
                BFS(i, j);
            }
        }
    }
    return s;
}

void solve() {
    cin >> T;
    for (int i = 0; i < MAX; ++i) {
        vs[i] = new bool[MAX];
        G[i] = new int[MAX];
    }
    while (T--) {
        cin >> n >> m;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                cin >> G[i][j];
                vs[i][j] = false;
            }
        }
        cout << count() << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}