#include <bits/stdc++.h>
using namespace std;
#define ll long long int

int T;

int BFS(int s, int v, vector<int> *list, bool *visited) {
    queue<int> q;
    q.push(s);
    visited[s] = true;
    int t = 1;
    while (!q.empty()) {
        s = q.front();
        q.pop();
        for (int u : list[s]) {
            if (!visited[u]) {
                t++;
                q.push(u);
                visited[u] = true;
            }
        }
    }
    return t;
}

int count(int n, vector<int> *list) {
    int ma = 0;
    bool vs[n + 5];
    memset(vs, false, sizeof(vs));
    for (int i = 1; i <= n; i++) {
        if (!vs[i]) {
            int t = BFS(i, n, list, vs);
            ma = (ma < t) ? t : ma;
        }
    }
    return ma;
}

void solve() {
    cin >> T;
    while (T--) {
        int n, m;
        cin >> n >> m;
        vector<int> a[n + 5];
        for (int i = 0; i < m; ++i) {
            int u, v;
            cin >> u >> v;
            a[u].push_back(v);
            a[v].push_back(u);
        }
        cout << count(n, a) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}