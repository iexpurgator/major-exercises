#include <bits/stdc++.h>
using namespace std;
int T;

int v;
string s, vs;
unordered_map <int, vector<int>> G;
bool m[55][55];

int s2i (string str){
	int i = 0, num = 0;
	while (i < (int)str.length())
		num = num*10 + str[i++] - '0';
	return num;
}

void solve() {
	cin >> v;
	cin.ignore();
	for (int i = 1; i <= v; ++i) {
		getline(cin, s);
		stringstream ss(s);
		while (ss >> vs)
			G[i].push_back(s2i(vs));
		sort(G[i].begin(), G[i].end());
		for (auto j : G[i]) {
			if(!m[i][j] && !m[j][i]) {
				cout << i << ' ' << j << endl;
				m[i][j] = m[j][i] = true;
			}
		}
	}
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
