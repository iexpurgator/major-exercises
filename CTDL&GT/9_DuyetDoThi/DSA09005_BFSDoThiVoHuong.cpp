#include <bits/stdc++.h>
using namespace std;
int T;

int v, e, taget, v1, v2;
unordered_map <int, vector<int>> G;
vector <bool> ok (10005);

void BFS (int u) {
	queue < int > q;
	q.push(u);
	while(!q.empty()) {
		int t = q.front();
		q.pop();
		cout << t << ' ';
		ok[t] = true;
		for (int i=0; i<(int)G[t].size(); ++i) {
			if (!ok[G[t][i]]) {
				ok[G[t][i]] = true;
				q.push(G[t][i]);
			}
		}
	}
}

void solve() {
    cin >> T;
    while(T--) {
		// v: vertice
		// e: edge
		G.clear();
		fill(ok.begin(), ok.begin()+v, 0);
		cin >> e >> v >> taget;
		for (int i=0; i<v; ++i) {
			cin >> v1 >> v2;
			G[v1].push_back(v2);
			G[v2].push_back(v1);
		}
		BFS(taget);
		cout << endl;
		
    }
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
