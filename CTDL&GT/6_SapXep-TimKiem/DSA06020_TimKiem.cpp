#include <bits/stdc++.h>
using namespace std;

int T;

void solve() {
    cin >> T;
    while (T--) {
		int n, x;
        cin >> n >> x;
		int a[n];
		for(int i = 0; i < n; i++)
			cin >> a[i];
        int *p = find(a, a+n, x);
		cout << (p != a+n ? 1 : -1) << '\n';
	}
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr); cout.tie(nullptr);
    solve();
    return 0;
}