#include <bits/stdc++.h>
using namespace std;

#define ll long long int

void solve() {
    int T;
    cin >> T;
    while (T--) {
        long n1, n2, n3;
        cin >> n1 >> n2 >> n3;
        ll *a = new ll[n1];
        for (int i = 0; i < n1; ++i) cin >> a[i];
        ll *b = new ll[n2];
        for (int i = 0; i < n2; ++i) cin >> b[i];
        ll *c = new ll[n3];
        for (int i = 0; i < n3; ++i) cin >> c[i];
        vector<ll> common;
        int i = 0, j = 0, k = 0;
        while (i < n1 && j < n2 && k < n3) {
            if (a[i] == b[j] && b[j] == c[k]) {
                common.push_back(a[i]);
                i++;
                j++;
                k++;
            }
            if (a[i] < b[j]) i++;
            else if (b[j] < c[k]) j++;
            else k++;
        }
        if (common.empty()) cout << -1;
        else
            for (ll i : common) cout << i << ' ';
        cout << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}