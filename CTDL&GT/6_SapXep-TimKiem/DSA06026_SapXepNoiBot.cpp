#include <iostream>
using namespace std;

int n, a[105];

void print(int step){
    cout << "Buoc " << step << ": ";
    for (int i = 0; i < n; ++i)
        cout << a[i] << ' ';
    cout << endl;
}

int main() {
    cin >> n;
    for (int i=0; i<n; ++i)
        cin >> a[i];
    bool haveswap = true;
    int i = 1;
    while (haveswap) {
        for (int j = 0, k = 0; j < n-1; ++j) {
            if (a[j] > a[j+1]) {
                swap(a[j+1], a[j]);
                haveswap = true;
                k++;
            }
            if (!k) haveswap = false;
        }
        if(haveswap) print(i);
        i++;
    }
    return 0;
}