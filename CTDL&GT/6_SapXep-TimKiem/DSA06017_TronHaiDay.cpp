#include <bits/stdc++.h>
using namespace std;

int T, n, m;

void solve() {
    cin >> T;
    while (T--) {
		cin >> n >> m;
		n += m;
		int a[n];
		for(int i = 0; i < n; i++)
			cin >> a[i];
		sort(a, a+n);
		for(int i = 0; i < n; i++)
			cout << a[i] << ' ';
		cout << endl;
	}
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr); cout.tie(nullptr);
    solve();
    return 0;
}