#include <bits/stdc++.h>
using namespace std;

int T;
int n, m, k, MAX;
vector <int> ch (1e5+5);

void solve() {
    cin >> T;
    while(T--) {
		MAX = 0;
		ch.clear();
		cin >> n >> m;
		for (int i = 0; i < n; ++i){
			cin >> k;
			MAX = max(k, MAX);
			ch[k]++;
		}
		for (int i = 0; i < m; ++i){
			cin >> k;
			MAX = max(k, MAX);
			ch[k]++;
		}
		for (int i = 0; i <= MAX; ++i){
			if (ch[i] > 0){
				cout << i << ' ';
				ch[i]--;
			}
		} cout << '\n';
		for (int i = 0; i <= MAX; ++i){
			if (ch[i] > 0){
				cout << i << ' ';
				ch[i]--;
			}
		} cout << '\n';
    }
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}