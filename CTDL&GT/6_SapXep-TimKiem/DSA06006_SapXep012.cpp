#include <bits/stdc++.h>
using namespace std;

int T;
int a[3];
int n, k;

void solve() {
    cin >> T;
    while (T--) {
        cin >> n;
        for (int i = 0; i < n; i++) {
            cin >> k;
            a[k]++;
        }
        for (int i = 0; i < 3; ++i) {
            while (a[i] > 0) {
                cout << i << ' ';
                a[i]--;
            }
        }
        cout << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr); cout.tie(nullptr);
    solve();
    return 0;
}