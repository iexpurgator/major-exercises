#include <bits/stdc++.h>
using namespace std;

int T, n;
const int MAX = 1000000;
bool isPrime[MAX];

void Prime() {
    fill(isPrime + 2, isPrime + MAX, true);
    for (long i = 2; i < MAX; i++)
        if (isPrime[i])
            for (long j = i * i; j < MAX; j += i)
                isPrime[j] = false;
}

void solve() {
    cin >> T;
    while (T--) {
        cin >> n;
        bool has = false;
        for (int i = 2; i < n; i++) {
            if (isPrime[i] && isPrime[n - i]) {
                cout << i << ' ' << n - i << '\n';
                has = true;
                break;
            }
        }
        if (!has) cout << -1 << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr); cout.tie(nullptr);
    Prime();
    solve();
    return 0;
}