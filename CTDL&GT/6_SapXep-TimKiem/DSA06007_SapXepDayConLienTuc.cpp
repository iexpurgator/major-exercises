#include <bits/stdc++.h>
using namespace std;

int T, n;

void solve() {
    cin >> T;
    while (T--) {
		cin >> n;
		int a[n], b[n];
		for(int i = 0; i < n; i++) {
			cin >> a[i];
			b[i] = a[i];
		}
		
		sort(a, a+n);
		int l = 1, r = 1;

		for (int i = 0; i < n; ++i)
			if(a[i]!=b[i]){
				l = i+1; break;
			}
		for (int i = n-1; i >= 0; --i)
			if(a[i]!=b[i]){
				r = i+1; break;
			}
		cout << l << ' ' << r << '\n';
	}
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}