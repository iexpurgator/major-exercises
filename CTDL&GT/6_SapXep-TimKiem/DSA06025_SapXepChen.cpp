#include <iostream>
using namespace std;

int n, a[105], b[105];

void print(int step){
    cout << "Buoc " << step << ": ";
    for (int i = 0; i <= step; ++i)
        cout << b[i] << ' ';
    cout << endl;
}

int main() {
    cin >> n;
    for (int i=0; i<n; ++i)
        cin >> a[i];
    for (int i = 0; i < n; ++i) {
        b[i] = a[i];
        for (int j = i; j > 0; --j) {
            if(b[j] < b[j-1]) swap(b[j-1], b[j]);
        }
        print(i);
    }
    return 0;
}