#include <bits/stdc++.h>
using namespace std;

int T;

// template <class Iter>
// void srotate(Iter first, Iter middle, Iter last) {
//     Iter next = middle;
//     while (first != next) {
//         swap(*first++, *next++);
//         if (next == last)
//             next = middle;
//         else if (first == middle)
//             middle = next;
//     }
// }

template <class Iter>
void merge(Iter first, Iter middle, Iter last) {
    while (first != middle && middle != last) {
        Iter i = middle;
        first = upper_bound(first, middle, *middle);
        middle = upper_bound(middle, last, *first);
        rotate(first, i, middle); // using srotate -> TLE
    }
}

template <class Iter>
void merge_sort(Iter first, Iter last) {
    if (last - first > 1) {
        Iter middle = first + (last - first) / 2;
        merge_sort(first, middle);
        merge_sort(middle, last);
        merge(first, middle, last); // std::inplace_merge -> fastest
    }
}

void solve() {
    cin >> T;
    while (T--) {
        int n;
        cin >> n;
        int a[n];
        for (int i = 0; i < n; i++)
            cin >> a[i];
        merge_sort(a, a + n);
        for (int i = 0; i < n; i++)
            cout << a[i] << ' ';
        cout << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr); cout.tie(nullptr);
    solve();
    return 0;
}