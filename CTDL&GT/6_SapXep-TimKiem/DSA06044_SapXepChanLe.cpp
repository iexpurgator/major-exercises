#include <bits/stdc++.h>

#define ull unsigned long long int
#define ll long long int
using namespace std;

void solve() {
    int n;
    cin >> n;
    ll a[n + 5];
    ll b[n + 5];
    int k = 0, h = 0;
    for (int i = 1, j; i <= n; ++i) {
        cin >> j;
        if (i & 1)
            a[h++] = j;
        else
            b[k++] = j;
    }
    sort(a, a + h);
    sort(b, b + k, greater<int>());
    for (int i = 0; i < min(h, k); ++i) {
        cout << a[i] << ' ' << b[i] << ' ';
    }
    if (n & 1) cout << a[h - 1];
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}