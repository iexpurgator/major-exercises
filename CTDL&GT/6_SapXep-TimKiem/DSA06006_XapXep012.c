#include <stdio.h>

int T;
int a[3];
int n, k;

int main() {
    scanf("%d", &T);
    while (T--) {
        scanf("%d", &n);
        while (n--) {
            scanf("%d", &k);
            a[k]++;
        }
        for (int i = 0; i < 3; ++i) {
            while (a[i] > 0) {
                printf("%d ", i);
                a[i]--;
            }
        }
        printf("\n");
    }
    return 0;
}