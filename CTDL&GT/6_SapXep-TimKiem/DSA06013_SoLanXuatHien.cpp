#include <bits/stdc++.h>
using namespace std;

int T;
int n, x;

void solve() {
    cin >> T;
    while (T--) {
        cin >> n >> x;
        int a[n];
        for (int i = 0; i < n; i++)
            cin >> a[i];
        int count = upper_bound(a, a + n, x) - lower_bound(a, a + n, x);
        cout << (count > 0 ? count : -1) << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr); cout.tie(nullptr);
    solve();
    return 0;
}