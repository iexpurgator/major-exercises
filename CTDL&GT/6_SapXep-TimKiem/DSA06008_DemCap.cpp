#include <bits/stdc++.h>
using namespace std;
const int N = 1005;

int T;
int n, m;

void solve() {
    cin >> T;
    while (T--) {
        cin >> n >> m;
        int a[n], b[m], dem[N] = {0};
        for (int i = 0; i < n; i++)
            cin >> a[i];
        for (int i = 0; i < m; i++) {
            cin >> b[i];
            dem[b[i]]++;
        }
        for (int i = 1000; i >= 3; i--)
            dem[i] += dem[i + 1];

        long count = 0;
        for (int i = 0; i < n; i++) {
            switch (a[i]) {
                case 0:
                    break;
                case 1:
                    count += dem[0];
                    break;
                case 2:
                    count += dem[5];
                    goto donald;
                    break;
                case 3:
                    count += (dem[2] + dem[4]);
                    goto donald;
                    break;
                default:
                    count += dem[a[i] + 1];
                donald:
                    count += (dem[0] + dem[1]);
                    break;
            }
        }
        cout << count << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}