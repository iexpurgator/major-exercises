#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define MOD 1000000007L;
#define ln '\n'
#define dbg(x) cout << #x << " = " << x << '\n'
#define all(x) (x).begin(), (x).end()
#define sz(x) ((ll)(x).size())

void solve() {
    int T = 1;
    // cin >> T;
    while (T--) {
        ll n;
        string s;
        cin >> n >> s;
        ll res = n * (n - 1LL) / 2LL;
        for (int x = 0; x < 2; ++x) {
            int cur = 1;
            for (int i = 1; i < n; ++i) {
                if (s[i] == s[i - 1])
                    cur += 1;
                else {
                    res -= cur - x;
                    cur = 1;
                }
            }
            reverse(all(s));
        }
        cout << res << ln;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}