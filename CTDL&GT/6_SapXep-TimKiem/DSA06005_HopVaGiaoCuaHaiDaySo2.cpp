#include <bits/stdc++.h>
using namespace std;

int T;
int n, m, k;
vector <int> v1, v2, uni, inte;

void solve() {
    cin >> T;
    while(T--) {
		cin >> n >> m;
		v1.clear(); v2.clear();
		uni.clear(); inte.clear();
		
		for (int i = 0; i < n; ++i){
			cin >> k;
			v1.push_back(k);
		}
		
		for (int i = 0; i < m; ++i){
			cin >> k;
			v2.push_back(k);
		}
	
		sort(v1.begin(), v1.end());
		sort(v2.begin(), v2.end());
		
		set_union(begin(v1), end(v1), begin(v2), end(v2), back_inserter(uni));
		for (int i : uni) cout << i << ' ';
		cout << '\n';

		set_intersection(begin(v1), end(v1), begin(v2), end(v2), back_inserter(inte));
		for (int i : inte) cout << i << ' ';
		cout << '\n';
		}
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}