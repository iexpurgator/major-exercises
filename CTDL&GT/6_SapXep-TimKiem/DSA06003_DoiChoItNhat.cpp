#include <bits/stdc++.h>
using namespace std;

int T, n, res;

void findMin(int *a, int &x) {
	for(int i = x; i < n; i++)
		if(a[i] < a[x])
			x = i;
}

void count_swap(int *a) {
	int x;
	for(int i = 0; i < n; i++) {
		x = i;
		// update x for index of min_element
		findMin(a, x);
		if(i != x) {
			res = res + 1;
			swap(a[i], a[x]);
		}
	}
}

void solve() {
	cin >> T;
	while(T--) {
		res = 0;
		cin >> n;
		int a[n];
		for(int i = 0; i < n; i++)
			cin >> a[i];
		count_swap(a);
		cout << res << '\n';
	}
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}