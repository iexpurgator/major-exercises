#include <iostream>
using namespace std;

int n, a[105];

void print(int step){
    cout << "Buoc " << step << ": ";
    for (int i = 0; i < n; ++i)
        cout << a[i] << ' ';
    cout << endl;
}

int main() {
    cin >> n;
    for (int i=0; i<n; ++i)
        cin >> a[i];
    for (int i = 0; i < n-1; ++i) {
        int sma = i;
        for (int j = i+1; j < n; ++j)
            if (a[sma] > a[j])
                sma = j;
        swap(a[sma], a[i]);
        print(i+1);
    }
    return 0;
}