#include <bits/stdc++.h>
using namespace std;

int T;

int sum_0(int *a, int n) {
    int s = INT_MAX;
    for (int i = 0; i < n - 1; i++)
        for (int j = i + 1; j < n; j++)
            if (abs(s) > abs(a[i] + a[j]))
                s = a[i] + a[j];
    return s;
}

void solve() {
    cin >> T;
    while (T--) {
		int n; cin >> n;
		int a[n];
		for(int i = 0; i < n; i++)
			cin >> a[i];
		cout << sum_0(a, n) << endl;
	}
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr); cout.tie(nullptr);
    solve();
    return 0;
}