#include <bits/stdc++.h>
using namespace std;

int T, n;
long inp;

void solve() {
    cin >> T;
    while (T--) {
        cin >> n;
        long L, R, S = 0;
        vector <bool> a(100001, 0);
        for (int i = 0; i < n; i++) {
            cin >> inp;
            if (!i) L = R = inp;
            if (!a[inp]) {
                a[inp] = true;
                L = min(inp, L);
                R = max(inp, R);
                S++;
            }
        }
        cout << R - L - S + 1 << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr); cout.tie(nullptr);
    solve();
    return 0;
}