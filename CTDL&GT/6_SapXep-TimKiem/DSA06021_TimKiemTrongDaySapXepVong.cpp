#include <bits/stdc++.h>
using namespace std;
#define ll long long int

int T;

void solve() {
    cin >> T;
    while (T--) {
        long n, k, z;
        bool found = false;
        cin >> n >> k;
        for (long i = 1; i <= n; ++i) {
            cin >> z;
            if (z == k && !found) {
                cout << i << '\n';
                found = true;
            }
        }
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr); cout.tie(nullptr);
    solve();
    return 0;
}