#include <bits/stdc++.h>
using namespace std;

int T, n, x;

struct triplet{
	int index;
	int val;
	int abs_val;
};

bool compare(triplet a, triplet b) {
	if (a.abs_val == b.abs_val)
		return a.index < b.index;
	return a.abs_val < b.abs_val;
}

void solve() {
	cin >> T;
	while(T--) {
		cin >> n >> x;
		vector <triplet> a(n);
		for(int i = 0; i < n; i++) {
			cin >> a[i].val;
			a[i].index = i;
			a[i].abs_val = abs(a[i].val - x);
		}
		sort(a.begin(), a.end(), compare);
		for(triplet tri : a)
			cout << tri.val << ' ';
		cout << '\n';
	}
}


int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(nullptr);
	solve();
	return 0;
}