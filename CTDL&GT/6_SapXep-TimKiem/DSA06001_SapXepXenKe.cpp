#include <bits/stdc++.h>
using namespace std;

int T, n;

void show(int *a) {
	sort(a, a+n);
	for(int i = 0; i < n/2; i++) 
		cout << a[n-i-1] << ' ' << a[i] << ' ';
	if(n%2 != 0)
		cout << a[n/2];
	cout << '\n';
}

void solve() {
	cin >> T;
	while(T--) {
		cin >> n;
		int a[n+5];
		for(int i = 0; i < n; i++)
			cin >> a[i];
		show(a);
	}
}

int main() {
	ios_base::sync_with_stdio(false);
	cin.tie(nullptr);
	solve();
}
