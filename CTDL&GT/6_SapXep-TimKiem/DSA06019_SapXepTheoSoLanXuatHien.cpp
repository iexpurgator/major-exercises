#include <bits/stdc++.h>
#define oo 100001
using namespace std;

int T;

void solve() {
    cin >> T;
    while (T--) {
        vector<long> a(oo, 0);
        long n, cnt = 0, end = 0;
        cin >> n;
        for (int i = 0, j; i < n; ++i) {
            cin >> j;
            a[j]++;
            cnt = (cnt > a[j]) ? cnt : a[j];
            end = (end > j) ? end : j;
        }
        do {
            for (int i = 0; i <= end; ++i)
                if (a[i] == cnt)
                    while (a[i]--)
                        cout << i << ' ';
            
        } while(cnt--);
        cout << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr); cout.tie(nullptr);
    solve();
    return 0;
}