#include <bits/stdc++.h>
using namespace std;

int T;
int n, m, e;

void solve() {
    cin >> T;
    while (T--) {
        cin >> n >> m;
        cin >> e; long long biggest = e;
        for (int i = 1; i < n; i++) {
            cin >> e;
            biggest = (e > biggest) ? e : biggest;
        }
        cin >> e; long long smallest = e;
        for (int i = 1; i < m; i++) {
            cin >> e;
            smallest = (e < smallest) ? e : smallest;
        }
        cout << biggest * smallest << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr); cout.tie(nullptr);
    solve();
    return 0;
}