#include <bits/stdc++.h>
using namespace std;
#define ll long long int

int T;

void solve() {
    cin >> T;
    while (T--) {
        long n, mn = -1;
        cin >> n;
        vector<long> a(n, 0);
        for (long i = 0; i < n; ++i)
            cin >> a[i];
        sort(a.begin(), a.end());
        for (int i = 1; i < n; ++i) {
            if (a[i] > a[0]) {
                mn = a[i];
                cout << a[0] << " " << a[i] << '\n';
                break;
            }
        }
        if (mn == -1) cout << -1 << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr); cout.tie(nullptr);
    solve();
    return 0;
}