#include <bits/stdc++.h>
using namespace std;
int n;

void subsetsUtil(string& A, vector<string>& res, string& subset, int index) {
    res.push_back(subset);
    for (int i = index; i < n; i++) {
        subset.push_back(A[i]);
        subsetsUtil(A, res, subset, i + 1);
        subset.pop_back();
    }
    return;
}
vector<string> subsets(string& A) {
    string subset;
    vector<string> res;
    int index = 0;
    subsetsUtil(A, res, subset, index);
    return res;
}

void solve() {
    int T;
    cin >> T;
    while (T--) {
        string s;
        cin >> n >> s;
        vector<string> res = subsets(s);
        sort(s.begin(), s.end());
        for (string i : res) {
            if (i.size() > 0) cout << i << ' ';
        }
        cout << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    solve();
    return 0;
}