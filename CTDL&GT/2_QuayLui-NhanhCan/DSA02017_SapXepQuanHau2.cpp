#include <iostream>
using namespace std;

int T;
const int n = 8;
bool hang[9], cot[9] , dcx[18], dcn[18];
int v[9];
int a[9][9];
int res;

void Try(int i) {
	for(int j = 0; j < n; j++) {
		if(!hang[j] && !cot[j] && !dcx[i-j+n-1] && !dcn[i+j]) {
			v[i] = j;
			hang[j] = cot[j] = dcx[i-j+n-1] = dcn[i+j] = true;
			if (i+1 == n){
				int ans = 0;
				for (int l =0; l  < 8;++l)
					ans += a[l][v[l]];
				res = max(res, ans);
			} else Try(i+1);
			hang[j] = cot[j] = dcx[i-j+n-1] = dcn[i+j] = false;
		}
	}
}

int main() {
	cin >> T;
	while(T--) {
		fill(cot, cot+9, false);
		fill(hang, hang+9, false);
		fill(dcx, dcx+18, false);
		fill(dcn, dcn+18, false);
		res = 0;
		for(int i = 0; i < n; i++) {
			for(int j = 0; j < n; j++)
				cin >> a[i][j];
		}
		Try(0);
		cout << res << '\n';
	}
}
