#include <bits/stdc++.h>
using namespace std;
const int MAX = 11;
int T;

void solve() {
    cin >> T;
    while(T--){
        string s;
        cin >> s;
        do {
			cout << s << ' ';
		} while (next_permutation(s.begin(), s.end()));
		cout << endl;
    }
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
