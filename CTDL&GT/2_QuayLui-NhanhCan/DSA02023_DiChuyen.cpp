#include <bits/stdc++.h>
using namespace std;
const int MAX = 11;

bool isSafe(int row, int col, int m[][MAX], int n, bool visited[][MAX]) {
    if (row == -1 || row == n || col == -1 || col == n || visited[row][col] || m[row][col] == 0)
        return false;
    return true;
}

void tryMove(int row, int col, int m[][MAX], int n, vector<char> &path, vector<string> &possible, bool visited[][MAX]) {

    if (!isSafe(row, col, m, n, visited)) return;
    if (row == n - 1 && col == n - 1) {
        string input(path.begin(), path.end());
        possible.push_back(input);
        return;
    }

    visited[row][col] = true;
    if (isSafe(row + 1, col, m, n, visited)){
        path.push_back('D');
        tryMove(row + 1, col, m, n, path, possible, visited);
        path.pop_back();
    }
    if (isSafe(row, col + 1, m, n, visited)) {
        path.push_back('R');
        tryMove(row, col + 1, m, n, path, possible, visited);
        path.pop_back();
    }
    visited[row][col] = false;
}

void show(int m[MAX][MAX], int n) {
    vector<string> possible;
    vector <char> path;
    bool visited[n][MAX];
    memset(visited, false, sizeof(visited));
    tryMove(0, 0, m, n, path, possible, visited);

    if(!possible.empty())
        for (int i = 0; i < possible.size(); i++)
            cout << possible[i] << " ";
    else cout << -1;
}

void solve() {
    int T; cin >> T;
    while(T--){
        int n, m[MAX][MAX];
        cin >> n;
        for(int i = 0; i < n; ++i)
            for(int j = 0; j <n; ++j)
                cin >> m[i][j];
        show(m, n);
        cout << endl;
    }
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}