#include <iostream>
using namespace std;

int T;
int n, m, a;

/* De Quy

*/
void Try(int x, int y, int &d) {
	if(x == m-1 && y == n-1)
		d++;
	if(x+1 < m && y < n)
		Try(x+1, y, d);
	if(x < m && y+1 < n)
		Try(x, y+1, d);
}

int main() {
	scanf("%d", &T);
    while(T--) {
		int d = 0;
		scanf("%d%d", &m, &n);
		for(int i = 0; i < n*m; i++) {
			scanf("%d", &a);
		}
		Try(0, 0, d);
		printf("%d\n", d);
	}
}
