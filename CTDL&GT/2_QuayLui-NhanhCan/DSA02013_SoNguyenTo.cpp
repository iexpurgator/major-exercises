#include <bits/stdc++.h>
using namespace std;

int T;
int n, p, s, loc;
bool prime_check[300];
vector <int> prime;
vector < vector <int> > res;

void Try(int i, vector<int> v, int sum){
	if (sum > s)
		return;
	if (sum == s && v.size() == n){
		res.push_back(v);
		return;
	}
	for (int j=i; j <prime.size(); j++){
		if (!prime_check[j] && sum + prime[j] <= s && v.size() < n){
			v.push_back(prime[j]);
			prime_check[j] = true;
			Try(j+1, v, sum+prime[j]);
			prime_check[j] = false;
			v.pop_back();
		}
	}
}

void solve() {
	for (int i=2; i < 300; ++i){
		if(!prime_check[i]) {
			prime.push_back(i);
			for (int j= i*i; j < 300; j += i)
				prime_check[j] = true;
		}
	}
	cin >> T;
	while(T--) {
		cin >> n >> p >> s;
		loc = (lower_bound(prime.begin(), prime.end(), p) - prime.begin()) + 1;
		fill(prime_check, prime_check+300, 0);
		res.clear();
		vector <int> v;
		Try(loc, v, 0);
		cout << res.size() << '\n';
		for (int i = 0; i < res.size(); ++i){
			for (int ri : res[i])
				cout << ri << ' ';
			cout << '\n';
		}
	}
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}