#include <bits/stdc++.h>
using namespace std;
int T;

void solve() {
    cin >> T;
    while (T--) {
		int n;
		cin >> n;
		int a[n];
		vector <int> rev;
		for (int i=0; i<n; ++i){
			cin >> a[i];
		}
		for (int i = 0; i < n; ++i) {
			for (int j = 0; j < n-i; ++j) {
				if(i) a[j] = a[j] + a[j+1];
				rev.push_back(a[j]);
			}
		}
		for (int i = 0; i < n; ++i) {
			int m = rev.size() - 1;
			cout << '[';
			for (int j = 0; j <= i; ++j) {
				cout << rev[m - i + j];
				if (j < i) cout << ' ';
			}
			cout << "] ";
			for (int j = 0; j <= i; ++j)
				rev.pop_back();
		} cout << '\n';
    }
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}