#include <bits/stdc++.h>
using namespace std;
const int MAX = 11;
int T;

int n, k, s;
bool a[15];
int v[15];
vector < vector<int> > res;
vector <int> sub;

void init() {
	cin >> n  >> k;
	for (int i = 1; i <= n; ++i)
		cin >> v[i];
	sort(v+1, v+n+1);
	fill(a, a+15, false);
	res.clear();
}

void find_sol(int i) {
	for (int j =0; j <=1; ++j) {
		a[i] = j;
		if (i==n) {
			s = 0;
			sub.clear();
			for (int l = 1; l <= n; ++l) {
				if (a[l]) {
					s += v[l];
					sub.push_back(v[l]);
				}
			}
			if (s==k) res.push_back(sub);
		}
		else find_sol(i+1);
	}
}

void solve() {
    cin >> T;
    while(T--){
		init();
        find_sol(1);
        if (res.size() == 0)
			cout << -1 << endl;
		else {
			sort(res.begin(), res.end());
			for (int i = 0; i < (int)res.size(); i++) {
				cout << '[';
				s = res[i].size();
				for (int j = 0; j < s; j++) {
					cout << res[i][j];
					if (j+1 < s) cout << ' ';
				}
				cout << "] ";
			}
			cout << endl;
		}
    }
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
