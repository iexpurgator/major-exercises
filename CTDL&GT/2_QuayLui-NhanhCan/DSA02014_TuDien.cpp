#include <bits/stdc++.h>
using namespace std; 
// Boggle (Find all possible words in a board of characters) 
string dictionary[101];
int T, n, N, M;
bool found;

bool isWord(string& str) { 
	for (int i = 0; i < n; i++) 
		if (str.compare(dictionary[i]) == 0)
			return true; 
	return false; 
}

void findWordsUtil(char** boggle, bool** visited, int i, int j, string& str) {
	visited[i][j] = true; 
	str = str + boggle[i][j];
	if (isWord(str)) {
        found = true;
		cout << str << ' ';
    }
	for (int row = i - 1; row <= i + 1 && row < M; row++)
		for (int col = j - 1; col <= j + 1 && col < N; col++)
			if (row >= 0 && col >= 0 && !visited[row][col])
				findWordsUtil(boggle, visited, row, col, str);
	str.erase(str.length() - 1);
	visited[i][j] = false;
}

void solve() {
    cin >> T;
    while (T--) {
        found = false;
        cin >> n >> M >> N;
        string str = "";
        for (int i=0; i<n; ++i)
            cin >> dictionary[i];
        char** boggle = new char* [M];
        bool** visited = new bool* [M];
        for (int i=0;i<M;++i){
            boggle[i] = new char[N];
            visited[i] = new bool[N];
            for (int j=0; j<N; ++j)
                cin >> boggle[i][j];
        }
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                findWordsUtil(boggle, visited, i, j, str);
        if (!found) cout << -1;
        cout << '\n';
    }
} 

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}