#include <bits/stdc++.h>
using namespace std;

int T;
int n, sum;
bool empty;
vector < int > st;

void find_backtrack(vector < int > & arr, int sum, int i) {
    if (sum < 0)
        return;
    if (sum == 0) {
        empty = false;
        cout << "["; // start show result
        for (int j = 0; j < st.size(); ++j){
            cout << st[j];
            if (j+1 < st.size()) cout << ' ';
        }
        cout << "]"; // end show result
        return;
    }
    while (i < arr.size() && sum - arr[i] >= 0) {
        st.push_back(arr[i]);
        find_backtrack(arr, sum - arr[i], i);
        i++;
        st.pop_back();
    }
}

void solve() {
    cin >> T;
    while(T--) {
        empty = true;
		cin >> n >> sum;
		vector < int > arr(n);
		for (int i=0; i<n; ++i)
			cin >> arr[i];
		find_backtrack(arr, sum, 0);
		if (empty) cout << -1;
		cout << '\n';
	}
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}