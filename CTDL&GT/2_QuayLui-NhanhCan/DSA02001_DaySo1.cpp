#include <bits/stdc++.h>
using namespace std;
int T;

void solve() {
    cin >> T;
    while (T--) {
		int n;
		cin >> n;
		int a[n];
		for (int i=0; i<n; ++i){
			cin >> a[i];
		}
		for (int i = 0; i < n; ++i) {
			cout << '[';
			for (int j = 0; j < n-i; ++j) {
				if(i) a[j] = a[j] + a[j+1];
				cout << a[j];
				if(j+1 < n-i) cout << ' ';
			}
			cout << ']' << endl;
		}
    }
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
