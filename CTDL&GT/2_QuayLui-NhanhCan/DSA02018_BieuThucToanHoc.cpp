#include <bits/stdc++.h>
using namespace std;

const int SUM = 23;
const string yes = "YES\n";
const string no = "NO\n";
vector < vector<int> > arr;
vector <int> u;
bool visit[10], found;
int val[5], per[10];
int T, s;

void try_dau() {
    for (int i = 1; i <= 3; ++i){
        u.push_back(i);
        if (u.size() == 4) {
            arr.push_back(u);
        } else try_dau();
        u.pop_back();
    }
}

bool check(){
    for (int i=0; i < 81; ++i) {
        s = val[per[0]];
        for (int j = 0; j < 4; ++j) {
            if (arr[i][j] == 1) s += val[per[j+1]];
            if (arr[i][j] == 2) s -= val[per[j+1]];
            if (arr[i][j] == 3) s *= val[per[j+1]];
        }
        if (s == SUM)
            return true;
    }
    return false;
}

void Try(int i){
    if (found) return;
    for (int j=0; j<5; ++j){
        if(!visit[j]){
            per[i] = j;
            visit[j] = true;
            if(i == 4){
                if (check()){
                    found = true;
                    return;
                }
            } else Try(i+1);
            visit[j] = false;
        }
    }
}

void solve() {
    try_dau();
    u.clear();
    cin >> T;
    while(T--){
        fill(visit, visit+10, false);
        fill(per, per+10, false);
        found = false;
        cin >> val[0] >> val[1] >> val[2] >> val[3] >> val[4];
        Try(0);
        cout << (found ? yes : no);
    }
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}