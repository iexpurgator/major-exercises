#include <bits/stdc++.h>
using namespace std;

int T;

void dfs(vector<int>& candidates, int target,
         vector<int>& cur_path, int start,
         vector<vector<int>>& ans, int len) {
    if (target < 0) return;
    if (target == 0) {
        ans.push_back(cur_path);
        return;
    }
    for (; start < len; ++start) {
        cur_path.push_back(candidates[start]);
        dfs(candidates, target - candidates[start],
            cur_path, start,
            ans, len);
        cur_path.pop_back();
    }
}

void solve() {
    int T;
    cin >> T;
    while (T--) {
        int n, sum, k;
        cin >> n >> sum;
        vector<int> a;
        for (int i = 0; i < n; ++i) {
            cin >> k;
            a.push_back(k);
        }
        vector<vector<int>> res;
        vector<int> cur_path;
        dfs(a, sum, cur_path, 0, res, n);

        if (res.empty()) {
            cout << -1 << "\n";
            continue;
        }

        cout << res.size() << " ";
        for (vector<int>& ires : res) {
            if (!ires.empty()) {
                cout << "{";
                for (int i = 0; i < ires.size(); ++i) {
                    cout << ires[i];
                    if (i < ires.size() - 1) cout << ' ';
                }
                cout << "} ";
            }
        }
        cout << "\n";
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}