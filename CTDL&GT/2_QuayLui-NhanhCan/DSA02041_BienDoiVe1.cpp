#include <bits/stdc++.h>
using namespace std;
int T;

void solve() {
    cin >> T;
    while (T--) {
        long n;
        cin >> n;
        int dp[n + 1], i;
        dp[1] = 0;
        for (i = 2; i <= n; i++) {
            dp[i] = 1 + dp[i - 1];
            if (i % 2 == 0) dp[i] = min(dp[i], 1 + dp[i / 2]);
            if (i % 3 == 0) dp[i] = min(dp[i], 1 + dp[i / 3]);
        }
        cout << dp[n] << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}