#include <bits/stdc++.h>
using namespace std;

int T;
bool stop;
int n, k, a[15];

void gen() {
    int i = k;
    while (i > 0 && a[i] == 1) i--;
    if (i <= 0)
        stop = true;
    else {
        a[i]--;
        int d = k - i + 1;
        k = i;
        for (int j = i + 1; j <= i + d / a[i]; ++j)
            a[j] = a[i];
        k += d / a[i];
        if (d % a[i])
            a[++k] = d % a[i];
    }
}

void solve() {
    cin >> T;
    while (T--) {
        cin >> n;
        k = 1;
        a[k] = n;
        stop = false;
        vector<vector<int>> res;
        while (!stop) {
            vector<int> ires;
            for (int i = 1; i <= k; ++i) {
                ires.push_back(a[i]);
            }
            res.push_back(ires);
            gen();
        }
        cout << res.size() << '\n';
        for (vector<int>& ires : res) {
            cout << '(';
            for (int i = 0; i < ires.size(); ++i) {
                cout << ires[i];
                if (i + 1 < ires.size()) cout << ' ';
            }
            cout << ')' << ' ';
        }
        cout << endl;
    }
}