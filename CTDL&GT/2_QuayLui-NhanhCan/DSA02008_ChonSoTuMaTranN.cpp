#include <bits/stdc++.h>
using namespace std;

int T;
int n, k, s;

void solve() {
	cin >> n >> k ;
	int a[n+5][n+5];
	int b[n];
	vector <int> c; 
	for (int i =0; i<n; ++i) {
		b[i] = i+1;
		for (int j=0; j<n; ++j)
			cin >> a[i][j];
	}
	do {
		s = 0;
		for (int i = 0; i <n; ++i) {
			s += a[i][b[i]-1];
			if (s > k) break;
		}
		if (s == k) {
			for (int i= 0; i < n; ++i)
				c.push_back(b[i]);
		}
		
	} while (next_permutation(b, b+n));
	cout << c.size()/n << '\n';
	for (int i = 0; i < (int)c.size(); ++i) {
		cout << c[i];
		if ((i+1)%n) cout << ' ';
		else cout << '\n';
	}
}

int main () {
	ios_base::sync_with_stdio(false);
	cin.tie(nullptr);
	solve();
	return 0;
}
