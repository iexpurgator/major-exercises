#include <bits/stdc++.h>
using namespace std;

const int N = 25;
bool visited[N][N];
vector<int> g[N];
int longest, cnt;
int T, n, k, a, b;

void Try(int k) {
    for (int i : g[k]) {
        if (!visited[i][k]) {
            visited[i][k] = visited[k][i] = 1;
            cnt++;
            longest = cnt > longest ? cnt : longest;
            Try(i);
            cnt--;
            visited[i][k] = visited[k][i] = 0;
        }
    }
}

void solve() {
    cin >> T;
    while (T--) {
        longest = cnt = 0;
        for (vector<int>& i : g) i.clear();
        cin >> n >> k;
        for (int i = 0; i < k; i++) {
            cin >> a >> b;
            g[a].push_back(b);
            g[b].push_back(a);
        }
        for (int i = 0; i < n; ++i) Try(i);
        cout << longest << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}