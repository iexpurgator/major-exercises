#include <bits/stdc++.h>
using namespace std;
const long LMAX = 0x3b9aca00;

int n, T;
long V, res;
int a[31];

void Try(int i , long sum , long d){
	if (sum > V || d > res)
        return;	
	if (i == n){
		if (sum == V)
            res = min(res, d);
		return;
	}
	Try(i+1, sum, d);
	Try(i+1, sum+a[i], d+1);
}

void solve() {
    cin >> T;
	while(T--){
        res = LMAX;
        cin >> n >> V;
        for (int i = 0; i < n; i++)
            cin >> a[i];
        Try(0,0,0);
        cout << (res == LMAX ? -1 : res) << '\n';
	}
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}