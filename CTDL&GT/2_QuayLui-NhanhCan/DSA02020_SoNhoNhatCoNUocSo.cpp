#include <stdio.h>
#define ll long long
 
int n, T;
ll ans;
const ll prime[]={0,2,3,5,7,11,13,17,19,23,29};
 
void fin(ll v, ll nn, ll p) {
    if (nn > n)
        return;
    if (nn == n) {
        if (v < ans)
            ans = v;
        return;
    }
    for (int i = 1; i <= 64; i++) {
        v *= prime[p];
        if (v >= ans)
            break;
        if (nn < n)
            fin(v, nn * (i + 1), p + 1);
    }
}

int main() {
	scanf("%d", &T);
	while(T--){
		scanf("%d",&n);
		ans = 0xde0b6b3a7640000;
		if (n == 1) puts("1");
		else {
            fin(1ll,1ll,1ll);
            printf("%lld\n",ans);
        }
	}
	return 0;
}