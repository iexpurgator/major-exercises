#include <bits/stdc++.h>
using namespace std;
int T;
string s;
int k;

void find_max(string s, int k, string &s_max, int i) {
	if (k==0) return;
	int n = (int)s.length();
	char n_max = *max_element(s.begin()+i, s.end());
	if (n_max != s[i]) --k;
	for (int j = i; j < n; ++j) {
		if (s[j] == n_max) {
			swap(s[j], s[i]);
			if (s.compare(s_max) > 0)
				s_max = s;
			// Back Track
			find_max(s, k, s_max, i+1);
			swap(s[j], s[i]);
		}
	}
}

void solve() {
    cin >> T;
    while(T--) {
		cin >> k >> s;
		string s_bak = s;
		find_max(s_bak, k, s, 0);
		cout << s << endl;
    }
}

int main () {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
