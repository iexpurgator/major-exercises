#include <bits/stdc++.h>
using namespace std;
#define M_7 1000000007L
#define ll long long int
int T;

void dfs(vector<string>& res, string& s, int start, int left, int right, int open, string path) {
    if (start == s.length()) {
        if (left == 0 && right == 0 && open == 0) {
            res.push_back(path);
        }
        return;
    }
    if (left < 0 || right < 0 || open < 0) return;

    char c = s[start];
    if (c == '(') {
        if (start == 0 || s[start - 1] != c) {
            for (int i = 0; start + i < s.length() && s[start + i] == c && i + 1 <= left; ++i) {
                dfs(res, s, start + i + 1, left - i - 1, right, open, path);
            }
        }
        dfs(res, s, start + 1, left, right, open + 1, path + c);
    } else if (c == ')') {
        if (start == 0 || s[start - 1] != c) {
            for (int i = 0; start + i < s.length() && s[start + i] == c && i + 1 <= right; ++i) {
                dfs(res, s, start + i + 1, left, right - i - 1, open, path);
            }
        }
        dfs(res, s, start + 1, left, right, open - 1, path + c);
    } else {
        dfs(res, s, start + 1, left, right, open, path + c);
    }
}

void removeInvalidParentheses(string s) {
    int left = 0, right = 0;
    for (char ch : s) {
        if (ch == '(') {
            ++left;
        } else if (ch == ')') {
            if (left > 0) {
                --left;
            } else {
                ++right;
            }
        }
    }
    vector<string> res;
    dfs(res, s, 0, left, right, 0, "");
    sort(res.begin(), res.end());
    for (string i : res) {
        if (i == "" || i.length() == 1) {
            cout << -1 << '\n';
            return;
        }
        cout << i << ' ';
    }
    cout << '\n';
}

void solve() {
    cin >> T;
    while (T--) {
        string s;
        cin >> s;
        removeInvalidParentheses(s);
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}