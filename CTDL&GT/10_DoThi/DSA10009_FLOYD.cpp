#include <bits/stdc++.h>
using namespace std;
#define ll long long int

int T;

void FLOYD(int n, int **d) {
    for (int k = 0; k < n; k++)
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                if (d[i][j] > d[i][k] + d[k][j])
                    d[i][j] = d[i][k] + d[k][j];
}

void solve() {
    T = 1;
    // cin >> T;
    while (T--) {
        int n, c, s;
        cin >> n >> c;
        int **a = new int *[n];
        for (int i = 0; i < n; i++) {
            a[i] = new int[n];
            for (int j = 0; j < n; j++)
                if (i == j) a[i][j] = 0;
                else a[i][j] = SHRT_MAX;
        }
        int beg, end, cost;
        for (int i = 0; i < c; i++) {
            cin >> beg >> end >> cost;
            a[beg - 1][end - 1] = cost;
            a[end - 1][beg - 1] = cost;
        }
        FLOYD(n, a);
        cin >> s;
        for (int i = 0; i < s; ++i) {
            cin >> beg >> end;
            cout << a[beg - 1][end - 1] << '\n';
        }
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}