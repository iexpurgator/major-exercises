#include <bits/stdc++.h>

#define ull unsigned long long int
#define ll long long int
using namespace std;
const long MODULO = 1e9L + 7L;

typedef pair<int, int> iPair;

struct Graph {
    int V, E;
    vector<pair<int, iPair> > edges;
    Graph(int V, int E) {
        this->V = V;
        this->E = E;
    }
    void addEdge(int u, int v, int w) {
        edges.push_back({w, {u, v}});
    }
    int kruskalMST();
};

struct DisjointSets {
    int *parent, *rnk;
    int n;
    DisjointSets(int n) {
        this->n = n;
        parent = new int[n + 1];
        rnk = new int[n + 1];
        for (int i = 0; i <= n; i++) {
            rnk[i] = 0;
            parent[i] = i;
        }
    }
    int find(int u) {
        if (u != parent[u])
            parent[u] = find(parent[u]);
        return parent[u];
    }
    void merge(int x, int y) {
        x = find(x), y = find(y);
        if (rnk[x] > rnk[y])
            parent[y] = x;
        else
            parent[x] = y;
        if (rnk[x] == rnk[y])
            rnk[y]++;
    }
};

int Graph::kruskalMST() {
    int mst_wt = 0;
    sort(edges.begin(), edges.end());
    DisjointSets ds(V);
    vector<pair<int, iPair> >::iterator it;
    for (it = edges.begin(); it != edges.end(); it++) {
        int u = it->second.first;
        int v = it->second.second;
        int set_u = ds.find(u);
        int set_v = ds.find(v);
        if (set_u != set_v) {
            // cout << u << " - " << v << endl;
            mst_wt += it->first;
            ds.merge(set_u, set_v);
        }
    }
    return mst_wt;
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int V, E;
        cin >> V >> E;
        Graph g(V, E);
        for (int i = 0; i < E; i++) {
            int u, v, c;
            cin >> u >> v >> c;
            g.addEdge(u, v, c);
        }
        int mst_wt = g.kruskalMST();
        cout << mst_wt << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}