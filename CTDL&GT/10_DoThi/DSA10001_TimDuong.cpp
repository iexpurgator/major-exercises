#include<bits/stdc++.h>
#define pb push_back
#define f first
#define s second
#define pii pair<int, int>
#define INF INT_MAX
#define LINF LLONG_MAX
#define FOR(i, m, n) for(int i=m; i<n; i++)
#define FORD(i, m, n) for(int i=m; i>=n; i--)
const int MAXN = 1e5 + 123;
const int mod = 1e9 + 7;
using namespace std;

int n, m; 
char S[600][600];
int A[] = {0, -1, 1, 0, 0}; // up(1) , down(2), left(3), right(4)
int B[] = {0, 0, 0, -1, 1};
pii Begin, End;
struct item{
	int x, y, c;
};
int get(int a, int b){
	int hashValue = 0, base = 13;
	hashValue = hashValue * base + a;
	hashValue = hashValue * base + b;
	return hashValue;
}
bool range(int a, int b, int x, int y){
	if(a + x >= n || b + y >= m || a + x < 0 || b + y < 0) return false;
	return true;
}
bool check(int i, int c){
    if(c == i) return true;
    return false;
}
bool move(int x, int y){
	if(S[x][y] == 'S' || S[x][y] == 'T' || S[x][y] == '.') return true;
	return false;
}
void BFS(){
	
	unordered_map<int, int> mp;
	queue<item> q;
	if(range(Begin.f, Begin.s , A[1], B[1]) && move(Begin.f + A[1], Begin.s + B[1])){
		q.push({Begin.f + A[1], Begin.s + B[1], 1});
		mp[get(Begin.f + A[1], Begin.s + B[1])] = 1;
	}
	if(range(Begin.f, Begin.s , A[2], B[2]) && move(Begin.f + A[2], Begin.s + B[2])){
		q.push({Begin.f + A[2], Begin.s + B[2], 2});
		mp[get(Begin.f + A[2], Begin.s + B[2])] = 1;
	}
	if(range(Begin.f, Begin.s , A[3], B[3]) && move(Begin.f + A[3], Begin.s + B[3])){
		q.push({Begin.f + A[3], Begin.s + B[3], 3});
		mp[get(Begin.f + A[3], Begin.s + B[3])] = 1;
	}
	if(range(Begin.f, Begin.s , A[4], B[4]) && move(Begin.f + A[4], Begin.s + B[4])){
		q.push({Begin.f + A[4], Begin.s + B[4], 4});
		mp[get(Begin.f + A[4], Begin.s + B[4])] = 1;
	}
	while(!q.empty()){
		item place = q.front(); q.pop();

		FOR(i, 1, 5){
			if(range(place.x, place.y, A[i], B[i]) && move(place.x + A[i], place.y + B[i]) && check(i, place.c) && mp[get(place.x + A[i], place.y + B[i])] == 0){
				mp[get(place.x + A[i], place.y + B[i])] = mp[get(place.x, place.y)];
				q.push({place.x + A[i], place.y + B[i], place.c});
			}
			else if (range(place.x, place.y, A[i], B[i]) && move(place.x + A[i], place.y + B[i]) && !check(i, place.c) && mp[get(place.x + A[i], place.y + B[i])] == 0){
				mp[get(place.x + A[i], place.y + B[i])] = mp[get(place.x, place.y)] + 1;
				q.push({place.x + A[i], place.y + B[i], i});
			}
		}
	}
	if( mp[get(End.f, End.s)] <= 3 && mp[  get(End.f, End.s)] > 0) cout << "YES";
	else cout << "NO";
	
}
void solution(){
	cin >> n >> m;
	FOR(i, 0, n){
		FOR(j, 0, m){
			cin >> S[i][j];
			if(S[i][j] == 'S') Begin.f = i, Begin.s = j;
			if(S[i][j] == 'T') End.f = i, End.s = j;
		}
	}
	BFS();
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(); cout.tie();
    int icase = 1; cin >> icase;
    while(icase--) solution(), cout << endl;
	return 0;
}