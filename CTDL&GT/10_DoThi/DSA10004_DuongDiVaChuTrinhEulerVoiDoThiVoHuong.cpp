#include <bits/stdc++.h>
using namespace std;

int T;

vector<int> g[1000];

void dfs(int cur, bool visited[]) {
    visited[cur] = true;
    for (int& adj : g[cur])
        if (!visited[adj])
            dfs(adj, visited);
}

bool isConnected(int n, bool visited[]) {
    int start = -1;
    for (int i = 0; i < n; ++i) {
        if (!visited[i] && g[i].size() > 0) {
            if (start == -1) {
                // First component
                dfs(i, visited);
                start = i;
            } else {
                // Second component
                return false;
            }
        }
        visited[i] = true;
    }
    return true;  // 0 or 1 Component
}

int checkGraph(int n) {
    bool visited[n];
    fill(visited, visited + n, false);
    if (isConnected(n, visited)) {
        int oddV = 0;  // Count odd degree vertices
        for (int i = 0; i < n; ++i)
            if (g[i].size() % 2 == 1)
                oddV++;
        if (oddV == 0)
            return 2;  // The graph has Euler Circuit
        else if (oddV == 2)
            return 1;  // The graph has Euler Path
        else
            return 0;  // The graph is not Eulerian
    }
    return 0;
}

void solve() {
    cin >> T;
    while (T--) {
        int n, m;
        int u, v;
        cin >> n >> m;
        for (int i = 0; i < n; ++i)
            g[i].clear();
        for (int i = 0; i < m; ++i) {
            cin >> u >> v;
            g[u-1].push_back(v-1);
            g[v-1].push_back(u-1);
        }
        cout << checkGraph(n) << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}