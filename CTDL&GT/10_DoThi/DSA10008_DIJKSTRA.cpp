#include <bits/stdc++.h>
using namespace std;
#define ll long long int

int T;

void DIJKSTRA(int **a, int n, int s) {
    bool *visited = new bool[n];
    int *d = new int[n];
    int *e = new int[n];
    int t = n - 1, mind;
    for (int i = 0; i < n; i++) {
        e[i] = s;
        d[i] = a[s][i];
        visited[i] = 0;
    }
    visited[s] = true;
    while (t) {
        mind = INT_MAX;
        int u = 0;
        for (int i = 0; i < n; i++) {
            if (!visited[i] && mind > d[i]) {
                mind = d[i];
                u = i;
            }
        }
        visited[u] = true;
        t--;
        for (int i = 0; i < n; i++) {
            if (d[i] > d[u] + a[u][i]) {
                d[i] = d[u] + a[u][i];
                e[i] = u;
            }
        }
    }

    for (int i = 0; i < n; i++)
        cout << d[i] << ' ';
    cout << endl;
}

void solve() {
    cin >> T;
    while (T--) {
        int n, c, s;
        cin >> n >> c >> s;
        int **a = new int *[n];
        for (int i = 0; i < n; i++) {
            a[i] = new int[n];
            for (int j = 0; j < n; j++)
                if (i == j)
                    a[i][j] = 0;
                else
                    a[i][j] = SHRT_MAX;
        }

        for (int i = 0; i < c; i++) {
            int beg, end, d;
            cin >> beg >> end >> d;
            if (a[beg - 1][end - 1] > d) {
                a[beg - 1][end - 1] = d;
                a[end - 1][beg - 1] = d;
            }
        }
        DIJKSTRA(a, n, s-1);
        cout << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}