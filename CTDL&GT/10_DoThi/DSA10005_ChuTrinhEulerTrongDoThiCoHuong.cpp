#include <bits/stdc++.h>
using namespace std;

int T;

// Hierholzer’s Algorithm for directed graph

int EulerCircuit(vector<vector<int> > adj) {
    unordered_map<int, int> edge_count;
    for (int i = 0; i < adj.size(); i++) {
        edge_count[i] = adj[i].size();
    }
    if (adj.empty()) return 0;
    stack<int> curr_path;
    vector<int> circuit;
    int curr_v = 0;
    curr_path.push(curr_v);
    while (!curr_path.empty()) {
        if (edge_count[curr_v]) {
            curr_path.push(curr_v);
            int next_v = adj[curr_v].back();
            edge_count[curr_v]--;
            adj[curr_v].pop_back();
            curr_v = next_v;
        }
        else {
            circuit.push_back(curr_v);
            curr_v = curr_path.top();
            curr_path.pop();
        }
    }
    return (circuit.front() == circuit.back());
}

void solve() {
    cin >> T;
    while (T--) {
        int n, m;
        cin >> n >> m;
        vector < vector <int> > a;
        int u, v;
        a.resize(n);
        for (int i = 0; i < m; ++i) {
            cin >> u >> v;
            a[u-1].push_back(v-1);
        }
        cout << EulerCircuit(a) << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}