#include <bits/stdc++.h>
using namespace std;
#define ll long long int

int T;

struct Edge {
    int src, dest;
};

struct Graph {
    int V, E;
    Edge* edge;
};

Graph* createGraph(int V, int E) {
    Graph* graph = (Graph*)malloc(sizeof(Graph));
    graph->V = V;
    graph->E = E;
    graph->edge = (Edge*)malloc(graph->E * sizeof(Edge));
    return graph;
}

int find(int parent[], int i) {
    if (parent[i] == -1) return i;
    return find(parent, parent[i]);
}

void Union(int parent[], int x, int y) {
    parent[x] = y;
}

bool isCycle(Graph* graph) {
    int* parent = (int*)malloc(graph->V * sizeof(int));
    memset(parent, -1, sizeof(int) * graph->V);
    for (int i = 0; i < graph->E; ++i) {
        int x = find(parent, graph->edge[i].src);
        int y = find(parent, graph->edge[i].dest);
        if (x == y)
            return 1;
        Union(parent, x, y);
    }
    return 0;
}

void solve() {
    cin >> T;
    while (T--) {
        int v, e;
        cin >> v >> e;
        Graph* graph = createGraph(v, e);
        for (int i = 0; i < e; ++i) {
            int src, dest;
            cin >> src >> dest;
            graph->edge[i].src = src - 1;
            graph->edge[i].dest = dest - 1;
        }
        if (isCycle(graph))
            cout << "YES\n";
        else
            cout << "NO\n";
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}