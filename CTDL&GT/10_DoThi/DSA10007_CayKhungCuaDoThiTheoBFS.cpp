#include <bits/stdc++.h>
using namespace std;
#define ll long long int

int T;

struct edge {
    int u;
    int v;
};

int n;
unordered_map<int, vector<int> > G;
bool vs[1005];
vector<edge> Tree;

void BFS(int u) {
    memset(vs, 0, sizeof(vs));
    Tree.clear();
    queue<int> q;
    q.push(u);
    while (!q.empty()) {
        u = q.front();
        q.pop();
        vs[u] = true;
        for (int &point : G[u]){
            if (!vs[point]) {
                vs[point] = true;
                Tree.push_back({u, point});
                q.push(point);
            }
        }
    }
}

void T_BFS(int u) {
    if (Tree.size() == n-1) {
        for(auto i : Tree){
            cout << i.u << ' ' << i.v << '\n';
        }
    } else
        cout << -1 << '\n';
}

void solve() {
    cin >> T;
    while (T--) {
        int m, u, v, t;
        cin >> n >> m >> t;
        G.clear();
        for (int i = 0; i < m; ++i) {
            cin >> u >> v;
            if (u <= n && u > 0 && v <= n && v > 0) {
                G[u].push_back(v);
                G[v].push_back(u);
            }
        }
        BFS(t);
        T_BFS(t);
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}