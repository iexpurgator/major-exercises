#include <bits/stdc++.h>
using namespace std;
#define ll long long int
#define fr(i, k, n) for (int i = (k); i <= (n); ++i)
#define fd(i, k, n) for (int i = (n); i >= (k); --i)
#define ifr(i, n) for (int i = 0; i < (n); ++i)

int T;

void print(int *a, int n, bool odd) {
    if (odd) {
        fr(i, 1, n) cout << a[i] << ' ';
        fd(i, 1, n-1) cout << a[i] << ' ';
        cout << '\n';
    } else {
        fr(i, 1, n) cout << a[i] << ' ';
        fd(i, 1, n) cout << a[i] << ' ';
        cout << '\n';
    }
}

void solve() {
    T = 1;
    // cin >> T;
    while (T--) {
        int n;
        bool ok = 1;
        cin >> n;
        bool isodd = n & 1;
        int a[n];
        fr(i, 1, n) a[i] = 0;
        if (isodd) n = n / 2 + 1;
        else n = n / 2;
        while (ok) {
            ok = 0;
            print(a, n, isodd);
            for (int i = n; i > 0; --i) {
                if (a[i] == 0)
                    ok = a[i] = 1;
                else if (a[i] == 1)
                    a[i] = 0;
                if (ok) break;
            }
        }
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
