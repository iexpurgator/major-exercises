#include <bits/stdc++.h>
using namespace std;
#define ll long long int
#define fr(i, k, n) for (int i = (k); i <= (n); ++i)
#define fd(i, k, n) for (int i = (n); i >= (k); --i)
#define ifr(i, n) for (int i = 0; i < (n); ++i)

int T;
int count(int arr[], int n, int k) {
    int dp[k][n], sum = 0;
    memset(dp, 0, sizeof(dp));
    ifr(i, n) dp[0][i] = 1;
    fr(l, 1, k - 1) {
        fr(i, l, n - 1) {
            fr(j, l - 1, i - 1) if (arr[j] < arr[i])
                dp[l][i] += dp[l - 1][j];
        }
    }
    for (int i = k - 1; i < n; i++)
        sum += dp[k - 1][i];
    return sum;
}

void solve() {
    T = 1;
    while (T--) {
        int n, k;
        cin >> n >> k;
        int a[n + 5];
        fr(i, 0, n-1) {
            cin >> a[i];
        }
        cout << count(a, n, k) << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}