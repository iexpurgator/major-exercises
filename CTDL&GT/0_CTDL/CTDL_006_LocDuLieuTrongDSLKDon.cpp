#include <bits/stdc++.h>
using namespace std;
#define ll long long int
#define fr(i, k, n) for (int i = (k); i <= (n); ++i)
#define fd(i, k, n) for (int i = (n); i >= (k); --i)
#define ifr(i, n) for (int i = 0; i < (n); ++i)

int T;

void solve() {
    T = 1;
    // cin >> T;
    while (T--) {
        int n, k, inp;
        cin >> n;
        unordered_map<int, bool> a;
        fr(i, 1, n) {
            cin >> inp;
            if (a[inp] == 0) {
                a[inp] = 1;
                cout << inp << ' ';
            }
        }
        cout << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}