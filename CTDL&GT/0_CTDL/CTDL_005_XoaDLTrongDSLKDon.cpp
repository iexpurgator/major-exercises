#include <bits/stdc++.h>
using namespace std;
#define ll long long int
#define fr(i, k, n) for (int i = (k); i <= (n); ++i)
#define fd(i, k, n) for (int i = (n); i >= (k); --i)
#define ifr(i, n) for (int i = 0; i < (n); ++i)

int T;

void solve() {
    T = 1;
    // cin >> T;
    while (T--) {
        int n, k, inp;
        cin >> n;
        vector < int > a;
        fr(i, 1, n) {
            cin >> inp;
            a.push_back(inp);
        }
        cin >> k;
        for(int & i : a){
            if(i != k) cout << i << ' ';
        }
        cout << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}