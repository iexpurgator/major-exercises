#include <bits/stdc++.h>
using namespace std;
#define ll long long int
#define fr(i, k, n) for (int i = (k); i <= (n); ++i)
#define fd(i, k, n) for (int i = (n); i >= (k); --i)
#define ifr(i, n) for (int i = 0; i < (n); ++i)

int T;

void dp1(int W, int wt[], int val[], int n) {
    int i, w;
    int K[n + 1][W + 1];
    memset(K, 0, sizeof(K));
    for (i = 1; i <= n; i++) {
        for (w = 1; w <= W; w++) {
            if (wt[i - 1] <= w)
                K[i][w] = max(val[i - 1] + K[i - 1][w - wt[i - 1]], K[i - 1][w]);
            else
                K[i][w] += K[i - 1][w];
        }
    }
    int res = K[n][W];
    cout << res << '\n';

    w = W;
    for (i = n; i > 0 && res > 0; i--) {
        if (res == K[i - 1][w]) {
            cout << 0 << ' ';
            continue;
        } else {
            cout << 1 << ' ';
            res = res - val[i - 1];
            w = w - wt[i - 1];
        }
    }
    cout << '\n';
}

void solve() {
    T = 1;
    // cin >> T;
    while (T--) {
        int n, w;
        cin >> n >> w;
        int a[n + 1], c[n + 1];
        fr(i, 0, n - 1) cin >> a[i];  // weight
        fr(i, 0, n - 1) cin >> c[i];  // value
        dp1(w, c, a, n);
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}