#include <bits/stdc++.h>
using namespace std;
#define ll long long int
#define fr(i, k, n) for (int i = (k); i <= (n); ++i)
#define fd(i, k, n) for (int i = (n); i >= (k); --i)
#define ifr(i, n) for (int i = 0; i < (n); ++i)

int T;
int cnt = 0;
void print(int *num, int *a, int n, int k) {
    int sum = 0;
    fr(i, 1, n) if (a[i]) sum += num[i];
    if (sum == k) {
        cnt++;
        fr(i, 1, n) if (a[i]) cout << num[i] << ' ';
        cout << '\n';
    }
}

void solve() {
    T = 1;
    cin >> T;
    while (T--) {
        int n, k;
        bool ok = 1;
        cin >> n >> k;
        int b[n + 1];
        int a[n + 1];
        fr(i, 1, n) {
            a[i] = 0;
            cin >> b[i];
        }
        while (ok) {
            ok = 0;
            print(b, a, n, k);
            for (int i = n; i > 0; --i) {
                if (a[i] == 0) {
                    ok = 1;
                    a[i] = 1;
                } else if (a[i] == 1)
                    a[i] = 0;
                if (ok) break;
            }
        }
        cout << cnt << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}
