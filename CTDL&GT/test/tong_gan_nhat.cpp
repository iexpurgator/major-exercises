#include <algorithm>
#include <cmath>
#include <iostream>
#include <string>
//#include <vector>
//#include <stack>
//#include <queue>
//#include <set>
//#include <unordered_set>
//#include <map>
//#include <unordered_map>
//#include <list>
//#include <random>
//#include <iomanip>
//#include <fstream>
//#include <complex>
using namespace std;

typedef long long ll;
#define MOD 1000000007L;

int sum_0(int *a, int n, int k) {
    int s = INT_MAX;
    for (int i = 0; i < n - 1; i++) {
        for (int j = i + 1; j < n; j++) {
            for (int h = j + 1; h < n; h++) {
                if (abs(s - k) >= abs(k - a[i] - a[j] - a[h]))
                    s = a[i] + a[j] + a[h];
            }
        }
    }
    return s;
}

void solve() {
    int T = 1;
    cin >> T;
    while (T--) {
        int n, k;
        cin >> n >> k;
        int *a = new int[n];
        for (int i = 0; i < n; i++)
            cin >> a[i];
        cout << sum_0(a, n, k) << endl;
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}