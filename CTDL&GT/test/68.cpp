#include <iostream>
#include <vector>
using namespace std;

string next_binary(string num) {
    int flag = 0;
    for (int i = num.size() - 1; i >= 0; i--) {
        if (num.at(i) == '6') {
            num.at(i) = '8';
            flag = 1;
            break;
        } else
            num.at(i) = '6';
    }
    if (flag < 0)
        num = "8" + num;
    return num;
}

int main() {
    int T;
    cin >> T;
    while (T--) {
        int n;
        cin >> n;
        string bin = "";
        for (int i = 0; i < n; ++i) bin += '6';
        vector<string> res;
        res.push_back(bin);
        string temp = bin;
        bin = next_binary(bin);
        while(temp != bin) {
            res.push_back(bin);
            bin = next_binary(bin);
        }
        cout << res.size() << endl;
        for(string &ss : res) cout << ss << ' ';
        cout << '\n';
    }
    return 0;
}