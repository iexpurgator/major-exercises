#include <algorithm>
#include <iostream>

using namespace std;
int n, k, a[1000], ok = 1;

void in(int x) {
    for (int i = 1; i <= x; i++)
        cout << a[i];
    cout << '\n';
}

void sinhNhiPhan() {
    int i = n;
    while (a[i] == 1 && i > 0) {
        a[i] = 0;
        i--;
    }
    if (i > 0)
        a[i] = 1;
    else
        ok = 0;
}

void sinhHoanVi() {
    int i = n - 1;
    while (i > 0 && a[i] > a[i + 1]) i--;
    if (i > 0) {
        int j = n;
        while (a[i] > a[j]) j--;
        swap(a[i], a[j]);
        sort(a + i + 1, a + n + 1);
    } else
        ok = 0;
}

void sinhToHop() {
    int i = k;
    while (i > 0 && a[i] == n - k + i) i--;
    if (i > 0) {
        a[i] += 1;
        for (int j = i + 1; j <= k; j++)
            a[j] = a[j - 1] + 1;
    } else
        ok = 0;
}

void quayLuiNP(int x) {
    for (int i = 0; i <= 1; i++) {
        a[x] = i;
        if (x == n)
            in(n);
        else
            quayLuiNP(x + 1);
        a[x] = 0;
    }
}

void quayLuiHV(int x) {
    for (int i = 1; i <= n; i++) {
        int c = 1;
        for (int j = 1; j <= x; j++)
            if (i == a[j]) c = 0;
        if (c) {
            a[x] = i;
            if (x == n)
                in(n);
            else
                quayLuiHV(x + 1);
            a[x] = 0;
        }
    }
}

void quayLuiTH(int x) {
    for (int i = a[x - 1] + 1; i <= n - k + x; i++) {
        a[x] = i;
        if (x == k) {
            in(x);
        } else {
            quayLuiTH(x + 1);
        }
        a[x] = 0;
    }
}

int main() {
    int c;
    do {
        cout << "=============MENU=============" << endl;
        cout << "1.Sinh nhi phan" << endl;
        cout << "2.Sinh hoan vi" << endl;
        cout << "3.Sinh to hop" << endl;
        cout << "4.Quay lui nhi phan" << endl;
        cout << "5.Quay lui hoan vi" << endl;
        cout << "6.Quay lui to hop" << endl;
        cout << "==============================" << endl;
        cout << "Lua chon: ";
        cin >> c;
    } while (c < 1 || c > 6);
    switch (c) {
        case 1:  // sinh nhi phan
            cout << "n: ";
            cin >> n;
            while (ok) {
                in(n);
                sinhNhiPhan();
            }
            break;
        case 2:  // sinh hoan vi
            cout << "n: ";
            cin >> n;
            for (int i = 1; i <= n; i++) a[i] = i;
            while (ok) {
                in(n);
                sinhHoanVi();
            }
            break;
        case 3:  // sinh to hop
            cout << "n: ";
            cin >> n;
            cout << "k: ";
            cin >> k;
            for (int i = 1; i <= k; i++) a[i] = i;
            while (ok) {
                in(k);
                sinhToHop();
            }
            break;
        case 4:  // quay lui nhi phan
            cout << "n: ";
            cin >> n;
            quayLuiNP(1);
            break;
        case 5:  // quay lui hoan vi
            cout << "n: ";
            cin >> n;
            quayLuiHV(1);
            break;
        case 6:  // quay lui to hop
            cout << "n: ";
            cin >> n;
            cout << "k: ";
            cin >> k;
            quayLuiTH(1);
            break;
        default:
            break;
    }
}