#include <bits/stdc++.h>
using namespace std;
#define ll long long

int T;

int valid (string a, string b){
    int n = a.length(), m = b.length();
    vector <vector < int > > arr (n+1, vector <int>(m+1, 0));
    for (int i = 1; i <= n; ++i){
        for (int j = 1; j <= m; ++j){
            if(a[i-1] == b[j-1]) arr[i][j] = arr[i-1][j-1] + 1;
            else arr[i][j] = max(arr[i-1][j], arr[i][j-1]);
        }
    }
    return arr[n][m];
}

void solve(){
    cin >> T;
    while (T--) {
        string a, b;
        cin >> a >> b;
        cout << valid(a, b) << "\n";
    }
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}