#include <algorithm>
#include <cmath>
#include <iostream>
#include <string>
#include <unordered_map>

using namespace std;
typedef long long ll;
#define MOD 1000000007L;

int subArraylen(ll * arr, ll n, ll K) {
    unordered_map<int, int> mp;
    mp[arr[0]] = 0;
    for (int i = 1; i < n; i++) {
        arr[i] = arr[i] + arr[i - 1];
        mp[arr[i]] = i;
    }
    int len = INT_MAX;
    for (int i = 0; i < n; i++) {
        if (arr[i] < K)
            continue;
        else {
            int x = arr[i] - K;
            if (x == 0)
                len = min(len, i);
            if (mp.find(x) == mp.end())
                continue;
            else {
                len = min(len, i - mp[x]);
            }
        }
    }
    return len;
}

void solve() {
    int T = 1;
    // cin >> T;
    while (T--) {
        ll n, k;
        cin >> n >> k;
        ll *a = new ll [n];
        for(int i = 0; i < n; ++i)
            cin >> a[i];
        cout << subArraylen(a, n, k) << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    solve();
    return 0;
}