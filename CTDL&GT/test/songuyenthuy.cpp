#include <iostream>

using namespace std;
int n, k, a[1000], ok = 1;

void in(int x) {
    for (int i = 1; i <= x; i++)
        cout << (a[i] ? 5 : 4);
    for (int i = x; i >= 1; i--)
        cout << (a[i] ? 5 : 4);
    cout << ' ';
}

void sinhNhiPhan() {
    int i = n;
    while (a[i] == 1 && i > 0) {
        a[i] = 0;
        i--;
    }
    if (i > 0)
        a[i] = 1;
    else
        ok = 0;
}

void solve() {
    int T;
    cin >> T;
    while (T--) {
        int cnt;
        cin >> cnt;
        n = 0;
        while (cnt > 0) {
            n++;
            while (ok && cnt) {
                in(n);
                cnt--;
                sinhNhiPhan();
            }
            for (int i = 0; i < n; ++i)
                a[i] = 0;
            ok = 1;
        }
        cout << '\n';
    }
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    solve();
    return 0;
}