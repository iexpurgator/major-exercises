import java.io.*;
import java.text.*;

public class J03030_BienDoiAB {
    static BufferedReader in;

    public static void main(String[] args) throws Exception {
        in = new BufferedReader(new InputStreamReader(System.in));
        int A = 0, B = 0;
        CharacterIterator it = new StringCharacterIterator(in.readLine());
        while (it.current() != CharacterIterator.DONE) {
            if(it.current() == 'A') B = 1 + (A < B ? A : B);
            else A = 1 + (A < B ? A : B);
            it.next();
        }
        System.out.println(A);
    }
}
