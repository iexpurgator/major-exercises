import java.io.*;
import java.util.*;

public class J07009_LopIntSet2 {
    // public static void main(String[] args) {;}

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(new File("DATA1.in"));
        int n = sc.nextInt();
        int m = sc.nextInt();
        int[] a = new int[n];
        int[] b = new int[m];
        for (int i = 0; i < n; i++) {
            a[i] = sc.nextInt();
        }
        for (int i = 0; i < m; i++) {
            b[i] = sc.nextInt();
        }
        IntSet s1 = new IntSet(a);
        IntSet s2 = new IntSet(b);
        IntSet s3 = s1.intersection(s2);
        System.out.println(s3);
    }
}

class IntSet {
    TreeSet<Integer> list;

    public IntSet(int[] a) {
        list = new TreeSet<>();
        for (int i = 0; i < a.length; i++) {
            if (!list.contains(a[i]))
                list.add(a[i]);
        }
    }

    public IntSet(IntSet s) {
        list = s.list;
    }

    public static IntSet union(IntSet s1, IntSet s2) {
        s1.list.addAll(s2.list);
        return new IntSet(s1);
    }

    public static IntSet intersection(IntSet s1, IntSet s2) {
        s1.list.retainAll(s2.list);
        return new IntSet(s1);
    }

    public IntSet union(IntSet s) {
        return union(this, s);
    }

    public IntSet intersection(IntSet s) {
        return intersection(this, s);
    }
    
    @Override
    public String toString() {
        return list.toString().replace("[", "").replace("]", "").replace(",", "");
    }
}