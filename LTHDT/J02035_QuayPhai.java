import java.util.*;
import java.io.*;

public class J02035_QuayPhai {
    public static void main(String[] args) throws IOException {
        J02035_Reader sc = new J02035_Reader();
        int T = Integer.parseInt(sc.next());
        while (T -- > 0) {
            int n = Integer.parseInt(sc.next());
            long[] a = new long[n];
            int idx = 0;
            for (int i = 0; i < n; i++) {
                a[i] = Integer.parseInt(sc.next());
            }
            long[] b = Arrays.copyOf(a, n);
            Arrays.sort(b);
            for (int i = 0; i < n; i++) {
                if(a[i] == b[0]) idx = i;
            }
            System.out.println(idx);
        }
    }
}

class J02035_Reader {
    BufferedReader br;
    StringTokenizer st;

    public J02035_Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}
