import java.util.Scanner;

public class J01013_TongUocSo {
    static long sum = 0;
    static final int MAXN = 2000001;
    static int spf[] = new int[MAXN];
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        sieve();
        int T = sc.nextInt();
        while (T > 0) {
            int a = sc.nextInt();
            while (a != 1) {
                sum += spf[a];
                a = a / spf[a];
            }
            T -= 1;
        }
        System.out.println(sum);
        sc.close();
    }

    static void sieve() {
        spf[1] = 1;
        for (int i = 2; i < MAXN; i++)
            spf[i] = i;
        for (int i = 4; i < MAXN; i += 2)
            spf[i] = 2;
        for (int i = 3; i * i < MAXN; i++) {
            if (spf[i] == i) {
                for (int j = i * i; j < MAXN; j += i)
                    if (spf[j] == j)
                        spf[j] = i;
            }
        }
    }
}
