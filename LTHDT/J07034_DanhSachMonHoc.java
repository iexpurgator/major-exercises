import java.io.*;
import java.util.*;

/**
 *
 * @author Donald
 */
public class J07034_DanhSachMonHoc {

    static Scanner sc;

    public static void main(String[] args) throws Exception {
        sc = new Scanner(new FileInputStream("MONHOC.in"));
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        ArrayList<Mon> list = new ArrayList<>();
        String idm, name;
        int cnt;
        while (id++ < T) {
            idm = sc.nextLine();
            name = sc.nextLine();
            cnt = Integer.parseInt(sc.nextLine());
            list.add(new Mon(idm, name, cnt));
        }
        Collections.sort(list, (o1, o2) -> {
            return o1.getName().compareTo(o2.getName());
        });
        list.forEach(System.out::println);
    }
}

class Mon {

    private String id, name;
    private int cnt;

    public Mon(String id, String name, int cnt) {
        this.id = id;
        this.name = name;
        this.cnt = cnt;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + cnt;
    }
}
