import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class J05053_SapXepDonHang {
    static Scanner sc;
    public static void main(String[] args) {
        sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        int i = 0;
        List<DH52> list = new ArrayList<>();
        while (i++ < T) {
            String name, id;
            name = sc.nextLine();
            id = sc.nextLine();
            long price = Integer.parseInt(sc.nextLine());
            int count = Integer.parseInt(sc.nextLine());
            list.add(new DH52(name, id, price, count));
        }
        list.sort((o1, o2) -> (o1.getId().compareTo(o2.getId())));
        list.forEach(System.out::println);
    }
}

class DH52 {
    private String name;
    private String id;
    private long price;
    private int count;
    private long discount;
    private long total;

    public DH52() {
    }

    public DH52(String name, String id, long price, int count) {
        this.name = name;
        this.id = id;
        this.price = price;
        this.count = count;
        calc();
    }

    public String getId() {
        return id.substring(1);
    }

    private void calc() {
        double code = Integer.parseInt(id.substring(4)) == 2 ? 0.3 : 0.5;
        discount = Math.round(price * count * code);
        total = (price * count) - discount;
    }

    @Override
    public String toString() {
        return name + " " + id + " " + id.substring(1, 4) + " " + discount + " " + total;
    }
}
