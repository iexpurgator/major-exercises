import java.util.*;

public class J02102_MaTranXoanOcTangDan {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int n = sc.nextInt();
        int[] a = new int[n * n];
        for (int i = 0; i < n * n; ++i)
            a[i] = sc.nextInt();
        Arrays.sort(a);
        int val = 0;
        int i = 0, j = 0;
        int start = 0, end = n;
        int d = 0;
        int[][] m = new int[n][n];
        while (val < n * n) {
            m[i][j] = a[val];
            // System.out.println("[" + i + ", " + j + "] " + d);
            if (d == 0) {
                if (j + 1 < end)
                    j++;
                else {
                    i++;
                    d = 1;
                }
            } else if (d == 1) {
                if (i + 1 < end)
                    i++;
                else {
                    j--;
                    end--;
                    d = 2;
                }
            } else if (d == 2) {
                if (j > start)
                    j--;
                else {
                    start++;
                    i--;
                    d = 4;
                }
            } else {
                if (i > start)
                    i--;
                else {
                    j++;
                    d = 0;
                }
            }
            val++;
        }
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                System.out.print(m[i][j] + " ");
            }
            System.out.println();
        }
    }
}
