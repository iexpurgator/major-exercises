package J06006;

public class HoaDon2 {
    private String maHD;
    private String maKH;
    private String maMH;
    private KhachHang khachHang;
    private MatHang matHang;
    private int count;
    private long income;
    private long profit;

    public HoaDon2() {
    }

    public HoaDon2(int maHD, String maKH, String maMH, int count) {
        this.maHD = String.format("HD%03d", maHD);
        this.maKH = maKH;
        this.maMH = maMH;
        this.count = count;
    }

    public String getMaHD() {
        return maHD;
    }

    public void setMaHD(String maHD) {
        this.maHD = maHD;
    }

    public String getMaKH() {
        return maKH;
    }

    public void setMaKH(String maKH) {
        this.maKH = maKH;
    }

    public String getMaMH() {
        return maMH;
    }

    public void setMaMH(String maMH) {
        this.maMH = maMH;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public KhachHang getKhachHang() {
        return khachHang;
    }

    public void setKhachHang(KhachHang khachHang) {
        this.khachHang = khachHang;
    }

    public MatHang getMatHang() {
        return matHang;
    }

    public void setMatHang(MatHang matHang) {
        this.matHang = matHang;
        income = (long) count * (long) matHang.getPout();
        profit = (long)(matHang.getPout() - matHang.getPin()) * (long) (count);
    }

    public long getIncome() {
        return income;
    }

    public long getProfit() {
        return profit;
    }

    @Override
    public String toString() {
        return maHD + " " + khachHang.getTen() + " " + khachHang.getDc() + " " + matHang.getTen() + " " + count + " " + income + " " + profit;
    }
}
