package J06006;

class KhachHang {
    private String ma;
    private String ten;
    private String gt;
    private String sn; // sinh nhat
    private String dc;

    public KhachHang() {
    }

    public KhachHang(int ma, String ten, String gt, String sn, String dc) {
        this.ma = String.format("KH%03d", ma);
        this.ten = ten;
        this.gt = gt;
        this.sn = sn;
        this.dc = dc;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getGt() {
        return gt;
    }

    public void setGt(String gt) {
        this.gt = gt;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getDc() {
        return dc;
    }

    public void setDc(String dc) {
        this.dc = dc;
    }
}
