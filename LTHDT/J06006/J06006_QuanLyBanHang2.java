package J06006;

import java.util.*;

public class J06006_QuanLyBanHang2 {
    static Scanner sc;
    public static void main(String[] args) {
        sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        List<KhachHang> listKH = new ArrayList<>();
        int i = 0;
        while (i++ < T) {
            String ten, gt, sn, dc;
            ten = sc.nextLine();
            gt = sc.nextLine();
            sn = sc.nextLine();
            dc = sc.nextLine();
            listKH.add(new KhachHang(i, ten, gt, sn, dc));
        }
        T = Integer.parseInt(sc.nextLine());
        i = 0;
        List<MatHang> listMH = new ArrayList<>();
        while (i++ < T) {
            String ten, dv;
            int pin, pout;
            ten = sc.nextLine();
            dv = sc.nextLine();
            pin = Integer.parseInt(sc.nextLine());
            pout = Integer.parseInt(sc.nextLine());
            listMH.add(new MatHang(i, ten, dv, pin, pout));
        }
        T = Integer.parseInt(sc.nextLine());
        i = 0;
        List<HoaDon2> listHD = new ArrayList<>();
        while (i++ < T) {
            String[] s = sc.nextLine().split("\\s+");
            listHD.add(new HoaDon2(i, s[0], s[1], Integer.parseInt(s[2])));
        }
        for (HoaDon2 hoaDon : listHD) {
            String maKH = hoaDon.getMaKH();
            String maMH = hoaDon.getMaMH();
            KhachHang khachHang = listKH.stream().filter(e -> e.getMa().equals(maKH)).findFirst().orElseGet(null);
            MatHang matHang = listMH.stream().filter(e -> e.getMa().equals(maMH)).findFirst().orElseGet(null);
            hoaDon.setKhachHang(khachHang);
            hoaDon.setMatHang(matHang);
        }
        Collections.sort(listHD, (o1, o2) -> (int) (o2.getProfit() - o1.getProfit()));
        listHD.stream().forEach(System.out::println);
    }
}
