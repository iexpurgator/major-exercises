import java.util.*;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class J05082_DanhSachKhachHang {
    static J05082_Reader sc;
    public static void main(String[] args) throws IOException, ParseException {
        sc = new J05082_Reader();
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        ArrayList<KhachHang82> list = new ArrayList<>();
        String name, gender, day, addr;
        while (id++ < T) {
            name = sc.nextLine();
            gender = sc.nextLine();
            day = sc.nextLine();
            addr = sc.nextLine();
            list.add(new KhachHang82(id, name, gender, day, addr));
        }
        list.sort((e1, e2) -> {
            return e1.getDay().compareTo(e2.getDay());
        });
        list.forEach(System.out::println);
    }
}

class KhachHang82 {
    private String id;
    private String name;
    private String gender;
    private Date day;
    private String addr;

    KhachHang82(int id, String name, String gender, String day, String addr) throws ParseException {
        this.id = String.format("KH%03d", id);
        String[] nameFmt = name.trim().toLowerCase().split("\\s++");
        this.name = "";
        for (String string : nameFmt) {
            this.name += Character.toUpperCase(string.charAt(0)) + "" + string.substring(1) + " ";
        }
        this.gender = gender;
        this.day = new SimpleDateFormat("dd/MM/yyyy").parse(day);
        this.addr = addr;
    }

    /**
     * @return the day
     */
    public String getDay() {
        return new SimpleDateFormat("yyyyMMdd").format(this.day);
    }

    @Override
    public String toString() {
        return id + " " + name + "" + gender + " " + addr + " " + new SimpleDateFormat("dd/MM/yyyy").format(day);
    }
}

class J05082_Reader {
    BufferedReader br;
    StringTokenizer st;

    public J05082_Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}
