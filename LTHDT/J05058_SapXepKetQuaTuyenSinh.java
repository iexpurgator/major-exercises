import java.util.Scanner;
import java.util.List;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class J05058_SapXepKetQuaTuyenSinh {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        List<HocSinh58> list = new ArrayList<>();
        while (id++ < T) {
            String uid, name;
            double s1, s2, s3;
            uid = sc.nextLine();
            name = sc.nextLine();
            s1 = Double.parseDouble(sc.nextLine());
            s2 = Double.parseDouble(sc.nextLine());
            s3 = Double.parseDouble(sc.nextLine());
            list.add(new HocSinh58(uid, name, s1, s2, s3));
        }
        list.sort((a, b) -> (int) ((b.getSum() * 100) - (b.getSum() * 100)));
        Collections.sort(list, new Comparator<HocSinh58>() {
            public int compare(HocSinh58 o1, HocSinh58 o2) {
                if (o1.getSum() == o2.getSum()) {
                    return o1.getId().compareTo(o2.getId());
                } else
                    return (int) ((o2.getSum() * 100) - (o1.getSum() * 100));
            };
        });
        double ok = 24;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getSum() >= ok) {
                System.out.println(list.get(i) + " " + "TRUNG TUYEN");
            } else
                System.out.println(list.get(i) + " " + "TRUOT");
        }
    }
}

class HocSinh58 {
    private String id;
    private String name;
    private double sum;
    private double bonus;
    private static DecimalFormat df = new DecimalFormat("#.#");

    public HocSinh58(String id, String name, double s1, double s2, double s3) {
        this.id = id;
        this.name = name;
        int kv = id.charAt(2) - '0';
        if (kv == 1)
            bonus = 0.5;
        else if (kv == 2)
            bonus = 1.0;
        else if (kv == 3)
            bonus = 2.5;
        else
            bonus = 0;
        this.sum = s1 * 2 + s2 + s3 + bonus;
    }

    public double getSum() {
        return sum;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + df.format(bonus) + " " + df.format(sum);
    }
}
