import java.util.*;
/**
 *
 * @author Hoan
 */
public class J05012_TinhTien {
    static Scanner sc;
    public static void main(String[] args) {
        List<HoaDon> list = new ArrayList<>();
        sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {            
            String id, name;
            int count;
            long price, sale;
            id = sc.nextLine();
            name = sc.nextLine();
            count = Integer.parseInt(sc.nextLine());
            price = Long.parseLong(sc.nextLine());
            sale = Long.parseLong(sc.nextLine());
            list.add(new HoaDon(id, name, count, price, sale));
        }
        Collections.sort(list, new Comparator<HoaDon>() {
            @Override
            public int compare(HoaDon t, HoaDon t1) {
                return (int) ((t1.getTicket()/1000) - (t.getTicket()/1000));
            }
        });
        list.forEach(System.out::println);
    }
}

class HoaDon{
    private String id;
    private String name;
    private int count;
    private long price;
    private long sale;

    public HoaDon() {
    }

    public HoaDon(String id, String name, int count, long price, long sale) {
        this.id = id;
        this.name = name;
        this.count = count;
        this.price = price;
        this.sale = sale;
    }

    public long getTicket(){
        return price * count - sale;
    }
    
    @Override
    public String toString() {
        return id + " " + name + " " + count + " " + price + " " + sale + ' ' + getTicket();
    }
}