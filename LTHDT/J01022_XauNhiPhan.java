import java.util.Scanner;

public class J01022_XauNhiPhan {
    private static Scanner sc = new Scanner(System.in);
    private static long[] x = new long[94];

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        x[1] = 1;
        for (int i = 2; i < 93; ++i) {
            x[i] = x[i - 2] + x[i - 1];
        }
        while (T > 0) {
            int n = sc.nextInt();
            long k = sc.nextLong();
            System.out.println(dac(n, k));
            T -= 1;
        }
    }

    private static int dac(int n, long k) {
        if (n == 1 && k == 1)
            return 0;
        if (n == 2 && k == 1)
            return 1;
        if (k <= x[n - 2])
            return dac(n - 2, k);
        return dac(n - 1, k - x[n - 2]);
    }
}
