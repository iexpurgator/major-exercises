import java.util.*;
import java.io.*;
import java.text.SimpleDateFormat;

public class J05072_TinhCuocDienThoaiCoDinh2 {
    public static void main(String[] args) throws IOException {
        J05072_Reader sc = new J05072_Reader();
        int T = Integer.parseInt(sc.nextLine());
        ManagerCall2 manager = new ManagerCall2();
        manager.addArea("-1", "Noi mang", 800);
        String id, name;
        long price;
        while (T-- > 0) {
            id = sc.nextLine();
            name = sc.nextLine();
            price = Long.parseLong(sc.nextLine());
            manager.addArea(id, name, price);
        }
        T = Integer.parseInt(sc.nextLine());
        String[] inp;
        while (T-- > 0) {
            inp = sc.nextLine().split("\\s+");
            manager.addCall(inp[0], inp[1], inp[2]);
        }
        manager.show();
    }
}

class ManagerCall2 {
    private HashMap<String, PhoneArea2> mapArea;
    private ArrayList<CallInf2> listCall;

    public ManagerCall2() {
        mapArea = new HashMap<>();
        listCall = new ArrayList<>();
    }

    public void addCall(String phoneNum, String timeStart, String timeEnd) {
        listCall.add(new CallInf2(phoneNum, timeStart, timeEnd));
    }

    public void addArea(String id, String name, long price) {
        String tid = (id == "-1") ? id : "0" + id;
        mapArea.put(tid, new PhoneArea2(id, name, price));
    }

    public void show() {
        listCall.forEach((o) -> {
            PhoneArea2 phoneArea = mapArea.get(o.getAreaId());
            o.setName(phoneArea.getName());
            o.setTotal(phoneArea.getPrice() * o.getTime());
        });
        Collections.sort(listCall, (o1, o2) -> (int) (o2.getTotal() - o1.getTotal()));
        listCall.forEach(System.out::println);
    }
}

class CallInf2 {
    private String areaId;
    private String num;
    private String timeStart;
    private String timeEnd;
    private long total;
    private String name;

    public CallInf2(String phoneNum, String timeStart, String timeEnd) {
        if (phoneNum.charAt(0) == '0') {
            areaId = phoneNum.split("-")[0];
        } else {
            areaId = "-1";
        }
        this.num = phoneNum;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
    }

    public String getNum() {
        return num;
    }

    public String getAreaId() {
        return areaId;
    }

    public long getTime() {
        long time;
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        try {
            Date date1 = format.parse(timeStart);
            Date date2 = format.parse(timeEnd);
            time = (date2.getTime() - date1.getTime()) / 60000;
            if (areaId == "-1") {
                double t = time / 3.0;
                time = Math.round(t);
            }
        } catch (Exception e) {
            time = 1;
        }
        return time;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getTotal() {
        return total;
    }

    @Override
    public String toString() {
        return num + " " + name + " " + getTime() + " " + total;
    }
}

class PhoneArea2 {
    // private String id;
    private String name;
    private long price;

    public PhoneArea2(String id, String name, long price) {
        // this.id = id;
        this.name = name;
        this.price = price;
    }

    public long getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }
}

class J05072_Reader {
    BufferedReader br;
    StringTokenizer st;

    public J05072_Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}
