import java.util.*;
import java.io.*;

/**
 * @author Donald
 */
public class TinhDiemTrungBinh {

    static Scanner sc;

    public static void main(String[] args) throws Exception {
        sc = new Scanner(new FileInputStream("BANGDIEM.in"));
//        sc = new Scanner(new FileInputStream("src/T24112021/BANGDIEM_XH.in"));
        int T = Integer.parseInt(sc.nextLine());
        ArrayList<SinhVienPtit> listSV = new ArrayList<>();
        String ten;
        float d1, d2, d3;
        for (int i = 1; i <= T; i++) {
            ten = sc.nextLine();
            d1 = Float.parseFloat(sc.nextLine());
            d2 = Float.parseFloat(sc.nextLine());
            d3 = Float.parseFloat(sc.nextLine());
            listSV.add(new SinhVienPtit(i, ten, d1, d2, d3));
        }
        Collections.sort(listSV);
        int lastRank = 1;
        listSV.get(0).setXepHang(lastRank);
        for (int j = 1; j < listSV.size(); j++) {
            lastRank = listSV.get(j).getDiemTB() == listSV.get(j - 1).getDiemTB() ? lastRank : j + 1;
            listSV.get(j).setXepHang(lastRank);
        }
        listSV.forEach(System.out::println);
    }
}

class SinhVienPtit implements Comparable<SinhVienPtit> {
    private String ma, ten;
    private float diem1, diem2, diem3;
    private int xepHang;

    public SinhVienPtit(int ma, String ten, float diem1, float diem2, float diem3) {
        this.ma = String.format("SV%02d", ma);
        this.ten = capitalize(ten);
        this.diem1 = diem1;
        this.diem2 = diem2;
        this.diem3 = diem3;
    }

    private String capitalize(String string) {
        String[] split = string.toLowerCase().trim().split("\\s+");
        String capitalized = "";
        for (String s : split) {
            capitalized += Character.toUpperCase(s.charAt(0)) + s.substring(1) + " ";
        }
        return capitalized.trim();
    }

    public void setXepHang(int xepHang) {
        this.xepHang = xepHang;
    }

    public float getDiemTB() {
        return ((diem1 * 3) + (diem2 * 3) + (diem3 * 2)) / 8;
    }

    @Override
    public String toString() {
        return ma + " " + ten + " " + String.format("%.02f", getDiemTB()) + " " + xepHang;
    }

    @Override
    public int compareTo(SinhVienPtit o) {
        if (o.getDiemTB() == getDiemTB()) return ma.compareTo(o.ma);
        else return (int) ((o.getDiemTB() * 100) - (getDiemTB() * 100));
    }
}