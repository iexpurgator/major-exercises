import java.util.*;

public class J04022_WordSet {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        WordSet s1 = new WordSet(in.nextLine());
        WordSet s2 = new WordSet(in.nextLine());
        System.out.println(s1.union(s2));
        System.out.println(s1.intersection(s2));
    }
}

class WordSet {
    private TreeSet<String> list;

    public WordSet(String s) {
        list = new TreeSet<>();
        String[] arr = s.toLowerCase().split("\\s+");
        for (String string : arr) {
            if (!list.contains(string)) {
                list.add(string);
            }
        }
    }
    
    public WordSet(TreeSet<String> l){
        this.list = l;
    }

    public WordSet union(WordSet s) {
        TreeSet<String> word = new TreeSet<>();
        word.addAll(s.list);
        word.addAll(this.list);
        return new WordSet(word);
    }
    
    public WordSet intersection(WordSet s) {
        TreeSet<String> word = new TreeSet<>();
        word.addAll(s.list);
        word.retainAll(this.list);
        return new WordSet(word);
    }

    @Override
    public String toString() {
        return list.toString().replace("[", "").replace("]", "").replace(",", "");
    }
}
