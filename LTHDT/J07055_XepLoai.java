import java.util.*;
import java.io.*;

/**
 * @author Donald
 */
public class XepLoai {

    static Scanner sc;

    public static void main(String[] args) throws Exception {
//        sc = new Scanner(new FileInputStream("src/T24112021/BANGDIEM.in"));
        sc = new Scanner(new FileInputStream("BANGDIEM.in"));
        int T = Integer.parseInt(sc.nextLine());
        ArrayList<SinhVien> listSV = new ArrayList<>();
        String ten;
        float d1, d2, d3;
        for (int i = 1; i <= T; i++) {
            ten = sc.nextLine();
            d1 = Float.parseFloat(sc.nextLine());
            d2 = Float.parseFloat(sc.nextLine());
            d3 = Float.parseFloat(sc.nextLine());
            listSV.add(new SinhVien(i, ten, d1, d2, d3));
        }
        Collections.sort(listSV);
        listSV.forEach(System.out::println);
    }
}

class SinhVien implements Comparable<SinhVien> {
    private String ma;
    private String ten;
    private float diemLT, diemTH, diemThi;

    public SinhVien(int ma, String ten, float diemLT, float diemTH, float diemThi) {
        this.ma = String.format("SV%02d", ma);
        this.ten = captitalize(ten);
        this.diemLT = diemLT;
        this.diemTH = diemTH;
        this.diemThi = diemThi;
    }

    private String captitalize(String str) {
        String[] split = str.trim().toLowerCase().split("\\s+");
        String res = "";
        for (String s : split) {
            res += Character.toUpperCase(s.charAt(0)) + s.substring(1) + " ";
        }
        return res.trim();
    }

    public float getDiem() {
        return (diemLT * 0.25F) + (diemTH * 0.35F) + (diemThi * 0.4F);
    }

    public String getXepLoai() {
        float diem = getDiem();
        if (diem >= 8) return "GIOI";
        else if (diem >= 6.5) return "KHA";
        else if (diem >= 5) return "TRUNG BINH";
        else return "KEM";
    }

    @Override
    public String toString() {
        return ma + " " + ten + " " + String.format("%.02f", getDiem()) + " " + getXepLoai();
    }

    @Override
    public int compareTo(SinhVien o) {
        return (int) ((o.getDiem() * 100) - (getDiem() * 100));
    }
}