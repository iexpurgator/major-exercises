import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class J05033_SapXepThoiGian {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        List<Time> list = new ArrayList<>();
        int T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            String in = sc.nextLine();
            list.add(new Time(in));
        }
        Collections.sort(list, new Comparator<Time>() {
            @Override
            public int compare(Time o1, Time o2) {
                return o1.toInt() - o2.toInt();
            }
        });
        list.stream().forEach(System.out::println);
    }
}

class Time {
    private int hour;
    private int min;
    private int sec;

    Time() {
    }

    Time(String in) {
        String[] s = in.split("\\s+");
        this.hour = Integer.parseInt(s[0]);
        this.min = Integer.parseInt(s[1]);
        this.sec = Integer.parseInt(s[2]);
    }

    Time(int hour, int min, int sec) {
        this.hour = hour;
        this.min = min;
        this.sec = sec;
    }

    public int toInt() {
        return hour * 3600 + min * 60 + sec;
    }

    @Override
    public String toString() {
        return hour + " " + min + " " + sec;
    }
}