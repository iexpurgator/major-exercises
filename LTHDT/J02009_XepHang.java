import java.util.*;

public class J02009_XepHang {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = sc.nextInt();
        int id = 0;
        ArrayList<pairInt> list = new ArrayList<>();
        while (id++ < T)
            list.add(new pairInt(sc.nextInt(), sc.nextInt()));
        Collections.sort(list);
        int res = 0;
        for (pairInt pair : list)
            res = pair.getTime(res);
        System.out.println(res);
    }
}

class pairInt implements Comparable<pairInt> {
    private int begin;
    private int time;

    public pairInt(int begin, int time) {
        this.begin = begin;
        this.time = time;
    }

    public int getTime(int base) {
        if (base > begin)
            return base + time;
        else
            return begin + time;
    }

    public int getBegin() {
        return begin;
    }

    @Override
    public int compareTo(pairInt o) {
        return begin - o.getBegin();
    }
}