import java.text.*;
import java.util.*;

/**
 *
 * @author Hoan
 */
public class J05016_HoaDonKhachSan {
    static Scanner sc;

    public static void main(String[] args) throws Exception {
        List<KhachHang> list = new ArrayList<>();
        sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        while (id++ < T) {
            String name, room, checkin, checkout;
            int incurred;
            name = sc.nextLine();
            room = sc.nextLine();
            checkin = sc.nextLine();
            checkout = sc.nextLine();
            incurred = Integer.parseInt(sc.nextLine());
            list.add(new KhachHang(id, name, room, checkin, checkout, incurred));
        }
        Collections.sort(list, new Comparator<KhachHang>() {
            @Override
            public int compare(KhachHang t, KhachHang t1) {
                return t1.getCost() - t.getCost();
            }
        });
        list.forEach(System.out::println);
    }
}

class KhachHang {

    private String id;
    private String name;
    private String room;
    private String checkin;
    private String checkout;
    private int incurred;
    private int dateCnt;
    private final Map<Character, Integer> map = new HashMap<Character, Integer>() {
        {
            put('1', 25);
            put('2', 34);
            put('3', 50);
            put('4', 80);
        }
    };

    public KhachHang() {
    }

    public KhachHang(int id, String name, String room, String checkin, String checkout, int incurred) throws Exception {
        this.id = String.format("KH%02d", id);
        this.name = name;
        this.room = room;
        this.checkin = checkin;
        this.checkout = checkout;
        this.incurred = incurred;
        getSoNgay();
    }

    private void getSoNgay() throws Exception {
        SimpleDateFormat d = new SimpleDateFormat("dd/MM/yyyy");
        Date in = d.parse(checkin);
        Date out = d.parse(checkout);
        long timecnt = (out.getTime() - in.getTime()) / (1000*60*60*24);
        this.dateCnt = (int) timecnt + 1;
    }

    public int getCost() {
        int cost = dateCnt * map.get(room.charAt(0)) + incurred;
        return cost;
    }

    @Override
    public String toString() {
        int cost = getCost();
        return id + " " + name + " " + room + " " + dateCnt + " " + cost;
    }
}