import java.util.*;
import java.util.Map.Entry;

public class J08011_LietKeVaDem {
    static Map<String, Integer> map = new LinkedHashMap<>();
    static int len = 0;
    static String str;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            try {
                str = sc.next();
            } catch (NoSuchElementException e) {
                sc.close();
                break;
            }
            if (isInc(str)) {
                map.put(str, map.getOrDefault(str, 0) + 1);
            }
        }
        Set<Map.Entry<String, Integer>> companyFounderSet = map.entrySet();
        List<Map.Entry<String, Integer>> companyFounderListEntry = new ArrayList<Map.Entry<String, Integer>>(
                companyFounderSet);
        Collections.sort(companyFounderListEntry, new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Entry<String, Integer> es1, Entry<String, Integer> es2) {
                return es2.getValue() - es1.getValue();
            }
        });
        map.clear();
        for (Map.Entry<String, Integer> lmap : companyFounderListEntry) {
            map.put(lmap.getKey(), lmap.getValue());
        }
        map.entrySet().stream().forEach(k -> {
            System.out.println(k.getKey() + " " + k.getValue());
        });
    }

    public static boolean isInc(String text) {
        for (int i = 0; i < text.length() - 1; i++) {
            if (text.charAt(i + 1) < text.charAt(i))
                return false;
        }
        return true;
    }
}
