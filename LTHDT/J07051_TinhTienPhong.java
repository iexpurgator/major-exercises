import java.text.*;
import java.util.*;
import java.io.*;

/**
 * @author Donald
 */
public class TinhTienPhong {

    static Scanner sc;

    public static void main(String[] args) throws Exception {
        sc = new Scanner(new FileInputStream("KHACHHANG.in"));
        int T = Integer.parseInt(sc.nextLine());
        ArrayList<KhachHang> listKH = new ArrayList<>();
        String ten, phong, nhan, tra;
        int tienPS;
        for (int i = 1; i <= T; i++) {
            ten = sc.nextLine();
            phong = sc.nextLine();
            nhan = sc.nextLine();
            tra = sc.nextLine();
            tienPS = Integer.parseInt(sc.nextLine());
            listKH.add(new KhachHang(i, ten, phong, nhan, tra, tienPS));
        }
        Collections.sort(listKH);
        listKH.forEach(System.out::println);
    }
}

class KhachHang implements Comparable<KhachHang> {
    private String ma;
    private String ten;
    private String phong;
    private Date ngayNhan;
    private Date ngayTra;
    private int tienPhatSinh;

    private HashMap<Character, Integer> donGia = new HashMap<Character, Integer>() {{
        put('1', 25);
        put('2', 34);
        put('3', 50);
        put('4', 80);
    }};

    public KhachHang(int ma, String ten, String phong, String ngayNhan, String ngayTra, int tienPhatSinh) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        this.ma = String.format("KH%02d", ma);
        this.ten = formatName(ten);
        this.phong = phong;
        this.ngayNhan = sdf.parse(ngayNhan);
        this.ngayTra = sdf.parse(ngayTra);
        this.tienPhatSinh = tienPhatSinh;
    }

    private String formatName(String name) {
        String[] split = name.toLowerCase().trim().split("\\s+");
        String ten = "";
        for (String s : split) {
            ten += Character.toUpperCase(s.charAt(0)) + s.substring(1) + " ";
        }
        return ten.trim();
    }

    public int getSoNgayO() {
        long diff = (ngayTra.getTime() - ngayNhan.getTime()) / (1000 * 60 * 60 * 24);
        return (int) (diff + 1);
    }

    public int getGiaTien() {
        char tang = phong.charAt(0);
        return getSoNgayO() * donGia.get(tang) + tienPhatSinh;
    }

    @Override
    public String toString() {
        return ma + " " + ten + " " + phong + " " + getSoNgayO() + " " + getGiaTien();
    }

    @Override
    public int compareTo(KhachHang o) {
        return o.getGiaTien() - getGiaTien();
    }
}