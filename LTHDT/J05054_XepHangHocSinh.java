import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class J05054_XepHangHocSinh {
    static Scanner sc;
    public static void main(String[] args) {
        sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        int i = 0;
        List<HS54> list = new ArrayList<>();
        while (i++ < T) {
            String name = sc.nextLine();
            double score = Double.parseDouble(sc.nextLine());
            list.add(new HS54(i, name, score));
        }
        list.sort((o1, o2) -> o2.getScore() - o1.getScore());
        int lastRank = 1;
        list.get(0).setRank(lastRank);
        for (int j = 1; j < list.size(); j++) {
            lastRank = list.get(j).getScore() == list.get(j - 1).getScore() ? lastRank : j + 1;
            list.get(j).setRank(lastRank);
        }
        list.sort((o1, o2) -> o1.getId().compareTo(o2.getId()));
        list.forEach(System.out::println);
    }
}

class HS54 {
    private String id;
    private String name;
    private double score;
    private int rank;

    public HS54() {
    }

    public HS54(int id, String name, double score) {
        this.id = String.format("HS%02d", id);
        this.name = name;
        this.score = score;
    }

    private String xl() {
        if (score > 9) {
            return "Gioi";
        } else if (score >= 7) {
            return "Kha";
        } else if (score >= 5) {
            return "Trung Binh";
        } else return "Yeu";
    }

    public String getId() {
        return id;
    }

    public int getScore() {
        return (int) (score * 10);
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + score + " " + xl() + " " + rank;
    }
}