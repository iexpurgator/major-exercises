import java.util.Scanner;

public class J01004_SoNguyenTo {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        while (T > 0) {
            long a;
            a = Long.parseLong(sc.nextLine());
            if (isPrime(a)) {
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }
            T -= 1;
        }
    }

    private static boolean isPrime(long a) {
        if (a == 2)
            return true;
        else if (a % 2 == 0)
            return false;
        for (long i = 3; i * i < a; ++i) {
            if (a % i == 0)
                return false;
        }
        return true;
    }
}
