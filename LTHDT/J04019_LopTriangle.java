import java.util.*;

public class J04019_LopTriangle {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while(t-- >0){
            Triangle a = new Triangle(Point.nextPoint(sc), Point.nextPoint(sc), Point.nextPoint(sc));
            if(!a.valid()){
                System.out.println("INVALID");
            } else{
                System.out.println(a.getPerimeter());
            }
        }
    }
    public static void main8771625(String[] args) {;}
}

class Triangle {
    private double d12, d13, d23;

    public Triangle(Point p1, Point p2, Point p3) {
        d12 = p1.distance(p2);
        d13 = p1.distance(p3);
        d23 = p3.distance(p2);
    }

    public boolean valid() {
        return !(d12 + d13 <= d23 || d12 + d23 <= d13 || d13 + d23 <= d12);
    }

    public String getPerimeter() {
        return String.format("%.3f", d12 + d13 + d23);
    }
}

class Point {
    private double x;
    private double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point(Point p) {
        this.x = p.x;
        this.y = p.y;
    }

    public static Point nextPoint(Scanner sc) {
        return new Point(sc.nextDouble(), sc.nextDouble());
    }

    public double distance(Point p) {
        return distance(this, p);
    }

    public static double distance(Point p1, Point p2) {
        return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
    }
}
