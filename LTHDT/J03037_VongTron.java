import java.util.*;
import java.io.*;

public class J03037_VongTron {
    public static void main(String[] args) throws IOException {
        J03037Reader sc = new J03037Reader();
        String s = sc.nextLine();
        int k;
        int[] a, b;
        a = new int[26];
        b = new int[26];
        for (int i = 0; i < s.length(); i++) {
            k = s.charAt(i) - 'A';
            if (a[k] == 0) {
                a[k] = i + 1;
            } else
                b[k] = i + 1;
        }
        k = 0;
        for (int i = 0; i < 26; i++) {
            for (int j = 0; j < 26; j++)
            // kí tự thứ j nằm trong khoảng kí tự i: a[i] < a[j] && a[j] < b[i]
            // chỉ có một kí tự j nằm trong khoảng kí tự i: b[j] > b[i]
                if (a[i] < a[j] && a[j] < b[i] && b[j] > b[i])
                    k++;
        }
        System.out.println(k);
    }
}

class J03037Reader {
    BufferedReader br;
    StringTokenizer st;

    public J03037Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}