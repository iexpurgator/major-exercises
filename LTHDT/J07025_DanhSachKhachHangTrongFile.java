import java.util.*;
import java.io.*;
/**
 *
 * @author Donald
 */
public class J07025_DanhSachKhachHangTrongFile {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(new File("KHACHHANG.in"));
        int T = Integer.parseInt(sc.nextLine());
        int i = 0;
        String name, gender, date, addr;
        String[] inName;
        ArrayList<KhachHang1> kh = new ArrayList<>();
        while (i++ < T) {
            inName = sc.nextLine().toLowerCase().trim().split("\\s+");
            name = "";
            for (String string : inName) {
                name += Character.toUpperCase(string.charAt(0)) + "" + string.substring(1) + " ";
            }
            gender = sc.nextLine();
            date = sc.nextLine();
            addr = sc.nextLine();
            kh.add(new KhachHang1(i, name, date, gender, addr));
        }
        kh.sort((e1, e2) -> {
            return e1.getDate().compareTo(e2.getDate());
        });
        kh.forEach(System.out::println);
    }
}

class KhachHang1 {
    private String id;
    private String name;
    private String date;
    private String gender;
    private String addr;

    KhachHang1(int i, String name, String date, String gender, String addr) {
        this.id = "KH" + String.format("%03d", i);
        this.name = name;
        this.date = date;
        this.gender = gender;
        this.addr = addr;
    }

    public String getDate() {
        String[] arDate = date.split("/");
        return String.format("%s%02d%02d", arDate[2], Integer.parseInt(arDate[1]), Integer.parseInt(arDate[0]));
    }
    
    @Override
    public String toString() {
        String[] arDate = date.split("/");
        date = String.format("%02d/%02d/%s", Integer.parseInt(arDate[0]), Integer.parseInt(arDate[1]
), arDate[2]);
        return id + " " + name + " " + gender + " " + addr + " " + date;
    }
}