import java.util.Scanner;

public class J03022_XuLyVanBan {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        String s = "";
        while (sc.hasNextLine()) {
            String in = sc.nextLine().toLowerCase().replaceAll("\\s+", " ");
            s += in;
        }
        String[] sequence = s.split("[?.!]+\\s*");
        for (String string : sequence) {
            string = Character.toUpperCase(string.charAt(0)) + string.substring(1);
            System.out.println(string);
        }
        // System.out.println(s);
    }
}
