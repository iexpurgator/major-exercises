import java.util.Scanner;

public class J02004_MangDoiXung {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        for (int k = 0; k < test; k++) {
            int n = sc.nextInt();
            int a[] = new int[n];
            boolean ck = true;
            for (int i = 0; i < n; i++)
                a[i] = sc.nextInt();
            for (int i = 0; i * 2 < n; ++i) {
                if (a[i] != a[n - 1 - i]) {
                    ck = false;
                    break;
                }
            }
            if (ck)
                System.out.println("YES");
            else
                System.out.println("NO");
        }
        sc.close();
    }
}
