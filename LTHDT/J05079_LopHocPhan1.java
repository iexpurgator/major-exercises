import java.util.*;
import java.io.*;

public class J05079_LopHocPhan1 {
    public static void main(String[] args) throws IOException {
        J05079_Reader sc = new J05079_Reader();
        int T = Integer.parseInt(sc.nextLine());
        ArrayList<HocPhan79> list = new ArrayList<>();
        String id, name, ordinal, teachName;
        while (T --> 0) {
            id = sc.nextLine();
            name = sc.nextLine();
            ordinal = sc.nextLine();
            teachName = sc.nextLine();
            list.add(new HocPhan79(id, name, ordinal, teachName));
        }
        list.sort((e1, e2) -> e1.getOrdinal().compareTo(e2.getOrdinal()));
        T = Integer.parseInt(sc.nextLine());
        while(T -- > 0){
            final String st = sc.nextLine();
            int cnt = 0;
            for (HocPhan79 hp : list) {
                if(hp.getId().equals(st)) {
                    if (cnt == 0) 
                        System.out.printf("Danh sach nhom lop mon %s:\n", hp.getName());
                    System.out.println(hp);
                    cnt++;
                }
            }
        }
    }
}

class HocPhan79{
    private String id, name, ordinal, teachName;

    public HocPhan79(String id, String name, String ordinal, String teachName) {
        this.id = id;
        this.name = name;
        this.ordinal = ordinal;
        this.teachName = teachName;
    }
    
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getOrdinal() {
        return ordinal;
    }

    @Override
    public String toString() {
        return ordinal + " " + teachName;
    }
}

class J05079_Reader {
    BufferedReader br;
    StringTokenizer st;

    public J05079_Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}
