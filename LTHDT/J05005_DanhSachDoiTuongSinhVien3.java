import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class J05005_DanhSachDoiTuongSinhVien3 {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        List<SinhVien3> list = new ArrayList<>();
        int i = 0;
        while (i++ < T) {
            String[] iname = sc.nextLine().toLowerCase().trim().split("\\s+");
            String name = "";
            for (String s : iname) {
                name += (s.substring(0, 1).toUpperCase() + s.substring(1) + " ");
            }
            String iclass = sc.nextLine();
            String date = sc.nextLine();
            double GPA = Double.parseDouble(sc.nextLine());
            list.add(new SinhVien3(i, name, date, iclass, GPA));
        }
        Collections.sort(list, new Comparator<SinhVien3>(){
            @Override
            public int compare(SinhVien3 o1, SinhVien3 o2) {
                return (int)((o2.getGPA() - o1.getGPA()) * 100); // o2 - o1 -> increase
            }
        });
        list.stream().forEach(System.out::println);
    }
}

class SinhVien3 {
    private String id;
    private String name;
    private String date;
    private String iclass;
    private double GPA;

    SinhVien3() {
    }

    SinhVien3(int i, String name, String date, String iclass, double GPA) {
        this.id = "B20DCCN" + String.format("%03d", i);
        this.name = name;
        this.date = date;
        this.iclass = iclass;
        this.GPA = GPA;
    }

    public double getGPA() {
        return GPA;
    }

    public String getDate() {
        String[] s = date.split("/");
        return String.format("%02d/%02d/%s", Integer.parseInt(s[0]), Integer.parseInt(s[1]), s[2]);
    }

    @Override
    public String toString() {
        return id + " " + name + " " + iclass + " " + getDate() + " " + String.format("%.02f", GPA);
    }
}