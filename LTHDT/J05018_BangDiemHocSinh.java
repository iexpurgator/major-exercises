import java.util.*;

/**
 *
 * @author Hoan
 */
public class J05018_BangDiemHocSinh {
    static Scanner sc;

    public static void main(String[] args) {
        sc = new Scanner(System.in);
        List<HocSinh18> list = new ArrayList<>();
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        while (id++ < T) {
            String name;
            double[] diem = new double[10];
            // id = Integer.parseInt(sc.nextLine());
            name = sc.nextLine();
            String[] line2 = sc.nextLine().split("\\s+");
            for (int i = 0; i < 10; i++) {
                diem[i] = Double.parseDouble(line2[i]);
            }
            list.add(new HocSinh18(id, name, diem));
        }
        Collections.sort(list, new Comparator<HocSinh18>() {
            @Override
            public int compare(HocSinh18 t, HocSinh18 t1) {
                return (int) ((t1.diemTB() * 100) - (t.diemTB() * 100));
            }
        });
        list.forEach(System.out::println);
    }
}

class HocSinh18 {

    private String id;
    private String name;
    private double[] diem = new double[10];

    public HocSinh18() {
    }

    public HocSinh18(int id, String name, double[] diem) {
        this.id = String.format("HS%02d", id);
        this.name = name;
        this.diem = diem;
    }

    public double diemTB() {
        double sum = 0;
        for (double i : diem)
            sum += i;
        sum += diem[0] + diem[1];
        return Math.round((sum / 12.0) * 10) / 10.0;
    }

    private String xepLoai() {
        double tb = diemTB();
        String xl;
        if (tb >= 9) {
            xl = "XUAT SAC";
        } else if (tb >= 8) {
            xl = "GIOI";
        } else if (tb >= 7) {
            xl = "KHA";
        } else if (tb >= 5) {
            xl = "TB";
        } else {
            xl = "YEU";
        }
        return String.format("%.1f %s", tb, xl);
    }

    @Override
    public String toString() {
        return id + " " + name + " " + xepLoai();
    }
}