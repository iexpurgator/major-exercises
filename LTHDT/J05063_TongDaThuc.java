import java.util.*;
import java.util.stream.*;

public class J05063_TongDaThuc {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int t = Integer.parseInt(sc.nextLine());
        while (t-- > 0) {
            DaThuc p = new DaThuc(sc.nextLine());
            DaThuc q = new DaThuc(sc.nextLine());
            DaThuc r = p.cong(q);
            System.out.println(r);
        }
    }
}

class DaThuc {
    private ArrayList<DonThuc> list = new ArrayList<>();

    public DaThuc(ArrayList<DonThuc> list) {
        this.list = list;
    }

    DaThuc(String s) {
        String[] ss = s.split("\\s[+]\\s");
        String[] sk;
        for (String si : ss) {
            sk = si.split("[*][x]\\^");
            list.add(new DonThuc(Integer.parseInt(sk[0]), Integer.parseInt(sk[1])));
        }
    }

    public static DaThuc cong(DaThuc p, DaThuc q) {
        ArrayList<DonThuc> list = new ArrayList<>();
        p.list.forEach((d) -> {
            boolean added = false;
            for (int i = 0; i < q.list.size(); i++) {
                if (q.list.get(i).getBac() == d.getBac()) {
                    list.add(DonThuc.cong(q.list.get(i), d));
                    q.list.remove(i);
                    added = true;
                }
            }
            if (!added)
                list.add(d);
        });
        q.list.forEach((d) -> list.add(d));
        return new DaThuc(list);
    }

    public DaThuc cong(DaThuc p) {
        return cong(this, p);
    }

    @Override
    public String toString() {
        Collections.sort(this.list, (o1, o2) -> o2.getBac() - o1.getBac());
        return list.stream().map(Object::toString).collect(Collectors.joining(" + ")).toString();
    }
}

class DonThuc {
    private int heSo;
    private int bac;

    public DonThuc(int heSo, int bac) {
        this.heSo = heSo;
        this.bac = bac;
    }

    public static DonThuc cong(DonThuc d1, DonThuc d2) {
        return new DonThuc(d1.heSo + d2.heSo, d1.bac);
    }

    public int getBac() {
        return bac;
    }

    @Override
    public String toString() {
        return heSo + "*x^" + bac;
    }
}
