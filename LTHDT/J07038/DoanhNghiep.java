package J07038;

/**
 *
 * @author Donald
 */
public class DoanhNghiep {

    private String id;
    private String name;
    private int count;

    public DoanhNghiep(String id, String name, int count) {
        this.id = id;
        this.name = name;
        this.count = count;
    }

    public String getId() {
        return id;
    }

    public int getCount() {
        return count;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + count;
    }
}
