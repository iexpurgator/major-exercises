package J07038;

import java.util.*;
import java.io.*;

/**
 *
 * @author Donald
 */
public class J07038_DanhSachThucTap {

    static Scanner sc;

    public static void main(String[] args) throws Exception {
        sc = new Scanner(new FileInputStream("SINHVIEN.in"));
        String name, msv, email, lop;
        String[] fmname;
        int T = Integer.parseInt(sc.nextLine());
        HashMap<String, SinhVien> sv = new HashMap<>();
        while (T-- > 0) {
            name = "";
            msv = sc.nextLine();
            fmname = sc.nextLine().toLowerCase().trim().split("\\s+");
            for (String string : fmname)
                name += Character.toUpperCase(string.charAt(0)) + string.substring(1) + " ";
            lop = sc.nextLine();
            email = sc.nextLine();
            sv.put(msv, new SinhVien(msv, name, lop, email));
        }
        //
        sc = new Scanner(new FileInputStream("DN.in"));
        T = Integer.parseInt(sc.nextLine());
        String id;
        int count;
        HashMap<String, DoanhNghiep> dn = new HashMap<>();
        while (T-- > 0) {
            id = sc.nextLine();
            name = sc.nextLine();
            count = Integer.parseInt(sc.nextLine());
            dn.put(id, new DoanhNghiep(id, name, count));
        }
        //
        sc = new Scanner(new FileInputStream("THUCTAP.in"));
        T = sc.nextInt();
        ArrayList<ThucTap> list = new ArrayList<>();
        while (T-- > 0) {
            msv = sc.next();
            id = sc.next();
            list.add(new ThucTap(sv.get(msv), dn.get(id)));
        }
        Collections.sort(list);
        T = sc.nextInt();
        while (T-- > 0) {
            id = sc.next();
            int ii = 0;
            int n = dn.get(id).getCount();
            System.out.printf("DANH SACH THUC TAP TAI %s:\n", dn.get(id).getName());
            for (Iterator<ThucTap> iter = list.iterator(); iter.hasNext() && ii < n;) {
                ThucTap e = iter.next();
                if (e.getIdDN().equals(id)) {
                    System.out.println(e);
                    ii++;
                }
            }
        }
    }
}

class ThucTap implements Comparable<ThucTap> {

    private SinhVien sv;
    private DoanhNghiep dn;

    public ThucTap(SinhVien sv, DoanhNghiep dn) {
        this.sv = sv;
        this.dn = dn;
    }

    public String getIdDN() {
        return dn.getId();
    }

    @Override
    public int compareTo(ThucTap o) {
        return sv.getMsv().compareTo(o.sv.getMsv());
    }

    @Override
    public String toString() {
        return sv.toString();
    }
}
