import java.util.Scanner;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

public class J05038_BangKeTienLuong {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        List<NV38> list = new ArrayList<>();
        long sum = 0;
        while (id++ < T) {
            String name, level;
            int luongNgay, count;
            name = sc.nextLine();
            luongNgay = Integer.parseInt(sc.nextLine());
            count = Integer.parseInt(sc.nextLine());
            level = sc.nextLine();
            list.add(new NV38(id, name, luongNgay, count, level));
        }
        for (NV38 nv : list) {
            sum += nv.getRes();
            System.out.println(nv);
        }
        System.out.println("Tong chi phi tien luong: " + sum);
    }
}

class NV38 {
    private String id;
    private String name;
    private int luongNgay;
    private int count;
    private String level;
    private long luongThang;
    private long bonus;
    private long bonusLevel;
    private long res;
    private final Map<String, Integer> map = new HashMap<String, Integer>() {
        {
            put("GD", 250000);
            put("PGD", 200000);
            put("TP", 180000);
            put("NV", 150000);
        }
    };

    NV38() {
    }

    public NV38(int id, String name, int luongNgay, int count, String level) {
        this.id = String.format("NV%02d", id);
        this.name = name;
        this.luongNgay = luongNgay;
        this.count = count;
        this.level = level;
        calc();
    }

    private void calc() {
        luongThang = luongNgay * count;
        double bn;
        if (count >= 25)
            bn = 0.2;
        else if (count >= 22)
            bn = 0.1;
        else
            bn = 0;
        bonus = Math.round(luongThang * bn);
        bonusLevel = map.get(level);
        res = luongThang + bonus + bonusLevel;
    }

    public long getRes() {
        return res;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + luongThang + " " + bonus + " " + bonusLevel + " " + res;
    }

}