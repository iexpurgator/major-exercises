package J07035;

/**
 *
 * @author Donald
 */
public class MonHoc {
    private String id, name;
    private int cnt;

    public MonHoc(String id, String name, int cnt) {
        this.id = id;
        this.name = name;
        this.cnt = cnt;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }
    
    @Override
    public String toString() {
        return id + " " + name + " " + cnt;
    }
}
