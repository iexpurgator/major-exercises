package J07035;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;

/**
 *
 * @author Donald
 */
public class J07035_BangDiemTheoMonHoc {

    static Scanner sc;

    public static void main(String[] args) throws Exception {
        sc = new Scanner(new FileInputStream("SINHVIEN.in"));
        String name, msv, email, lop;
        String[] fmname;
        int T = Integer.parseInt(sc.nextLine());
        HashMap<String, SinhVien> sv = new HashMap<>();
        while (T-- > 0) {
            name = "";
            msv = sc.nextLine();
            fmname = sc.nextLine().toLowerCase().trim().split("\\s+");
            for (String string : fmname)
                name += Character.toUpperCase(string.charAt(0)) + string.substring(1) + " ";
            lop = sc.nextLine();
            email = sc.nextLine();
            sv.put(msv, new SinhVien(msv, name, lop, email));
        }
        //
        sc = new Scanner(new FileInputStream("MONHOC.in"));
        T = Integer.parseInt(sc.nextLine());
        HashMap<String, MonHoc> mon = new HashMap<>();
        String idm, monname;
        int cnt;
        while (T-- > 0) {
            idm = sc.nextLine();
            monname = sc.nextLine();
            cnt = Integer.parseInt(sc.nextLine());
            mon.put(idm, new MonHoc(idm, monname, cnt));
        }
        //
        sc = new Scanner(new FileInputStream("BANGDIEM.in"));
        T = sc.nextInt();
        SinhVien sv0;
        MonHoc mh;
        double diem;
        ArrayList<BD> bang = new ArrayList<>();
        while (T-- > 0) {
            msv = sc.next();
            idm = sc.next();
            diem = sc.nextDouble();
            sv0 = sv.get(msv);
            mh = mon.get(idm);
            bang.add(new BD(sv0, mh, diem));
        }
        // theo mon
        Collections.sort(bang, (o1, o2) -> {
            if (o1.getDiem() - o2.getDiem() == 0) return o1.getMsv().compareTo(o2.getMsv());
            else return (int) ((o2.getDiem() * 100) - (o1.getDiem() * 100));
        });
        T = sc.nextInt();
        while (T-- > 0) {
            idm = sc.next();
            final String idfn = idm;
            System.out.printf("BANG DIEM MON %s:\n", mon.get(idfn).getName());
            bang.stream().filter(e -> e.getMaMon().equals(idfn)).forEach(System.out::println);
        }
    }
}

class BD {

    private SinhVien sv;
    private MonHoc mon;
    private Double diem;
    private String lop, msv, maMon, monName;

    public BD(SinhVien sv, MonHoc mon, Double diem) {
        this.sv = sv;
        this.mon = mon;
        this.diem = diem;
        lop = sv.getLop();
        msv = sv.getMsv();
        maMon = mon.getId();
        monName = mon.getName();
    }

    public Double getDiem() {
        return diem;
    }

    public String getMsv() {
        return msv;
    }

    public String getMaMon() {
        return maMon;
    }
    
    @Override
    public String toString() {
        return sv.getMsv() + " " + sv.getName() + "" + lop + " " + new DecimalFormat("#.##").format(diem);
    }
}
