import java.util.*;

public class J04015_TinhThuNhapGiaoVien {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        String id = sc.nextLine();
        String name = sc.nextLine();
        long tien = Long.parseLong(sc.nextLine());
        GV15 gv = new GV15(id, name, tien);
        System.out.println(gv);
    }
}

class GV15 {
    private String id;
    private String name;
    private long tien;
    private int level, phucap;
    private long thunhap;

    public GV15(String id, String name, long tien) {
        this.id = id;
        this.name = name;
        this.tien = tien;
        calc();
    }

    private void calc() {
        level = Integer.parseInt(id.substring(2));
        String lv = id.substring(0, 2);
        if (lv.equals("HT"))
            phucap = 2000000;
        else if (lv.equals("HP"))
            phucap = 900000;
        else if (lv.equals("GV"))
            phucap = 500000;
        else
            phucap = 0;
        thunhap = (tien * level) + phucap;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + level + " " + phucap + " " + thunhap;
    }
}