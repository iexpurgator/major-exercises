import java.util.*;

public class J04010_DienTichHinhTronNgoaiTiepTamGiac {
    static Scanner sc;
    public static void main(String[] args) {
        // List<Integer> list = new ArrayList<>();
        sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            double x, y;
            x = sc.nextDouble();
            y = sc.nextDouble();
            Point10 p1 = new Point10(x, y);
            x = sc.nextDouble();
            y = sc.nextDouble();
            Point10 p2 = new Point10(x, y);
            x = sc.nextDouble();
            y = sc.nextDouble();
            Point10 p3 = new Point10(x, y);
            double d12, d13, d23;
            d12 = p1.distance(p2);
            d13 = p1.distance(p3);
            d23 = p3.distance(p2);
            if (d12 + d13 <= d23 || d12 + d23 <= d13 || d13 + d23 <= d12) {
                System.out.println("INVALID");
            } else {
                double s = Math.sqrt((d12 + d13 + d23) * (d12 + d13 - d23) * (d12 - d13 + d23) * (-d12 + d13 + d23));
                double r = (d12 * d13 * d23)/s;
                System.out.printf("%.03f\n", Math.PI*r*r);
            }
        }
    }
}

class Point10 {
    private double x;
    private double y;

    public Point10() {
    }

    public Point10(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point10(Point10 p) {
        this.x = p.getX();
        this.y = p.getY();
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double distance(Point10 p) {
        return Math.sqrt(((x - p.getX()) * (x - p.getX())) + ((y - p.getY()) * (y - p.getY())));
    }

    public double distance(Point10 p1, Point10 p2) {
        return Math.sqrt(((p1.getX() - p2.getX()) * (p1.getX() - p2.getX()))
                + ((p1.getY() - p2.getY()) * (p1.getY() - p2.getY())));
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}
