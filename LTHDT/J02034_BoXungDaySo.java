import java.util.*;
import java.io.*;

public class J02034_BoXungDaySo {
    public static void main(String[] args) throws IOException {
        J02034_Reader sc = new J02034_Reader();
        int n = Integer.parseInt(sc.next());
        int[] a = new int[205];
        int j = 0;
        while (n-- > 0) {
            j = Integer.parseInt(sc.next());
            a[j] = 1;
        }
        boolean hasVal = false;
        for (int i = 1; i < j; i++) {
            if (a[i] == 0) {
                System.out.println(i);
                hasVal = true;
            }
        }
        if (!hasVal)
            System.out.println("Excellent!");
    }
}

class J02034_Reader {
    BufferedReader br;
    StringTokenizer st;

    public J02034_Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}