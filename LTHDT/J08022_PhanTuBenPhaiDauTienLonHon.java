import java.util.*;
/**
 *
 * @author hoan
 */
public class J08022_PhanTuBenPhaiDauTienLonHon {
    static Scanner sc;
    public static void main(String[] args) {
        sc = new Scanner(System.in);
        int T = sc.nextInt();
        int n;
        int[] a;
        int[] greater;
        Stack<Integer> st;
        while(T-- > 0){
            st = new Stack<>();
            n = sc.nextInt();
            a = new int[n];
            greater = new int[n];
            for (int i = 0; i < n; i++) {
                a[i] = sc.nextInt();
            }
            for(int i = n-1; i >= 0; i--){
                while(!st.empty() && a[st.peek()] <= a[i]) st.pop();
                greater[i] = (st.empty() ? -1 : st.peek());
                st.push(i);
            }
            for(int i = 0; i < n; i++){
                if(greater[i] != -1) System.out.print(a[greater[i]] + " ");
                else System.out.print("-1 ");
            }
            System.out.println();
        }
    }
}

/*
3
4
4 5 2 25
6
1 2 3 4 2 3
4
4 4 5 5

*/