import java.util.LinkedHashMap;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class J08010_TimTuThuanNghichDaiNhat { // https://ideone.com/HJLIoe
    static LinkedHashMap<String, Integer> map = new LinkedHashMap<>();
    static int len = 0;
    static String str;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            try {
                str = sc.next();
            } catch (NoSuchElementException e) {
                sc.close();
                break;
            }
            if (isPalindrome(str)) {
                map.put(str, map.getOrDefault(str, 0) + 1);
                len = Math.max(len, str.length());
            }
        }
        map.entrySet().stream().filter(s -> s.getKey().length() == len).forEach(k -> {
            System.out.println(k.getKey() + " " + k.getValue());
        });
    }

    public static boolean isPalindrome(String text) {
        String reversed = new StringBuilder(text).reverse().toString();
        return text.equalsIgnoreCase(reversed);
    }
}
