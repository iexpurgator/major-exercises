import java.util.*;
import java.io.*;

public class J07008_DayConTangDan {
    static String subsequence(String[] s, int binary, int len) {
        String sub = "";
        boolean check = true;
        int cnt = 0;
        int[] arr = new int[len];
        for (int j = 0; j < len; j++) {
            if ((binary & (1 << j)) != 0) {
                sub += s[j] + " ";
                arr[cnt] = Integer.parseInt(s[j]);
                if(cnt > 0 && arr[cnt - 1] > arr[cnt]) check = false;
                cnt++;
            }
        }
        if (cnt == 1 || !check)
            return "";
        else
            return sub;
    }

    static void possibleSubsequences(String[] s) {
        ArrayList<String> list = new ArrayList<>();
        int len = s.length;
        int limit = (int) Math.pow(2, len);
        for (int i = 1; i <= limit - 1; i++) {
            String sub = subsequence(s, i, len);
            if (!sub.isEmpty())
                list.add(sub);
        }
        list.sort((e1, e2) -> (e1.compareTo(e2)));
        list.forEach(System.out::println);
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {
        BufferedReader sc = new BufferedReader(new FileReader("DAYSO.in"));
        int n = Integer.parseInt(sc.readLine());
        String[] in = sc.readLine().split("\\s+");
        possibleSubsequences(in);
    }
}
/**
 */
