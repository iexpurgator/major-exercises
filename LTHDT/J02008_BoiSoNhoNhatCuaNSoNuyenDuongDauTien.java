import java.util.Scanner;

public class J02008_BoiSoNhoNhatCuaNSoNuyenDuongDauTien {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        while (T > 0) {
            long n = Long.parseLong(sc.nextLine());
            if (n < 3)
                System.out.println(n);
            else
                System.out.println(flcm(n, n - 1));
            T -= 1;
        }
    }

    private static long gcd(long a, long b) {
        return b == 0 ? a : gcd(b, a % b);
    }

    private static long flcm(long a, long b) {
        if (b == 1)
            return a;
        a = (a * b) / gcd(a, b);
        return flcm(a, b - 1);
    }
}
