import java.util.Scanner;

public class J01002_TinhTong {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        while (T > 0) {
            long n = Long.parseLong(sc.nextLine());
            long sum = (n * (n + 1)) / 2;
            System.out.println(sum);
            T -= 1;
        }
    }
}