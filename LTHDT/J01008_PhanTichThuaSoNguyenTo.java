import java.util.Scanner;

public class J01008_PhanTichThuaSoNguyenTo {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        int t = 1;
        while (t <= T) {
            long a = Integer.parseInt(sc.nextLine());
            System.out.print("Test " + t + ": ");
            primeFactors(a);
            t++;
        }
    }

    public static void primeFactors(long n) {
        int cnt = 0;
        while (n % 2 == 0) {
            cnt++;
            n /= 2;
        }
        if (cnt > 0) System.out.print("2(" + cnt + ") ");
        cnt = 0;
        for (int i = 3; i <= Math.sqrt(n); i += 2) {
            while (n % i == 0) {
                // System.out.print(i + " ");
                cnt++;
                n /= i;
            }
            if (cnt > 0) {
                System.out.print(i + "(" + cnt + ") ");
                cnt = 0;
            }
        }
        if (n > 2)
            System.out.print(n + "(1)");
        System.out.println();
    }
}
