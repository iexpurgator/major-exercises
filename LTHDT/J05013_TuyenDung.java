import java.util.*;

/**
 *
 * @author Hoan
 */
public class J05013_TuyenDung {
    static Scanner sc;
    
    public static void main(String[] args) {
        sc = new Scanner(System.in);
        List<NhanVien13> list = new ArrayList<>();
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        while (id++ < T) {
            String name;
            double diemLT, diemTH;
            // id = Integer.parseInt(sc.nextLine());
            name = sc.nextLine();
            diemLT = fmDiem(Double.parseDouble(sc.nextLine()));
            diemTH = fmDiem(Double.parseDouble(sc.nextLine()));
            list.add(new NhanVien13(id, name, diemLT, diemTH));
        }
        Collections.sort(list, new Comparator<NhanVien13>() {
            @Override
            public int compare(NhanVien13 t, NhanVien13 t1) {
                return (int) ((t1.diemTB() * 100) - (t.diemTB() * 100));
            }
        });
        list.forEach(System.out::println);
    }
    
    static double fmDiem(double diem) {
        return diem > 10 ? diem / 10.0 : diem;
    }
}

class NhanVien13 {
    
    private String id;
    private String name;
    private double diemLT;
    private double diemTH;
    
    public NhanVien13() {
    }
    
    public NhanVien13(int id, String name, double diemLT, double diemTH) {
        this.id = String.format("TS%02d", id);
        this.name = name;
        this.diemLT = diemLT;
        this.diemTH = diemTH;
    }
    
    public double diemTB() {
        return (diemLT + diemTH) / 2.0;
    }
    
    private String xepLoai() {
        double tb = diemTB();
        String xl;
        if (tb > 9.5) {
            xl = "XUAT SAC";
        } else if (tb >= 8) {
            xl = "DAT";
        } else if (tb > 5) {
            xl = "CAN NHAC";
        } else {
            xl = "TRUOT";
        }
        return String.format("%.02f %s", tb, xl);
    }
    
    @Override
    public String toString() {
        return id + " " + name + " " + xepLoai();
    }
}