import java.util.*;

public class J06001_TinhToanHoaDonQuanAo {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        ArrayList<QuanAo1> list = new ArrayList<>();
        String id, name;
        long p1, p2;
        while (T-- > 0) {
            id = sc.nextLine();
            name = sc.nextLine();
            p1 = Integer.parseInt(sc.nextLine());
            p2 = Integer.parseInt(sc.nextLine());
            list.add(new QuanAo1(id, name, p1, p2));
        }
        T = Integer.parseInt(sc.nextLine());
        int serial = 0;
        String[] inp;
        int kind, count;
        while (serial++ < T) {
            inp = sc.nextLine().split("\\s+");
            id = inp[0].substring(0, 2);
            kind = Integer.parseInt(inp[0].substring(2));
            count = Integer.parseInt(inp[1]);
            for (QuanAo1 i : list) {
                if (i.getId().equals(id)) {
                    System.out.println(i.getCode(kind, serial) + " " +
                                       i.getName() + " " +
                                       i.getDiscound(kind, count)+ " " +
                                       i.getTotal(kind, count));
                    break;
                }
            }
        }
    }
}

class QuanAo1 {
    private String id;
    private String name;
    private long[] p;

    public QuanAo1(String id, String name, long p1, long p2) {
        p = new long[2];
        this.id = id;
        this.name = name;
        this.p[0] = p1;
        this.p[1] = p2;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public long getDiscound(int kind, int count) {
        double dis;
        if (count > 149)
            dis = 0.5;
        else if (count > 99)
            dis = 0.3;
        else if (count > 49)
            dis = 0.15;
        else
            dis = 0;
        return Math.round(p[kind - 1] * count * dis);
    }

    public long getTotal(int kind, int count) {
        long dis = getDiscound(kind, count);
        return (p[kind - 1] * count) - dis;
    }

    public String getCode(int kind, int serial) {
        return String.format("%s%d-%03d", id, kind, serial);
    }
}