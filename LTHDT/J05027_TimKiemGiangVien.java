import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class J05027_TimKiemGiangVien {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        List<GV27> list = new ArrayList<>();
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        while (id++ < T) {
            String name, ma;
            name = sc.nextLine();
            ma = sc.nextLine().toUpperCase();
            list.add(new GV27(id, name, ma));
        }
        T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            String s = sc.nextLine();
            System.out.println("DANH SACH GIANG VIEN THEO TU KHOA " + s + ":");
            final String s1 = s.toLowerCase();
            list.stream().filter(e -> e.getName().toLowerCase().contains(s1)).forEach(System.out::println);
        }
    }
}

class GV27 {
    private String id;
    private String name;
    private String ma;

    GV27() {
    }

    GV27(int id, String name, String ma) {
        String[] sma = ma.split("\\s+");
        this.id = String.format("GV%02d", id);
        this.name = name;
        this.ma = "";
        for (String string : sma) {
            this.ma += string.charAt(0);
        }
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + ma;
    }
}
