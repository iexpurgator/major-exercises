import java.util.*;
import java.io.*;

public class J02037_DayUuThe {
    public static void main(String[] args) throws IOException {
        J02037_Reader sc = new J02037_Reader();
        int T = Integer.parseInt(sc.nextLine());
        String[] inp;
        int odd, even;
        while (T-- > 0) {
            inp = sc.nextLine().split("\\s+");
            odd = even = 0;
            for (int i = 0; i < inp.length; i++) {
                if(Integer.parseInt(inp[i]) % 2 == 1) odd++;
                else even++;
            }
            if(inp.length % 2 == 0 && even > odd) System.out.println("YES");
            else if(inp.length % 2 == 1 && even < odd) System.out.println("YES");
            else System.out.println("NO");
        }
    }
}

class J02037_Reader {
    BufferedReader br;
    StringTokenizer st;

    public J02037_Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}
