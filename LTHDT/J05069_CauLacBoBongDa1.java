import java.util.*;
import java.io.*;

public class J05069_CauLacBoBongDa1 {
    static J05069_CauLacBoBongDa1Reader sc;

    public static void main(String[] args) throws IOException {
        sc = new J05069_CauLacBoBongDa1Reader();
        int T = Integer.parseInt(sc.nextLine());
        HashMap<String, FC69> map = new HashMap<>();
        String id, name;
        int cost;
        while (T-- > 0) {
            id = sc.nextLine();
            name = sc.nextLine();
            cost = Integer.parseInt(sc.nextLine());
            map.put(id, new FC69(id, name, cost));
        }
        T = Integer.parseInt(sc.nextLine());
        int count;
        String[] inp;
        FC69 fc;
        while (T-- > 0) {
            inp = sc.nextLine().split("\\s+");
            count = Integer.parseInt(inp[1]);
            fc = map.get(inp[0].substring(1, 3));
            System.out.println(inp[0] + " " + fc.getName() + " " + fc.getPrice(count));

        }
    }
}

class FC69 {
    private String id;
    private String name;
    private int cost;

    public FC69(String id, String name, int cost) {
        this.id = id;
        this.name = name;
        this.cost = cost;
    }

    public String getName() {
        return name;
    }

    public long getPrice(int count) {
        return (long) cost * (long) count;
    }
}

class J05069_CauLacBoBongDa1Reader {
    BufferedReader br;
    StringTokenizer st;

    public J05069_CauLacBoBongDa1Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}