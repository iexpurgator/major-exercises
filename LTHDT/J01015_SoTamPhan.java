import java.util.*;

public class J01015_SoTamPhan {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        while (T-- > 0)
            System.out.println(sc.nextLine().matches("[012]+") ? "YES" : "NO");
    }
}