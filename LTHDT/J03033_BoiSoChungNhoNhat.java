import java.util.*;
import java.math.*;

public class J03033_BoiSoChungNhoNhat {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        BigInteger a, b;
        while (T-- > 0) {
            a = new BigInteger(sc.nextLine());
            b = new BigInteger(sc.nextLine());
            System.out.println(a.multiply(b).divide(a.gcd(b)));
        }
    }
}
