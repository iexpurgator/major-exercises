import java.util.*;
import java.io.*;
import java.text.*;

public class J07018_ChuanHoaDanhSachSinhVien {
    public static void main(String[] args) throws IOException, ParseException {
        BufferedReader sc = new BufferedReader(new FileReader("SINHVIEN.in"));
        int T = Integer.parseInt(sc.readLine());
        int i = 0;
        SinhVien18 sv;
        String[] iname;
        String name, iclass, date;
        float gpa;
        while (i++ < T) {
            iname = sc.readLine().toLowerCase().trim().split("\\s+");
            name = "";
            for (String s : iname) {
                name += (s.substring(0, 1).toUpperCase() + s.substring(1) + " ");
            }
            iclass = sc.readLine();
            date = sc.readLine();
            gpa = Float.parseFloat(sc.readLine());
            sv = new SinhVien18(i, name, iclass, date, gpa);
            System.out.println(sv);
        }
        sc.close();
        // list.forEach(System.out::println);
    }
}

class SinhVien18 implements Serializable {
    private String ma, ten, lop;
    private Date ngaysinh;
    private float gpa;

    public SinhVien18(int id, String ten, String lop, String ngaysinh, float gpa) throws ParseException {
        this.ma = "B20DCCN" + String.format("%03d", id);
        this.ten = ten;
        this.lop = lop;
        this.ngaysinh = new SimpleDateFormat("dd/MM/yyyy").parse(ngaysinh);
        this.gpa = gpa;
    }

    @Override
    public String toString() {
        return ma + " " + ten + " " + lop + " " + new SimpleDateFormat("dd/MM/yyyy").format(ngaysinh) + " "
               + String.format("%.2f", gpa);
    }
}
