import java.util.*;
import java.io.*;

public class J01025_HinhVuong {
    public static void main(String[] args) throws IOException {
        J01025_Reader sc = new J01025_Reader();
        int[] x = new int[4];
        int[] y = new int[4];
        for (int i = 0; i < 4; i++) {
            x[i] = Integer.parseInt(sc.next());
            y[i] = Integer.parseInt(sc.next());
        }
        Arrays.sort(x);
        Arrays.sort(y);
        long k = Math.max(distance(x[3], x[0]), distance(y[3], y[0]));
        System.out.println(k* k);
    }

    static int distance(int a, int b) {
        if (a < b)
            return Math.abs(b - a);
        else
            return Math.abs(a - b);
    }
}

class J01025_Reader {
    BufferedReader br;
    StringTokenizer st;

    public J01025_Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}
