package J07041;

import java.util.*;
import java.io.*;

/**
 *
 * @author Donald
 */
public class LietKeCapSo1 {
    
    public static void main(String[] args) throws Exception {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream("DATA.in"));
        ArrayList<Pair> inp = (ArrayList<Pair>) in.readObject();
        Collections.sort(inp);
        inp.stream().filter(e -> e.getFirst() < e.getSecond()).forEach(System.out::println);
    }
}
