import java.util.LinkedHashMap;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class J03020_TimTuThuanNghichDaiNhat { //https://ideone.com/HJLIoe
    static LinkedHashMap<String, Integer> map = new LinkedHashMap<>();
    static int len = 0;
    static String str;
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            try {
                str = sc.next();
            } catch (NoSuchElementException e) {
                sc.close();
                break;
            }
            if (isPalindrome(str)) {
                map.put(str, (map.get(str) == null) ? 1 : map.get(str) + 1);
                len = Math.max(len, str.length());
            }
        }
        map.entrySet().stream().filter(s -> s.getKey().length() == len).forEach(k -> {
            System.out.println(k.getKey() + " " + k.getValue());
        });
    }

    private static boolean isPalindrome(String text) {
        String reversed = new StringBuilder(text).reverse().toString();
        return text.equalsIgnoreCase(reversed);
    }
}
