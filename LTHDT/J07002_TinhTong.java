import java.io.*;
import java.util.*;

public class J07002_TinhTong {
    static Scanner sc;

    public static void main(String[] args) throws Exception {
        sc = new Scanner(new FileInputStream("DATA.in"));
        long sum = 0;
        int numInt;
        String num;
        while (sc.hasNext()) {
            num = sc.next();
            try {
                numInt = Integer.parseInt(num);
            } catch (NumberFormatException e) {
                continue;
            }
            sum += numInt;
        }
        System.out.println(sum);
    }
}