import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class J05043_TinhThuNhapChoNhanVien {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        List<NV43> list = new ArrayList<>();
        int i = 0;
        while (i++ < T) {
            String name, level;
            int base, count;
            name = sc.nextLine();
            level = sc.nextLine();
            base = Integer.parseInt(sc.nextLine());
            count = Integer.parseInt(sc.nextLine());
            list.add(new NV43(i, name, level, base, count));
        }
        list.stream().forEach(System.out::println);
    }
}

class NV43 {
    private String id;
    private String name;
    private String level;
    private int base;
    private int count;
    private int bonus;
    private int main;
    private int tamung;
    private int conlai;
    private Map<String, Integer> map = new HashMap<String, Integer>() {
        {
            put("GD", 500);
            put("PGD", 400);
            put("TP", 300);
            put("KT", 250);
        }
    };

    NV43() {
    }

    NV43(int id, String name, String level, int base, int count) {
        this.id = String.format("NV%02d", id);
        this.name = name;
        this.level = level;
        this.base = base;
        this.count = count;
    }

    private void caculator() {
        main = base * count;
        bonus = map.get(level) == null ? 100 : map.get(level);
        int thanhtien = bonus + main;
        double c = (thanhtien * (2.0 / 3.0)) / 1000;
        tamung = (int) Math.round(c) * 1000;
        if (tamung > 25000)
            tamung = 25000;
        conlai = (main + bonus) - tamung;
    }

    @Override
    public String toString() {
        caculator();
        return id + " " + name + " " + bonus + " " + main + " " + tamung + " " + conlai;
    }
}