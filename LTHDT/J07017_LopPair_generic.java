import java.util.*;
import java.io.*;

public class J07017_LopPair_generic {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(new File("DATA.in"));
        int t = sc.nextInt();
        while (t-- > 0) {
            int n = sc.nextInt();
            boolean check = false;
            for (int i = 2; i <= 2 * Math.sqrt(n); i++) {
                Pair<Integer, Integer> p = new Pair<>(i, n - i);
                if (p.isPrime()) {
                    System.out.println(p);
                    check = true;
                    break;
                }
            }
            if (!check) System.out.println(-1);
        }
    }
}

class Pair<T, S>{
    private T first;
    private S second;

    Pair(T first, S second){
        this.first = first;
        this.second = second;
    }

    public boolean isPrime(){
        return isPrime(first) && isPrime(second);
    }

    private boolean isPrime(Object n) {
        Integer number = (Integer)n;
        if (number == 2) return true;
        else if (number < 2 || number % 2 == 0) return false;
        for (int i = 3; i <= Math.sqrt(number); i += 2) {
            if (number % i == 0) return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return first + " " + second;
    }
}

