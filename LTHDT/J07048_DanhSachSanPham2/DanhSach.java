package J07048_DanhSachSanPham2;

import java.util.*;
import java.io.*;

/**
 *
 * @author Donald
 */
public class DanhSach {
    
    static Scanner sc;
    
    public static void main(String[] args) throws Exception {
        sc = new Scanner(new FileInputStream("SANPHAM.in"));
        int T = Integer.parseInt(sc.nextLine());
        String id, name;
        int price, baohanhTime;
        ArrayList<SanPham> list = new ArrayList<>();
        while (T-- > 0) {
            id = sc.nextLine();
            name = sc.nextLine();
            price = Integer.parseInt(sc.nextLine());
            baohanhTime = Integer.parseInt(sc.nextLine());
            list.add(new SanPham(id, name, price, baohanhTime));
        }
        Collections.sort(list);
        list.forEach(System.out::println);
    }
}
