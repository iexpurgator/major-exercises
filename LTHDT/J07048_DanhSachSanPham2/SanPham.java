package J07048_DanhSachSanPham2;

/**
 *
 * @author Donald
 */
public class SanPham implements Comparable<SanPham> {

    private String id, name;
    private int price, baohanhTime;

    public SanPham(String id, String name, int price, int baohanhTime) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.baohanhTime = baohanhTime;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + price + " " + baohanhTime;
    }

    @Override
    public int compareTo(SanPham o) {
        if (price == o.price) return id.compareTo(o.id);
        else return o.price - price;
    }
}
