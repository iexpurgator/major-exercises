import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

public class J05041_SapXepBangTinhCong {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        List<TinhCong41> list = new ArrayList<>();
        while (id++ < T) {
            String name = sc.nextLine();
            int base = Integer.parseInt(sc.nextLine());
            int count = Integer.parseInt(sc.nextLine());
            String level = sc.nextLine();
            list.add(new TinhCong41(id, name, base, count, level));
        }
        list.sort((o1, o2) -> (int)(o2.getRes() - o1.getRes()));
        list.forEach(System.out::println);
    }
}

class TinhCong41 {
    private String id;
    private String name;
    private int base;
    private int count;
    private String level;

    private long monthS, bonusS, addS, res;

    public TinhCong41(int i, String name, int base, int count, String level) {
        this.id = String.format("NV%02d", i);
        this.name = name;
        this.base = base;
        this.count = count;
        this.level = level;
        calc();
    }

    private void calc() {
        if (level.equals("GD"))
            addS = 250000;
        else if (level.equals("PGD"))
            addS = 200000;
        else if (level.equals("TP"))
            addS = 180000;
        else
            addS = 150000;
        monthS = base * count;
        if (count > 24)
            bonusS = (int) Math.round(monthS * 0.2);
        else if (count > 21)
            bonusS = (int) Math.round(monthS * 0.1);
        else bonusS = 0;
        res = monthS + bonusS + addS;
    }

    public long getRes() {
        return res;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + monthS + " " + bonusS + " " + addS + " " + res;
    }
}