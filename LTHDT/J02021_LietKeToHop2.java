import java.util.Scanner;

public class J02021_LietKeToHop2 {
    private static Scanner sc = new Scanner(System.in);
    static int cnt = 0;

    public static void main(String[] args) {
        int n, k;
        String[] in = sc.nextLine().split("\\s");
        n = Integer.parseInt(in[0]);
        k = Integer.parseInt(in[1]);
        int[] a = new int[n + 1];
        gen_com(a, n, k, 1);
        System.out.println("\nTong cong co " + cnt + " to hop");
    }

    private static void gen_com(int[] a, int n, int k, int x) {
        for (int i = a[x - 1] + 1; i <= n - k + x; i++) {
            a[x] = i;
            if (x == k) {
                cnt++;
                for (int j = 1; j <= k; ++j) {
                    System.out.print(a[j]);
                }
                System.out.print(" ");
            } else {
                gen_com(a, n, k, x + 1);
            }
            a[x] = 0;
        }
    }
}
