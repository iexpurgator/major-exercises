import java.util.Scanner;

public class J08012_HinhSao {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int n = sc.nextInt();
        int a[] = new int[n];
        for (int i = 0; i < n - 1; i++) {
            int j = sc.nextInt();
            int k = sc.nextInt();
            a[j - 1]++;
            a[k - 1]++;
        }
        boolean b = false;
        for (int i = 0; i < n; i++) {
            if (a[i] == n - 1) {
                b = true;
                break;
            }
        }
        if (b) {
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }
    }
}
/*
import java.util.*;

public class J08012_HinhSao {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int V = sc.nextInt();
        int E = V - 1;
        starTopology star = new starTopology(V);
        int u, v;
        for (int i = 0; i < E; i++) {
            u = sc.nextInt();
            v = sc.nextInt();
            star.addEdge(u, v);
        }
        boolean isStar = star.isStarTopology();
        if (isStar) {
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }
    }

}

class starTopology {
    private ArrayList<ArrayList<Integer>> adj;
    private int V;
    private int E;

    starTopology(int V) {
        this.V = V;
        this.E = V - 1;
        adj = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < V + 1; i++) {
            adj.add(new ArrayList<>());
        }
    }

    public void addEdge(int u, int v) {
        adj.get(u).add(v);
        adj.get(v).add(u);
    }

    public boolean isStarTopology() {
        if (E != (V - 1)) {
            return false;
        }
        if (V == 1) {
            return true;
        }
        int[] vertexDegree = new int[V + 1];
        for (int i = 1; i <= V; i++) {
            for (int v : adj.get(i)) {
                vertexDegree[v]++;
            }
        }
        int countCentralNodes = 0, centralNode = 0;
        for (int i = 1; i <= V; i++) {
            if (vertexDegree[i] == (V - 1)) {
                countCentralNodes++;
                centralNode = i;
            }
        }
        if (countCentralNodes != 1)
            return false;
        for (int i = 1; i <= V; i++) {
            if (i == centralNode)
                continue;
            if (vertexDegree[i] != 1) {
                return false;
            }
        }
        return true;
    }
}
*/