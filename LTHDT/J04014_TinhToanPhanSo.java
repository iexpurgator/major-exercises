import java.util.Scanner;

public class J04014_TinhToanPhanSo {
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        while (id++ < T) {
            long tu, mau;
            tu = sc.nextLong();
            mau = sc.nextLong();
            PhanSo1 p = new PhanSo1(tu, mau);
            tu = sc.nextLong();
            mau = sc.nextLong();
            PhanSo1 p2 = new PhanSo1(tu, mau);
            PhanSo1 c = PhanSo1.add(p, p2);
            c.mul(c);
            PhanSo1 d = PhanSo1.mul(p, p2);
            d.mul(c);
            System.out.println(c + " " + d);
        }
    }
}

class PhanSo1 {
    private long tu;
    private long mau;

    PhanSo1() {
    }

    PhanSo1(long tu, long mau) {
        this.tu = tu;
        this.mau = mau;
    }

    public void nhap() {
        Scanner sc = new Scanner(System.in);
        tu = sc.nextLong();
        mau = sc.nextLong();
        sc.close();
    }

    private long GCD(long a, long b) {
        return a == 0 ? b : GCD(b % a, a);
    }

    public void rutGon() {
        long k = GCD(this.tu, this.mau);
        this.tu /= k;
        this.mau /= k;
    }

    public void add(PhanSo1 p){
        PhanSo1.add(this, p);
        rutGon();
    }

    public void mul(PhanSo1 p){
        PhanSo1.mul(this, p);
        rutGon();
    }

    public static PhanSo1 mul(PhanSo1 p1, PhanSo1 p2){
        p1.tu *= p2.tu;
        p1.mau *= p2.mau;
        p1.rutGon();
        return p1;
    }

    public static PhanSo1 add(PhanSo1 p1, PhanSo1 p2){
        PhanSo1 p = new PhanSo1((p1.tu * p2.mau) + (p2.tu * p1.mau), p2.mau * p1.mau);
        p.rutGon();
        return p;
    }

    @Override
    public String toString() {
        return (this.tu + "/" + this.mau);
    }
}
