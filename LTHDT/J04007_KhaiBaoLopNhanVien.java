import java.util.Scanner;

public class J04007_KhaiBaoLopNhanVien {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String Name = sc.nextLine();
        String Sex = sc.nextLine();
        String Date = sc.nextLine();
        String AddrHome = sc.nextLine();
        String Phone = sc.nextLine();
        String SignDate = sc.nextLine();
        sc.close();
        NhanVien nv = new NhanVien(Name, Sex, Date, AddrHome, Phone, SignDate);
        System.out.println(nv);
    }
}

class NhanVien {
    private String Name;
    private String Sex;
    private String Date;
    private String AddrHome;
    private String Phone;
    private String SignDate;
    private String ID;

    NhanVien() {
    }

    public NhanVien(String name, String sex, String date, String addrHome, String phone, String signDate) {
        Name = name;
        Sex = sex;
        Date = date;
        AddrHome = addrHome;
        Phone = phone;
        SignDate = signDate;
        ID = "00001";
    }

    @Override
    public String toString() {
        return ID + " " + Name + " " + Sex + " " + Date + " " + AddrHome + " " + Phone + " " + SignDate;
    }
}
