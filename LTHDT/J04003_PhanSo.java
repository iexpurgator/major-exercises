import java.util.Scanner;

public class J04003_PhanSo {
    public static void main(String[] args) {
        PhanSo p = new PhanSo(1, 1);
        p.nhap();
        p.rutGon();
        System.out.println(p);
    }
}

class PhanSo {
    private long tu;
    private long mau;

    PhanSo() {
    }

    PhanSo(long tu, long mau) {
        this.tu = tu;
        this.mau = mau;
    }

    public void nhap() {
        Scanner sc = new Scanner(System.in);
        tu = sc.nextLong();
        mau = sc.nextLong();
        sc.close();
    }

    private long GCD(long a, long b) {
        return a == 0 ? b : GCD(b % a, a);
    }

    public void rutGon() {
        long k = GCD(tu, mau);
        tu /= k;
        mau /= k;
    }

    @Override
    public String toString() {
        return tu + "/" + mau;
    }
}