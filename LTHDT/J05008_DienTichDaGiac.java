import java.util.*;

public class J05008_DienTichDaGiac {
    static Scanner sc;
    public static void main(String[] args) {
        sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            int n = sc.nextInt();
            double x, y;
            Point58[] dg = new Point58[n];
            for (int i = 0; i < n; ++i) {
                x = sc.nextDouble();
                y = sc.nextDouble();
                dg[i] = new Point58(x, y);
            }
            double sm = dg[n-1].getX()*dg[0].getY() - dg[n-1].getY()*dg[0].getX();
            for (int i = 0; i < n - 1; i++) {
                sm += (dg[i].getX()*dg[i+1].getY() - dg[i].getY()*dg[i+1].getX());
            }
            System.out.printf("%.03f\n", 0.5 * Math.abs(sm));
        }
    }
}

class Point58 {
    private double x;
    private double y;

    public Point58() {
    }

    public Point58(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point58(Point58 p) {
        this.x = p.getX();
        this.y = p.getY();
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double distance(Point58 p) {
        return Math.sqrt(((x - p.getX()) * (x - p.getX())) + ((y - p.getY()) * (y - p.getY())));
    }

    public double distance(Point58 p1, Point58 p2) {
        return Math.sqrt(((p1.getX() - p2.getX()) * (p1.getX() - p2.getX()))
                + ((p1.getY() - p2.getY()) * (p1.getY() - p2.getY())));
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}
