import java.util.*;

/**
 *
 * @author Hoan
 */
public class J04002_KhaiBaoLopHinhChuNhat {
    static Scanner sc;

    public static void main(String[] args) {
        sc = new Scanner(System.in);
        int width, height;
        String color;
        width = Integer.parseInt(sc.next());
        height = Integer.parseInt(sc.next());
        color = sc.next();
        if (width > 0 && height > 0){
            Retange aa = new  Retange(width, height, color);
            System.out.println(aa);
        } else {
            System.out.println("INVALID");
        }
    }
}

class Retange {

    private double width;
    private double height;
    private String color;

    public Retange() {
        width = 1;
        height = 1;
    }

    public Retange(double width, double height, String color) {
        this.width = width;
        this.height = height;
        this.color = color;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public String getColor() {
        String c = color.toLowerCase();
        return Character.toUpperCase(c.charAt(0)) + "" + c.substring(1);
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double findArea() {
        return width * height;
    }

    public double findPerimeter() {
        return (width + height) * 2;
    }

    @Override
    public String toString() {
        return String.format("%d %d %s", (int)findPerimeter(), (int) findArea(), getColor());
    }
}
