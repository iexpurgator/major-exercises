package J07042;

import java.io.*;
import java.util.*;

/**
 *
 * @author Donald
 */
public class LietKeCapSo2 {

    public static void main(String[] args) throws Exception {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream("DATA.in"));
        ArrayList<Pair> inp = (ArrayList<Pair>) in.readObject();
        Collections.sort(inp);
        inp.stream().filter(e -> e.getFirst() < e.getSecond() && isCoprime(e)).forEach(System.out::println);
    }

    public static int gcd(int a, int b) {
        return b != 0 ? gcd(b, a % b) : a;
    }

    public static boolean isCoprime(Pair p) {
        return gcd(p.getFirst(), p.getSecond()) == 1;
    }
}
