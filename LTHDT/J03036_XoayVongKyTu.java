import java.util.*;
import java.io.*;

public class J03036_XoayVongKyTu {
    static int xoayVongNhoNhat(String arr[], int n) {
        int ans = Integer.MAX_VALUE;
        for (int i = 0; i < n; i++) {
            int curr_count = 0;
            String tmp = "";
            for (int j = 0; j < n; j++) {
                tmp = arr[j] + arr[j];
                int index = tmp.indexOf(arr[i]);
                if (index != -1)
                    curr_count += index;
                else
                    curr_count = -1;
            }
            ans = Math.min(curr_count, ans);
        }
        return ans;
    }

    public static void main(String[] args) throws IOException {
        J03036_Reader sc = new J03036_Reader();
        int n = Integer.parseInt(sc.nextLine());
        String[] s = new String[n];
        for (int i = 0; i < n; i++) {
            s[i] = sc.nextLine();
        }
        System.out.println(xoayVongNhoNhat(s, n));
    }
}

class J03036_Reader {
    BufferedReader br;
    StringTokenizer st;

    public J03036_Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}
