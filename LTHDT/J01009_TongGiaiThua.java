import java.util.Scanner;

public class J01009_TongGiaiThua {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int n = Integer.parseInt(sc.nextLine());
        long gt = 0, cgt = 1;
        for (int i = 1; i <= n; i++) {
            cgt *= i;
            gt += cgt;
        }
        System.out.println(gt);
    }
}
