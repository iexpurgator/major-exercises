import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class J05025_SapXepDanhSachGiangVien {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        List<GV25> list = new ArrayList<>();
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        while (id++ < T) {
            String name, ma;
            name = sc.nextLine();
            ma = sc.nextLine().toUpperCase();
            list.add(new GV25(id, name, ma));
        }
        Collections.sort(list, new Comparator<GV25>() {
            @Override
            public int compare(GV25 o1, GV25 o2) {
                if (o1.getName().equals(o2.getName())) {
                    return o1.getId().compareTo(o2.getId());
                } else {
                    return o1.getName().compareTo(o2.getName());
                }
            }
        });
        list.forEach(System.out::println);
    }
}

class GV25 {
    private String id;
    private String name;
    private String ma;

    GV25() {
    }

    GV25(int id, String name, String ma) {
        String[] sma = ma.split("\\s+");
        this.id = String.format("GV%02d", id);
        this.name = name;
        this.ma = "";
        for (String string : sma) {
            this.ma += string.charAt(0);
        }
    }

    public String getId() {
        return id;
    }

    public String getName() {
        String[] n = name.split("\\s+");
        return n[n.length - 1];
    }

    @Override
    public String toString() {
        return id + " " + name + " " + ma;
    }
}