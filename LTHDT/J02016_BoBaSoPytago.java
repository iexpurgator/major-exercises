import java.util.Arrays;
import java.util.Scanner;

public class J02016_BoBaSoPytago {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        while (T > 0) {
            int n = Integer.parseInt(sc.nextLine());
            String[] in = sc.nextLine().split("\\s");
            long[] a = new long[n];
            for (int i = 0; i < n; ++i) {
                a[i] = Long.parseLong(in[i]);
            }
            if (isTriplet(a, n))
                System.out.println("YES");
            else
                System.out.println("NO");
            T -= 1;
        }
    }

    static boolean isTriplet(long arr[], int n) {
        for (int i = 0; i < n; i++)
            arr[i] = arr[i] * arr[i];
        Arrays.sort(arr);
        for (int i = n - 1; i >= 2; i--) {
            int l = 0;
            int r = i - 1;
            while (l < r) {
                if (arr[l] + arr[r] == arr[i])
                    return true;
                if (arr[l] + arr[r] < arr[i])
                    l++;
                else
                    r--;
            }
        }
        return false;
    }
}
