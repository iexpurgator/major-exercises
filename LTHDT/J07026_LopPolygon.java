import java.io.*;
import java.util.*;

/**
 *
 * @author Donald
 */
public class J07026_LopPolygon {

//    public static void main(String[] args) {}
    public static void main(String[] args) throws FileNotFoundException {
        Scanner in = new Scanner(new File("POLYGON.in"));
        int t = in.nextInt();
        while (t-- > 0) {
            int n = in.nextInt();
            Point p[] = new Point[n];
            for (int i = 0; i < n; i++) {
                p[i] = new Point(in.nextInt(), in.nextInt());
            }
            Polygon poly = new Polygon(p);
            System.out.println(poly.getArea());
        }
    }
}

class Polygon {

    private Point[] p;

    public Polygon(Point[] p) {
        this.p = p;
    }

    public String getArea() {
        double area = 0;
        int n = p.length;
        for (int i = 0, j; i < n; i++) {
            j = (i + 1) % n;
            area += (p[i].getX() * p[j].getY()) - (p[i].getY() * p[j].getX());
        }
        return String.format("%.3f", area * 0.5);
    }
}

class Point {

    private int x, y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
