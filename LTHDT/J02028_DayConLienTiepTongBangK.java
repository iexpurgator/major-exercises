import java.util.*;

public class J02028_DayConLienTiepTongBangK {
    static Scanner sc;
    
    public static void main(String[] args) {
        sc = new Scanner(System.in);
        int T = sc.nextInt();
        while (T-- > 0) {
            int n = sc.nextInt();
            long k = sc.nextLong();
            int[] a = new int[100005];
            for (int i = 0; i < n; i++) {
                a[i] = sc.nextInt();
            }
            if (solve(a, n, k)) {
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }
        }
    }

    static boolean solve(int[] arr, int n, long sum) {
        long curr_sum = (long) arr[0];
        int start = 0;
        for (int i = 1; i <= n; i++) {
            while (curr_sum > sum && start < i - 1) {
                curr_sum = curr_sum - arr[start];
                start++;
            }
            if (curr_sum == sum)
                return true;
            if (i < n)
                curr_sum = curr_sum + arr[i];
        }
        return false;
    }
}