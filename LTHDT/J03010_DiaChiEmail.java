import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class J03010_DiaChiEmail {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        List<String> list = new ArrayList<>();
        final String mail = "@ptit.edu.vn";
        while (T > 0) {
            String[] name = sc.nextLine().toLowerCase().trim().split("\\s+");
            String res = name[name.length - 1];
            for (int i = 0; i < name.length - 1; i++) {
                res += (name[i].charAt(0));
            }
            int tm = 2;
            String tmp = res;
            while (list.contains(tmp)) {
                tmp = res + tm;
                tm++;
            }
            list.add(tmp);
            T -= 1;
        }
        for (String i : list) {
            System.out.println(i + mail);
        }
    }
}
