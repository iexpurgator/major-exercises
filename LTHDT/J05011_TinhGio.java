import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class J05011_TinhGio {
    private static Scanner sc = new Scanner(System.in);
    static List<Gamer> list = new ArrayList<>();
    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            String id, name, timeS, timeE;
            id = sc.nextLine();
            name = sc.nextLine();
            timeS = sc.nextLine();
            timeE = sc.nextLine();
            list.add(new Gamer(id, name, timeS, timeE));
        }
        Collections.sort(list, new Comparator<Gamer>(){
            @Override
            public int compare(Gamer o1, Gamer o2) {
                return o2.getTimeTotal() - o1.getTimeTotal();
            }
        });
        list.forEach(System.out::println);
    }
}


class Gamer{
    private String id;
    private String name;
    private String timeS;
    private String timeE;

    Gamer(){}

    Gamer(String id, String name, String timeS, String timeE){
        this.id = id;
        this.name = name;
        this.timeS = timeS;
        this.timeE = timeE;
    }

    public int getTimeTotal(){
        String[] time = timeE.split(":");
        int hour = Integer.parseInt(time[0]);
        int mins = Integer.parseInt(time[1]);
        int res = hour * 60 + mins;
        time = timeS.split(":");
        hour = Integer.parseInt(time[0]);
        mins = Integer.parseInt(time[1]);
        res = res - (hour * 60 + mins);
        return res;
    }
    
    @Override
    public String toString() {
        int timeTotal = getTimeTotal();
        int hour = (int)(timeTotal / 60);
        int mins = timeTotal % 60;
        return id + " " + name + " " + hour + " gio " + mins + " phut";
    }
}