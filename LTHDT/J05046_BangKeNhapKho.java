import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class J05046_BangKeNhapKho {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        List<Kho46> list = new ArrayList<>();
        Map<String, Integer> map = new HashMap<String, Integer>();
        while (T-- > 0) {
            String name;
            int count, price;
            name = sc.nextLine();
            count = Integer.parseInt(sc.nextLine());
            price = Integer.parseInt(sc.nextLine());
            String[] s = name.toUpperCase().split("\\s+");
            String str = s[0].charAt(0) + "" + s[1].charAt(0);
            map.put(str, map.getOrDefault(str, 0) + 1);
            String id = String.format("%c%c%02d", s[0].charAt(0), s[1].charAt(0), map.get(str));
            list.add(new Kho46(id, name, count, price));
        }
        list.stream().forEach(System.out::println);
    }
}

class Kho46 {
    private String id;
    private String name;
    private int count;
    private int price;
    private long va;
    private long res;

    Kho46() {
    }

    Kho46(String id, String name, int count, int price) {
        this.id = id;
        this.name = name;
        this.count = count;
        this.price = price;
        calc();
    }

    private void calc() {
        if (count > 10) {
            va = (int) (price * count * 0.05);
        } else if (count >= 8) {
            va = (int) (price * count * 0.02);
        } else if (count >= 5) {
            va = (int) (price * count * 0.01);
        } else {
            va = 0;
        }
        res = price * count - va;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + va + " " + res;
    }
}