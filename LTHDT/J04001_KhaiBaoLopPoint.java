import java.util.Scanner;

/**
 *
 * @author Hoan
 */
public class J04001_KhaiBaoLopPoint {
    static Scanner sc;
    public static void main(String[] args) {
        // List<Integer> list = new ArrayList<>();
        sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            double x, y;
            x = sc.nextDouble();
            y = sc.nextDouble();
            Point p1 = new Point(x, y);
            x = sc.nextDouble();
            y = sc.nextDouble();
            Point p2 = new Point(x, y);
            System.out.printf("%.04f\n", p1.distance(p2));
        }
    }
}

class Point{
    private double x;
    private double y;

    public Point() {
    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point(Point p){
        this.x = p.getX();
        this.y = p.getY();
    }
    
    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
    
    public double distance(Point p){
        return Math.sqrt(((x - p.getX())*(x - p.getX())) + ((y - p.getY())*(y - p.getY())));
    }
    
    public double distance(Point p1, Point p2){
        return Math.sqrt(((p1.getX() - p2.getX())*(p1.getX() - p2.getX())) + ((p1.getY() - p2.getY())*(p1.getY() - p2.getY())));
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}