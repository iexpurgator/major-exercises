import java.text.*;
import java.time.*;
import java.util.*;
import java.io.*;

/**
 * @author Donald
 */
public class J07053_XetTuyen {

    static Scanner sc;

    public static void main(String[] args) throws Exception {
//        sc = new Scanner(new FileInputStream("src/T24112021/XETTUYEN.in"));
        sc = new Scanner(new FileInputStream("XETTUYEN.in"));
        int T = Integer.parseInt(sc.nextLine());
        String ten, ns;
        float lt, th;
        ThiSinhXT thisinh;
        for (int i = 1; i <= T; i++) {
            ten = sc.nextLine();
            ns = sc.nextLine();
            lt = Float.parseFloat(sc.nextLine());
            th = Float.parseFloat(sc.nextLine());
            thisinh = new ThiSinhXT(i, ten, ns, lt, th);
            System.out.println(thisinh);
        }
    }
}

class ThiSinhXT {
    private String ma;
    private String ten;
    private Date ngaySinh;
    private float diemLyThuyet, diemThucHanh;

    public ThiSinhXT(int ma, String ten, String ngaySinh, float diemLyThuyet, float diemThucHanh) {
        this.ma = String.format("PH%02d", ma);
        this.ten = capitalize(ten);
        try {
            this.ngaySinh = new SimpleDateFormat("dd/MM/yyyy").parse(ngaySinh);
        } catch (ParseException e) {
        }
        this.diemLyThuyet = diemLyThuyet;
        this.diemThucHanh = diemThucHanh;
    }

    private String capitalize(String string) {
        String[] split = string.toLowerCase().trim().split("\\s+");
        String capitalized = "";
        for (String s : split) {
            capitalized += Character.toUpperCase(s.charAt(0)) + s.substring(1) + " ";
        }
        return capitalized.trim();
    }

    public int getTuoi() {
        Date today = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        return Integer.parseInt(sdf.format(today)) - Integer.parseInt(sdf.format(ngaySinh));
    }

    public int getDiemTB() {
        float thuong = 0;
        if (diemLyThuyet >= 8 && diemThucHanh >= 8) thuong = 1;
        else if (diemLyThuyet >= 7.5 && diemLyThuyet >= 7.5) thuong = 0.5f;
        int TB = Math.round((diemLyThuyet + diemThucHanh) / 2 + thuong);
        return Math.min(TB, 10);
    }

    public String getXepLoai() {
        int diem = getDiemTB();
        if (getDiemTB() >= 9) return "Xuat sac";
        else if (getDiemTB() == 8) return "Gioi";
        else if (getDiemTB() == 7) return "Kha";
        else if (getDiemTB() >= 5) return "Trung binh";
        else return "Truot";
    }

    @Override
    public String toString() {
        return ma + " " + ten + " " + getTuoi() + " " + getDiemTB() + " " + getXepLoai();
    }
}