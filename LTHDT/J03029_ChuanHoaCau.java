import java.util.*;

public class J03029_ChuanHoaCau {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        String s = "";
        String[] ip;
        int len;
        int lenW;
        while (sc.hasNextLine()) {
            ip = sc.nextLine().toLowerCase().split("\\s+");
            len = ip.length - 1;
            ip[0] = Character.toUpperCase(ip[0].charAt(0)) + ip[0].substring(1);
            if (ip[len].equals("!") || ip[len].equals(".") || ip[len].equals("?")) {
                ip[len - 1] = ip[len - 1] + ip[len];
                ip[len] = "";
            }
            lenW = ip[len].length() - 1;
            if (lenW > 0 && ip[len].charAt(lenW) != '.' && ip[len].charAt(lenW) != '?' && ip[len].charAt(lenW) != '!')
                ip[len] += ".";
            for (String str : ip) {
                s += str + " ";
            }
            s += "\n";
        }
        System.out.println(s);
    }
}
