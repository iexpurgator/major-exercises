import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class J03009_TapTuRiengCuaHaiXau {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        while (T > 0) {
            String[] s = sc.nextLine().split("\\s");
            List<String> ss = new ArrayList<>();
            for (int i = 0; i < s.length; i++) {
                if (ss.indexOf(s[i]) == -1)
                    ss.add(s[i]);
            }
            s = sc.nextLine().split("\\s");
            for (int i = 0; i < s.length; i++) {
                if (ss.indexOf(s[i]) != -1) {
                    ss.remove(s[i]);
                }
            }
            Collections.sort(ss);
            for (String i : ss)
                System.out.print(i + " ");
            System.out.println();
            T -= 1;
        }
    }
}
