import java.util.Scanner;

public class J04012_BaiToanTinhCong {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        String name = sc.nextLine();
        int base = Integer.parseInt(sc.nextLine());
        int count = Integer.parseInt(sc.nextLine());
        String level = sc.nextLine();
        TinhCong12 tc = new TinhCong12(name, base, count, level);
        System.out.println(tc);
    }
}

class TinhCong12 {
    private String id = "NV01";
    private String name;
    private int base;
    private int count;
    private String level;

    private long monthS, bonusS, addS, res;

    public TinhCong12(String name, int base, int count, String level) {
        this.name = name;
        this.base = base;
        this.count = count;
        this.level = level;
        calc();
    }

    private void calc() {
        if (level.equals("GD"))
            addS = 250000;
        else if (level.equals("PGD"))
            addS = 200000;
        else if (level.equals("TP"))
            addS = 180000;
        else
            addS = 150000;
        monthS = base * count;
        if (count > 24)
            bonusS = (int) Math.round(monthS * 0.2);
        else if (count > 21)
            bonusS = (int) Math.round(monthS * 0.1);
        else bonusS = 0;
        res = monthS + bonusS + addS;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + monthS + " " + bonusS + " " + addS + " " + res;
    }
}