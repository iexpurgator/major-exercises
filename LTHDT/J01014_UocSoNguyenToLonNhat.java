import java.util.Scanner;

public class J01014_UocSoNguyenToLonNhat {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        while (T > 0) {
            long a = Long.parseLong(sc.nextLine());
            System.out.println(primeFactors(a));
            T -= 1;
        }
    }

    public static long primeFactors(long n) {
        long cnt = 0;
        while (n % 2 == 0) {
            cnt = 2;
            n /= 2;
        }
        for (int i = 3; i <= Math.sqrt(n); i += 2) {
            while (n % i == 0) {
                cnt = i;
                n /= i;
            }
        }
        if (n > 2)
            cnt = n;
        return cnt;
    }
}
