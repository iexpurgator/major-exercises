import java.io.*;
import java.util.*;

public class J03031_XauDayDu {

    public static void main(String[] args) throws IOException {
        FastReader sc = new FastReader();
        int T = Integer.parseInt(sc.nextLine());
        int k, cnt;
        while (T-- > 0) {
            cnt = (int) sc.next().chars().distinct().count();
            k = Integer.parseInt(sc.nextLine());
            System.out.println(k >= (26 - cnt) ? "YES" : "NO");
        }
    }

}

class FastReader {
    BufferedReader br;
    StringTokenizer st;

    public FastReader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}