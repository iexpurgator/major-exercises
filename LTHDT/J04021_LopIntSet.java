import java.util.*;
import java.io.*;

public class J04021_LopIntSet {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int n = sc.nextInt();
        int m = sc.nextInt();
        int[] a = new int[n];
        int[] b = new int[m];
        for (int i = 0; i < n; i++) {
            a[i] = sc.nextInt();
        }
        for (int i = 0; i < m; i++) {
            b[i] = sc.nextInt();
        }
        IntSet s1 = new IntSet(a);
        IntSet s2 = new IntSet(b);
        IntSet s3 = s1.union(s2);
        System.out.println(s3);
    }
}

class IntSet {
    TreeSet<Integer> list;

    public IntSet(int[] a) {
        list = new TreeSet<>();
        for (int i = 0; i < a.length; i++) {
            if (!list.contains(a[i]))
                list.add(a[i]);
        }
    }

    public IntSet(IntSet s) {
        list = s.list;
    }

    public static IntSet union(IntSet s1, IntSet s2) {
        s1.list.addAll(s2.list);
        return new IntSet(s1);
    }

    public IntSet union(IntSet s) {
        return union(this, s);
    }

    @Override
    public String toString() {
        return list.toString().replace("[", "").replace("]", "").replace(",", "");
    }
}
