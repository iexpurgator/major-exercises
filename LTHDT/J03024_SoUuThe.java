import java.util.Scanner;

public class J03024_SoUuThe {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        while (id++ < T) {
            boolean valid = true;
            boolean ok = false;
            String s = sc.nextLine();
            if (s.charAt(0) == '0' || !s.matches("\\d+")){
                valid = false;
            } else {
                valid = true;
                int odd = 0, even = 0;
                for (int i = 0; i < s.length(); i++) {
                    int si = s.charAt(i) - '0';
                    if (si % 2 == 1) odd ++;
                    else even ++;
                }
                if (odd > even && s.length() % 2 == 1) ok = true;
                else if (odd < even && s.length() % 2 == 0) ok = true;
                else ok = false;
            }
            if (valid && ok) {
                System.out.println("YES");
            } else if (valid && !ok) {
                System.out.println("NO");
            } else {
                System.out.println("INVALID");
            }
        }
        
    }
}
