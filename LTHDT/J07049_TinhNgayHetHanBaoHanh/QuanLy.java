package J07049_TinhNgayHetHanBaoHanh;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Donald
 */
public class QuanLy implements Comparable<QuanLy>{

    private KhachHang kh;
    private SanPham sp;
    private Date dateEnd;
    
    public QuanLy(KhachHang kh, SanPham sp) {
        this.kh = kh;
        this.sp = sp;
        Calendar day = Calendar.getInstance();
        day.setTime(kh.getDateBuySP());
        day.add(Calendar.MONTH, sp.getBaohanhTime());
        dateEnd = day.getTime();
    }
    
    public String getDateEnd() {
        return new SimpleDateFormat("yyyyMMdd").format(dateEnd);
    }
    
    public int tinhTien(){
        return kh.getCntSP() * sp.getPrice();
    }
    
    @Override
    public String toString() {
        return kh + " " + tinhTien() + " " + new SimpleDateFormat("dd/MM/yyyy").format(dateEnd);
    }

    @Override
    public int compareTo(QuanLy o) {
        if(getDateEnd().equals(o.getDateEnd())) return kh.compareTo(o.kh);
        else return getDateEnd().compareTo(o.getDateEnd());
    }
}
