package J07049_TinhNgayHetHanBaoHanh;

import java.io.*;
import java.util.*;

/**
 *
 * @author Donald
 */
public class Main {

    static Scanner sc;

    public static void main(String[] args) throws Exception {
        sc = new Scanner(new FileInputStream("MUAHANG.in"));
        int T = Integer.parseInt(sc.nextLine());
        String id, name;
        int price, baohanhTime;
        HashMap<String, SanPham> dsSP = new HashMap<>();
        while (T-- > 0) {
            id = sc.nextLine();
            name = sc.nextLine();
            price = Integer.parseInt(sc.nextLine());
            baohanhTime = Integer.parseInt(sc.nextLine());
            dsSP.put(id, new SanPham(id, name, price, baohanhTime));
        }
        T = Integer.parseInt(sc.nextLine());
        String addr, dateBuySP;
        int cntSP;
        ArrayList<QuanLy> ql = new ArrayList<>();
        for(int i = 1; i <= T; ++i){
            name = sc.nextLine();
            addr = sc.nextLine();
            id = sc.nextLine();
            cntSP = Integer.parseInt(sc.nextLine());
            dateBuySP = sc.nextLine();
            ql.add(new QuanLy(new KhachHang(i, name, addr, id, cntSP, dateBuySP), dsSP.get(id)));
        }
        Collections.sort(ql);
        ql.forEach(System.out::println);
    }
}
