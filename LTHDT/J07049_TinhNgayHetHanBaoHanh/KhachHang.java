package J07049_TinhNgayHetHanBaoHanh;

import java.text.*;
import java.util.*;

/**
 *
 * @author Donald
 */
public class KhachHang implements Comparable<KhachHang> {
    
    private String id;
    private String name;
    private String addr;
    private String idSP;
    private int cntSP;
    private Date dateBuySP;
    
    public KhachHang(int id, String name, String addr, String idSP, int cntSP, String dateBuySP) {
        this.id = String.format("KH%02d", id);
        this.name = name;
        this.addr = addr;
        this.idSP = idSP;
        this.cntSP = cntSP;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            this.dateBuySP = sdf.parse(dateBuySP);
        } catch (ParseException e) {
        }
    }

    public int getCntSP() {
        return cntSP;
    }

    public Date getDateBuySP() {
        return dateBuySP;
    }
    
    @Override
    public String toString() {
        return id + " " + name + " " + addr + " " + idSP;
    }
    
    @Override
    public int compareTo(KhachHang o) {
        return id.compareTo(o.id);
    }
}
