import java.util.Scanner;
import java.util.List;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class J05057_BangDiemTuyenSinh {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        List<HocSinh57> list = new ArrayList<>();
        while (id++ < T) {
            String uid, name;
            double s1, s2, s3;
            uid = sc.nextLine();
            name = sc.nextLine();
            s1 = Double.parseDouble(sc.nextLine());
            s2 = Double.parseDouble(sc.nextLine());
            s3 = Double.parseDouble(sc.nextLine());
            list.add(new HocSinh57(uid, name, s1, s2, s3));
        }
        double ok = 24;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getSum() >= ok) {
                System.out.println(list.get(i) + " " + "TRUNG TUYEN");
            } else
                System.out.println(list.get(i) + " " + "TRUOT");
        }
    }
}

class HocSinh57 {
    private String id;
    private String name;
    private double sum;
    private double bonus;
    private static DecimalFormat df = new DecimalFormat("#.#");

    public HocSinh57(String id, String name, double s1, double s2, double s3) {
        this.id = id;
        this.name = name;
        int kv = id.charAt(2) - '0';
        if (kv == 1)
            bonus = 0.5;
        else if (kv == 2)
            bonus = 1.0;
        else if (kv == 3)
            bonus = 2.5;
        else
            bonus = 0;
        this.sum = s1 * 2 + s2 + s3;
    }

    public double getSum() {
        return sum + bonus;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + df.format(bonus) + " " + df.format(sum);
    }
}
