import java.math.BigInteger;
import java.util.Scanner;

public class J03014_TongSoNguyenLon2 {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        BigInteger a = new BigInteger(sc.next());
        BigInteger b = new BigInteger(sc.next());
        System.out.println(b.add(a));
    }
}
