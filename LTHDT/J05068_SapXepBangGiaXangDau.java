import java.util.*;

public class J05068_SapXepBangGiaXangDau {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        int i = 0;
        ArrayList<XD68> list = new ArrayList<>();
        while (i++ < T) {
            String[] inp = sc.nextLine().split("\\s+");
            list.add(new XD68(inp[0], Integer.parseInt(inp[1])));
        }
        Collections.sort(list, (o1, o2) -> (int) ((o2.getTotal() - o1.getTotal()) / 1000));
        list.forEach(System.out::println);
    }
}

class XD68 {
    private String id;
    private String brand;
    private long price;
    private double tax;
    private long lit;

    private Map<String, String> map = new HashMap<String, String>() {
        {
            put("BP", "British Petro");
            put("ES", "Esso");
            put("SH", "Shell");
            put("CA", "Castrol");
            put("MO", "Mobil");
            put("TN", "Trong Nuoc");
        }
    };

    public XD68(String id, int lit) {
        this.id = id;
        this.lit = lit;
        analyzerId();
    }

    private void analyzerId() {
        char pid = id.charAt(0);
        if (pid == 'X') {
            price = 128000;
            tax = 0.03;
        } else if (pid == 'D') {
            price = 11200;
            tax = 0.035;
        } else if (pid == 'N') {
            price = 9700;
            tax = 0.02;
        } else {
            price = 0;
            tax = 0;
        }
        brand = map.get(id.substring(3));
        if (brand.equals("Trong Nuoc"))
            tax = 0;
    }

    public long getTaxL() {
        return Math.round(lit * price * tax);
    }

    public long getTotal() {
        return (lit * price) + getTaxL();
    }

    @Override
    public String toString() {
        return id + " " + brand + " " + price + " " + getTaxL() + " " + getTotal();
    }
}