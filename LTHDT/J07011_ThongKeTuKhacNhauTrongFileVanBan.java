import java.io.*;
import java.util.*;

public class J07011_ThongKeTuKhacNhauTrongFileVanBan {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        BufferedReader sc = new BufferedReader(new FileReader("VANBAN.in"));
        Map<String, Integer> map = new HashMap<>();
        String[] words;
        int T = Integer.parseInt(sc.readLine());
        while (T-- > 0) {
            words = sc.readLine().toLowerCase().split("\\W+");
            for (String word : words) {
                if (word.equals("")) continue;
                if (!map.containsKey(word)) {
                    map.put(word, 1);
                } else {
                    map.put(word, (map.get(word)) + 1);
                }
            }
        }
        map.entrySet().stream().sorted((k1, k2) -> {
            if (k1.getValue() == k2.getValue()) {
                return k1.getKey().compareTo(k2.getKey());
            } else
                return k2.getValue() - k1.getValue();
        }).forEach(k -> System.out.println(k.getKey() + " " + k.getValue()));
        sc.close();
    }
}
