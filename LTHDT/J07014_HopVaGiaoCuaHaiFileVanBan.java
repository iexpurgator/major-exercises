import java.util.*;
import java.io.*;

public class J07014_HopVaGiaoCuaHaiFileVanBan {
    //public static void main(String[] args) {}
    public static void main(String[] args) throws IOException {
        WordSet s1 = new WordSet("DATA1.in");
        WordSet s2 = new WordSet("DATA2.in");
        System.out.println(s1.union(s2));
        System.out.println(s1.intersection(s2));
    }
}

class WordSet {
    private TreeSet<String> list;

    public WordSet(String file) {
        list = new TreeSet<>();
        BufferedReader br ;
        try {
            br = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            return;
        }
        String[] aLine;
        while (true) {
            try {
                aLine = br.readLine().toLowerCase().split("\\s+");
                for (String string : aLine) {
                    if (string.isEmpty()) continue;
                    list.add(string);
                }
            } catch (IOException e) {
            } catch (NullPointerException e) {
                break;
            }
        }
    }
    
    public WordSet(TreeSet<String> l) {
        this.list = l;
    }

    public WordSet union(WordSet s) {
        TreeSet<String> word = new TreeSet<>();
        word.addAll(s.list);
        word.addAll(this.list);
        return new WordSet(word);
    }

    public WordSet intersection(WordSet s) {
        TreeSet<String> word = new TreeSet<>();
        word.addAll(s.list);
        word.retainAll(this.list);
        return new WordSet(word);
    }

    @Override
    public String toString() {
        return list.toString().replace("[", "").replace("]", "").replace(",", "");
    }
}
