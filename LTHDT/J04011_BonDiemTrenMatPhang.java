import java.util.Scanner;

public class J04011_BonDiemTrenMatPhang {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = sc.nextInt();
        int id = 0;
        while (id++ < T) {
            Point3D p1 = new Point3D(sc.nextInt(), sc.nextInt(), sc.nextInt());
            Point3D p2 = new Point3D(sc.nextInt(), sc.nextInt(), sc.nextInt());
            Point3D p3 = new Point3D(sc.nextInt(), sc.nextInt(), sc.nextInt());
            Point3D p4 = new Point3D(sc.nextInt(), sc.nextInt(), sc.nextInt());
            if (Point3D.check(p1, p2, p3, p4)) {
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }
        }
    }
}

class Point3D {
    private int x, y, z;

    Point3D() {
    }

    public Point3D(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3D to(Point3D p) {
        return new Vector3D(x - p.x, y - p.y, z - p.z);
    }

    public static boolean check(Point3D p1, Point3D p2, Point3D p3, Point3D p4) {
        return p1.to(p4).mul(p1.to(p2).cross(p1.to(p3))) == 0;
    }
}

class Vector3D {
    private int x, y, z;

    Vector3D() {
    }

    public Vector3D(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3D cross(Vector3D v) {
        int x = (this.y * v.z) - (this.z * v.y);
        int y = (this.z * v.x) - (this.x * v.z);
        int z = (this.x * v.y) - (this.y * v.x);
        return new Vector3D(x, y, z);
    }

    public int mul(Vector3D v) {
        return (x * v.x) + (y * v.y) + (z * v.z);
    }
}