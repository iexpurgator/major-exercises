import java.util.Scanner;

public class J05004_DanhSachDoiTuongSinhVien2 {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        int i = 0;
        while (i++ < T) {
            String[] iname = sc.nextLine().toLowerCase().trim().split("\\s+");
            String name = "";
            for (String s : iname) {
                name += (s.substring(0, 1).toUpperCase() + s.substring(1) + " ");
            }
            String iclass = sc.nextLine();
            String date = sc.nextLine();
            double GPA = Double.parseDouble(sc.nextLine());
            SinhVien2 sv = new SinhVien2(i, name, date, iclass, GPA);
            System.out.println(sv);
        }
    }
}

class SinhVien2 {
    private String id;
    private String name;
    private String date;
    private String iclass;
    private double GPA;

    SinhVien2() {
    }

    SinhVien2(int i, String name, String date, String iclass, double GPA) {
        this.id = "B20DCCN" + String.format("%03d", i);
        this.name = name;
        this.date = date;
        this.iclass = iclass;
        this.GPA = GPA;
    }

    public String getDate() {
        String[] s = date.split("/");
        return String.format("%02d/%02d/%s", Integer.parseInt(s[0]), Integer.parseInt(s[1]), s[2]);
    }

    @Override
    public String toString() {
        return id + " " + name + iclass + " " + getDate() + " " + String.format("%.02f", GPA);
    }
}