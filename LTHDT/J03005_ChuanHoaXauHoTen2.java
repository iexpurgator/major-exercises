import java.util.Scanner;

public class J03005_ChuanHoaXauHoTen2 {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        while (T > 0) {
            args = sc.nextLine().toLowerCase().trim().split("\\s+");
            String Ho = ", " + args[0].toUpperCase();
            for (int i = 1; i < args.length; i++) {
                System.out.print(args[i].substring(0, 1).toUpperCase() + args[i].substring(1));
                if (i + 1 < args.length) System.out.print(" ");
            }
            System.out.println(Ho);
            T -= 1;
        }
    }
}
