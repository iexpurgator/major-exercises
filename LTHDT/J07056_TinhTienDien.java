import java.util.*;
import java.io.*;

/**
 * @author Donald
 */
public class J07056_TinhTienDien {

    static Scanner sc;

    public static void main(String[] args) throws Exception {
       sc = new Scanner(new FileInputStream("KHACHHANG.in"));
        // sc = new Scanner(new FileInputStream("src/T24112021/KHACHHANG_DIEN.in"));
        int T = Integer.parseInt(sc.nextLine());
        ArrayList<KhachHangDien> listKH = new ArrayList<>();
        for (int i = 1; i <= T; i++) {
            listKH.add(new KhachHangDien(i, sc.nextLine(), sc.nextLine()));
        }
        Collections.sort(listKH);
        listKH.forEach(System.out::println);
    }
}

class KhachHangDien implements Comparable<KhachHangDien> {
    private final HashMap<Character, Integer> dm = new HashMap<Character, Integer>() {{
        put('A', 100);
        put('B', 500);
        put('C', 200);
    }};
    private String ma;
    private String ten;
    private Character loai;
    private int chisoDau, chisoCuoi;

    public KhachHangDien(int ma, String ten, String linein) {
        this.ma = String.format("KH%02d", ma);
        this.ten = capitalize(ten);
        String[] line = linein.split("\\s+");
        this.loai = line[0].charAt(0);
        this.chisoDau = Integer.parseInt(line[1]);
        this.chisoCuoi = Integer.parseInt(line[2]);
    }

    private String capitalize(String string) {
        String[] split = string.toLowerCase().trim().split("\\s+");
        String capitalized = "";
        for (String s : split) {
            capitalized += Character.toUpperCase(s.charAt(0)) + s.substring(1) + " ";
        }
        return capitalized.trim();
    }

    public int getTienTrongDM() {
        int chiso = chisoCuoi - chisoDau;
        if (chiso > dm.get(loai)) return dm.get(loai) * 450;
        else return chiso * 450;
    }

    public int getTienVuotDM() {
        int chiso = chisoCuoi - chisoDau - dm.get(loai);
        if (chiso > 0) return chiso * 1000;
        else return 0;
    }

    public int getVAT() {
        return (int) Math.round(getTienVuotDM() * 0.05);
    }

    public int getTongTien() {
        return getTienTrongDM() + getTienVuotDM() + getVAT();
    }

    @Override
    public String toString() {
        return ma + " " + ten + " " + getTienTrongDM() + " " + getTienVuotDM() + " "
                + getVAT() + " " + getTongTien();
    }

    @Override
    public int compareTo(KhachHangDien o) {
        return o.getTongTien() - getTongTien();
    }
}