import java.util.*;

/**
 *
 * @author Hoan
 */
public class J05019_LuongMuaTrungBinh {
    static Scanner sc;

    public static void main(String[] args) {
        LinkedList<LuongMua> list = new LinkedList<>();
        sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        int id = 1;
        int i;
        while (T-- > 0) {
            String name, t1, t2;
            int rain;
            name = sc.nextLine();
            t1 = sc.nextLine();
            t2 = sc.nextLine();
            rain = Integer.parseInt(sc.nextLine());
            double time = minusTime(t1, t2);
            for (i = 0; i < list.size(); i++) {
                if (list.get(i).getName().equals(name)) {
                    break;
                }
            }
            if (i == list.size()) {
                list.add(new LuongMua(id, name, time, rain));
                id++;
            } else {
                double ti = list.get(i).getTime();
                int ri = list.get(i).getRain();
                list.get(i).setTime(time + ti);
                list.get(i).setRain(rain + ri);
            }
        }
        list.forEach(System.out::println);
    }

    static double minusTime(String t1, String t2) {
        String[] tt1 = t1.split(":");
        String[] tt2 = t2.split(":");
        int h = Integer.parseInt(tt2[0]) - Integer.parseInt(tt1[0]);
        int m = Integer.parseInt(tt2[1]) - Integer.parseInt(tt1[1]);
        return h + (m / 60.0);
    }
}

class LuongMua {

    private int id;
    private String name;
    private double time;
    private int rain;

    public LuongMua() {
    }

    public LuongMua(int id, String name, double time, int rain) {
        this.id = id;
        this.name = name;
        this.time = time;
        this.rain = rain;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public int getRain() {
        return rain;
    }

    public void setRain(int rain) {
        this.rain = rain;
    }

    @Override
    public String toString() {
        return String.format("T%02d %s %.02f", id, name, rain / time);
    }
}
