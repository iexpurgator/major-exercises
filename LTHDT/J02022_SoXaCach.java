import java.util.Scanner;

public class J02022_SoXaCach {
    private static Scanner sc = new Scanner(System.in);
    static String num = "123456789";
    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            int n = Integer.parseInt(sc.nextLine());
            String s = num.substring(0, n);
            if(n < 4) System.out.println();
            else{
                printPermutn(s, "");
                System.out.println();
            }
        }
    }

    static boolean check(String s){
        for (int i = 0; i < s.length()-1; i++) {
            if (Math.abs((int) s.charAt(i) - (int)s.charAt(i+1)) == 1){
                return false;
            }
        }
        return true;
    }

    static void printPermutn(String str, String ans) {
        if (str.length() == 0) {
            if (check(ans)) System.out.println(ans);
            return;
        }
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            String ros = str.substring(0, i) + str.substring(i + 1);
            printPermutn(ros, ans + ch);
        }
    }
}
