import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class J05034_DanhSachThucTap1 {
    static Scanner sc;
    public static void main(String[] args) {
        List<SV34> list = new ArrayList<>();
        sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        int i = 0;
        while (i++ < T) {
            String id, name, iclass, email, dn;
            id = sc.nextLine();
            name = sc.nextLine();
            iclass = sc.nextLine();
            email = sc.nextLine();
            dn = sc.nextLine();
            list.add(new SV34(i, id, name, iclass, email, dn));
        }
        Collections.sort(list, new Comparator<SV34>() {
            @Override
            public int compare(SV34 t1, SV34 t2) {
                return t1.getName().compareTo(t2.getName());
            }
        });
        T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            String dn = sc.nextLine();
            list.stream().filter(e -> e.getDn().equals(dn)).forEach(System.out::println);
        }
    }
}

class SV34 {
    private int stt;
    private String id;
    private String name;
    private String iclass;
    private String email;
    private String dn;

    public SV34() {
    }

    public SV34(int stt, String id, String name, String iclass, String email, String dn) {
        this.stt = stt;
        this.id = id;
        this.name = name;
        this.iclass = iclass;
        this.email = email;
        this.dn = dn;
    }

    public String getDn() {
        return dn;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return stt + " " + id + " " + name + " " + iclass + " " + email + " " + dn;
    }
}
