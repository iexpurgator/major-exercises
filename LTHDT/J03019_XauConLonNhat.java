import java.util.*;

public class J03019_XauConLonNhat {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        String s = sc.nextLine();
        int t = 0;
        int[] st = new int[s.length() + 1];
        st[0] = 'z' + 1;
        int l = s.length();
        s = "*" + s;
        for (int i = 1; i <= l; i++) {
            while (s.charAt(i) > st[t])
                t--;
            st[++t] = s.charAt(i);
        }
        for (int i = 1; i <= t; i++)
            System.out.print((char) st[i]);
    }
}