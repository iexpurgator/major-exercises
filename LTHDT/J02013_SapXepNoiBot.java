import java.util.Scanner;

public class J02013_SapXepNoiBot {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int n = Integer.parseInt(sc.nextLine());
        int step = 1;
        String[] in = sc.nextLine().split("\\s");
        int[] a = new int[n];
        for (int i = 0; i < n; ++i) {
            a[i] = Integer.parseInt(in[i]);
        }

        boolean hswap = true;
        while (hswap) {
            for (int j = 0, k = 0; j < n - 1; j++) {
                if (a[j] > a[j + 1]) {
                    int tmp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = tmp;
                    hswap = true;
                    k++;
                }
                if (k == 0)
                    hswap = false;
            }
            if (hswap)
                show(step++, n, a);
        }
    }

    private static void show(int i, int n, int[] a) {
        final String st = "Buoc ";
        System.out.print(st + i + ":");
        for (int j = 0; j < n; ++j) {
            System.out.print(" " + a[j]);
        }
        System.out.println();
    }
}
