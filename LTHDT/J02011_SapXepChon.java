import java.util.Scanner;

public class J02011_SapXepChon {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int n = Integer.parseInt(sc.nextLine());
        int step = 1;
        String[] in = sc.nextLine().split("\\s");
        int[] a = new int[n];
        for (int i = 0; i < n; ++i) {
            a[i] = Integer.parseInt(in[i]);
        }

        for (int i = 0; i < n - 1; ++i) {
            int sma = i;
            for (int j = i + 1; j < n; j++) {
                if (a[sma] > a[j]) {
                    sma = j;
                }
            }
            int tmp = a[sma];
            a[sma] = a[i];
            a[i] = tmp;
            show(step++, n, a);
        }
    }

    private static void show(int i, int n, int[] a) {
        final String st = "Buoc ";
        System.out.print(st + i + ":");
        for (int j = 0; j < n; ++j) {
            System.out.print(" " + a[j]);
        }
        System.out.println();
    }
}
