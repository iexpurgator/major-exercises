import java.util.Scanner;

public class J02030_SapXepChen_LietKeNguoc {
    private static Scanner sc = new Scanner(System.in);
    static final String st = "Buoc ";

    public static void main(String[] args) {
        int n = Integer.parseInt(sc.nextLine());
        String res = "";
        String[] in = sc.nextLine().split("\\s");
        int[] a = new int[n];
        for (int i = 0; i < n; ++i) {
            a[i] = Integer.parseInt(in[i]);
        }

        for (int i = 0; i < n; ++i) {
            int cur = a[i];
            boolean hswap = false;
            for (int j = 0; j <= i; j++) {
                if (a[j] > cur) {
                    int tmp = cur;
                    cur = a[j];
                    a[j] = tmp;
                    hswap = true;
                }
            }
            if (hswap)
                a[i] = cur;
            res = show(i, i + 1, a) + res;
        }
        System.out.print(res);
    }

    private static String show(int i, int n, int[] a) {
        String s = st + i + ":";
        for (int j = 0; j < n; ++j) {
            s += " " + a[j];
        }
        s += "\n";
        return s;
    }
}
