import java.util.*;
import java.io.*;

/**
 *
 * @author Donald
 */
public class J07037_DanhSachDoanhNghiep {

    static Scanner sc;

    public static void main(String[] args) throws Exception {
        sc = new Scanner(new FileInputStream("DN.in"));
        int T = Integer.parseInt(sc.nextLine());
        String id, name;
        int count;
        ArrayList<DoanhNghiep> list = new ArrayList<>();
        while (T-- > 0) {
            id = sc.nextLine();
            name = sc.nextLine();
            count = Integer.parseInt(sc.nextLine());
            list.add(new DoanhNghiep(id, name, count));
        }
        list.sort((e1, e2) -> {
            return e1.getId().compareTo(e2.getId());
        });
        list.forEach(System.out::println);
    }
}

class DoanhNghiep {

    private String id;
    private String name;
    private int count;

    public DoanhNghiep(String id, String name, int count) {
        this.id = id;
        this.name = name;
        this.count = count;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + count;
    }
}
