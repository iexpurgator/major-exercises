import java.util.*;
import java.io.*;

/**
 * @author Donald
 */
public class J07061_SoKhongGiam {

    public static void main(String[] args) throws Exception {
        ObjectInputStream in;
        in = new ObjectInputStream (new FileInputStream("DATA1.in"));
        ArrayList<Integer> arr1 = (ArrayList<Integer>) in.readObject();
        in = new ObjectInputStream (new FileInputStream("DATA2.in"));
        ArrayList<Integer> arr2 = (ArrayList<Integer>) in.readObject();

        HashMap <Integer, Integer> map1 = new HashMap<>();
        HashMap <Integer, Integer> map2 = new HashMap<>();

        TreeSet<Integer> set = new TreeSet<>();
        set.addAll(arr1);
        set.addAll(arr2);

        for(int i : arr1) {
            if (check(i)) {
                if (map1.containsKey(i)) map1.put(i, map1.get(i) + 1);
                else map1.put(i, 1);
            }
        }
        for(int i : arr2) {
            if (check(i)) {
                if (map2.containsKey(i)) map2.put(i, map2.get(i) + 1);
                else map2.put(i, 1);
            }
        }
        for (int i : set) {
            if (map1.containsKey(i) || map2.containsKey(i)) {
                int cnt1 = map1.get(i) == null ? 0 : map1.get(i);
                int cnt2 = map2.get(i) == null ? 0 : map2.get(i);
                System.out.println(i + " " + cnt1 + " " + cnt2);
            }
        }
    }

    private static boolean check(int i) {
        if (i < 10) return false;
        int k = i % 10;
        while (i > 0) {
            i /= 10;
            if (i % 10 > k) return false;
            k = i % 10;
        }
        return true;
    }
}