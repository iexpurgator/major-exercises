import java.util.HashMap;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class J08015_CapSoCoTongBangK {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int T = Integer.parseInt(br.readLine().trim());
        while (T-- > 0) {
            int n, key, k;
            long cnt = 0;
            args = br.readLine().trim().split("\\s");
            n = Integer.parseInt(args[0]);
            k = Integer.parseInt(args[1]);
            args = br.readLine().trim().split("\\s");
            HashMap <Integer, Integer> a = new HashMap<>();
            for (int i = 0; i < n; ++i) {
                key = Integer.parseInt(args[i]);
                if (a.containsKey(k - key)) {
                    cnt += a.get(k - key);
                }
                a.put(key, a.getOrDefault(key, 0) + 1);
            }
            System.out.println(cnt);
        }
    }
}