import java.util.*;

public class J06002_SapXepHoaDonBanQuanAo {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        HashMap<String, QuanAo2> map = new HashMap<>();
        String id, name;
        int p1, p2;
        while (T-- > 0) {
            id = sc.nextLine();
            name = sc.nextLine();
            p1 = Integer.parseInt(sc.nextLine());
            p2 = Integer.parseInt(sc.nextLine());
            map.put(id, new QuanAo2(id, name, p1, p2));
        }
        T = Integer.parseInt(sc.nextLine());
        int serial = 0;
        String[] inp;
        QuanAo2 quanAo;
        ArrayList<HoaDon2> hoadon = new ArrayList<>();
        while (serial++ < T) {
            inp = sc.nextLine().split("\\s+");
            p1 = Integer.parseInt(inp[0].substring(2)); // kind
            p2 = Integer.parseInt(inp[1]); // count
            quanAo = map.get(inp[0].substring(0, 2));
            hoadon.add(new HoaDon2(quanAo.getCode(p1, serial),
                                   quanAo.getName(),
                                   quanAo.getDiscound(p1, p2),
                                   quanAo.getTotal(p1, p2)));
        }
        Collections.sort(hoadon);
        hoadon.forEach(System.out::println);
    }
}

class QuanAo2 {
    private String id;
    private String name;
    private long[] p;

    public QuanAo2(String id, String name, long p1, long p2) {
        p = new long[2];
        this.id = id;
        this.name = name;
        this.p[0] = p1;
        this.p[1] = p2;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public long getDiscound(int kind, int count) {
        double dis;
        if (count > 149)
            dis = 0.5;
        else if (count > 99)
            dis = 0.3;
        else if (count > 49)
            dis = 0.15;
        else
            dis = 0;
        return Math.round(p[kind - 1] * count * dis);
    }

    public long getTotal(int kind, int count) {
        long dis = getDiscound(kind, count);
        return (p[kind - 1] * count) - dis;
    }

    public String getCode(int kind, int serial) {
        return String.format("%s%d-%03d", id, kind, serial);
    }
}

class HoaDon2 implements Comparable<HoaDon2> {
    private String code;
    private String name;
    private long dis;
    private long total;

    public HoaDon2(String code, String name, long dis, long total) {
        this.code = code;
        this.name = name;
        this.dis = dis;
        this.total = total;
    }

    @Override
    public String toString() {
        return code + " " + name + " " + dis + " " + total;
    }

    @Override
    public int compareTo(HoaDon2 o) {
        return (int) ((o.total - total) / 100);
    }
}