import java.io.*;
import java.util.*;
/**
 *
 * @author Donald
 */
public class J07021_ChuanHoaXauHoTenTrongFile {
    static Scanner sc;
    public static void main(String[] args) throws Exception{
        sc = new Scanner(new FileReader("DATA.in"));
        String[] in;
        while(true){
            in = sc.nextLine().toLowerCase().trim().split("\\s+");
            if(in[0].equals("end")) break;
            for (String string : in) {
                System.out.print(Character.toUpperCase(string.charAt(0)) + "" + string.substring(1) + " ");
            } System.out.println();
        }
    }
}
