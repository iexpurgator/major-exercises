import java.util.*;
import java.io.*;

/**
 * @author Donald
 */
public class J07058_DanhSachMonThi {

    static Scanner sc;

    public static void main(String[] args) throws Exception {
        sc = new Scanner(new FileInputStream("MONHOC.in"));
//        sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        ArrayList<MonThi> dsMonThi = new ArrayList<>();
        while (T-- > 0) {
            String ma, ten, mon;
            ma = sc.nextLine();
            ten = sc.nextLine();
            mon = sc.nextLine();
            dsMonThi.add(new MonThi(ma, ten, mon));
        }
        Collections.sort(dsMonThi);
        dsMonThi.forEach(System.out::println);
    }
}

class MonThi implements Comparable<MonThi> {
    private String ma;
    private String ten;
    private String hinhThuc;

    public MonThi(String ma, String ten, String hinhThuc) {
        this.ma = ma;
        this.ten = ten;
        this.hinhThuc = hinhThuc;
    }

    @Override
    public String toString() {
        return ma + " " + ten + " " + hinhThuc;
    }

    @Override
    public int compareTo(MonThi o) {
        return ma.compareTo(o.ma);
    }
}