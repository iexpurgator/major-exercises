import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class J05026_DanhSachGiangVienTheoBoMon {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        List<GV26> list = new ArrayList<>();
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        while (id++ < T) {
            String name, ma;
            name = sc.nextLine();
            ma = sc.nextLine().toUpperCase();
            list.add(new GV26(id, name, ma));
        }
        T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            String[] s = sc.nextLine().toUpperCase().split("\\s+");
            String f = "";
            for (String i : s) {
                f += i.charAt(0);
            }
            final String fi = f;
            System.out.println("DANH SACH GIANG VIEN BO MON " + fi + ":");
            list.stream().filter(e -> e.getMa().equals(fi)).forEach(System.out::println);
        }
    }
}

class GV26 {
    private String id;
    private String name;
    private String ma;

    GV26() {
    }

    GV26(int id, String name, String ma) {
        String[] sma = ma.split("\\s+");
        this.id = String.format("GV%02d", id);
        this.name = name;
        this.ma = "";
        for (String string : sma) {
            this.ma += string.charAt(0);
        }
    }

    public String getId() {
        return id;
    }

    public String getName() {
        String[] n = name.split("\\s+");
        return n[n.length - 1];
    }

    public String getMa() {
        return ma;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + ma;
    }
}
