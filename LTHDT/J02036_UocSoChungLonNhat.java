import java.util.*;
import java.io.*;

public class J02036_UocSoChungLonNhat {
    public static void main(String[] args) throws IOException {
        J02036Reader sc = new J02036Reader();
        int T = Integer.parseInt(sc.next());
        int n;
        int[] a;
        while (T-- > 0) {
            n = Integer.parseInt(sc.next());
            a = new int[n];
            for (int i = 0; i < n; i++) {
                a[i] = Integer.parseInt(sc.next());
            }
            System.out.print(a[0] + " ");
            for (int i = 1; i < n; i++) {
                System.out.print(lcm(a[i - 1], a[i]) + " ");
            }
            System.out.println(a[n - 1]);
        }
    }

    static int gcd(int a, int b) {
        return b != 0 ? gcd(b, a % b) : a;
    }

    static int lcm(int a, int b) {
        return a * b / gcd(a, b);
    }
}

class J02036Reader {
    BufferedReader br;
    StringTokenizer st;

    public J02036Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}