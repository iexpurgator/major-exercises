package J06005;

class HoaDon {
    private String maHD;
    private String maKH;
    private String maMH;
    private int count;

    public HoaDon() {
    }

    public HoaDon(int maHD, String maKH, String maMH, int count) {
        this.maHD = String.format("HD%03d", maHD);
        this.maKH = maKH;
        this.maMH = maMH;
        this.count = count;
    }

    public String getMaHD() {
        return maHD;
    }

    public void setMaHD(String maHD) {
        this.maHD = maHD;
    }

    public String getMaKH() {
        return maKH;
    }

    public void setMaKH(String maKH) {
        this.maKH = maKH;
    }

    public String getMaMH() {
        return maMH;
    }

    public void setMaMH(String maMH) {
        this.maMH = maMH;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
