package J06005;

class MatHang {
    private String ma;
    private String ten;
    private String dv;
    private int pin;
    private int pout;

    public MatHang() {
    }

    public MatHang(int ma, String ten, String dv, int pin, int pout) {
        this.ma = String.format("MH%03d", ma);
        this.ten = ten;
        this.dv = dv;
        this.pin = pin;
        this.pout = pout;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getDv() {
        return dv;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }

    public int getPin() {
        return pin;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }

    public int getPout() {
        return pout;
    }

    public void setPout(int pout) {
        this.pout = pout;
    }
}
