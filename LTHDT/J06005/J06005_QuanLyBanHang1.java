package J06005;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class J06005_QuanLyBanHang1 {
    static Scanner sc;
    public static void main(String[] args) {
        sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        List<KhachHang> listKH = new ArrayList<>();
        int i = 0;
        while (i++ < T) {
            String ten, gt, sn, dc;
            ten = sc.nextLine();
            gt = sc.nextLine();
            sn = sc.nextLine();
            dc = sc.nextLine();
            listKH.add(new KhachHang(i, ten, gt, sn, dc));
        }
        T = Integer.parseInt(sc.nextLine());
        i = 0;
        List<MatHang> listMH = new ArrayList<>();
        while (i++ < T) {
            String ten, dv;
            int pin, pout;
            ten = sc.nextLine();
            dv = sc.nextLine();
            pin = Integer.parseInt(sc.nextLine());
            pout = Integer.parseInt(sc.nextLine());
            listMH.add(new MatHang(i, ten, dv, pin, pout));
        }
        T = Integer.parseInt(sc.nextLine());
        i = 0;
        List<HoaDon> listHD = new ArrayList<>();
        while (i++ < T) {
            String[] s = sc.nextLine().split("\\s+");
            listHD.add(new HoaDon(i, s[0], s[1], Integer.parseInt(s[2])));
        }
        for (HoaDon hoaDon : listHD) {
            String maKH = hoaDon.getMaKH();
            String maMH = hoaDon.getMaMH();
            KhachHang khachHang = listKH.stream().filter(e -> e.getMa().equals(maKH)).findFirst().orElseGet(null);
            MatHang matHang = listMH.stream().filter(e -> e.getMa().equals(maMH)).findFirst().orElseGet(null);
            long tien = (long) hoaDon.getCount() * (long) matHang.getPout();
            System.out.printf("%s %s %s %s %s %d %d %d %d\n", hoaDon.getMaHD(), khachHang.getTen(), khachHang.getDc(), matHang.getTen(), matHang.getDv(), matHang.getPin(), matHang.getPout(), hoaDon.getCount(), tien);
        }
    }
}
