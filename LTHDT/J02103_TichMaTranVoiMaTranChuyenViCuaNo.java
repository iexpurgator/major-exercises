import java.util.Scanner;

public class J02103_TichMaTranVoiMaTranChuyenViCuaNo {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        int t = 1;
        while (t <= T) {
            System.out.println("Test " + t + ":");
            int n, m, i, j, k, sum;
            n = sc.nextInt();
            m = sc.nextInt();
            int[][] arr = new int[n][m];
            int[][] tar = new int[m][n];
            for (i = 0; i < n; i++) {
                for (j = 0; j < m; j++) {
                    arr[i][j] = sc.nextInt();
                    tar[j][i] = arr[i][j];
                }
            }
            for (i = 0; i < n; i++) {
                for (j = 0; j < n; j++) {
                    sum = 0;
                    for (k = 0; k < m; k++) {
                        sum += arr[i][k] * tar[k][j];
                    }
                    System.out.print(sum + " ");
                }
                System.out.println();
            }
            t += 1;
        }
        sc.close();
    }
}
