import java.util.Scanner;

public class J03018_TimSoDu {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        String in;
        int m, T;
        T = Integer.parseInt(sc.nextLine());
        while (T > 0) {
            // 1^x mod 5 return 1 all x
            // 2^x + 3^x + 4^x mod 5 (return 3) if x%4 == 0 else (return 4)
            // => 1^x + 2^x + 3^x + 4^x mod 5 (return 4) if x%4 == 0 else (return 0)
            in = (0 + sc.nextLine());
            m = Integer.parseInt(in.substring(in.length() - 2));
            System.out.println(m % 4 == 0 ? 4 : 0);
            T -= 1;
        }
    }
}
