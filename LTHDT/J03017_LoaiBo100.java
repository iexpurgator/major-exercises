import java.util.ArrayDeque;
import java.util.HashMap;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class J03017_LoaiBo100 {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int T = Integer.parseInt(br.readLine().trim());
        while (T > 0) {
            String s = br.readLine();
            System.out.println(longestNull(s));
            T -= 1;
        }
    }

    public static int longestNull(String str) {
        int len = str.length();
        if (len < 3)
            return 0;
        char[] arr = new char[len];
        ArrayDeque<Integer> stk = new ArrayDeque<>();
        HashMap<Integer, Integer> hm = new HashMap<>();
        int max = 0, j = 0;
        for (int i = 0; i < len; i++) {
            arr[j++] = str.charAt(i);
            if (j > 2 && "100".equals(new String(arr, j - 3, 3))) {
                int stk_top = stk.pollLast();
                int total = hm.containsKey(stk_top - 1) ? hm.get(stk_top - 1) + i - stk_top : i - stk_top;
                max = Math.max(max, total + 1);
                hm.put(i, total + 1);
                j -= 3;
            }
            if (str.charAt(i) == '1')
                stk.addLast(i);
        }
        return max;
    }
}
