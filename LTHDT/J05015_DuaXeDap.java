import java.util.*;

/**
 *
 * @author Hoan
 */
public class J05015_DuaXeDap {
    static Scanner sc;

    public static void main(String[] args) {
        List<Bikecircle> list = new ArrayList<>();
        sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            String name, team, time;
            name = sc.nextLine();
            team = sc.nextLine();
            time = sc.nextLine();
            list.add(new Bikecircle(name, team, time));
        }
        Collections.sort(list, new Comparator<Bikecircle>() {
            @Override
            public int compare(Bikecircle t, Bikecircle t1) {
                return t.getTime() - t1.getTime();
            }
        });
        list.forEach(System.out::println);
    }
}

class Bikecircle {

    private String name;
    private String team;
    private String time;

    public Bikecircle() {
    }

    public Bikecircle(String name, String team, String time) {
        this.name = name;
        this.team = team;
        this.time = time;
    }

    public int getTime() {
        String[] t = time.split(":");
        return Integer.parseInt(t[0]) * 60 + Integer.parseInt(t[1]);
    }

    public int getVantoc() {
        String[] t = time.split(":");
        double ti = (Double.parseDouble(t[0]) - 6) + (Double.parseDouble(t[1]) / 60.0);
        return (int) Math.round(120.0 / ti);
    }

    private String convertID() {
        String id = "";
        String tmp = team + " " + name;
        String[] iid = tmp.split("\\s");
        for (String string : iid) {
            id += string.charAt(0);
        }
        return id;
    }

    @Override
    public String toString() {
        return convertID() + " " + name + " " + team + " " + getVantoc() + " Km/h";
    }
}
