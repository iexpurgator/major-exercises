import java.util.*;
import java.io.*;

public class J07016_SoNguyenToTrongHaiFileNhiPhan {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        boolean[] isPrime = new boolean[10005];
        Arrays.fill(isPrime, true);
        isPrime[0] = isPrime[1] = true;
        for (int i = 2; i < isPrime.length; i++) {
            if (isPrime[i])
                for (int j = i * i; j < isPrime.length; j += i) {
                    isPrime[j] = false;
                }
        }

        ObjectInputStream in = new ObjectInputStream (new FileInputStream("DATA1.in"));
        ArrayList<Integer> arr = (ArrayList<Integer>) in.readObject();
        int[] cnt1 = new int[10005];
        for (int i : arr) {
            if (isPrime[i]) {
                cnt1[i]++;
            }
        }

        in = new ObjectInputStream(new FileInputStream("DATA2.in"));
        arr = (ArrayList<Integer>) in.readObject();
        int[] cnt2 = new int[10005];
        for (int i : arr) {
            if (isPrime[i]) {
                cnt2[i]++;
            }
        }

        if (cnt1[2] > 0 || cnt2[2] > 0) System.out.println(2 + " " + cnt1[2] + " " + cnt2[2]);
        for (int i = 3; i < isPrime.length; i += 2) {
            if (cnt1[i] > 0 || cnt2[i] > 0)
                System.out.println(i + " " + cnt1[i] + " " + cnt2[i]);
        }
    }
}
