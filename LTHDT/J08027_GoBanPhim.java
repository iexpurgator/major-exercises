import java.text.*;
import java.util.*;
import java.io.*;

public class J08027_GoBanPhim {
    static BufferedReader in;
    public static void main(String[] args) throws Exception {
        in = new BufferedReader(new InputStreamReader(System.in));
        Stack<Character> a = new Stack<>();
        LinkedList<Character> b = new LinkedList<>();
        CharacterIterator it = new StringCharacterIterator(in.readLine());
        while (it.current() != CharacterIterator.DONE) {
            switch (it.current()) {
                case '<': if (!a.empty()) b.push(a.pop()); break;
                case '>': if (!b.isEmpty()) a.push(b.pop()); break;
                case '-': if (!a.empty()) a.pop(); break;
                default: a.push(it.current());
            }
            it.next();
        }
        a.forEach(System.out::print);
        b.forEach(System.out::print);
    }
}
/*
    public static void main(String[] args) throws Exception {
        sc = new Scanner(System.in);
        Stack<Character> a = new Stack<>(); // trước con trỏ
        LinkedList<Character> b = new LinkedList<>(); // sau con trỏ
        char[] s = sc.nextLine().toCharArray();
        for (char chr : s) {
            if (chr == '<' && !a.empty()) b.push(a.pop());
            else if (chr == '>' && !b.isEmpty()) a.push(b.pop());
            else if (chr == '-' && !a.empty()) a.pop();
            else if (chr != '<' && chr != '>' && chr != '-') a.push(chr);
        }
        a.forEach(System.out::print);
        b.forEach(System.out::print);
    }
*/