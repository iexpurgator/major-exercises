import java.io.*;
import java.util.*;

public class J07006_SoKhacNhauTrongFile3 {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream("DATA.in"));
        int[] cnt = new int[1001];
        ArrayList<Integer> arr;
        try {
            arr = (ArrayList) in.readObject();
            in.close();
            for (Integer i : arr) {
                cnt[i]++;
            }
            for (int i = 0; i < 1001; i++) {
                if (cnt[i] > 0) {
                    System.out.println(i + " " + cnt[i]);
                }
            }
        } catch (EOFException e) {
        } catch (ClassNotFoundException e) {
        }
    }
}