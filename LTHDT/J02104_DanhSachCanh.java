import java.util.Scanner;

public class J02104_DanhSachCanh {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        int[][] m = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                m[i][j] = sc.nextInt();
            }
        }
        for (int i = 0; i < n-1; i++) {
            for (int j = i+1; j < n; j++) {
                if (m[i][j] == 1 && m[j][i] == 1)
                    System.out.println("(" + (i + 1) + "," + (j + 1) + ")");
            }
        }
        sc.close();
    }
}
