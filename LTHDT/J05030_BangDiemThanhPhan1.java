import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class J05030_BangDiemThanhPhan1 {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        List<SinhVien30> list = new ArrayList<>();
        while (T-- > 0) {
            String id, name, iclass;
            double d1, d2, d3;
            id = sc.nextLine();
            name = sc.nextLine();
            iclass = sc.nextLine();
            d1 = Double.parseDouble(sc.nextLine());
            d2 = Double.parseDouble(sc.nextLine());
            d3 = Double.parseDouble(sc.nextLine());
            list.add(new SinhVien30(id, name, iclass, d1, d2, d3));
        }
        Collections.sort(list, new Comparator<SinhVien30>() {
            @Override
            public int compare(SinhVien30 o1, SinhVien30 o2) {
                return o1.getId().compareTo(o2.getId());
            }
        });
        // list.stream().forEach(System.out::println);
        int t = 1;
        for (SinhVien30 sinhVien30 : list) {
            System.out.println(t++ + " " + sinhVien30);
        }
    }
}

class SinhVien30 {
    private String id;
    private String name;
    private String iclass;
    private double d1;
    private double d2;
    private double d3;

    SinhVien30() {
    }

    SinhVien30(String id, String name, String iclass, double d1, double d2, double d3) {
        this.id = id;
        this.name = name;
        this.iclass = iclass;
        this.d1 = d1;
        this.d2 = d2;
        this.d3 = d3;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + iclass + " " + String.format("%.1f %.1f %.1f", d1, d2, d3);
    }
}