import java.util.*;
import java.io.*;

/**
 * @author Donald
 */
public class SapXepMatHang {

    static Scanner sc;

    public static void main(String[] args) throws Exception {
        sc = new Scanner(new FileInputStream("MATHANG.in"));
//        sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        ArrayList<MatHang> list = new ArrayList<>();
        String ten, nhom;
        double mua, ban;
        for (int i = 1; i <= T; i++) {
            ten = sc.nextLine();
            nhom = sc.nextLine();
            mua = Double.parseDouble(sc.nextLine());
            ban = Double.parseDouble(sc.nextLine());
            list.add(new MatHang(i, ten, nhom, mua, ban));
        }
        Collections.sort(list);
        list.forEach(System.out::println);
    }
}

class MatHang implements Comparable<MatHang> {
    private String ma;
    private String ten;
    private String nhom;
    private double giaMua;
    private double giaBan;

    public MatHang(int ma, String ten, String nhom, double giaMua, double giaBan) {
        this.ma = String.format("MH%02d", ma);
        this.ten = ten;
        this.nhom = nhom;
        this.giaMua = giaMua;
        this.giaBan = giaBan;
    }

    public double getLoiNhuan() {
        return giaBan - giaMua;
    }

    @Override
    public String toString() {
        return ma + " " + ten + " " + nhom + " " + String.format("%.02f", getLoiNhuan());
    }

    @Override
    public int compareTo(MatHang o) {
        return (int) ((o.getLoiNhuan() * 100) - (getLoiNhuan() * 100));
    }
}