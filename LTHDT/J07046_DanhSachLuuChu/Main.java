package J07046_DanhSachLuuChu;

import java.util.*;
import java.io.*;

/**
 *
 * @author Donald
 */
public class Main {
    static Scanner sc;

    public static void main(String[] args) throws Exception {
        sc = new Scanner(new FileInputStream("KHACH.in"));
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        ArrayList<KhachHang> list = new ArrayList<>();
        String name, roomId, checkIn, checkOut;
        while (id++ < T) {
            name = sc.nextLine();
            roomId = sc.nextLine();
            checkIn = sc.nextLine();
            checkOut = sc.nextLine();
            list.add(new KhachHang(id, name, roomId, checkIn, checkOut));
        }
        Collections.sort(list);
        list.forEach(System.out::println);
    }
}
