import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class J05028_DanhSachDoanhNghiepNhanSinhVienThucTap1 {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        List<DN28> list = new ArrayList<>();
        int T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            String id, name;
            int count;
            id = sc.nextLine();
            name = sc.nextLine();
            count = Integer.parseInt(sc.nextLine());
            list.add(new DN28(id, name, count));
        }
        Collections.sort(list, new Comparator<DN28>() {
            @Override
            public int compare(DN28 o1, DN28 o2) {
                if (o1.getCount() == o2.getCount())
                    return o1.getId().compareTo(o2.getId());
                return o2.getCount() - o1.getCount();
            }
        });
        list.stream().forEach(System.out::println);
    }
}

class DN28 {
    private String id;
    private String name;
    private int count;

    DN28() {
    }

    DN28(String id, String name, int count) {
        this.id = id;
        this.name = name;
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + count;
    }
}