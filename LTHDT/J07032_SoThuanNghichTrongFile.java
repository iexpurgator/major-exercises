import java.util.*;
import java.io.*;

/**
 *
 * @author Donald
 */
public class J07032_SoThuanNghichTrongFile {
    
    public static void main(String[] args) throws Exception {
        ObjectInputStream sc;
        sc = new ObjectInputStream(new FileInputStream("DATA1.in"));
        ArrayList<Integer> arr = (ArrayList<Integer>) sc.readObject();
        sc.close();
        sc = new ObjectInputStream(new FileInputStream("DATA2.in"));
        ArrayList<Integer> tmp = (ArrayList<Integer>) sc.readObject();
        sc.close();
        LinkedHashSet<Integer> group = new LinkedHashSet<>(genPalindrome());
        group.retainAll(tmp);
        group.retainAll(arr);
        int cnt = 0, total = 0;
        arr.addAll(tmp); // merge(arr, tmp); 

        for (Iterator<Integer> iter = group.iterator(); iter.hasNext();) {
            Integer e = iter.next();
//            if (group.contains(e)) {
            cnt = Collections.frequency(arr, e);
            System.out.println(e + " " + cnt);
            total++;
//            }
            if (total == 10) break;
        }
    }
    
    static ArrayList<Integer> genPalindrome() {
        ArrayList<Integer> list = new ArrayList<>();
        Queue<String> q = new LinkedList<>();
        q.add("1");
        q.add("3");
        q.add("5");
        q.add("7");
        q.add("9");
        String s1, s3, s5, s7, s9;
        while (q.peek().length() < 4) {
            String cur = q.poll();
            s1 = "1" + cur + "1";
            list.add(Integer.parseInt(s1));
            q.add(s1);
            s3 = "3" + cur + "3";
            list.add(Integer.parseInt(s3));
            q.add(s3);
            s5 = "5" + cur + "5";
            list.add(Integer.parseInt(s5));
            q.add(s5);
            s7 = "7" + cur + "7";
            list.add(Integer.parseInt(s7));
            q.add(s7);
            s9 = "9" + cur + "9";
            list.add(Integer.parseInt(s9));
            q.add(s9);
        }
        Collections.sort(list);
        return list;
    }
}
