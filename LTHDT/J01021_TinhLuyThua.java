import java.util.Scanner;

public class J01021_TinhLuyThua { // Modular Exponentiation
    private static Scanner sc = new Scanner(System.in);
    private static long MOD = (long) (1e9 + 7);

    public static void main(String[] args) {
        while (true) {
            long a, b;
            a = sc.nextLong();
            b = sc.nextLong();
            if (a == 0 && b == 0)
                break;
            System.out.println(pow(a, b));
        }
    }

    static long pow(long x, long y) {
        long res = 1;
        x = x % MOD;
        if (x == 0)
            return 0;
        while (y > 0) {
            if ((y & 1) != 0)
                res = (res * x) % MOD;
            y = y >> 1;
            x = (x * x) % MOD;
        }
        return res;
    }
}
