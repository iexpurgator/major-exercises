import java.util.*;
import java.io.*;

public class J07012_ThongKeTuKhacNhauTrongFileNhiPhan {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream("DATA.in"));
        ArrayList<String> arr;
        Map<String, Integer> map = new HashMap<>();
        String[] cau;
        try {
            arr = (ArrayList) in.readObject();
            in.close();
            for (String string : arr) {
                cau = string.toLowerCase().split("\\W+");
                for (String str : cau) {
                    if (str.equals("")) continue;
                    if (map.get(str) == null) {
                        map.put(str, 1);
                    } else {
                        map.put(str, (map.get(str)) + 1);
                    }
                }
            }
            map.entrySet().stream().sorted((k1, k2) -> {
                if (k1.getValue() == k2.getValue()) {
                    return k1.getKey().compareTo(k2.getKey());
                } else
                    return k2.getValue() - k1.getValue();
            }).forEach(k -> System.out.println(k.getKey() + " " + k.getValue()));
        } catch (EOFException e) {
        } catch (ClassNotFoundException e) {
        }
    }
}
