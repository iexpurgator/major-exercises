import java.util.*;

public class J05051_SapXepBangTinhTienDien {
    static Scanner sc;
    public static void main(String[] args) {
        sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        int i = 0;
        List<Dien50> list = new ArrayList<>();
        while (i++ < T) {
            String sp = sc.nextLine();
            int val_old, val_new;
            val_old = Integer.parseInt(sc.nextLine());
            val_new = Integer.parseInt(sc.nextLine());
            list.add(new Dien50(i, sp, val_old, val_new));
        }
        list.sort((o1, o2) -> (int) (o2.getTotal() - o1.getTotal()));
        list.forEach(System.out::println);
    }
}

class Dien50 {
    private final Map<String, Integer> map = new HashMap<String, Integer>() {
        {
            put("NN", 5);
            put("TT", 4);
            put("KD", 3);
            put("CN", 2);
        }
    };
    private String id;
    private String sp;
    private long val_old;
    private long val_new;
    private int hs;
    private long price;
    private long oth;
    private long total;

    public Dien50() {
    }

    public Dien50(int id, String sp, long val_old, long val_new) {
        this.id = String.format("KH%02d", id);
        this.sp = sp;
        this.val_old = val_old;
        this.val_new = val_new;
        calc();
    }

    private void calc() {
        hs = map.get(sp);
        int change = (int) (val_new - val_old);
        price = change * hs * 550L;
        if (change > 100) {
            oth = price;
        } else if (change >= 50) {
            oth = Math.round(price * 0.35f);
        } else {
            oth = 0;
        }
        total = price + oth;
    }

    public long getTotal() {
        return total;
    }

    @Override
    public String toString() {
        return id + " " + hs + " " + price + " " + oth + " " + total;
    }
}
