import java.util.*;

public class J02106_MaTranNhiPhan {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int n = sc.nextInt();
        int cnt = 0, res = 0;
        for (int i = 0; i < n; i++) {
            cnt = sc.nextInt() + sc.nextInt() + sc.nextInt();
            if (cnt > 1) res++;
            cnt = 0;
        }
        System.out.println(res);
    }
}
