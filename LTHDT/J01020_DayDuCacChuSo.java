import java.util.*;
import java.math.*;

public class J01020_DayDuCacChuSo {
    static Scanner sc = new Scanner(System.in);
    static boolean[] present;

    public static void main(String[] args) {
        int T = sc.nextInt();
        String s;
        while (T-- > 0) {
            present = new boolean[10];
            int bi = sc.nextInt();
            if (bi == 0) {
                System.out.println("Impossible");
                continue;
            }
            BigInteger k = BigInteger.valueOf(bi);
            int i = 1;
            do {
                s = BigInteger.valueOf(i).multiply(k).toString();
                i++;
            } while (!allDigits(s));
            System.out.println(s);
        }
    }

    static boolean allDigits(String str) {
        int len = str.length();
        for (int i = 0; i < len; i++) {
            if (Character.isDigit(str.charAt(i))) {
                int digit = str.charAt(i) - '0';
                present[digit] = true;
            }
        }
        for (int i = 0; i < 10; i++) {
            if (!present[i])
                return false;
        }
        return true;
    }
}