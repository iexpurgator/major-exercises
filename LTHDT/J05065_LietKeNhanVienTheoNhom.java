import java.util.*;

public class J05065_LietKeNhanVienTheoNhom {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        ArrayList<NV65> list = new ArrayList<>();
        String inp, level;
        int cGD = 0, cTP = 0, cPP = 0;
        while (T-- > 0) {
            inp = sc.nextLine();
            level = inp.substring(0, 2);
            if ((level.equals("GD") && ++cGD > 1) ||
                (level.equals("TP") && ++cTP > 3) ||
                (level.equals("PP") && ++cPP > 3)) {
                level = "NV";
            }
            list.add(new NV65(level + inp.substring(2, 7), inp.substring(8)));
        }
        Collections.sort(list);
        T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            String s = sc.nextLine();
            for (NV65 nv : list) {
                if (nv.getLevel().equals(s))
                    System.out.println(nv);
            }
            System.out.println();
        }
    }
}

class NV65 implements Comparable<NV65> {
    private String[] id;
    private String name;

    public NV65(String id, String name) {
        this.id = new String[3];
        this.id[0] = id.substring(0, 2);
        this.id[2] = id.substring(2, 4);
        this.id[1] = id.substring(4);
        this.name = name;
    }

    public String getLevel() {
        return id[0];
    }

    @Override
    public String toString() {
        return name + " " + id[0] + " " + id[1] + " " + id[2];
    }

    @Override
    public int compareTo(NV65 o) {
        if (id[2].equals(o.id[2]))
            return id[1].compareTo(o.id[1]);
        return o.id[2].compareTo(id[2]);
    }
}