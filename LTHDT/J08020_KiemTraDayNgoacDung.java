import java.util.*;

/**
 *
 * @author Donald
 */
public class J08020_KiemTraDayNgoacDung {

    static Scanner sc;

    public static void main(String[] args) {
        sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        String s;
        Stack<Character> st;
        while (T-- > 0) {
            s = sc.nextLine();
            st = new Stack<>();
            for (char c : s.toCharArray()) {
                if (c == '[' || c == '{' || c == '(') {
                    st.push(c);
                } else {
                    switch (c) {
                        case ']':
                            if (!st.isEmpty() && st.peek()== '[') {
                                st.pop();
                            }
                            break;
                        case '}':
                            if (!st.isEmpty() && st.peek()== '{') {
                                st.pop();
                            }
                            break;
                        case ')':
                            if (!st.isEmpty() && st.peek()== '(') {
                                st.pop();
                            }
                            break;
                    }
                }
            }
            if (st.isEmpty()) {
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }
        }
    }
}
/*
2
[()]{}{[()()]()}
[(])


*/
