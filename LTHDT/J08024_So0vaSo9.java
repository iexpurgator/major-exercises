import java.util.*;

/**
 *
 * @author hoan
 */
public class J08024_So0vaSo9 {

    static Scanner sc;

    public static void main(String[] args) {
        sc = new Scanner(System.in);
        int T = sc.nextInt();
        int n;
        String s;
        Queue<String> q;
        while (T-- > 0) {
            q = new LinkedList<>();
            s = "9";
            q.add(s);
            n = sc.nextInt();
            while (!q.isEmpty()) {
                s = q.poll();
                if (Long.parseLong(s) % n == 0) {
                    System.out.println(s);
                    break;
                }
                q.add(s + "0");
                q.add(s + "9");
            }
        }
    }
}
