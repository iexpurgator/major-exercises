import java.util.*;

public class J05024_LietKeSinhVienTheoNganh {
    static Scanner sc;
    public static void main(String[] args) {
        List<SV24> list = new ArrayList<>();
        sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            String id, name, iclass, email;
            id = sc.nextLine();
            name = sc.nextLine();
            iclass = sc.nextLine();
            email = sc.nextLine();
            list.add(new SV24(id, name, iclass, email));
        }
        T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            String nganh = sc.nextLine().toUpperCase();
            System.out.println("DANH SACH SINH VIEN NGANH " + nganh + ':');
            list.stream().filter(e -> e.getNganh().equals(nganh)).forEach(System.out::println);
        }
    }
}

class SV24 {

    private String id;
    private String name;
    private String iclass;
    private String email;
    private final Map<String, String> map = new HashMap<String, String>() {
        {
            put("DCKT", "KE TOAN");
            put("DCCN", "CONG NGHE THONG TIN");
            put("DCAT", "AN TOAN THONG TIN");
            put("DCVT", "VIEN THONG");
            put("DCDT", "DIEN TU");
        }
    };

    public SV24() {
    }

    public SV24(String id, String name, String iclass, String email) {
        this.id = id;
        this.name = name;
        this.iclass = iclass;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public String getIclass() {
        return iclass;
    }

    public String getNganh() {
        if (iclass.startsWith("E") && (id.substring(3, 7).equals("DCCN") || id.substring(3, 7).equals("DCAT")))
            return "";
        return map.get(id.substring(3, 7));
    }

    @Override
    public String toString() {
        return id + " " + name + " " + iclass + " " + email;
    }
}
