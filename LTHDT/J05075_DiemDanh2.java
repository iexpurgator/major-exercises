import java.util.*;
import java.io.*;

public class J05075_DiemDanh2 {
    public static void main(String[] args) throws IOException {
        J05075_Reader sc = new J05075_Reader();
        int T = Integer.parseInt(sc.nextLine());
        int i = 0;
        LinkedHashMap<String, DiemDanhSV2> map = new LinkedHashMap<>();
        String id, name, idClass;
        while (i++ < T) {
            id = sc.nextLine();
            name = sc.nextLine();
            idClass = sc.nextLine();
            map.put(id, new DiemDanhSV2(id, name, idClass));
        }
        i = 0;
        String[] inp;
        while (i++ < T) {
            inp = sc.nextLine().split("\\s+");
            map.get(inp[0]).setCheck(inp[1]);
        }
        final String id1 = sc.nextLine();
        map.entrySet().forEach((e) -> {
            if (e.getValue().getIdClass().equals(id1))
                System.out.println(e.getValue());
        });
    }
}

class DiemDanhSV2 {
    private String id;
    private String name;
    private String idClass;
    private int score;
    private String check;
    private String note;

    public DiemDanhSV2(String id, String name, String idClass) {
        this.id = id;
        this.name = name;
        this.idClass = idClass;
        score = 10;
        note = "";
    }

    public void setCheck(String check) {
        this.check = check;
        calc();
    }

    private void calc() {
        for (int i = 0; i < check.length(); i++) {
            if (check.charAt(i) == 'm')
                score--;
            else if (check.charAt(i) == 'v')
                score -= 2;
        }
        if (score <= 0)
            note = "KDDK";
    }

    public int getScore() {
        if (score <= 0)
            return 0;
        return score;
    }

    public String getIdClass() {
        return idClass;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + idClass + " " + getScore() + " " + note;
    }
}

class J05075_Reader {
    BufferedReader br;
    StringTokenizer st;

    public J05075_Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}
