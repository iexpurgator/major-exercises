import java.util.*;
import java.io.*;

public class J05071_TinhCuocDienThoaiCoDinh1 {
    public static void main(String[] args) {
        J05071Reader sc = new J05071Reader();
        int T = Integer.parseInt(sc.nextLine());
        HashMap<String, PhoneArea1> mapArea = new HashMap<>();
        mapArea.put("-1", new PhoneArea1("-1", "Noi mang", 800));
        String id, name;
        long price;
        while (T-- > 0) {
            id = sc.nextLine();
            name = sc.nextLine();
            price = Long.parseLong(sc.nextLine());
            mapArea.put("0" + id, new PhoneArea1(id, name, price));
        }
        T = Integer.parseInt(sc.nextLine());
        String[] inp;
        long time;
        int h_begin, m_begin, h_end, m_end;
        PhoneArea1 pa;
        while (T-- > 0) {
            inp = sc.nextLine().split("\\s+");
            id = (inp[0].charAt(0) == '0') ? inp[0].split("-")[0] : "-1";
            pa = mapArea.get(id);
            // calc time
            h_begin = Integer.valueOf(inp[1].split(":")[0]);
            m_begin = Integer.valueOf(inp[1].split(":")[1]);
            h_end = Integer.valueOf(inp[2].split(":")[0]);
            m_end = Integer.valueOf(inp[2].split(":")[1]);
            time = (h_end * 60 + m_end) - (h_begin * 60 + m_begin);
            if (id == "-1") {
                double t = time / 3.0;
                time = Math.round(t);
            }
            System.out.println(inp[0] + " " + pa.getName() + " " + time + " " + pa.getPrice() * time);
        }
    }
}

class PhoneArea1 {
    private String id;
    private String name;
    private long price;

    public PhoneArea1(String id, String name, long price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public long getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }
}

class J05071Reader {
    BufferedReader br;
    StringTokenizer st;

    public J05071Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}