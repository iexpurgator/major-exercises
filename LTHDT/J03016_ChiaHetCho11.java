import java.math.BigInteger;
import java.util.Scanner;

public class J03016_ChiaHetCho11 {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        BigInteger eleven = new BigInteger("11");
        while (T > 0) {
            BigInteger N = new BigInteger(sc.nextLine());
            System.out.println(N.remainder(eleven).equals(BigInteger.ZERO) ? 1 : 0);
            T -= 1;
        }
    }
}
