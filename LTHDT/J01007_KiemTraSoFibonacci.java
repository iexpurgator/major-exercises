import java.util.Scanner;

public class J01007_KiemTraSoFibonacci {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        while (T > 0) {
            long a;
            a = Long.parseLong(sc.nextLine());
            System.out.println(Fib(a) ? "YES" : "NO");
            T -= 1;
        }
    }

    private static boolean Fib(long a) {
        if (a == 0 || a == 1) return true;
        long f1, f2, tm;
        f1 = f2 = 1;
        while(true) {
            tm = f1;
            f1 = f1 + f2;
            f2 = tm;
            if (f1 == a) return true;
            if (f1 > a) break;
        }
        return false;
    }
}
