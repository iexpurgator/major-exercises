import java.util.*;

public class J05066_TimKiemNhanVienTheoTen {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        ArrayList<NV66> list = new ArrayList<>();
        String inp;
        while (T-- > 0) {
            inp = sc.nextLine();
            String level = inp.substring(0, 2);
            if ((level.equals("GD") && Integer.parseInt(inp.substring(4,7)) > 1)
                 || ((level.equals("TP") || level.equals("PP"))
                     && (Integer.parseInt(inp.substring(4, 7)) > 3))) {
                list.add(new NV66("NV" + inp.substring(2, 7), inp.substring(8)));
            } else
                list.add(new NV66(inp.substring(0, 7), inp.substring(8)));
        }
        Collections.sort(list);
        T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            String s = sc.nextLine().toLowerCase();
            for (NV66 nv : list) {
                if (nv.getName().contains(s))
                    System.out.println(nv);
            }
            System.out.println();
        }
    }
}

class NV66 implements Comparable<NV66> {
    private String[] id;
    private String name;

    public NV66(String id, String name) {
        this.id = new String[3];
        this.id[0] = id.substring(0, 2);
        this.id[2] = id.substring(2, 4);
        this.id[1] = id.substring(4);
        this.name = name;
    }

    public String getName() {
        return name.toLowerCase();
    }

    @Override
    public String toString() {
        return name + " " + id[0] + " " + id[1] + " " + id[2];
    }

    @Override
    public int compareTo(NV66 o) {
        if (id[2].equals(o.id[2]))
            return id[1].compareTo(o.id[1]);
        return o.id[2].compareTo(id[2]);
    }
}
