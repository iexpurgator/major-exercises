import java.util.*;

public class J01016_ChuSo4VaChuSo7 {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        String num = sc.nextLine();
        System.out.println(check(num) ? "YES" : "NO");
    }

    static boolean check(String num) {
        int cnt = 0;
        for (int i = 0; i < num.length(); i++) {
            if (num.charAt(i) == '4' || num.charAt(i) == '7')
                cnt++;
        }
        if (cnt == 4)
            return true;
        else if (cnt == 7)
            return true;
        else
            return false;
    }
}
