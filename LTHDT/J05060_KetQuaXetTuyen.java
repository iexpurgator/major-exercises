import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

public class J05060_KetQuaXetTuyen {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        List<PH> list = new ArrayList<>();
        while (id++ < T) {
            String name, date;
            double score1, score2;
            name = sc.nextLine();
            date = sc.nextLine();
            score1 = Double.parseDouble(sc.nextLine());
            score2 = Double.parseDouble(sc.nextLine());
            list.add(new PH(id, name, date, score1, score2));
        }
        list.forEach(System.out::println);
    }
}

class PH {
    private String id;
    private String name;
    private int avg, age;

    public PH(int id, String name, String date, double score1, double score2) {
        this.id = String.format("PH%02d", id);
        this.name = name;
        this.age = 2021 - Integer.parseInt(date.split("/")[2]);
        double avg = (score1 + score2) / 2;
        if (score1 >= 8 && score2 >= 8) {
            avg += 1;
        } else if (score1 >= 7.5 && score2 >= 7.5) {
            avg += 0.5;
        }
        this.avg = (int) Math.round(avg);
        if(this.avg > 10) this.avg = 10;
    }



    public String XepLoai(){
        if (avg >= 9) {
            return "Xuat sac";
        } else if (avg == 8) {
            return "Gioi";
        } else if (avg == 7) {
            return "Kha";
        } else if (avg >= 5) {
            return "Trung binh";
        } else {
            return "Truot";
        }
    }

    @Override
    public String toString() {
        return id + " " + name + " " + age + " " + avg + " " + XepLoai();
    }
}
