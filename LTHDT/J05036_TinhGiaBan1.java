import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class J05036_TinhGiaBan1 {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        List<MH36> list = new ArrayList<>();
        int id = 0;
        while (id++ < T) {
            String name, dv;
            long pin;
            int count;
            name = sc.nextLine();
            dv = sc.nextLine();
            pin = Long.parseLong(sc.nextLine());
            count = Integer.parseInt(sc.nextLine());
            list.add(new MH36(id, name, dv, pin, count));
        }
        list.forEach(System.out::println);
    }
}

class MH36 {
    private String id;
    private String name;
    private String dv;
    private long pin;
    private long ship;
    private long pout;
    private long res;
    private int count;

    MH36() {
    }

    public MH36(int id, String name, String dv, long pin, int count) {
        this.id = String.format("MH%02d", id);
        this.name = name;
        this.dv = dv;
        this.pin = pin;
        this.count = count;
        calc();
    }

    private void calc() {
        ship = Math.round((pin * count) * 0.05);
        pout = (pin * count) + ship;
        res = Math.round(pout * 1.02);
    }

    @Override
    public String toString() {
        return id + " " + name + " " + dv + " " + ship + " " + pout + " " + res;
    }
}