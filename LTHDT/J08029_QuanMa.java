import java.util.*;
import java.io.*;

public class J08029_QuanMa {
    static int[] xqX = { 2, 1, -1, -2, -2, -1, 1, 2 };
    static int[] xqY = { 1, 2, 2, 1, -1, -2, -2, -1 };

    public static void main(String[] args) throws IOException {
        J08029_Reader sc = new J08029_Reader();
        int T = Integer.parseInt(sc.nextLine());
        String start, target;
        int x1, y1, x2, y2;
        while (T-- > 0) {
            start = sc.next();
            target = sc.next();
            x1 = start.charAt(0) - 'a' + 1;
            y1 = start.charAt(1) - '0';
            x2 = target.charAt(0) - 'a' + 1;
            y2 = target.charAt(1) - '0';
            System.out.println(bfs(x1, y1, x2, y2));
        }
    }

    static int bfs(int x1, int y1, int x2, int y2) {
        Queue<ChessMov> q = new LinkedList<>();
        int cnt = 0;
        q.add(new ChessMov(x1, y1, cnt));
        while (!q.isEmpty()) {
            ChessMov u = q.poll();
            if (u.x == x2 && u.y == y2) return u.cnt;
            for (int i = 0; i < 8; i++) {
                int x = u.x + xqX[i];
                int y = u.y + xqY[i];
                int z = u.cnt + 1;
                if (x >= 1 && y >= 1 && x <= 8 && y <= 8)
                    q.add(new ChessMov(x, y, z));
            }
        }
        return 0;
    }
}

class ChessMov {
    int x;
    int y;
    int cnt;

    public ChessMov(int x, int y, int cnt) {
        this.x = x;
        this.y = y;
        this.cnt = cnt;
    }
}

class J08029_Reader {
    BufferedReader br;
    StringTokenizer st;

    public J08029_Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    int nextInt() {
        return Integer.parseInt(next());
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}
