import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class J05029_DanhSachDoanhNghiepNhanSinhVienThucTap2 {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        List<DN29> list = new ArrayList<>();
        int T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            String id, name;
            int count;
            id = sc.nextLine();
            name = sc.nextLine();
            count = Integer.parseInt(sc.nextLine());
            list.add(new DN29(id, name, count));
        }
        Collections.sort(list, new Comparator<DN29>() {
            @Override
            public int compare(DN29 o1, DN29 o2) {
                if (o1.getCount() == o2.getCount())
                    return o1.getId().compareTo(o2.getId());
                return o2.getCount() - o1.getCount();
            }
        });
        T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            int d, c;
            String[] s = sc.nextLine().split("\\s+");
            d = Integer.parseInt(s[0]);
            c = Integer.parseInt(s[1]);
            System.out.println("DANH SACH DOANH NGHIEP NHAN TU " + d + " DEN " + c + " SINH VIEN:");
            list.stream().filter(e -> (d <= e.getCount() && e.getCount() <= c)).forEach(System.out::println);
        }
    }
}

class DN29 {
    private String id;
    private String name;
    private int count;

    DN29() {
    }

    DN29(String id, String name, int count) {
        this.id = id;
        this.name = name;
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + count;
    }
}