import java.util.*;
import java.io.*;

public class J07020_HoaDon2 {
    static BufferedReader in;
    public static void main(String[] args) throws IOException {
        in = new BufferedReader(new FileReader("KH.in"));
        int T = Integer.parseInt(in.readLine());
        Map<String, KhachHang20> listKH = new HashMap<>();
        int id = 0;
        String ten, gt, sn, dc;
        while (id++ < T) {
            ten = in.readLine();
            gt = in.readLine();
            sn = in.readLine();
            dc = in.readLine();
            listKH.put(String.format("KH%03d", id), new KhachHang20(id, ten, gt, sn, dc));
        }
        in = new BufferedReader(new FileReader("MH.in"));
        T = Integer.parseInt(in.readLine());
        id = 0;
        Map<String, MatHang20> listMH = new HashMap<>();
        int pin, pout;
        while (id++ < T) {
            ten = in.readLine();
            gt = in.readLine();
            pin = Integer.parseInt(in.readLine());
            pout = Integer.parseInt(in.readLine());
            listMH.put(String.format("MH%03d", id), new MatHang20(id, ten, gt, pin, pout));
        }
        in = new BufferedReader(new FileReader("HD.in"));
        T = Integer.parseInt(in.readLine());
        HoaDon20 hd;
        String[] s;
        for (int i = 1; i <= T; ++i) {
            s = in.readLine().split("\\s+");
            hd = new HoaDon20(i, listKH.get(s[0]), listMH.get(s[1]), Integer.parseInt(s[2]));
            System.out.println(hd);
        }
    }
}

class HoaDon20 {
    private String maHD;
    private KhachHang20 khachHang;
    private MatHang20 matHang;
    private int count;

    public HoaDon20(int maHD, KhachHang20 khachHang, MatHang20 matHang, int count) {
        this.maHD = String.format("HD%03d", maHD);
        this.khachHang = khachHang;
        this.matHang = matHang;
        this.count = count;
    }

    long getGia() {
        return count * matHang.getPout();
    }

    @Override
    public String toString() {
        return maHD + " " + khachHang.getTen() + " " + khachHang.getDc()
               + " " + matHang.getTen() + " " + matHang.getDv()
               + " " + matHang.getPin() + " " + matHang.getPout()
               + " " + count + " " + getGia();
    }
}

class KhachHang20 {
    private String ma;
    private String ten;
    private String gt;
    private String sn;
    private String dc;

    public KhachHang20(int ma, String ten, String gt, String sn, String dc) {
        this.ma = String.format("KH%03d", ma);
        this.ten = ten;
        this.gt = gt;
        this.sn = sn;
        this.dc = dc;
    }

    public String getTen() {
        return ten;
    }

    public String getDc() {
        return dc;
    }
}

class MatHang20 {
    private String ma;
    private String ten;
    private String dv;
    private int pin;
    private int pout;

    public MatHang20(int ma, String ten, String dv, int pin, int pout) {
        this.ma = String.format("MH%03d", ma);
        this.ten = ten;
        this.dv = dv;
        this.pin = pin;
        this.pout = pout;
    }

    public String getTen() {
        return ten;
    }

    public String getDv() {
        return dv;
    }

    public int getPin() {
        return pin;
    }

    public int getPout() {
        return pout;
    }
}