import java.util.*;
import java.io.*;

public class J06008_TinhGioChuanChoTungGiangVien {
    public static void main(String[] args) throws IOException {
        J06008_Reader sc = new J06008_Reader();
        String inp;
        int n = Integer.parseInt(sc.nextLine());
        // nhap vao mon hoc
        HashMap<String, String> subject = new HashMap<>();
        while (n-- > 0) {
            inp = sc.nextLine();
            subject.put(inp.substring(0, 7), inp.substring(8));
        }
        // nhap vao giao vien
        int m = Integer.parseInt(sc.nextLine());
        ArrayList<Teacher8> list = new ArrayList<>();
        while (m-- > 0) {
            inp = sc.nextLine();
            list.add(new Teacher8(inp.substring(0, 4), inp.substring(5)));
        }
        // nhap vao thoi gian
        int k = Integer.parseInt(sc.nextLine());
        int i;
        Teacher8 gv;
        String[] in;
        while (k-- > 0) {
            in = sc.nextLine().split("\\s+");
            i = 0;
            do {
                gv = list.get(i);
            } while (!gv.getId().equals(in[0]) && i++ < list.size());
            gv.putSubject(in[1], Double.parseDouble(in[2]));
        }
        // tim va in ra giao vien
        in = sc.nextLine().split("\\s+");
        i = 0;
        do {
            gv = list.get(i);
        } while (!gv.getId().equals(in[0]) && i++ < list.size());
        System.out.println("Giang vien: " + gv.getName());
        gv.getListSubject().entrySet().forEach(e -> {
            System.out.println(subject.get(e.getKey()) + " " + e.getValue());
        });
        System.out.printf("Tong: %.2f", gv.getT());
    }
}

class Teacher8 {
    private String id;
    private String name;
    private double t;
    private LinkedHashMap<String, Double> listSubject;

    public Teacher8(String id, String name) {
        this.id = id;
        this.name = name;
        t = 0;
        listSubject = new LinkedHashMap<>();
    }

    public void putSubject(String id, Double t) {
        listSubject.put(id, t);
        this.t += t;
    }

    public LinkedHashMap<String, Double> getListSubject() {
        return listSubject;
    }

    public String getId() {
        return id;
    }

    public double getT() {
        return t;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name + " " + String.format("%.2f", t);
    }
}

class J06008_Reader {
    BufferedReader br;
    StringTokenizer st;

    public J06008_Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}