import java.util.*;
import java.io.*;

/**
 *
 * @author Donald
 */
public class J07029_SoNguyenToLonNhatTrongFile {

    public static void main(String[] args) throws Exception {
        boolean[] isPrime = new boolean[1000005];
        Arrays.fill(isPrime, true);
        isPrime[0] = isPrime[1] = false;
        //
        for (int i = 2; i*i <= 1000000; i++)
            if (isPrime[i])
                for (int j = i * i; j <= 1000000; j += i)
                    isPrime[j] = false;
        //
        ObjectInputStream in = new ObjectInputStream(new FileInputStream("DATA.in"));
        ArrayList<Integer> songuyen = (ArrayList<Integer>) in.readObject();
        Map<Integer, Integer> map = new HashMap<>();
        songuyen.forEach(i -> {
            if (isPrime[i])
                if (map.containsKey(i)) map.put(i, map.get(i) + 1);
                else map.put(i, 1);
        });
        int cnt = 0;
        Iterator<Map.Entry<Integer, Integer>> iterator;
        for (iterator = map.entrySet().stream().sorted((k1, k2) -> k2.getKey() - k1.getKey()).iterator();
                iterator.hasNext() && cnt < 10; cnt++) {
            Map.Entry<Integer, Integer> next = iterator.next();
            System.out.println(next.getKey() + " " + next.getValue());
        }
    }

}
