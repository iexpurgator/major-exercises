import java.io.*;
import java.util.*;

public class J07019_HoaDon1 {
    static BufferedReader in;
    public static void main(String[] args) throws IOException, FileNotFoundException {
        in = new BufferedReader(new FileReader("DATA1.in"));
        int T = Integer.parseInt(in.readLine());
        Map<String, QuanAo19> map = new HashMap<>();
        String id, name;
        long p1, p2;
        while (T-- > 0) {
            id = in.readLine();
            name = in.readLine();
            p1 = Integer.parseInt(in.readLine());
            p2 = Integer.parseInt(in.readLine());
            map.put(id, new QuanAo19(id, name, p1, p2));
        }
        in = new BufferedReader(new FileReader("DATA2.in"));
        T = Integer.parseInt(in.readLine());
        int serial = 0;
        String[] inp;
        HoaDon19 hd;
        while (serial++ < T) {
            inp = in.readLine().split("\\s+");
            id = inp[0].substring(0, 2);
            hd = new HoaDon19(serial, inp[0], Integer.parseInt(inp[1]), map.get(id));
            System.out.println(hd);
        }
    }
}

class HoaDon19 {
    private String code;
    private int count;
    private int classPo;
    private QuanAo19 type;

    HoaDon19(int serial, String code, int count, QuanAo19 type) {
        this.code = code + "-" + String.format("%03d", serial);
        this.classPo = Integer.parseInt(code.substring(2));
        this.count = count;
        this.type = type;
    }

    @Override
    public String toString() {
        return code + " " + type.getName() + " " + type.getDiscound(classPo, count) + " " + type.getTotal(classPo, count);
    }
}

class QuanAo19 {
    private String id;
    private String name;
    private long[] p;

    public QuanAo19(String id, String name, long p1, long p2) {
        p = new long[2];
        this.id = id;
        this.name = name;
        this.p[0] = p1;
        this.p[1] = p2;
    }

    public String getName() {
        return name;
    }

    public long getDiscound(int kind, int count) {
        double dis;
        if (count > 149)
            dis = 0.5;
        else if (count > 99)
            dis = 0.3;
        else if (count > 49)
            dis = 0.15;
        else
            dis = 0;
        return Math.round(p[kind - 1] * count * dis);
    }

    public long getTotal(int kind, int count) {
        long dis = getDiscound(kind, count);
        return (p[kind - 1] * count) - dis;
    }

    public String getCode(int kind, int serial) {
        return String.format("%s%d-%03d", id, kind, serial);
    }
}
