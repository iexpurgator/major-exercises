import java.util.*;
import java.io.*;

public class J01026_SoChinhPhuong {
    public static void main(String[] args) throws IOException {
        J01026_Reader sc = new J01026_Reader();
        int T = Integer.parseInt(sc.nextLine());
        int m, sqrtm;
        while (T-- > 0) {
            m = Integer.parseInt(sc.next());
            sqrtm = (int) Math.sqrt(m);
            if(sqrtm * sqrtm == m) {
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }
        }
    }
}

class J01026_Reader {
    BufferedReader br;
    StringTokenizer st;

    public J01026_Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}
