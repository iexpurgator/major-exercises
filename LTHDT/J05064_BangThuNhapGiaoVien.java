import java.util.*;

public class J05064_BangThuNhapGiaoVien {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        int i = 0;
        ArrayList<GV64> list = new ArrayList<>();
        int cntHT = 0;
        int cntHP = 0;
        while (i++ < T) {
            String id = sc.nextLine();
            String name = sc.nextLine();
            long tien = Long.parseLong(sc.nextLine());
            if (id.substring(0, 2).equals("HT")) {
                cntHT++;
                if (cntHT < 2)
                    list.add(new GV64(id, name, tien));
            } else if (id.substring(0, 2).equals("HP")) {
                cntHP++;
                if (cntHP < 3)
                    list.add(new GV64(id, name, tien));
            } else
                list.add(new GV64(id, name, tien));
        }
        list.forEach(System.out::println);
    }
}

class GV64 {
    private String id;
    private String name;
    private long tien;
    private int level, phucap;
    private long thunhap;

    public GV64(String id, String name, long tien) {
        this.id = id;
        this.name = name;
        this.tien = tien;
        calc();
    }

    private void calc() {
        level = Integer.parseInt(id.substring(2));
        String lv = id.substring(0, 2);
        if (lv.equals("HT"))
            phucap = 2000000;
        else if (lv.equals("HP"))
            phucap = 900000;
        else if (lv.equals("GV"))
            phucap = 500000;
        else
            phucap = 0;
        thunhap = (tien * level) + phucap;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + level + " " + phucap + " " + thunhap;
    }
}