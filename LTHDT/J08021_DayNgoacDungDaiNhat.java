import java.util.*;

/**
 *
 * @author Donald
 */
public class J08021_DayNgoacDungDaiNhat {

    static Scanner sc;

    public static void main(String[] args) {
        sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        String s;
        Stack<Integer> st;
        long res;
        while (T-- > 0) {
            res = 0;
            s = sc.nextLine();
            st = new Stack<>();
            st.push(-1);
            for (int i = 0; i < s.length(); i++) {
                if(s.charAt(i) == '(') st.push(i);
                else {
                    st.pop();
                    if(st.isEmpty()) st.push(i);
                    else res = Math.max(res, i - st.peek());
                }
            }
            System.out.println(res);
        }
    }
}
/*
3
((()
)()())
()(()))))

*/
