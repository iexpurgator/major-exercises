import java.util.Scanner;

public class J03026_XauKhacNhauDaiNhat {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        while (T > 0) {
            String a = sc.nextLine();
            String b = sc.nextLine();
            if (a.equals(b) == true)
                System.out.println(-1);
            else
                System.out.println(Math.max(a.length(), b.length()));
            T -= 1;
        }
    }
}
