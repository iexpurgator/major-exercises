import java.util.*;

/**
 *
 * @author hoan
 */
public class J08025_QuayHinhVuong {

    static Scanner sc;

    public static void main(String[] args) {
        sc = new Scanner(System.in);
        int T = sc.nextInt();
        int[] arr;
        BoxRectangle og, res, cur;
        Queue<BoxRectangle> q;
        while (T-- > 0) {
            q = new LinkedList<>();
            arr = new int[6];
            for (int i = 0; i < 6; i++) {
                arr[i] = sc.nextInt();
            }
            og = new BoxRectangle(arr, 0);
            q.add(og);
            //
            arr = new int[6];
            for (int i = 0; i < 6; i++) {
                arr[i] = sc.nextInt();
            }
            res = new BoxRectangle(arr, 0);
            while (true) {
                cur = q.poll();
                if (cur.check(res)) {
                    System.out.println(cur.getStep());
                    break;
                }
                q.add(BoxRectangle.rotageLeft(cur));
                q.add(BoxRectangle.rotageRight(cur));
            }
        }
    }
}

/*
1
1 2 3 4 5 6
4 1 2 6 5 3

 */

class BoxRectangle {

    private int[] r;
    private int step;

    public BoxRectangle(int[] r, int step) {
        this.r = r;
        this.step = step;
    }

    public static BoxRectangle rotageRight(BoxRectangle cur) {
        int[] r = new int[6];
        r[0] = cur.r[0];
        r[1] = cur.r[4];
        r[2] = cur.r[1];
        r[3] = cur.r[3];
        r[4] = cur.r[5];
        r[5] = cur.r[2];
        return new BoxRectangle(r, cur.step + 1);
    }

    public static BoxRectangle rotageLeft(BoxRectangle cur) {
        int[] r = new int[6];
        r[0] = cur.r[3];
        r[1] = cur.r[0];
        r[2] = cur.r[2];
        r[3] = cur.r[4];
        r[4] = cur.r[1];
        r[5] = cur.r[5];
        return new BoxRectangle(r, cur.step + 1);
    }

    public boolean check(BoxRectangle res) {
        for (int i = 0; i < 6; ++i) {
            if (this.r[i] != res.r[i]) {
                return false;
            }
        }
        return true;
    }

    public int getStep() {
        return step;
    }
    
    public void print() {
        for (int i = 0; i < 6; i++) {
            System.out.print(r[i] + " ");
        } System.out.println("| " + step);
    }
}
