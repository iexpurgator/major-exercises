import java.util.*;
import java.io.*;

public class J03040_BienSoDep {
    // static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        J03040_Reader sc = new J03040_Reader();
        int T = Integer.parseInt(sc.nextLine());
        BienSo bs;
        while (T-- > 0) {
            bs = new BienSo(sc.nextLine());
            if (bs.isInc() || bs.isTheSame() || bs.isPartSame() || bs.isLocPhat()) {
                System.out.println("YES");
            } else
                System.out.println("NO");
        }
    }
}

class BienSo {
    int tinh;
    char[] kv = new char[2];
    int[] so = new int[5];

    public BienSo(String inp) {
        this.tinh = Integer.parseInt(inp.substring(0, 2));
        this.kv[0] = inp.charAt(2);
        this.kv[1] = inp.charAt(3);
        this.so[0] = inp.charAt(5) - '0';
        this.so[1] = inp.charAt(6) - '0';
        this.so[2] = inp.charAt(7) - '0';
        this.so[3] = inp.charAt(9) - '0';
        this.so[4] = inp.charAt(10) - '0';
    }

    public boolean isInc() {
        for (int i = 1; i < 5; i++) {
            if (so[i] - so[i - 1] <= 0)
                return false;
        }
        return true;
    }

    public boolean isTheSame() {
        for (int i = 1; i < 5; i++) {
            if (so[i - 1] != so[i])
                return false;
        }
        return true;
    }

    public boolean isPartSame() {
        return so[0] == so[1] && so[1] == so[2] && so[3] == so[4];
    }

    public boolean isLocPhat() {
        for (int i = 0; i < 5; i++) {
            if (so[i] != 6 && so[i] != 8)
                return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return tinh + "" + kv[0] + "" + kv[1] + "-" + so[0] + "" + so[1] + "" + so[2] + "." + so[3] + "" + so[4];
    }
}

class J03040_Reader {
    BufferedReader br;
    StringTokenizer st;

    public J03040_Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}
