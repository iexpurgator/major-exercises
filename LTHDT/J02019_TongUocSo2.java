import java.util.Arrays;
import java.util.Scanner;

public class J02019_TongUocSo2 { // Abundant Number
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int i, j;
        int abLimit = 1000000;
        int[] xp = new int[abLimit+1];
        Arrays.fill(xp, 0);
        for (i = 2; i <= abLimit; i++) {
            for (j = i * 2; j <= abLimit; j += i)
                xp[j] += i;
        }
        int a, b;
        a = Integer.parseInt(sc.next());
        b = Integer.parseInt(sc.next());
        int cnt = 0;
        for (i = a; i <= b; i++)
            if (xp[i] > i)
                cnt++;
        System.out.println(cnt);
    }
}