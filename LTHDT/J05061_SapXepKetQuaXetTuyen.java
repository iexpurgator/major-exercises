import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class J05061_SapXepKetQuaXetTuyen {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        List<PH61> list = new ArrayList<>();
        while (id++ < T) {
            String name, date;
            double score1, score2;
            name = sc.nextLine();
            date = sc.nextLine();
            score1 = Double.parseDouble(sc.nextLine());
            score2 = Double.parseDouble(sc.nextLine());
            list.add(new PH61(id, name, date, score1, score2));
        }
        Collections.sort(list, new Comparator<PH61>() {
            @Override
            public int compare(PH61 o1, PH61 o2) {
                if (o1.getAvg() == o2.getAvg()) {
                    return o1.getId().compareTo(o2.getId());
                } else {
                    return o2.getAvg() - o1.getAvg();
                }
            }
        });
        list.forEach(System.out::println);
    }
}

class PH61 {
    private String id;
    private String name;
    private int avg, age;

    public PH61(int id, String name, String date, double score1, double score2) {
        this.id = String.format("PH%02d", id);
        this.name = name;
        this.age = 2021 - Integer.parseInt(date.split("/")[2]);
        double avg = (score1 + score2) / 2;
        if (score1 >= 8 && score2 >= 8) {
            avg += 1;
        } else if (score1 >= 7.5 && score2 >= 7.5) {
            avg += 0.5;
        }
        this.avg = (int) Math.round(avg);
        if (this.avg > 10)
            this.avg = 10;
    }

    public String XepLoai() {
        if (avg >= 9) {
            return "Xuat sac";
        } else if (avg == 8) {
            return "Gioi";
        } else if (avg == 7) {
            return "Kha";
        } else if (avg >= 5) {
            return "Trung binh";
        } else {
            return "Truot";
        }
    }

    public int getAvg() {
        return avg;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + age + " " + avg + " " + XepLoai();
    }
}
