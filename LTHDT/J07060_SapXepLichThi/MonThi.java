package J07060_SapXepLichThi;

public class MonThi implements Comparable<MonThi> {
    private String ma;
    private String ten;
    private String hinhThuc;

    public MonThi(String ma, String ten, String hinhThuc) {
        this.ma = ma;
        this.ten = ten;
        this.hinhThuc = hinhThuc;
    }

    public String getTen() {
        return ten;
    }

    @Override
    public String toString() {
        return ma + " " + ten + " " + hinhThuc;
    }

    @Override
    public int compareTo(MonThi o) {
        return ma.compareTo(o.ma);
    }
}
