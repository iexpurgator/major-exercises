package J07060_SapXepLichThi;

public class LichThi implements Comparable<LichThi>{
    private CaThi ca;
    private MonThi mon;
    private String nhom;
    private int soLuongSV;

    public LichThi(CaThi ca, MonThi mon, String nhom, int soLuongSV) {
        this.ca = ca;
        this.mon = mon;
        this.nhom = nhom;
        this.soLuongSV = soLuongSV;
    }

    @Override
    public String toString() {
        return ca + " " + mon.getTen() + " " + nhom + " " + soLuongSV;
    }

    @Override
    public int compareTo(LichThi o) {
        return mon.compareTo(o.mon);
    }
}
