package J07060_SapXepLichThi;

import java.util.*;
import java.io.*;

/**
 * @author Donald
 */
public class SapXepLichThi {

    static Scanner sc;

    public static void main(String[] args) throws Exception {
//        sc = new Scanner(new FileInputStream("in"));
        int T;
        //
        Scanner sc1 = new Scanner(new FileInputStream("MONTHI.in"));
        T = Integer.parseInt(sc1.nextLine());
        Map<String, MonThi> mapMonThi = new HashMap<>();
        while (T-- > 0) {
            String ma, ten, mon;
            ma = sc1.nextLine();
            ten = sc1.nextLine();
            mon = sc1.nextLine();
            mapMonThi.put(ma, new MonThi(ma, ten, mon));
        }
        //
        Scanner sc2 = new Scanner(new FileInputStream("CATHI.in"));
        T = Integer.parseInt(sc2.nextLine());
        Map<String, CaThi> mapCaThi = new HashMap<>();
        int maCaThi = 1;
        while (T-- > 0) {
            String ngay, gio, phong;
            ngay = sc2.nextLine();
            gio = sc2.nextLine();
            phong = sc2.nextLine();
            mapCaThi.put(String.format("C%03d", maCaThi), new CaThi(maCaThi, ngay, gio, phong));
            maCaThi++;
        }
        //
        Scanner sc3 = new Scanner(new FileInputStream("LICHTHI.in"));
        T = Integer.parseInt(sc3.nextLine());
        ArrayList <LichThi> dsLichThi = new ArrayList<>();
        String[] inpLine;
        while (T-- > 0){
            inpLine = sc3.nextLine().split("\\s+");
            final String masCaThi = inpLine[0];
            final String masMonThi = inpLine[1];
            dsLichThi.add(new LichThi(mapCaThi.get(masCaThi), mapMonThi.get(masMonThi), inpLine[2], Integer.parseInt(inpLine[3])));
        }
//        Collections.sort(dsLichThi);
        for (LichThi lichThi : dsLichThi) System.out.println(lichThi);
    }
}