package J07060_SapXepLichThi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CaThi implements Comparable<CaThi> {
    private String ma;
    private Date ngay_gio;
    private String phong;

    public CaThi(int ma, String ngay, String gio, String phong) {
        this.ma = String.format("C%03d", ma);
        try {
            this.ngay_gio = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(ngay + " " + gio);
        } catch (ParseException e) {
        }
        this.phong = phong;
    }

    @Override
    public String toString() {
//        return ma + " " + new SimpleDateFormat("dd/MM/yyyy HH:mm").format(ngay_gio) + " " + phong;
        return new SimpleDateFormat("dd/MM/yyyy HH:mm").format(ngay_gio) + " " + phong;
    }

    @Override
    public int compareTo(CaThi o) {
        if (ngay_gio.equals(o.ngay_gio)) return ma.compareTo(o.ma);
        else return ngay_gio.compareTo(o.ngay_gio);
    }
}
