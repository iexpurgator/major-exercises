import java.util.*;

public class J05021_SapXepTheoMaSinhVien {
    static Scanner sc;
    public static void main(String[] args) {
        List<SV21> list = new ArrayList<>();
        sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            String id, name, iclass, email;
            id = sc.nextLine();
            name = sc.nextLine();
            iclass = sc.nextLine();
            email = sc.nextLine();
            list.add(new SV21(id, name, iclass, email));
        }
        Collections.sort(list, new Comparator<SV21>() {
            @Override
            public int compare(SV21 t1, SV21 t2) {
                return t1.getId().compareTo(t2.getId());
            }
        });
        list.stream().forEach(System.out::println);
    }
}

class SV21 {

    private String id;
    private String name;
    private String iclass;
    private String email;

    public SV21() {
    }

    public SV21(String id, String name, String iclass, String email) {
        this.id = id;
        this.name = name;
        this.iclass = iclass;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public String getIclass() {
        return iclass;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + iclass + " " + email;
    }
}
