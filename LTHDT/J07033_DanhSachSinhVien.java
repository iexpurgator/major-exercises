import java.io.*;
import java.util.*;

/**
 *
 * @author Donald
 */
public class J07033_DanhSachSinhVien {

    static Scanner in;

    public static void main(String[] args) throws Exception {
        in = new Scanner(new FileInputStream("SINHVIEN.in"));
        int T = Integer.parseInt(in.nextLine());
        String name, msv, email, lop;
        String[] fmname;
        ArrayList<SinhVien> list = new ArrayList<>();
        while (T-- > 0) {
            name = "";
            msv = in.nextLine();
            fmname = in.nextLine().toLowerCase().trim().split("\\s+");
            for (String string : fmname)
                name += Character.toUpperCase(string.charAt(0)) + string.substring(1) + " ";
            lop = in.nextLine();
            email = in.nextLine();
            list.add(new SinhVien(msv, name, lop, email));
        }
        Collections.sort(list, (o1, o2) -> {
            return o1.getMsv().compareTo(o2.getMsv());
        });
        list.forEach(System.out::println);
    }
}

class SinhVien {

    private String msv, name, lop, email;

    public SinhVien(String msv, String name, String lop, String email) {
        this.msv = msv;
        this.name = name;
        this.lop = lop;
        this.email = email;
    }

    public String getMsv() {
        return msv;
    }

    @Override
    public String toString() {
        return msv + " " + name + "" + lop + " " + email;
    }

}
