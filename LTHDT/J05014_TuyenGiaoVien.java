import java.util.*;

/**
 *
 * @author Hoan
 */
public class J05014_TuyenGiaoVien {
    static Scanner sc;

    public static void main(String[] args) {
        List<GiaoVien14> list = new ArrayList<>();
        sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        while (id++ < T) {
            String name;
            String ma;
            double diemTin, diemChMon;
            name = sc.nextLine();
            ma = sc.nextLine();
            diemTin = Double.parseDouble(sc.nextLine());
            diemChMon = Double.parseDouble(sc.nextLine());
            list.add(new GiaoVien14(id, name, ma, diemTin, diemChMon));
        }
        Collections.sort(list, new Comparator<GiaoVien14>(){
            @Override
            public int compare(GiaoVien14 t, GiaoVien14 t1) {
                return (int) (t1.getDiem()*10 - t.getDiem()*10);
            }
        });
        list.forEach(System.out::println);
    }
}

class GiaoVien14{
    private String id;
    private String name;
    private String ma;
    private double diemTin;
    private double diemChMon;

    public GiaoVien14() {
    }

    public GiaoVien14(int id, String name, String ma, double diemTin, double diemChMon) {
        this.id = String.format("GV%02d", id);
        this.name = name;
        this.ma = ma;
        this.diemTin = diemTin;
        this.diemChMon = diemChMon;
    }

    public double getDiem(){
        double diemP;
        int maUT = (int)ma.charAt(1) - (int)'0';
        switch(maUT){
            case 1:
                diemP = 2.0;
                break;
            case 2:
                diemP = 1.5;
                break;
            case 3:
                diemP = 1.0;
                break;
            default:
                diemP = 0.0;
                break;
        }
        return diemTin * 2 + diemChMon + diemP;
    }
    
    private String xl(){
        String sh = "TRUNG TUYEN";
        double diem = getDiem();
        if (diem < 18){
            sh = "LOAI";
        }
        String maMon = "" + ma.charAt(0);
        String chuyenMon = "";
        switch(maMon){
            case"A":
                chuyenMon = "TOAN";
                break;
            case"B":
                chuyenMon = "LY";
                break;
            case"C":
                chuyenMon = "HOA";
                break;
        }
        return String.format("%s %.1f %s",chuyenMon, diem, sh);
    }
    
    @Override
    public String toString() {
        return id + " " + name + " " + xl();
    }
}