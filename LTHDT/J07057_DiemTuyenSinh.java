import java.util.*;
import java.io.*;

/**
 * @author Donald
 */
public class DiemTuyenSinh {

    static Scanner sc;

    public static void main(String[] args) throws Exception {
//        sc = new Scanner(new FileInputStream("src/T24112021/THISINH_DH.in"));
        sc = new Scanner(new FileInputStream("THISINH.in"));
        int T = Integer.parseInt(sc.nextLine());
        ArrayList<ThiSinhDH> listTS = new ArrayList<>();
        String ten, dantoc;
        int kv;
        float diem;
        for (int i = 1; i <= T; i++) {
            ten = sc.nextLine();
            diem = Float.parseFloat(sc.nextLine());
            dantoc = sc.nextLine();
            kv = Integer.parseInt(sc.nextLine());
            listTS.add(new ThiSinhDH(i, ten, dantoc, diem, kv));
        }
        Collections.sort(listTS);
        listTS.forEach(System.out::println);
    }
}

class ThiSinhDH implements Comparable<ThiSinhDH> {
    private String ma, ten, danToc;
    private int khuVuc;
    private float diem;

    public ThiSinhDH(int ma, String ten, String danToc, float diem, int khuVuc) {
        this.ma = String.format("TS%02d", ma);
        this.ten = capitalize(ten);
        this.danToc = danToc;
        this.diem = diem;
        this.khuVuc = khuVuc;
    }

    private String capitalize(String string) {
        String[] split = string.toLowerCase().trim().split("\\s+");
        String capitalized = "";
        for (String s : split) {
            capitalized += Character.toUpperCase(s.charAt(0)) + s.substring(1) + " ";
        }
        return capitalized.trim();
    }

    public float getDiem() {
        float cong = 0;
        if (!danToc.equals("Kinh")) cong = 1.5f;
        if (khuVuc == 1) cong += 1.5f;
        else if (khuVuc == 2) cong += 1;
        return diem + cong;
    }

    @Override
    public String toString() {
        return ma + " " + ten + " " + getDiem() + " " + (getDiem() >= 20.5 ? "Do" : "Truot");
    }

    @Override
    public int compareTo(ThiSinhDH o) {
        if (o.getDiem() == getDiem()) return ma.compareTo(o.ma);
        else return (int) ((o.getDiem() * 100) - (getDiem() * 100));
    }
}