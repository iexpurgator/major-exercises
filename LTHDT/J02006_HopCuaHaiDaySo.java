import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

public class J02006_HopCuaHaiDaySo {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int n = sc.nextInt();
        int m = sc.nextInt();
        int k;
        List<Integer> a = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            k = sc.nextInt();
            if (!a.contains(k))
                a.add(k);
        }
        for (int i = 0; i < m; i++) {
            k = sc.nextInt();
            if (!a.contains(k)) {
                a.add(k);
            }
        }
        a.stream().sorted((i, j) -> (i - j)).forEach(i -> System.out.printf("%d ", i));
    }
}
