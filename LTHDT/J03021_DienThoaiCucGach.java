import java.util.Scanner;

public class J03021_DienThoaiCucGach {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        String c = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String n = "22233344455566677778889999";
        int T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            String in = sc.nextLine().toUpperCase();
            String ou = "";
            for (int i = 0; i < in.length(); i++) {
                ou += n.charAt(c.indexOf(in.charAt(i)));
            }
            System.out.println(isPalindrome(ou) ? "YES" : "NO");
        }
    }

    static boolean isPalindrome(String str) {
        int n = str.length();
        for (int i = 0; i < n / 2; ++i) {
            if (str.charAt(i) != str.charAt(n - i - 1))
                return false;
        }
        return true;
    }
}
