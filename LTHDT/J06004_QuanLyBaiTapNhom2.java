import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class J06004_QuanLyBaiTapNhom2 {
    static Scanner sc;
    public static void main(String[] args) {
        List<WG04> list = new ArrayList<>();
        sc = new Scanner(System.in);
        String[] ip = sc.nextLine().split("\\s+");
        int n, m;
        n = Integer.parseInt(ip[0]);
        m = Integer.parseInt(ip[1]);
        int i;
        while (n-- > 0) {
            String id, name, phone;
            id = sc.nextLine();
            name = sc.nextLine();
            phone = sc.nextLine();
            i = Integer.parseInt(sc.nextLine());
            list.add(new WG04(i, id, name, phone));
        }
        String[] detai = new String[m];
        for (int j = 0; j < m; j++) {
            detai[j] = sc.nextLine();
        }
        Collections.sort(list, new Comparator<WG04>() {
            @Override
            public int compare(WG04 t1, WG04 t2) {
                return t1.getId().compareTo(t2.getId());
            }
        });
        String dt = "";
        for (WG04 g : list) {
            for (int j = 0; j < m; j++) {
                dt = detai[g.getStt() - 1];
            }
            System.out.println(g + " " + dt);
        }
    }
}

class WG04 {
    private int stt;
    private String id;
    private String name;
    private String phone;

    public WG04() {
    }

    public WG04(int stt, String id, String name, String phone) {
        this.stt = stt;
        this.id = id;
        this.name = name;
        this.phone = phone;
    }

    public String getId() {
        return id;
    }

    public int getStt() {
        return stt;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + phone + " " + stt;
    }
}
