import java.util.Scanner;

public class J02029_SapXepDoiCho_LietKeNguoc {
    private static Scanner sc = new Scanner(System.in);
    static final String st = "Buoc ";
    
    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        while (T > 0) {
            int n = Integer.parseInt(sc.nextLine());
            int step = 1;
            String res = "";
            String[] in = sc.nextLine().split("\\s");
            int[] a = new int[n];
            for (int i = 0; i < n; ++i) {
                a[i] = Integer.parseInt(in[i]);
            }
    
            for (int i = 0; i < n - 1; ++i) {
                for (int j = i + 1; j < n; j++) {
                    if (a[j] < a[i]) {
                        int tmp = a[j];
                        a[j] = a[i];
                        a[i] = tmp;
                    }
                }
                res = show(step++, n, a) + res;
            }
            System.out.print(res);
            T -= 1;
        }
    }

    private static String show(int i, int n, int[] a) {
        String s = st + i + ":";
        for (int j = 0; j < n; ++j) {
            s += " " + a[j];
        }
        s += "\n";
        return s;
    }
}
