import java.util.Scanner;

public class J04005_KhaiBaoLopThiSinh {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        String hoTen = sc.nextLine();
        String Date = sc.nextLine();
        double s1, s2, s3;
        s1 = Double.parseDouble(sc.nextLine());
        s2 = Double.parseDouble(sc.nextLine());
        s3 = Double.parseDouble(sc.nextLine());
        ThiSinh ts = new ThiSinh(hoTen, Date, s1, s2, s3);
        System.out.println(ts);
    }
}

class ThiSinh {
    private String hoTen;
    private String Date;
    private double s1, s2, s3, st;

    ThiSinh() {
    }

    ThiSinh(String hoTen, String Date, double s1, double s2, double s3) {
        this.hoTen = hoTen;
        this.Date = Date;
        this.s1 = s1;
        this.s2 = s2;
        this.s3 = s3;
    }

    @Override
    public String toString() {
        this.st = this.s1 + this.s2 + this.s3;
        return (this.hoTen + " " + this.Date + " " + this.st);
    }
}
