import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.io.*;

/**
 * @author Donald
 */
public class J07059_DanhSachCaThi {

    static Scanner sc;

    public static void main(String[] args) throws Exception {
//        sc = new Scanner(new FileInputStream("in"));
        sc = new Scanner(new FileInputStream("CATHI.in"));
        int T = Integer.parseInt(sc.nextLine());
        ArrayList<CaThi> dsCaThi = new ArrayList<>();
        int maCaThi = 1;
        while (T-- > 0) {
            String ngay, gio, phong;
            ngay = sc.nextLine();
            gio = sc.nextLine();
            phong = sc.nextLine();
            dsCaThi.add(new CaThi(maCaThi, ngay, gio, phong));
            maCaThi++;
        }
        Collections.sort(dsCaThi);
        dsCaThi.forEach(System.out::println);
    }
}

class CaThi implements Comparable<CaThi> {
    private String ma;
    private Date ngay_gio;
    private String phong;

    public CaThi(int ma, String ngay, String gio, String phong) {
        this.ma = String.format("C%03d", ma);
        try {
            this.ngay_gio = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(ngay + " " + gio);
        } catch (ParseException e) {
        }
        this.phong = phong;
    }

    @Override
    public String toString() {
        return ma + " " + new SimpleDateFormat("dd/MM/yyyy HH:mm").format(ngay_gio) + " " + phong;
    }

    @Override
    public int compareTo(CaThi o) {
        if (ngay_gio.equals(o.ngay_gio)) return ma.compareTo(o.ma);
        else return ngay_gio.compareTo(o.ngay_gio);
    }
}