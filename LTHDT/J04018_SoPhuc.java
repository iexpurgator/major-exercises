import java.util.Scanner;

public class J04018_SoPhuc {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        String[] s = new String[4];
        while (id++ < T) {
            s = sc.nextLine().split("\\s+");
            ComplexNumber A = new ComplexNumber(Integer.parseInt(s[0]), Integer.parseInt(s[1]));
            ComplexNumber B = new ComplexNumber(Integer.parseInt(s[2]), Integer.parseInt(s[3]));
            ComplexNumber C = ComplexNumber.add(A, B).mul(A);
            ComplexNumber D = ComplexNumber.sqr(ComplexNumber.add(A, B));
            System.out.println(C + ", " + D);
        }
    }
}

class ComplexNumber {
    private int a, b;

    ComplexNumber() {
    }

    public ComplexNumber(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public static ComplexNumber add(ComplexNumber c1, ComplexNumber c2) {
        return c1.add(c2);
    }

    public static ComplexNumber mul(ComplexNumber c1, ComplexNumber c2) {
        return c1.mul(c2);
    }

    public static ComplexNumber sqr(ComplexNumber c) {
        return ComplexNumber.mul(c, c);
    }

    public ComplexNumber add(ComplexNumber c) {
        int a = this.a + c.a;
        int b = this.b + c.b;
        return new ComplexNumber(a, b);
    }

    public ComplexNumber mul(ComplexNumber c) {
        int a = (this.a * c.a) - (this.b * c.b);
        int b = (this.a * c.b) + (this.b * c.a);
        return new ComplexNumber(a, b);
    }

    @Override
    public String toString() {
        if (b > 0)
            return a + " + " + b + "i";
        else
            return a + " - " + Math.abs(b) + "i";
    }
}