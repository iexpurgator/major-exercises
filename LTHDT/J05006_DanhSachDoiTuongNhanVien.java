import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class J05006_DanhSachDoiTuongNhanVien {
    public static void main(String[] args) {
        List<NhanVien1> list = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        int i = 0;
        while (i++ < T) {
            String Name = sc.nextLine();
            String Sex = sc.nextLine();
            String Date = sc.nextLine();
            String AddrHome = sc.nextLine();
            String Phone = sc.nextLine();
            String SignDate = sc.nextLine();
            list.add(new NhanVien1(i, Name, Sex, Date, AddrHome, Phone, SignDate));
        }
        sc.close();
        list.stream().forEach(System.out::println);
    }
}

class NhanVien1 {
    private String ID;
    private String Name;
    private String Sex;
    private String Date;
    private String AddrHome;
    private String Phone;
    private String SignDate;

    NhanVien1() {
    }

    public NhanVien1(int i, String name, String sex, String date, String addrHome, String phone, String signDate) {
        Name = name;
        Sex = sex;
        Date = date;
        AddrHome = addrHome;
        Phone = phone;
        SignDate = signDate;
        ID = String.format("%05d", i);
    }

    @Override
    public String toString() {
        return ID + " " + Name + " " + Sex + " " + Date + " " + AddrHome + " " + Phone + " " + SignDate;
    }
}