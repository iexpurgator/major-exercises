import java.math.BigInteger;
import java.util.Scanner;

public class J03012_TongSoNguyenLon1 {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        while (T > 0) {
            BigInteger a = new BigInteger(sc.next());
            BigInteger b = new BigInteger(sc.next());
            System.out.println(b.add(a));
            T -= 1;
        }
    }
}
