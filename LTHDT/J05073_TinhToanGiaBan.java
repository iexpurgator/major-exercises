import java.util.*;
import java.io.*;

public class J05073_TinhToanGiaBan {
    public static void main(String[] args) throws IOException {
        J05073_Reader sc = new J05073_Reader();
        int T = Integer.parseInt(sc.nextLine());
        String[] inp;
        TaxProfit tp;
        while (T-- > 0) {
            inp = sc.nextLine().split("\\s+");
            tp = new TaxProfit(inp[0], Integer.parseInt(inp[1]), Integer.parseInt(inp[2]));
            System.out.println(tp);
        }
    }
}

class TaxProfit {
    private String id;
    private int price;
    private int count;

    class TaxShip {
        double tax, ship;

        public TaxShip(double tax, double ship) {
            this.tax = tax;
            this.ship = ship;
        }
    }

    HashMap<Character, TaxShip> map = new HashMap<Character, TaxShip>() {
        {
            put('T', new TaxShip(0.29, 0.04));
            put('C', new TaxShip(0.1, 0.03));
            put('D', new TaxShip(0.08, 0.025));
            put('M', new TaxShip(0.02, 0.005));
        }
    };

    public TaxProfit(String id, int price, int count) {
        this.id = id;
        this.price = price;
        this.count = count;
    }

    public double toMoney() {
        double tax = map.get(id.charAt(0)).tax;
        tax = ((id.charAt(3) == 'C') ? tax * 0.95 : tax);
        double ship = map.get(id.charAt(0)).ship;
        double moneyIn = price * count;
        double money = moneyIn + (moneyIn * tax) + (moneyIn * ship);
        long lMoney = Math.round(((money / (float) count) * 1.2) * 100);
        return lMoney / 100.0;
    }

    @Override
    public String toString() {
        return id + " " + String.format("%.02f", toMoney());
    }
}

class J05073_Reader {
    BufferedReader br;
    StringTokenizer st;

    public J05073_Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}
