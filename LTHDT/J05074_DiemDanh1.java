import java.util.*;
import java.io.*;

public class J05074_DiemDanh1 {
    // static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        J05074_Reader sc = new J05074_Reader();
        int T = Integer.parseInt(sc.nextLine());
        int i = 0;
        ArrayList<DiemDanhSV1> list = new ArrayList<>();
        String id, name, idClass;
        while (i++ < T) {
            id = sc.nextLine();
            name = sc.nextLine();
            idClass = sc.nextLine();
            list.add(new DiemDanhSV1(id, name, idClass));
        }
        i = 0;
        DiemDanhSV1 temp;
        String[] inp;
        while (i++ < T) {
            inp = sc.nextLine().split("\\s+");
            int j = 0;
            do {
                temp = list.get(j);
            } while (!temp.getId().equals(inp[0]) && j++ < T);
            temp.setCheck(inp[1]);
        }
        list.forEach(System.out::println);
    }
}

class DiemDanhSV1 {
    private String id;
    private String name;
    private String idClass;
    private int score;
    private String check;
    private String note;

    public DiemDanhSV1(String id, String name, String idClass) {
        this.id = id;
        this.name = name;
        this.idClass = idClass;
        score = 10;
        note = "";
    }

    public void setCheck(String check) {
        this.check = check;
        calc();
    }

    private void calc() {
        for (int i = 0; i < check.length(); i++) {
            if (check.charAt(i) == 'm')
                score--;
            else if (check.charAt(i) == 'v')
                score -= 2;
        }
        if (score <= 0)
            note = "KDDK";
    }

    public int getScore() {
        if (score <= 0)
            return 0;
        return score;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + idClass + " " + getScore() + " " + note;
    }
}

class J05074_Reader {
    BufferedReader br;
    StringTokenizer st;

    public J05074_Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}
