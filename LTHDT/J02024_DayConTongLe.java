import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class J02024_DayConTongLe {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            int n = Integer.parseInt(sc.next());
            Integer[] a = new Integer[n];
            for (int i = 0; i < n; i++) {
                a[i] = Integer.parseInt(sc.next());
            }
            Arrays.sort(a, Collections.reverseOrder());
            ArrayList<Integer> path = new ArrayList<>();
            printSubsequences(a, 0, 0, path);
        }
    }

    static void printSubsequences(Integer[] arr, int sum, int index, ArrayList<Integer> path) {
        if (index == arr.length) {
            if (path.size() > 0 && sum % 2 == 1) {
                path.forEach(d -> {
                    System.out.print(d + " ");
                });
                System.out.println();
            }
        } else {
            printSubsequences(arr, sum, index + 1, path);
            path.add(arr[index]);
            printSubsequences(arr, sum + arr[index], index + 1, path);
            path.remove(path.size() - 1);
        }
    }
}
