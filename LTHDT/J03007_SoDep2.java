import java.util.Scanner;

public class J03007_SoDep2 {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        while (T > 0) {
            char[] s = sc.nextLine().toCharArray();
            int sum = 0;
            boolean perfect = (s[0] == '8' && s[s.length - 1] == '8');
            if (perfect) {
                for (int i = 0; i < s.length; i++) {
                    if (s[i] != s[s.length - i - 1]) {
                        perfect = false;
                    }
                    sum += (int) (s[i] - '0');
                }
            }
            if (perfect && sum % 10 == 0)
                System.out.println("YES");
            else
                System.out.println("NO");
            T -= 1;
        }
    }
}
