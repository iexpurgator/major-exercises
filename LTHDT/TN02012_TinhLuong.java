import java.util.*;
import java.io.*;

public class TN02012_TinhLuong {
    public static void main(String[] args) throws IOException {
        TN02012_Reader sc = new TN02012_Reader();
        int T = Integer.parseInt(sc.nextLine());
        ArrayList<TinhLuongTN2012> list = new ArrayList<>();
        HashMap<String, String> phongBan = new HashMap<>();
        final int[][] heso = { { 10, 12, 14, 20 }, { 10, 11, 13, 16 }, { 9, 10, 12, 14 }, { 8, 9, 11, 13 } };
        String p;
        while (T-- > 0) {
            p = sc.nextLine();
            phongBan.put(p.substring(0, 2), p.substring(3));
        }
        T = Integer.parseInt(sc.nextLine());
        String id, name;
        int base, count;
        TinhLuongTN2012 temp;
        while (T-- > 0) {
            id = sc.nextLine();
            name = sc.nextLine();
            base = Integer.parseInt(sc.nextLine());
            count = Integer.parseInt(sc.nextLine());
            temp = new TinhLuongTN2012(id, name, base, count);
            temp.setTenPhongBan(phongBan);
            temp.setHeso(heso);
            list.add(temp);
        }
        list.forEach(System.out::println);
    }
}

class TinhLuongTN2012 {
    private String id;
    private String name;
    private int base;
    private int count;
    private int heso;
    private String tenPhongBan;

    public TinhLuongTN2012(String id, String name, int base, int count) {
        this.id = id;
        this.name = name;
        this.base = base;
        this.count = count;
    }

    public void setHeso(int[][] heso) {
        int level = id.charAt(0) - 'A';
        int yrs = Integer.parseInt(id.substring(1, 3));
        int k;
        if (yrs < 4)
            k = 0;
        else if (yrs < 9)
            k = 1;
        else if (yrs < 16)
            k = 2;
        else
            k = 3;
        this.heso = heso[level][k];
    }

    public int getMoney() {
        return count * base * heso;
    }

    public void setTenPhongBan(HashMap<String, String> phongBan) {
        tenPhongBan = phongBan.get(id.substring(3));
    }

    public String getTenPhongBan() {
        return tenPhongBan;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + tenPhongBan + " " + getMoney() + "000";
    }
}

class TN02012_Reader {
    BufferedReader br;
    StringTokenizer st;

    public TN02012_Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}
