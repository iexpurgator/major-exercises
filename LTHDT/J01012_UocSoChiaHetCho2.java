import java.util.Scanner;

public class J01012_UocSoChiaHetCho2 {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        while (T > 0) {
            long a = Integer.parseInt(sc.nextLine());
            int cnt = 0, i;
            for (i = 1; i <= Math.sqrt(a); i++) {
                if (a % i == 0) {
                    if (i % 2 == 0) {
                        cnt++;
                    }
                    if ((a / i) % 2 == 0) {
                        cnt++;
                    }
                }
            }
            i--;
            if ((i * i == a) && (i % 2 == 0)) {
                cnt--;
            }
            System.out.println(cnt);
            T -= 1;
        }
    }
}
