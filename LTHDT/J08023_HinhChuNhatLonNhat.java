import java.util.*;

/**
 *
 * @author hoan
 */
public class J08023_HinhChuNhatLonNhat {

    static Scanner sc;

    public static void main(String[] args) {
        sc = new Scanner(System.in);
        int T = sc.nextInt();
        int[] a;
        int n, i;
        long res;
        Stack<Integer> s;
        while (T-- > 0) {
            s = new Stack<>();
            n = sc.nextInt();
            a = new int[n];
            for (i = 0; i < n; i++) {
                a[i] = sc.nextInt();
            }
            res = 0L;
            for (i = 0; i < n;) {
                if (s.isEmpty() || a[s.peek()] <= a[i]) {
                    s.push(i++);
                } else {
                    res = update(s, a, i, res);
                }
            }
            while (!s.isEmpty()) {
                res = update(s, a, i, res);
            }
            System.out.println(res);
        }
    }

    static long update(Stack<Integer> s, int[] a, int i, long res) {
        int cur = s.pop();
        long area = (long) a[cur] * (long) (s.empty() ? i : i - s.peek() - 1);
        return Math.max(area, res);
    }
}
/*
2
7
6 2 5 4 5 1 6
3
2 2 2

*/
