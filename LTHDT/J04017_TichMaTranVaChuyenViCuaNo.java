import java.util.*;

public class J04017_TichMaTranVaChuyenViCuaNo {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            int n = sc.nextInt(), m = sc.nextInt();
            Matrix17 a = new Matrix17(n, m);
            a.nextMatrix(sc);
            Matrix17 b = a.trans();
            System.out.println(a.mul(b));
        }
    }
}

class Matrix17 {
    private int x, y;
    private int[][] m;

    public Matrix17(int x, int y, int[][] m) {
        this.x = x;
        this.y = y;
        this.m = m;
    }

    public Matrix17(int x, int y) {
        this.x = x;
        this.y = y;
        m = new int[x][y];
    }

    public void nextMatrix(Scanner sc) {
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                m[i][j] = sc.nextInt();
            }
        }
    }

    public Matrix17 trans() {
        int[][] m = new int[y][x];
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                m[j][i] = this.m[i][j];
            }
        }
        return new Matrix17(y, x, m);
    }

    public static Matrix17 mul(Matrix17 m1, Matrix17 m2) {
        int[][] m = new int[m1.x][m2.y];
        for (int i = 0; i < m1.x; i++) {
            for (int j = 0; j < m2.y; j++) {
                for (int k = 0; k < m1.y; k++) {
                    m[i][j] += m1.m[i][k] * m2.m[k][j];
                }
            }
        }
        return new Matrix17(m1.x, m2.y, m);
    }

    public Matrix17 mul(Matrix17 mtx) {
        return mul(this, mtx);
    }

    @Override
    public String toString() {
        String str = "";
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y - 1; j++) {
                str += m[i][j] + " ";
            }
            str += m[i][y - 1] + "\n";
        }
        return str;
    }
}