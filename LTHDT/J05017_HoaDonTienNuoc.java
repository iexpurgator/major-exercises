import java.util.*;

/**
 *
 * @author Hoan
 */
public class J05017_HoaDonTienNuoc {
    static Scanner sc;

    public static void main(String[] args) {
        List<KHDungNuoc> list = new ArrayList<>();
        sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        while (id++ < T) {
            String name;
            int valNew, valOld;
            name = sc.nextLine();
            valOld = Integer.parseInt(sc.nextLine());
            valNew = Integer.parseInt(sc.nextLine());
            list.add(new KHDungNuoc(id, name, valOld, valNew));
        }
        Collections.sort(list, new Comparator<KHDungNuoc>() {
            @Override
            public int compare(KHDungNuoc t, KHDungNuoc t1) {
                return t1.getDiff() - t.getDiff();
            }
        });
        list.forEach(System.out::println);
    }
}

class KHDungNuoc {

    private String id;
    private String name;
    private long valOld;
    private long valNew;
    private final Map<Integer, Double> map = new HashMap<Integer, Double>() {
        {
            put(100, 0.02);
            put(150, 0.03);
            put(200, 0.05);
        }
    };

    public KHDungNuoc() {
    }

    public KHDungNuoc(int id, String name, long valOld, long valNew) {
        this.id = String.format("KH%02d", id);
        this.name = name;
        this.valOld = valOld;
        this.valNew = valNew;
    }

    public int getDiff() {
        return (int) (valNew - valOld);
    }

    private int toMoney() {
        int used = getDiff();
        int donGia;
        int[] tien = new int[3];
        if (used > 100) {
            tien[0] = 50 * 100;
            tien[1] = 50 * 150;
            tien[2] = (used - 100) * 200;
            donGia = 200;
        } else if (used > 50) {
            tien[0] = 50 * 100;
            tien[1] = (used - 50) * 150;
            tien[2] = 0;
            donGia = 150;
        } else {
            tien[0] = used * 100;
            tien[1] = tien[2] = 0;
            donGia = 100;
        }

        int tt = tien[0] + tien[1] + tien[2];
        return tt + (int) Math.round(tt * map.get(donGia));
    }

    @Override
    public String toString() {
        return id + " " + name + " " + toMoney();
    }
}
