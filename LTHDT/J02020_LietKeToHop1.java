import java.util.Scanner;

public class J02020_LietKeToHop1 {
    private static Scanner sc = new Scanner(System.in);
    static int cnt = 0;

    public static void main(String[] args) {
        int n, k;
        String[] in = sc.nextLine().split("\\s");
        n = Integer.parseInt(in[0]);
        k = Integer.parseInt(in[1]);
        int[] a = new int[n + 1];
        for (int i = 1; i <= k; ++i) {
            a[i] = i;
        }
        gen_com(a, n, k);
    }

    private static void gen_com(int[] a, int n, int k) {
        cnt++;
        for (int j = 1; j <= k; ++j) {
            System.out.print(a[j] + " ");
        }
        int i = k;
        while (i > 0 && a[i] >= n + i - k)
            i--;
        if (i == 0) {
            System.out.println("\nTong cong co " + cnt + " to hop");
            return;
        }
        System.out.println();
        a[i]++;
        for (int j = i + 1; j <= n; ++j) {
            a[j] = a[i] + j - i;
        }
        gen_com(a, n, k);
    }
}
