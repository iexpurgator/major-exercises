import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.io.*;

public class J07015_SoNguyenToTrongFileNhiPhan {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        boolean[] isPrime = new boolean[10005];
        Arrays.fill(isPrime, true);
        isPrime[0] = isPrime[1] = false;
        for (int i = 2; i < isPrime.length; ++i) {
            if (isPrime[i]) {
                for (int j = i * i; j < isPrime.length; j += i)
                    isPrime[j] = false;
            }
        }
        ObjectInputStream in = new ObjectInputStream(new FileInputStream("SONGUYEN.in"));
        ArrayList<Integer> songuyen = (ArrayList<Integer>) in.readObject();
        songuyen.stream().filter(e -> isPrime[e])
        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
        .entrySet().stream()
        .sorted(Map.Entry.comparingByKey())
        .forEach(e -> {
            System.out.println(e.getKey() + " " + e.getValue());
        });
    }
}
