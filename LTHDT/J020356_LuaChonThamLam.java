import java.util.*;
import java.io.*;

public class J020356_LuaChonThamLam {
    public static void main(String[] args) throws IOException {
        J020356_Reader sc = new J020356_Reader();
        int n = Integer.parseInt(sc.next());
        int s = Integer.parseInt(sc.next());
        find(n, s, "small");
        System.out.print(" ");
        find(n, s, "big");
        System.out.println();
    }

    static void find(int n, int s, String option) {
        if (s == 0) {
            System.out.print(n == 1 ? "0" : "-1");
            return;
        }
        if (s > 9 * n) {
            System.out.print("-1");
            return;
        }
        int[] res = new int[n];
        if (option == "small") {
            s -= 1;
            for (int i = n - 1; i > 0; i--) {
                if (s > 9) {
                    res[i] = 9;
                    s -= 9;
                } else {
                    res[i] = s;
                    s = 0;
                }
            }
            res[0] = s + 1;
        } else if (option == "big") {
            for (int i = 0; i < n; i++) {
                if (s >= 9) {
                    res[i] = 9;
                    s -= 9;
                } else {
                    res[i] = s;
                    s = 0;
                }
            }
        } else
            return;
        for (int i = 0; i < n; i++)
            System.out.print(res[i]);
    }
}

class J020356_Reader {
    BufferedReader br;
    StringTokenizer st;

    public J020356_Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}
