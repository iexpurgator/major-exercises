import java.util.*;
import java.io.*;
import java.util.stream.Collectors;

/**
 *
 * @author Donald
 */
public class J07030_CapSoNguyenToTrongFile1 {

    public static void main(String[] args) throws Exception {
        ObjectInputStream sc;
        sc = new ObjectInputStream(new FileInputStream("DATA1.in"));
        ArrayList<Integer> N = (ArrayList<Integer>) sc.readObject();
        sc.close();
        sc = new ObjectInputStream(new FileInputStream("DATA2.in"));
        ArrayList<Integer> M = (ArrayList<Integer>) sc.readObject();
        sc.close();
        //
        boolean[] isPrime = new boolean[1000005];
        Arrays.fill(isPrime, true);
        isPrime[0] = isPrime[1] = false;
        for (int i = 2; i * i <= 1000000; i++)
            if (isPrime[i])
                for (int j = i * i; j <= 1000000; j += i)
                    isPrime[j] = false;
        //
        HashSet<Integer> setPrime2 = new HashSet<>(M);
        // remove duplicate in arraylist
        N = (ArrayList<Integer>) N.stream().sorted().distinct().collect(Collectors.toList());
        for (Iterator<Integer> iterator = N.iterator(); iterator.hasNext();) {
            Integer key = iterator.next();
            int value = 1000000 - key;
            if (value > key && isPrime[key] && isPrime[value] && setPrime2.contains(value))
                System.out.println(key + " " + value);
        }
    }
}
