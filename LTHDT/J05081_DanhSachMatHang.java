import java.util.*;
import java.io.*;

public class J05081_DanhSachMatHang {
    public static void main(String[] args) throws IOException {
        J05081_Reader sc = new J05081_Reader();
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        ArrayList<MatHang81> list = new ArrayList<>();
        String name, type;
        int in, out;
        while (id++ < T) {
            name = sc.nextLine();
            type = sc.nextLine();
            in = Integer.parseInt(sc.nextLine());
            out = Integer.parseInt(sc.nextLine());
            list.add(new MatHang81(id, name, type, in, out));
        }
        list.sort((e1, e2) -> {
            if (e1.getMoney() == e2.getMoney()) {
                return e1.getId().compareTo(e2.getId());
            } else return e2.getMoney() - e1.getMoney();
        });
        list.forEach(System.out::println);
    }
}

class MatHang81 {
    private String id;
    private String name;
    private String type;
    private int in;
    private int out;

    MatHang81(int id, String name, String type, int in, int out) {
        this.id = String.format("MH%03d", id);
        this.name = name;
        this.type = type;
        this.in = in;
        this.out = out;
    }

    int getMoney() {
        return out - in;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + type + " " + in + " " + out + " " + getMoney();
    }
}

class J05081_Reader {
    BufferedReader br;
    StringTokenizer st;

    public J05081_Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}
