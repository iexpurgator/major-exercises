import java.util.*;

public class J02033_DaoDau {
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        int n, k;
        n = sc.nextInt();
        k = sc.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++)
            a[i] = sc.nextInt();
        Arrays.sort(a);
        for (int i = 0; i < n && k > 0; i++) {
            if (a[i] < 0) {
                a[i] *= -1;
                k--;
            }
            else break;
        }
        Arrays.sort(a);
        long sum = 0;
        for (int i : a) sum += i;
        sum -= solve(a, n, k);
        System.out.println(sum);
    }

    static int solve(int[] a, int n, int k){
        if (k == 0 || a[0] == 0 || k % 2 == 0) {
            return 0;
        } else {
            return a[0]*2;
        }
    }
}