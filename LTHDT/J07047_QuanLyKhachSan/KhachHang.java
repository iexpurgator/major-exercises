package J07047_QuanLyKhachSan;

import java.text.*;
import java.util.*;

/**
 *
 * @author Donald
 */
public class KhachHang implements Comparable<KhachHang>{

    private String id;
    private String name;
    private String roomId;
    private Date checkIn, checkOut;
    
    public KhachHang(int id, String name, String roomId, String checkIn, String checkOut) {
        this.id = String.format("KH%02d", id);
        this.name = name;
        this.roomId = roomId;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            this.checkIn = sdf.parse(checkIn);
            this.checkOut = sdf.parse(checkOut);
        } catch (ParseException e) {
        }
    }
    
    public int diffDay(){
        // SimpleDateFormat d = new SimpleDateFormat("dd/MM/yyyy");
        long timecnt = (checkOut.getTime() - checkIn.getTime()) / (1000 * 60 * 60 * 24);
        return (int) timecnt;
    }

    public String getRoomId() {
        return roomId;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + roomId + " " + diffDay();
    }

    @Override
    public int compareTo(KhachHang o) {
        return o.diffDay() - diffDay();
    }
}
