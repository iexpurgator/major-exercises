package J07047_QuanLyKhachSan;

/**
 *
 * @author Donald
 */
public class QuanLy implements Comparable<QuanLy> {

    private KhachHang kh;
    private LoaiPhong phong;
    private int dayCnt;

    public QuanLy(KhachHang kh, LoaiPhong phong) {
        this.kh = kh;
        this.phong = phong;
        dayCnt = kh.diffDay();
        if (dayCnt == 0) dayCnt++;
    }

    private double discout() {
        if (dayCnt >= 30) return 0.06;
        else if (dayCnt >= 20) return 0.04;
        else if (dayCnt >= 10) return 0.02;
        else return 0;
    }

    public double thanhTien() {
        double tienPhong = phong.getPrice() * dayCnt;
        double tienDv = phong.getCost() * tienPhong;
        double tienKm = (tienPhong + tienDv) * discout();
        int tong = (int) Math.round((tienPhong + tienDv - tienKm)*100);
        return tong / 100.0;
    }

    @Override
    public String toString() {
        return kh + " " + String.format("%.02f", thanhTien());
    }

    @Override
    public int compareTo(QuanLy o) {
        return o.kh.diffDay() - kh.diffDay();
    }
}
