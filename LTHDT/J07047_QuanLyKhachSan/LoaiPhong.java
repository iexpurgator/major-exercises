package J07047_QuanLyKhachSan;

/**
 *
 * @author Donald
 */
public class LoaiPhong implements Comparable<LoaiPhong> {

    private String type;
    private String name;
    private int price;
    private double cost;

    public LoaiPhong(String input) {
        String[] in = input.split("\\s+");
        type = in[0];
        name = in[1];
        price = Integer.parseInt(in[2]);
        cost = Double.parseDouble(in[3]);
    }

    public LoaiPhong(String type, String name, int price, double cost) {
        this.type = type;
        this.name = name;
        this.price = price;
        this.cost = cost;
    }

    public String getType() {
        return type;
    }

    public int getPrice() {
        return price;
    }

    public double getCost() {
        return cost;
    }

    @Override
    public String toString() {
        return type + " " + name + " " + price + " " + cost;
    }

    @Override
    public int compareTo(LoaiPhong o) {
        return name.compareTo(o.name);
    }
}
