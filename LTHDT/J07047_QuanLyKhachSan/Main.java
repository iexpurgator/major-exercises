package J07047_QuanLyKhachSan;

import java.io.*;
import java.util.*;

/**
 *
 * @author Donald
 */
public class Main {

    static Scanner sc;

    public static void main(String[] args) throws Exception {
        sc = new Scanner(new FileInputStream("DATA.in"));
        // Phong
        HashMap<String, LoaiPhong> dsPhong = new HashMap<>();
        LoaiPhong tmp_LoaiPhong;
        int T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            tmp_LoaiPhong = new LoaiPhong(sc.nextLine());
            dsPhong.put(tmp_LoaiPhong.getType(), tmp_LoaiPhong);
        }
        // Khach
        T = Integer.parseInt(sc.nextLine());
        int id = 0;
        ArrayList<QuanLy> qlKhachSan = new ArrayList<>();
        KhachHang tmp_KhachHang;
        String name, roomId, checkIn, checkOut;
        while (id++ < T) {
            name = sc.nextLine();
            roomId = sc.nextLine();
            checkIn = sc.nextLine();
            checkOut = sc.nextLine();
            tmp_LoaiPhong = dsPhong.get(roomId.substring(2, 3));
            tmp_KhachHang = new KhachHang(id, name, roomId, checkIn, checkOut);
            qlKhachSan.add(new QuanLy(tmp_KhachHang, tmp_LoaiPhong));
        }
        Collections.sort(qlKhachSan);
        qlKhachSan.forEach(System.out::println);
    }
}
