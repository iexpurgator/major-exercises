import java.util.*;
import java.io.*;

/**
 *
 * @author Donald
 */
public class J07024_HieuCuaHaiTapTu {

//    public static void main(String[] args) {}
    public static void main(String[] args) throws IOException {
        WordSet s1 = new WordSet("DATA1.in");
        WordSet s2 = new WordSet("DATA2.in");
        System.out.println(s1.difference(s2));
        System.out.println(s2.difference(s1));
    }
}

class WordSet {

    private TreeSet<String> list;

    public WordSet(String s) throws FileNotFoundException {
        Scanner sc = new Scanner(new FileInputStream(s));
        list = new TreeSet<>();
        String in;
        while (sc.hasNext()) {
            in = sc.next().toLowerCase();
            if (!list.contains(in)) {
                list.add(in);
            }
        }
    }

    public WordSet(TreeSet<String> l) {
        this.list = l;
    }

    public WordSet union(WordSet s) {
        TreeSet<String> word = new TreeSet<>();
        word.addAll(s.list);
        word.addAll(this.list);
        return new WordSet(word);
    }

    public WordSet intersection(WordSet s) {
        TreeSet<String> word = new TreeSet<>();
        word.addAll(s.list);
        word.retainAll(this.list);
        return new WordSet(word);
    }

    public WordSet difference(WordSet s) {
        TreeSet<String> curr = new TreeSet<>();
        TreeSet<String> word = this.intersection(s).list;
        curr.addAll(this.list);
        curr.removeAll(word);
        return new WordSet(curr);
    }

    @Override
    public String toString() {
        return list.toString().replace("[", "").replace("]", "").replace(",", "");
    }
}
