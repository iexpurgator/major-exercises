import java.util.*;
import java.io.*;

public class J07007_LietKeTuKhacNhau {
    public static void main(String[] args) throws IOException {
        WordSet ws = new WordSet("VANBAN.in");
        System.out.println(ws);
    }
}

class WordSet {
    TreeSet<String> set;

    WordSet(String file) {
        set = new TreeSet<>();
        BufferedReader br ;
        try {
            br = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            return;
        }
        String[] aLine;
        while (true) {
            try {
                aLine = br.readLine().toLowerCase().split("\\s+");
                for (String string : aLine) {
                    if(string.isEmpty()) continue;
                    set.add(string);
                }
            } catch (IOException e) {
            } catch (NullPointerException e) {
                break;
            }
        }
    }

    @Override
    public String toString() {
        return set.toString().replace("[", "").replace("]", "").replaceAll(", ", "\n");
    }
}
