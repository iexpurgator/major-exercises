import java.io.*;

public class J07005_SoKhacNhauTrongFile2 {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        DataInputStream dataInStream = new DataInputStream(new FileInputStream("DATA.IN"));
        int[] cnt = new int[1001];
        try {
            while (true) {
                cnt[dataInStream.readInt()]++;
            }
        } catch (EOFException e) {
            for (int i = 0; i < 1001; i++) {
                if (cnt[i] > 0) {
                    System.out.println(i + " " + cnt[i]);
                }
            }
            dataInStream.close();
        }
    }
}