import java.util.*;

public class J05048_BangTheoDoiNhapXuatHang {
    static Scanner sc;
    public static void main(String[] args) {
        sc = new Scanner(System.in);
        List<HangHoa48> list = new ArrayList<>();
        int T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            String id = sc.nextLine();
            int count = Integer.parseInt(sc.nextLine());
            list.add(new HangHoa48(id, count));
        }
        list.forEach(System.out::println);
    }
}

class HangHoa48 {
    private String id;
    private long count;
    private final Map<String, MaHang> map = new HashMap<String, MaHang>() {{
        put("AY", new MaHang(0.6, 110000, 0.08));
        put("AN", new MaHang(0.6, 135000, 0.11));
        put("BY", new MaHang(0.7, 110000, 0.17));
        put("BN", new MaHang(0.7, 135000, 0.22));
    }};
    private long pout;
    private long price;
    private long cost;
    private long va;

    public HangHoa48() {
    }

    public HangHoa48(String id, long count) {
        this.id = id;
        this.count = count;
        analysis();
    }

    private void analysis() {
        String code = id.charAt(0) + "" + id.charAt(4);
        MaHang maHang = map.get(code);
        pout = Math.round(maHang.getOut() * count);
        price = maHang.getPrice();
        cost = pout * price;
        va = Math.round(maHang.getVa() * cost);
    }

    @Override
    public String toString() {
        return id + " " + count + " " + pout + " " + price + " " + cost + " " + va;
    }
}

class MaHang {
    private final double out;
    private final long price;
    private final double va;

    public MaHang(double out, long price, double va) {
        this.out = out;
        this.price = price;
        this.va = va;
    }

    public double getOut() {
        return out;
    }

    public long getPrice() {
        return price;
    }

    public double getVa() {
        return va;
    }
}