import java.util.*;

/**
 *
 * @author Hoan
 */
public class J05020_SapXepSinhVienTheoLop {
    static Scanner sc;
    
    public static void main(String[] args) {
        List<SV20> list = new ArrayList<>();
        sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            String id, name, iclass, email;
            id = sc.nextLine();
            name = sc.nextLine();
            iclass = sc.nextLine();
            email = sc.nextLine();
            list.add(new SV20(id, name, iclass, email));
        }
        Collections.sort(list, new Comparator<SV20>() {
            @Override
            public int compare(SV20 t1, SV20 t2) {
                if (t1.getIclass().equals(t2.getIclass())) {
                    return t1.getId().compareTo(t2.getId());
                } else {
                    return t1.getIclass().compareTo(t2.getIclass());
                }
            }
        });
        list.stream().forEach(System.out::println);
    }
}

class SV20 {

    private String id;
    private String name;
    private String iclass;
    private String email;

    public SV20() {
    }

    public SV20(String id, String name, String iclass, String email) {
        this.id = id;
        this.name = name;
        this.iclass = iclass;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public String getIclass() {
        return iclass;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + iclass + " " + email;
    }
}
