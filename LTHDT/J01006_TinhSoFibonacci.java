import java.util.Scanner;

public class J01006_TinhSoFibonacci {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        while (T > 0) {
            long a;
            a = Long.parseLong(sc.nextLine());
            System.out.println(Fib(a));
            T -= 1;
        }
    }

    private static long Fib(long a) {
        if (a == 0)
            return 1;
        long f1, f2, tm;
        f1 = f2 = 1;
        for (int i = 2; i < a; ++i) {
            tm = f1;
            f1 = f1 + f2;
            f2 = tm;
        }
        return f1;
    }
}
