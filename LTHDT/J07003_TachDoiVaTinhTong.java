import java.io.*;
import java.math.*;

public class J07003_TachDoiVaTinhTong {
    static BufferedReader sc;
    public static void main(String[] args) throws FileNotFoundException, IOException {
        sc = new BufferedReader(new FileReader("DATA.in"));
        String number = sc.readLine();
        int len = number.length(), i;
        BigInteger n1, n2;
        while(len != 1){
            i = len / 2;
            n1 = new BigInteger(number.substring(0, i));
            n2 = new BigInteger(number.substring(i));
            n1 = n1.add(n2);
            number = n1.toString();
            len = number.length();
            System.out.println(number);
        }
    }
}
