import java.util.Scanner;

public class J01010_CatDoi {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        while (id++ < T) {
            String s = sc.nextLine();
            String res = "";
            boolean valid = true;
            int zeroLength = 0;
            for (int i = 0; i < s.length() && valid; i++) {
                char c = s.charAt(i);
                if (c == '1'){
                    res += "1";
                } else if (c == '0' || c == '8' || c == '9'){
                    if(res != "" ) res += "0";
                    zeroLength ++;
                } else {
                    valid = false;
                }
            }
            if (zeroLength == s.length() || !valid){
                System.out.println("INVALID");
            } else {
                System.out.println(res);
            }
        }
    }
}
