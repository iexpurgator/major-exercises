import java.util.*;
import java.io.*;

/**
 *
 * @author Donald
 */
public class J07028_TinhGioChuan {

    static Scanner sc;

    public static void main(String[] args) throws Exception {
        String inp, in1;
        double t;
        int n, m;
        HashMap<String, String> subject;
        Teacher8[] list;

        sc = new Scanner(new FileInputStream("MONHOC.in"));
        n = Integer.parseInt(sc.nextLine());
        // nhap vao mon hoc
        subject = new HashMap<>();
        while (n-- > 0) {
            inp = sc.nextLine();
            subject.put(inp.substring(0, 7), inp.substring(8));
        }
        // nhap vao giao vien
        sc = new Scanner(new FileInputStream("GIANGVIEN.in"));
        m = Integer.parseInt(sc.nextLine());
        list = new Teacher8[m];
        for (int i = 0; i < m; i++) {
            inp = sc.nextLine();
            list[i] = new Teacher8(inp.substring(0, 4), inp.substring(5));
        }
        // nhap vao thoi gian
        sc = new Scanner(new FileInputStream("GIOCHUAN.in"));
        n = Integer.parseInt(sc.nextLine());
        while (n-- > 0) {
            inp = sc.next();
            in1 = sc.next();
            t = Double.parseDouble(sc.next());
            for (int i = 0; i < m; i++) {
                if (list[i].getId().equals(inp)) {
                    list[i].putSubject(in1, t);
                    break;
                }
            }
        }
        for (Teacher8 o : list) {
            System.out.println(o);
        }
    }
}

class Teacher8 {

    private String id;
    private String name;
    private double t;
    private LinkedHashMap<String, Double> listSubject;

    public Teacher8(String id, String name) {
        this.id = id;
        this.name = name;
        t = 0;
        listSubject = new LinkedHashMap<>();
    }

    public void putSubject(String id, Double t) {
        listSubject.put(id, t);
        this.t += t;
    }

    public LinkedHashMap<String, Double> getListSubject() {
        return listSubject;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return name + " " + String.format("%.2f", t);
    }
}
