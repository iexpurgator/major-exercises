import java.util.Map;
import java.util.HashMap;
import java.util.Scanner;

public class J02007_DemSoLanXuatHien {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();
        int t = 1;
        while (t <= T) {
            System.out.println("Test " + t + ":");
            int n = sc.nextInt();
            int[] arr = new int[n];
            int i = 0;
            for (i = 0; i < n; ++i) {
                arr[i] = sc.nextInt();
            }
            Map<Integer, Integer> mp = new HashMap<>();
            for (i = 0; i < n; i++) {
                mp.put(arr[i], mp.get(arr[i]) == null ? 1 : mp.get(arr[i]) + 1);
            }
            for (i = 0; i < n; i++) {
                if (mp.get(arr[i]) != -1) {
                    System.out.println(arr[i] + " xuat hien " + mp.get(arr[i]) + " lan");
                    mp.put(arr[i], -1);
                }
            }
            t++;
        }
        sc.close();
    }
}
