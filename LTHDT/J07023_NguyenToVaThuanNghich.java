import java.io.*;
import java.util.*;

/**
 *
 * @author Donald
 */
public class J07023_NguyenToVaThuanNghich {

    static ObjectInputStream in;

    public static void main(String[] args) throws Exception {
        ArrayList<Integer> prime1, prime2;
        boolean[] isPrime = new boolean[1005];
        Arrays.fill(isPrime, true);
        isPrime[0] = isPrime[1] = false;
        for (int i = 2; i < isPrime.length; i++) {
            if (isPrime[i]) {
                for (int j = i * i; j < isPrime.length; j += i)
                    isPrime[j] = false;
            }
        }
        
        for(int i = 13; i < isPrime.length; i+=2)
            if(isPrime[i]) isPrime[i] = isPalindrome(i);
        
        in = new ObjectInputStream(new FileInputStream("DATA1.in"));
        prime1 = (ArrayList<Integer>) in.readObject();

        in = new ObjectInputStream(new FileInputStream("DATA2.in"));
        prime2 = (ArrayList<Integer>) in.readObject();
        
        int cnt1, cnt2;
        for(int i = 2 ; i < 1000; ++i){
            if(isPrime[i]) {
                cnt1 = Collections.frequency(prime1, i);
                cnt2 = Collections.frequency(prime2, i);
                System.out.println(i + " " + cnt1 + " " + cnt2);
            }
        }
    }

    static boolean isPalindrome(int num) {
        String str = "" + num;
        int n = str.length();
        for (int i = 0; i < n / 2; ++i) {
            if (str.charAt(i) != str.charAt(n - i - 1))
                return false;
        }
        return true;
    }
}