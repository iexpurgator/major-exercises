import java.util.Scanner;

public class J01003_GiaiPhuongTrinhBacNhat {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        // int T = Integer.parseInt(sc.nextLine());
        int T = 1;
        while (T > 0) {
            double a, b;
            a = sc.nextInt();
            b = sc.nextInt();
            if (a == 0) {
                if (b == 0)
                    System.out.println("VSN");
                else
                    System.out.println("VN");
            } else {
                System.out.println(String.format("%.2f", -b / a));
            }
            T -= 1;
        }
    }
}