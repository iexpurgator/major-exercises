import java.util.Scanner;
import java.util.List;
import java.text.ParseException;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class J05055_XepHangVanDongVien1 {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) throws Exception {
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        List<VDV55> list = new ArrayList<>();
        while (id++ < T) {
            String name = sc.nextLine();
            String date = sc.nextLine();
            String timeStart = sc.nextLine();
            String timeEnd = sc.nextLine();
            list.add(new VDV55(id, name, date, timeStart, timeEnd));
        }
        list.sort((o1, o2) -> o1.getTime().compareTo(o2.getTime()));
        int lastRank = 1;
        list.get(0).rank = lastRank;
        for (int j = 1; j < list.size(); j++) {
            lastRank = list.get(j).getTime().equals(list.get(j - 1).getTime()) ? lastRank : j + 1;
            list.get(j).rank = lastRank;
        }
        list.sort((o1, o2) -> o1.getId().compareTo(o2.getId()));
        list.forEach(System.out::println);
    }
}

class VDV55 {
    private String id;
    private String name;
    private int age;
    private String timeReal;
    private String timeBonus;
    private String timeResult;
    int rank;

    public VDV55(int id, String name, String date, String timeStart, String timeEnd) throws ParseException {
        this.id = String.format("VDV%02d", id);
        this.name = name;
        this.age = 2021 - Integer.parseInt(date.split("/")[2]);
        this.timeReal = getDifference(timeStart, timeEnd);
        calcTimeBonus();
        this.timeResult = getDifference(timeBonus, timeReal);
    }

    private void calcTimeBonus() {
        if (age > 31) {
            timeBonus = "00:00:03";
        } else if (age > 24) {
            timeBonus = "00:00:02";
        } else if (age > 17) {
            timeBonus = "00:00:01";
        } else
            timeBonus = "00:00:00";
    }

    public String getTime() {
        return timeResult;
    }

    public String getId() {
        return id;
    }

    private static String getDifference(final String first, final String second) {
        LocalTime time1 = LocalTime.parse(first);
        LocalTime time2 = LocalTime.parse(second);
        long hours = ChronoUnit.HOURS.between(time1, time2);
        long minutes = ChronoUnit.MINUTES.between(time1, time2) % 60;
        long seconds = ChronoUnit.SECONDS.between(time1, time2) % 60;
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    @Override
    public String toString() {
        return id + " " + name + " " + timeReal + " " + timeBonus + " " + timeResult + " " + rank;
    }

}