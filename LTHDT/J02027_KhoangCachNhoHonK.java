import java.util.Arrays;
import java.util.Scanner;

public class J02027_KhoangCachNhoHonK { // https://ideone.com/a9N6yi
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            int n = sc.nextInt();
            long k = sc.nextLong();
            long[] a = new long[n];
            for (int i = 0; i < n; i++) {
                a[i] = sc.nextInt();
            }
            Arrays.sort(a);
            int cnt = 0;
            for (int i = 0; i < n - 1; i++) {
                int temp = LowerBound(a, n, a[i] + k);
                int c = temp - i - 1;
                if (c >= 1) {
                    cnt += c;
                }
            }
            System.out.println(cnt);
        }
    }

    static int LowerBound(long a[], int n, long x) {
        int l = -1, r = n;
        while (l < r - 1) {
            int m = (l + r) >>> 1;
            if (a[m] >= x)
                r = m;
            else
                l = m;
        }
        return r;
    }
}