import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class J05042_BangXepHang {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        List<BXH42> list = new ArrayList<>();
        String[] cnttt = new String[2];
        while (id++ < T) {
            String name;
            int count, total;
            name = sc.nextLine();
            cnttt = sc.nextLine().split("\\s+");
            count = Integer.parseInt(cnttt[0]);
            total = Integer.parseInt(cnttt[1]);
            list.add(new BXH42(name, count, total));
        }
        Collections.sort(list, new Comparator<BXH42>(){
            @Override
            public int compare(BXH42 o1, BXH42 o2) {
                if (o1.getCount() == o2.getCount() && o1.getTotal() != o2.getTotal()){
                    return o1.getTotal() - o2.getTotal();
                } else if (o1.getCount() == o2.getCount() && o1.getTotal() == o2.getTotal()){
                    return o1.getName().compareTo(o2.getName());
                } else {
                    return o2.getCount() - o1.getCount();
                }
            }
        });
        list.forEach(System.out::println);
    }
}

class BXH42 {
    private String name;
    private int count;
    private int total;

    public BXH42(String name, int count, int total) {
        this.name = name;
        this.count = count;
        this.total = total;
    }

    public String getName() {
        return name;
    }

    public int getCount() {
        return count;
    }

    public int getTotal() {
        return total;
    }

    @Override
    public String toString() {
        return name + " " + count + " " + total;
    }
}