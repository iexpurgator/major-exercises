import java.util.*;
import java.io.*;

/**
 *
 * @author Donald
 */
public class J07031_CapSoNguyenToTrongFile2 {

    public static void main(String[] args) throws Exception {
        ObjectInputStream sc;
        sc = new ObjectInputStream(new FileInputStream("DATA1.in"));
        ArrayList<Integer> N = (ArrayList<Integer>) sc.readObject();
        sc.close();
        sc = new ObjectInputStream(new FileInputStream("DATA2.in"));
        ArrayList<Integer> M = (ArrayList<Integer>) sc.readObject();
        sc.close();
        //
        boolean[] isPrime = new boolean[1000005];
        Arrays.fill(isPrime, true);
        isPrime[0] = isPrime[1] = false;
        for (int i = 2; i * i <= 1000000; i++)
            if (isPrime[i])
                for (int j = i * i; j <= 1000000; j += i)
                    isPrime[j] = false;
        //
        HashSet<Integer> setPrime2 = new HashSet<>(M);
        Collections.sort(N);
        LinkedHashSet<Integer> setPrime1 = new LinkedHashSet<>(N);
        for (Iterator<Integer> iterator = setPrime1.iterator(); iterator.hasNext();) {
            Integer key = iterator.next();
            int value = 1000000 - key;
            if (!setPrime2.contains(value) && !setPrime2.contains(key)
                    && value > key && isPrime[key] && isPrime[value] && setPrime1.contains(value))
                System.out.println(key + " " + value);
        }
    }
}
