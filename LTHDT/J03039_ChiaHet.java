import java.util.*;
import java.io.*;
import java.math.BigInteger;

public class J03039_ChiaHet {
    public static void main(String[] args) throws IOException {
        J03039_Reader sc = new J03039_Reader();
        int T = Integer.parseInt(sc.next());
        BigInteger a, b;
        while (T-- > 0) {
            a = new BigInteger(sc.next());
            b = new BigInteger(sc.next());
            if(a.mod(b).equals(BigInteger.ZERO) || b.mod(a).equals(BigInteger.ZERO)){
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }
        }
    }
}

class J03039_Reader {
    BufferedReader br;
    StringTokenizer st;

    public J03039_Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}
