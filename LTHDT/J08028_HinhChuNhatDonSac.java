import java.util.*;
import java.io.*;

public class J08028_HinhChuNhatDonSac {
    public static void main(String[] args) throws IOException {
        J08028_Reader sc = new J08028_Reader();
        int n = sc.nextInt();
        int m = sc.nextInt();
        int[] h = new int[(int) 1e6 + 7];
        for (int i = 1; i <= m; i++) h[i] = sc.nextInt();
        long r1 = solve(h, n, m);
        for (int i = 1; i <= m; i++) h[i] = n - h[i];
        long r2 = solve(h, n, m);
        System.out.println(r1 > r2 ? r1 : r2);
    }

    static long solve(int[] h, int n, int m) {
        long ans = 0;
        int d = 0;
        int[] pos_min, pos_max, st;
        pos_min = new int[(int) 1e6 + 7];
        pos_max = new int[(int) 1e6 + 7];
        st = new int[(int) 1e6 + 7];
        st[++d] = 0;
        for (int i = 1; i <= m; ++i) {
            while (d != 0 && h[st[d]] >= h[i]) --d;
            if (h[i] != 0) pos_min[i] = st[d] + 1;
            st[++d] = i;
        }
        d = 0;
        st[++d] = m + 1;
        for (int i = m; i >= 1; --i) {
            while (d != 0 && h[st[d]] >= h[i]) --d;
            if (h[i] != 0) pos_max[i] = st[d] - 1;
            st[++d] = i;
        }
        for (int i = 1; i <= m; ++i) {
            if (h[i] != 0) {
                long val = 1L * h[i] * (pos_max[i] - pos_min[i] + 1);
                if (val > ans) ans = val;
            }
        }
        return ans;
    }
}

class J08028_Reader {
    BufferedReader br;
    StringTokenizer st;

    public J08028_Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    int nextInt() {
        return Integer.parseInt(next());
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}
