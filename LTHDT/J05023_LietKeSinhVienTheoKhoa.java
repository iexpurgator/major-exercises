import java.util.*;

public class J05023_LietKeSinhVienTheoKhoa {
    static Scanner sc;
    public static void main(String[] args) {
        List<SV23> list = new ArrayList<>();
        sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            String id, name, iclass, email;
            id = sc.nextLine();
            name = sc.nextLine();
            iclass = sc.nextLine();
            email = sc.nextLine();
            list.add(new SV23(id, name, iclass, email));
        }
        T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            String nam = sc.nextLine();
            System.out.println("DANH SACH SINH VIEN KHOA " + nam + ':');
            list.stream().filter(e -> e.checkYear(nam) == 0).forEach(System.out::println);
        }
    }
}

class SV23 {

    private String id;
    private String name;
    private String iclass;
    private String email;

    public SV23() {
    }

    public SV23(String id, String name, String iclass, String email) {
        this.id = id;
        this.name = name;
        this.iclass = iclass;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public String getIclass() {
        return iclass;
    }

    public int checkYear(String year) {
        return iclass.substring(1, 3).compareTo(year.substring(2));
    }

    @Override
    public String toString() {
        return id + " " + name + " " + iclass + " " + email;
    }
}
