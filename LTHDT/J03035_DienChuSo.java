import java.util.*;
import java.io.*;

public class J03035_DienChuSo {
    public static void main(String[] args) {
        J03035_Reader sc = new J03035_Reader();
        int t = sc.nextInt();
        while (t-- > 0) {
            long dp[][][] = new long[11][10][2];
            char[] a = sc.next().toCharArray();
            char[] b = sc.next().toCharArray();
            for (int i = 0; i < a.length; ++i) {
                int cur = a[i];
                if (cur != '?') {
                    cur -= '0';
                    compute(i, cur, dp, b);
                } else {
                    if (i > 0) compute(i, 0, dp, b);
                    for (cur = 1; cur < 10; ++cur)
                        compute(i, cur, dp, b);
                }
            }
            long res = 0;
            for (int i = 0; i < 10; ++i) {
                res += dp[a.length - 1][i][1];
            }
            System.out.println(res);
        }
    }

    static void compute(int i, int cur, long[][][] dp, char[] b) {
        int curB = b[i] - '0';
        if (cur == curB) {
            dp[i][cur][0] = i > 0 ? dp[i - 1][b[i - 1] - '0'][0] : 1;
            if (i > 0) {
                for (int j = 0; j < 10; ++j)
                    dp[i][cur][1] += dp[i - 1][j][1];
            }
        } else if (cur > curB) {
            if (i > 0) {
                for (int j = 0; j < 10; ++j)
                    dp[i][j][1] += dp[i - 1][j][0] + dp[i - 1][j][1];
            } else
                dp[i][cur][1] = 1;
        } else {
            if (i > 0) {
                for (int j = 0; j < 10; ++j)
                    dp[i][cur][1] += dp[i - 1][j][1];
            }
        }
    }
}

class J03035_Reader {
    BufferedReader br;
    StringTokenizer st;

    public J03035_Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    int nextInt() {
        return Integer.parseInt(next());
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}
