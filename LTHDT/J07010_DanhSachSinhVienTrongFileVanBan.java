import java.util.*;
import java.io.*;

public class J07010_DanhSachSinhVienTrongFileVanBan {
    
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(new File("SV.in"));
        int T = Integer.parseInt(sc.nextLine());
        int i = 0;
        while (i++ < T) {
            String name = sc.nextLine();
            String iclass = sc.nextLine();
            String date = sc.nextLine();
            double GPA = Double.parseDouble(sc.nextLine());
            SinhVien1 sv = new SinhVien1(i, name, date, iclass, GPA);
            System.out.println(sv);
        }
    }
}

class SinhVien1 {
    private String id;
    private String name;
    private String date;
    private String iclass;
    private double GPA;

    SinhVien1() {
    }

    SinhVien1(int i, String name, String date, String iclass, double GPA) {
        this.id = "B20DCCN" + String.format("%03d", i);
        this.name = name;
        this.date = date;
        this.iclass = iclass;
        this.GPA = GPA;
    }

    @Override
    public String toString() {
        String[] arDate = date.split("/");
        date = String.format("%02d/%02d/%s", Integer.parseInt(arDate[0]), Integer.parseInt(arDate[1]), arDate[2]);
        return id + " " + name + " " + iclass + " " + date + " " + String.format("%.02f", GPA);
    }
}