import java.util.*;

public class J03028_MaHoaDRM {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        String s;
        while (T-- > 0) {
            s = sc.nextLine();
            System.out.println(new DRM(s).toString());
        }
    }
}

class DRM {
    private String res;
    private String d1, d2;

    public DRM(String s) {
        divide(s);
        d1 = rotate(d1);
        d2 = rotate(d2);
        res = merge(d1, d2);
    }

    private void divide(String s) {
        int len = s.length() / 2;
        d1 = s.substring(0, len);
        d2 = s.substring(len);
    }

    private String rotate(String s) {
        int sum = 0;
        char[] ch = new char[s.length()];
        for (int i = 0; i < s.length(); i++) {
            sum += s.charAt(i) - 'A';
        }
        for (int i = 0; i < s.length(); i++) {
            ch[i] = (char) (65 + ((s.charAt(i) - 'A' + sum) % 26));
        }
        return new String(ch);
    }

    private String merge(String d1, String d2) {
        char[] ch = new char[d1.length()];
        for (int i = 0; i < d2.length(); i++) {
            ch[i] = (char) (65 + ((d2.charAt(i) - 'A' + d1.charAt(i) - 'A') % 26));
        }
        return new String(ch);
    }

    @Override
    public String toString() {
        return res;
    }
}
