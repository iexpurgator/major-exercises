import java.util.*;

public class J01017_SoLienKe {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        while (id++ < T) {
            String num = sc.nextLine();
            System.out.println(check(num) ? "YES" : "NO");
        }
    }

    static boolean check(String num) {
        int a, b, c;
        for (int i = 1; i < num.length() - 1; i++) {
            a = num.charAt(i - 1) - '0';
            b = num.charAt(i) - '0';
            c = num.charAt(i + 1) - '0';
            if (Math.abs(b - a) != 1)
                return false;
            if (Math.abs(b - c) != 1)
                return false;
        }
        return true;
    }
}
