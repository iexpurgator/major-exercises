import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class J02025_DayConCoTongNguyenTo {
    static Scanner sc = new Scanner(System.in);
    static boolean[] isPrime = new boolean[5051];

    public static void main(String[] args) {
        sieve();
        int T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            int n = Integer.parseInt(sc.next());
            Integer[] a = new Integer[n];
            for (int i = 0; i < n; i++) {
                a[i] = Integer.parseInt(sc.next());
            }
            Arrays.sort(a, Collections.reverseOrder());
            ArrayList<Integer> path = new ArrayList<>();
            printSubsequences(a, 0, 0, path);
        }
    }

    static void printSubsequences(Integer[] arr, int sum, int index, ArrayList<Integer> path) {
        if (index == arr.length) {
            if (path.size() > 0 && isPrime[sum]) {
                path.forEach(d -> {
                    System.out.print(d + " ");
                });
                System.out.println();
            }
        } else {
            printSubsequences(arr, sum, index + 1, path);
            path.add(arr[index]);
            printSubsequences(arr, sum + arr[index], index + 1, path);
            path.remove(path.size() - 1);
        }
    }

    static void sieve(){
        Arrays.fill(isPrime, true);
        isPrime[0] = false;
        isPrime[1] = false;
        for (int i = 2; i*i < isPrime.length; i++) {
            if (isPrime[i]){
                for (int j = i*i; j < isPrime.length; j+=i) {
                    isPrime[j] = false;
                }
            }
        }
    }
}
