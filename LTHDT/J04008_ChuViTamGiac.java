import java.util.Scanner;

public class J04008_ChuViTamGiac {
    static Scanner sc;
    public static void main(String[] args) {
        // List<Integer> list = new ArrayList<>();
        sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            double x, y;
            x = sc.nextDouble();
            y = sc.nextDouble();
            Point8 p1 = new Point8(x, y);
            x = sc.nextDouble();
            y = sc.nextDouble();
            Point8 p2 = new Point8(x, y);
            x = sc.nextDouble();
            y = sc.nextDouble();
            Point8 p3 = new Point8(x, y);
            double d12, d13, d23;
            d12 = p1.distance(p2);
            d13 = p1.distance(p3);
            d23 = p3.distance(p2);
            if (d12 + d13 <= d23 || d12 + d23 <= d13 || d13 + d23 <= d12) {
                System.out.println("INVALID");
            } else
                System.out.printf("%.03f\n", d12+d13+d23);
        }
    }
}

class Point8 {
    private double x;
    private double y;

    public Point8() {
    }

    public Point8(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point8(Point8 p) {
        this.x = p.getX();
        this.y = p.getY();
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double distance(Point8 p) {
        return Math.sqrt(((x - p.getX()) * (x - p.getX())) + ((y - p.getY()) * (y - p.getY())));
    }

    public double distance(Point8 p1, Point8 p2) {
        return Math.sqrt(((p1.getX() - p2.getX()) * (p1.getX() - p2.getX()))
                + ((p1.getY() - p2.getY()) * (p1.getY() - p2.getY())));
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}