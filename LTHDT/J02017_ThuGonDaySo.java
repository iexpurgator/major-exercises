import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class J02017_ThuGonDaySo {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        List <Integer> arr = new ArrayList<>();
        for (int i = 0; i < T; i++) {
            arr.add(sc.nextInt());
        }
        int cnt = 0;
        do {
            cnt = 0;
            for (int j = 1; j < arr.size(); j++) {
                if((arr.get(j) + arr.get(j-1)) % 2 == 0) {
                    arr.remove(j-1);
                    arr.remove(j-1);
                    cnt++;
                }
            }
        } while (cnt != 0);
        System.out.println(arr.size());
    }
}
