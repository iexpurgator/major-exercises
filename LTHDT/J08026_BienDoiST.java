import java.util.*;

/**
 *
 * @author hoan
 */
public class J08026_BienDoiST {

    static Scanner sc;

    public static void main(String[] args) {
        sc = new Scanner(System.in);
        int T = sc.nextInt();
        int s, t, cnt;
        Queue<PairST> q;
        PairST u;
        boolean[] visited;
        while (T-- > 0) {
            q = new LinkedList<>();
            s = sc.nextInt();
            t = sc.nextInt();
            visited = new boolean[10005];
            q.add(new PairST(s, t, 0));
            visited[s] = true;
            while (!q.isEmpty() && !q.peek().found()) {
                u = q.poll();
                s = u.getS();
                cnt = u.getCount();
                if (s - 1 > 0 && !visited[s - 1]) {
                    q.add(new PairST(s - 1, t, cnt + 1));
                    visited[s - 1] = true;
                }
                if (s * 2 < t + 2 && !visited[s * 2]) {
                    q.add(new PairST(s * 2, t, cnt + 1));
                    visited[s * 2] = true;
                }
//                System.out.println(q);
            }
            System.out.println(q.peek().getCount());
        }
    }
}

/*
3
2 5
3 7
7 4

 */
class PairST {

    private int s;
    private int t;
    private int count;

    public PairST(int s, int t, int count) {
        this.s = s;
        this.t = t;
        this.count = count;
    }

    public int getS() {
        return s;
    }

    public int getCount() {
        return count;
    }

    public boolean found() {
        return s == t;
    }

    @Override
    public String toString() {
        return "{" + "s= " + s + ", t= " + t + ", count= " + count + '}';
    }
}
