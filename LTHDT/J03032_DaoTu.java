import java.util.*;

public class J03032_DaoTu {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        String[] inp;
        while (T-- > 0) {
            inp = sc.nextLine().split("\\s+");
            for (int i = 0; i < inp.length; ++i){
                StringBuilder s = new StringBuilder(inp[i]);
                System.out.print(s.reverse().toString() + " ");
            }
            System.out.println();
        }
    }
}
