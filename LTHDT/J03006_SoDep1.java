import java.util.Scanner;

public class J03006_SoDep1 {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        String odd = "13579";
        int T = Integer.parseInt(sc.nextLine());
        while (T > 0) {
            char[] s = sc.nextLine().toCharArray();
            boolean perfect = true;
            for (int i = 0; i < s.length; i++) {
                if (s[i] != s[s.length - i - 1] || odd.indexOf(s[i]) != -1) {
                    perfect = false;
                    break;
                }
            }
            if (perfect)
                System.out.println("YES");
            else
                System.out.println("NO");
            T -= 1;
        }
    }
}
