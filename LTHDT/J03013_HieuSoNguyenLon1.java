import java.math.BigInteger;
import java.util.Scanner;

public class J03013_HieuSoNguyenLon1 {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        while (T > 0) {
            BigInteger a = new BigInteger(sc.next());
            BigInteger b = new BigInteger(sc.next());
            int m = Math.max(a.toString().length(), b.toString().length());
            String sub = (b.compareTo(a) > 0) ? b.subtract(a).toString() : a.subtract(b).toString();
            while (sub.length() < m) {
                sub = "0" + sub;
            }
            System.out.println(sub);
            T -= 1;
        }
    }
}
