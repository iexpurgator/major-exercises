import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class J06003_QuanLyBaiTapNhom1 {
    static Scanner sc;
    public static void main(String[] args) {
        List<WG03> list = new ArrayList<>();
        sc = new Scanner(System.in);
        String[] ip = sc.nextLine().split("\\s+");
        int n, m;
        n = Integer.parseInt(ip[0]);
        m = Integer.parseInt(ip[1]);
        int i;
        while (n-- > 0) {
            String id, name, phone;
            id = sc.nextLine();
            name = sc.nextLine();
            phone = sc.nextLine();
            i = Integer.parseInt(sc.nextLine());
            list.add(new WG03(i, id, name, phone));
        }
        String[] detai = new String[m];
        for (int j = 0; j < m; j++) {
            detai[j] = sc.nextLine();
        }
        m = Integer.parseInt(sc.nextLine());
        while (m-- > 0) {
            int stt = Integer.parseInt(sc.nextLine());
            System.out.println("DANH SACH NHOM " + stt + ":");
            list.stream().filter(e -> (e.getStt() == stt)).forEach(System.out::println);
            System.out.println("Bai tap dang ky: " + detai[stt - 1]);
        }
    }
}

class WG03 {
    private int stt;
    private String id;
    private String name;
    private String phone;

    public WG03() {
    }

    public WG03(int stt, String id, String name, String phone) {
        this.stt = stt;
        this.id = id;
        this.name = name;
        this.phone = phone;
    }

    public int getStt() {
        return stt;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + phone + " ";
    }
}
