import java.util.*;
import java.io.*;

public class J06007_BangTinhGioChuan {
    public static void main(String[] args) throws IOException {
        J06007_Reader sc = new J06007_Reader();
        String inp;
        int n = Integer.parseInt(sc.nextLine());
        // HashMap<String, String> subject = new HashMap<>();
        while (n-- > 0) {
            inp = sc.nextLine();
            // subject.put(inp.substring(0, 7), inp.substring(5));
        }
        int m = Integer.parseInt(sc.nextLine());
        ArrayList<Teacher7> list = new ArrayList<>();
        while (m-- > 0) {
            inp = sc.nextLine();
            list.add(new Teacher7(inp.substring(0, 4), inp.substring(5)));
        }
        int k = Integer.parseInt(sc.nextLine());
        int i;
        Teacher7 gv;
        String[] in;
        while (k-- > 0) {
            in = sc.nextLine().split("\\s+");
            i = 0;
            do {
                gv = list.get(i);
            } while (!gv.getId().equals(in[0]) && i++ < list.size());
            gv.addT(Double.parseDouble(in[2]));
        }
        list.forEach(System.out::println);
    }
}

class Teacher7 {
    private String id;
    private String name;
    private double t;
    // private List<String> listSubject;

    public Teacher7(String id, String name) {
        this.id = id;
        this.name = name;
        t = 0;
    }

    public void addT(double t) {
        this.t += t;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return name + " " + String.format("%.2f", t);
    }
}

class J06007_Reader {
    BufferedReader br;
    StringTokenizer st;

    public J06007_Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}
