import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

public class J05010_SapXepDanhSachMatHang {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        List<MatHang> list = new ArrayList<>();
        while (id++ < T) {
            String name, group;
            double in, out;
            name = sc.nextLine();
            group = sc.nextLine();
            in = Double.parseDouble(sc.nextLine());
            out = Double.parseDouble(sc.nextLine());
            list.add(new MatHang(id, name, group, in, out));
        }
        list.sort((p1, p2) -> (int) ((p2.getM() - p1.getM()) * 100));
        list.forEach(System.out::println);
    }
}

class MatHang {
    private int id;
    private String name;
    private String group;
    private double in;
    private double out;

    public MatHang(int id, String name, String group, double in, double out) {
        this.id = id;
        this.name = name;
        this.group = group;
        this.in = in;
        this.out = out;
    }

    public double getM() {
        return out - in;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + group + " " + String.format("%.02f", out - in);
    }
}