import java.util.*;
import java.io.*;

public class J05070_CauLacBoBongDa2 {
    public static void main(String[] args) throws IOException {
        J05070_CauLacBoBongDa2Reader sc = new J05070_CauLacBoBongDa2Reader();
        int T = Integer.parseInt(sc.nextLine());
        HashMap<String, FC70> map = new HashMap<>();
        String id, name;
        int cost;
        while (T-- > 0) {
            id = sc.nextLine();
            name = sc.nextLine();
            cost = Integer.parseInt(sc.nextLine());
            map.put(id, new FC70(id, name, cost));
        }
        T = Integer.parseInt(sc.nextLine());
        int count;
        String[] inp;
        FC70 fc;
        ArrayList<Ticket70> list = new ArrayList<>();
        while (T-- > 0) {
            inp = sc.nextLine().split("\\s+");
            count = Integer.parseInt(inp[1]);
            fc = map.get(inp[0].substring(1, 3));
            list.add(new Ticket70(inp[0], fc.getName(), fc.getPrice(count)));
        }
        Collections.sort(list, (o1, o2) -> {
            if (o1.getPrice() == o2.getPrice())
                return o1.getName().compareTo(o2.getName());
            else
                return (int) (o2.getPrice() - o1.getPrice());
        });
        list.forEach(System.out::println);
    }
}

class FC70 {
    private String id;
    private String name;
    private int cost;

    public FC70(String id, String name, int cost) {
        this.id = id;
        this.name = name;
        this.cost = cost;
    }

    public String getName() {
        return name;
    }

    public long getPrice(int count) {
        return (long) cost * (long) count;
    }
}

class Ticket70 {
    private String code;
    private String name;
    private long price;

    public Ticket70(String code, String name, long price) {
        this.code = code;
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public long getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return code + " " + name + " " + price;
    }
}

class J05070_CauLacBoBongDa2Reader {
    BufferedReader br;
    StringTokenizer st;

    public J05070_CauLacBoBongDa2Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}