import java.io.*;

public class J07001_DocFileVanBan {
    
    public static void main(String[] args) throws FileNotFoundException, IOException {
        BufferedReader sc = new BufferedReader(new FileReader("DATA.in"));
        String line = sc.readLine();
        while (line != null) {
            System.out.println(line);
            line = sc.readLine();
        }
        sc.close();
    }
}