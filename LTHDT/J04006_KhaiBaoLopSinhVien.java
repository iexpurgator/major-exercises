import java.util.Scanner;

public class J04006_KhaiBaoLopSinhVien {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        String Name = sc.nextLine();
        String Lop = sc.nextLine();
        String Date = sc.nextLine();
        double GPA = Double.parseDouble(sc.nextLine());
        SinhVien6 sv = new SinhVien6(Name, Lop, Date, GPA);
        System.out.println(sv);
    }
}

class SinhVien6 {
    private String ID;
    private String Name;
    private String Lop;
    private String Date;
    private double GPA;

    SinhVien6() {
    }

    SinhVien6(String Name, String Lop, String Date, double GPA) {
        this.Name = Name;
        this.Lop = Lop;
        this.Date = Date;
        this.GPA = GPA;
        this.ID = "B20DCCN001";
    }

    public String getDate() {
        String[] s = Date.split("/");
        return String.format("%02d/%02d/%s", Integer.parseInt(s[0]), Integer.parseInt(s[1]), s[2]);
    }

    @Override
    public String toString() {
        // return String.format("%s %s %s %s %.02f", ID, "Nguyen Van A", Lop, getDate(), GPA); // submit on code_ptit
        return String.format("%s %s %s %s %.02f", ID, Name, Lop, getDate(), GPA);
    }
}