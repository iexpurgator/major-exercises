import java.text.DecimalFormat;
import java.util.Scanner;

public class J01005_ChiaTamGiac {
    private static Scanner sc = new Scanner(System.in);
    private static DecimalFormat df = new DecimalFormat("#.000000"); // test 1000 100000

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        while (T > 0) {
            String in = sc.nextLine();
            String[] inp = in.split(" ");
            double n = Double.parseDouble(inp[0]);
            double h = Double.parseDouble(inp[1]);
            double area = h / n;
            for (int i = 1; i < n; ++i) {
                double k = Math.sqrt((i * area) / h) * h;
                System.out.print(df.format(k));
                if (i + 1 < n)
                    System.out.print(' ');
                else
                    System.out.println();
            }
            T -= 1;
        }
    }
}
