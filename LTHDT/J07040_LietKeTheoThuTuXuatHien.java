import java.util.*;
import java.io.*;

/**
 *
 * @author Donald
 */
public class J07040_LietKeTheoThuTuXuatHien {

    static Scanner sc;

    public static void main(String[] args) throws Exception {
        sc = new Scanner(new FileInputStream("VANBAN.in"));
        ArrayList<String> vb = new ArrayList<>();
        while (sc.hasNext()) {
            vb.add(sc.next().toLowerCase());
        }
        sc.close();
        ObjectInputStream in = new ObjectInputStream(new FileInputStream("NHIPHAN.in"));
        ArrayList<String> np = (ArrayList<String>) in.readObject();
        in.close();
        ArrayList<String> npWords = new ArrayList<>();
        np.stream().map(str -> str.toLowerCase().split("\\s+")).forEachOrdered(tmp -> {
            npWords.addAll(Arrays.asList(tmp));
        });
        LinkedHashSet<String> setWord1 = new LinkedHashSet<>();
        setWord1.addAll(vb);
        setWord1.retainAll(npWords);
        setWord1.forEach(System.out::println);
    }
}
