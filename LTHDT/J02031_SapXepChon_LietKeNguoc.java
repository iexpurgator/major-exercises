import java.util.Scanner;

public class J02031_SapXepChon_LietKeNguoc {
    private static Scanner sc = new Scanner(System.in);
    static final String st = "Buoc ";

    public static void main(String[] args) {
        int n = Integer.parseInt(sc.nextLine());
        int step = 1;
        String res = "";
        String[] in = sc.nextLine().split("\\s");
        int[] a = new int[n];
        for (int i = 0; i < n; ++i) {
            a[i] = Integer.parseInt(in[i]);
        }

        for (int i = 0; i < n - 1; ++i) {
            int sma = i;
            for (int j = i + 1; j < n; j++) {
                if (a[sma] > a[j]) {
                    sma = j;
                }
            }
            int tmp = a[sma];
            a[sma] = a[i];
            a[i] = tmp;
            res = show(step++, n, a) + res;
        }
        System.out.print(res);
    }

    private static String show(int i, int n, int[] a) {
        String s = st + i + ":";
        for (int j = 0; j < n; ++j) {
            s += " " + a[j];
        }
        s += "\n";
        return s;
    }
}
