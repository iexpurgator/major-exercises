package danhsachsinhvien1;

import java.util.*;
import java.io.*;

public class J07013_DanhSachSinhVienTrongFileNhiPhan {
    public static void main(String[] args) throws Exception {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream("SV.in"));
        ArrayList<SinhVien> ds = (ArrayList<SinhVien>) in.readObject();
        ds.forEach(System.out::println);
    }
}