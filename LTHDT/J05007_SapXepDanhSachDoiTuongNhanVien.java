import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class J05007_SapXepDanhSachDoiTuongNhanVien {
    public static void main(String[] args) {
        List<NhanVien2> list = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        int i = 0;
        while (i++ < T) {
            String Name = sc.nextLine();
            String Sex = sc.nextLine();
            String Date = sc.nextLine();
            String AddrHome = sc.nextLine();
            String Phone = sc.nextLine();
            String SignDate = sc.nextLine();
            list.add(new NhanVien2(i, Name, Sex, Date, AddrHome, Phone, SignDate));
        }
        sc.close();
        Collections.sort(list, new Comparator<NhanVien2>() {
            @Override
            public int compare(NhanVien2 o1, NhanVien2 o2) {
                int d1 = formatDate(o1.getDate());
                int d2 = formatDate(o2.getDate());
                return (d1 - d2);
            }
        });
        list.stream().forEach(System.out::println);
    }

    private static int formatDate(String Date) {
        String[] s = Date.split("/");
        return Integer.parseInt(s[2] + s[0] + s[1] + ""); // MM/dd/yyyy -> yyyy/MM/dd
    }
}

class NhanVien2 {
    private String ID;
    private String Name;
    private String Sex;
    private String Date;
    private String AddrHome;
    private String Phone;
    private String SignDate;

    NhanVien2() {
    }

    public NhanVien2(int i, String name, String sex, String date, String addrHome, String phone, String signDate) {
        Name = name;
        Sex = sex;
        Date = date;
        AddrHome = addrHome;
        Phone = phone;
        SignDate = signDate;
        ID = String.format("%05d", i);
    }

    public String getDate() {
        return Date;
    }

    @Override
    public String toString() {
        return ID + " " + Name + " " + Sex + " " + Date + " " + AddrHome + " " + Phone + " " + SignDate;
    }
}
