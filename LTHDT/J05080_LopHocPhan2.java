import java.util.*;
import java.io.*;

public class J05080_LopHocPhan2 {
    public static void main(String[] args) throws IOException {
        J05080_Reader sc = new J05080_Reader();
        int T = Integer.parseInt(sc.nextLine());
        ArrayList<HocPhan80> list = new ArrayList<>();
        String id, name, ordinal, teachName;
        while (T-- > 0) {
            id = sc.nextLine();
            name = sc.nextLine();
            ordinal = sc.nextLine();
            teachName = sc.nextLine();
            list.add(new HocPhan80(id, name, ordinal, teachName));
        }
        list.sort((e1, e2) -> {
            if (e1.getId().equals(e2.getId()))
                return e1.getOrdinal().compareTo(e2.getOrdinal());
            else
                return e1.getId().compareTo(e2.getId());
        });
        T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            final String tName = sc.nextLine();
            int cnt = 0;
            for (HocPhan80 hp : list) {
                if (hp.getTeachName().equals(tName)) {
                    if (cnt == 0)
                        System.out.printf("Danh sach cho giang vien %s:\n", hp.getTeachName());
                    System.out.println(hp);
                    cnt++;
                }
            }
        }
    }
}

class HocPhan80 {
    private String id, name, ordinal, teachName;

    public HocPhan80(String id, String name, String ordinal, String teachName) {
        this.id = id;
        this.name = name;
        this.ordinal = ordinal;
        this.teachName = teachName;
    }

    public String getTeachName() {
        return teachName;
    }

    public String getId() {
        return id;
    }

    public String getOrdinal() {
        return ordinal;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + ordinal;
    }
}

class J05080_Reader {
    BufferedReader br;
    StringTokenizer tName;

    public J05080_Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (tName == null || !tName.hasMoreElements()) {
            try {
                tName = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return tName.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}
