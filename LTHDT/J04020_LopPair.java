import java.util.*;
import java.io.*;

public class J04020_LopPair {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        int T = sc.nextInt();
        while (T-- > 0) {
            int n = sc.nextInt();
            boolean check = false;
            for (int i = 2; i <= 2*Math.sqrt(n); i++) {
                Pair p = new Pair(i, n-i);
                if(p.isPrime()) {
                    System.out.println(p);
                    check = true;
                    break;
                }
            }
            if(!check) System.out.println(-1);
        }
    }
}

class Pair{
    private int a, b;

    public Pair(int a, int b) {
        this.a = a;
        this.b = b;
    }

    private boolean isPrime(int v){
        if(v < 2) return false;
        for(int i = 2; i <= (int) Math.sqrt(v); i++){
            if(v%i == 0) return false;
        }
        return true;
    }

    public boolean isPrime(){
        return isPrime(a) && isPrime(b);
    }

    @Override
    public String toString() {
        return a + " " + b;
    }
}