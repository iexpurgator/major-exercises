import java.io.*;
import java.util.*;

public class J07004_SoKhacNhauTrongFile1 {
    static Scanner sc;
    public static void main(String[] args) throws Exception {
        sc = new Scanner(new FileReader("DATA.in"));
        int[] cnt = new int[1001];
        String[] in;
        while (sc.hasNextLine()) {
            in = sc.nextLine().split("\\s+");
            for (String i : in) {
                cnt[Integer.parseInt(i)]++;
            }
        }
        for (int i = 0; i < 1001; i++) {
            if (cnt[i] > 0) {
                System.out.println(i + " " + cnt[i]);
            }
        }
    }
}
