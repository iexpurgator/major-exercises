import java.util.*;

public class J04009_DienTichTamGiac {
    static Scanner sc;
    public static void main(String[] args) {
        // List<Integer> list = new ArrayList<>();
        sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            double x, y;
            x = sc.nextDouble();
            y = sc.nextDouble();
            Point9 p1 = new Point9(x, y);
            x = sc.nextDouble();
            y = sc.nextDouble();
            Point9 p2 = new Point9(x, y);
            x = sc.nextDouble();
            y = sc.nextDouble();
            Point9 p3 = new Point9(x, y);
            double d12, d13, d23;
            d12 = p1.distance(p2);
            d13 = p1.distance(p3);
            d23 = p3.distance(p2);
            if (d12 + d13 <= d23 || d12 + d23 <= d13 || d13 + d23 <= d12) {
                System.out.println("INVALID");
            } else
                System.out.printf("%.02f\n", 0.25 * Math.sqrt((d12+d13+d23)*(d12+d13-d23)*(d12-d13+d23)*(-d12+d13+d23)));
        }
    }
}

class Point9 {
    private double x;
    private double y;

    public Point9() {
    }

    public Point9(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point9(Point9 p) {
        this.x = p.getX();
        this.y = p.getY();
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double distance(Point9 p) {
        return Math.sqrt(((x - p.getX()) * (x - p.getX())) + ((y - p.getY()) * (y - p.getY())));
    }

    public double distance(Point9 p1, Point9 p2) {
        return Math.sqrt(((p1.getX() - p2.getX()) * (p1.getX() - p2.getX()))
                + ((p1.getY() - p2.getY()) * (p1.getY() - p2.getY())));
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}
