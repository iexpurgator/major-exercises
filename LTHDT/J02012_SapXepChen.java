import java.util.Scanner;

public class J02012_SapXepChen {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int n = Integer.parseInt(sc.nextLine());
        String[] in = sc.nextLine().split("\\s");
        int[] a = new int[n];
        for (int i = 0; i < n; ++i) {
            a[i] = Integer.parseInt(in[i]);
        }

        for (int i = 0; i < n; ++i) {
            int cur = a[i];
            boolean hswap = false;
            for (int j = 0; j <= i; j++) {
                if (a[j] > cur) {
                    int tmp = cur;
                    cur = a[j];
                    a[j] = tmp;
                    hswap = true;
                }
            }
            if(hswap) a[i] = cur;
            show(i, i+1, a);
        }
    }

    private static void show(int i, int n, int[] a) {
        final String st = "Buoc ";
        System.out.print(st + i + ":");
        for (int j = 0; j < n; ++j) {
            System.out.print(" " + a[j]);
        }
        System.out.println();
    }
}
