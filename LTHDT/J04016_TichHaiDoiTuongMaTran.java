import java.util.*;

public class J04016_TichHaiDoiTuongMaTran {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int n = sc.nextInt(), m = sc.nextInt(), p = sc.nextInt();
        Matrix16 a = new Matrix16(n, m);
        a.nextMatrix(sc);
        Matrix16 b = new Matrix16(m, p);
        b.nextMatrix(sc);
        System.out.println(a.mul(b));
    }
}

class Matrix16 {
    private int x, y;
    private int[][] m;

    public Matrix16(int x, int y, int[][] m) {
        this.x = x;
        this.y = y;
        this.m = m;
    }

    public Matrix16(int x, int y) {
        this.x = x;
        this.y = y;
        m = new int[x][y];
    }

    public void nextMatrix(Scanner sc) {
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                m[i][j] = sc.nextInt();
            }
        }
    }

    public static Matrix16 mul(Matrix16 m1, Matrix16 m2) {
        int[][] m = new int[m1.x][m2.y];
        for (int i = 0; i < m1.x; i++) {
            for (int j = 0; j < m2.y; j++) {
                for (int k = 0; k < m1.y; k++) {
                    m[i][j] += m1.m[i][k] * m2.m[k][j];
                }
            }
        }
        return new Matrix16(m1.x, m2.y, m);
    }

    public Matrix16 mul(Matrix16 mtx) {
        return mul(this, mtx);
    }

    @Override
    public String toString() {
        String str = "";
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y - 1; j++) {
                str += m[i][j] + " ";
            }
            str += m[i][y - 1] + "\n";
        }
        return str;
    }
}