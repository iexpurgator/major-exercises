import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class J05032_TreNhatGiaNhat {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        List<Humar32> list = new ArrayList<>();
        int T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            String in = sc.nextLine();
            list.add(new Humar32(in));
        }
        Collections.sort(list, new Comparator<Humar32>(){
            @Override
            public int compare(Humar32 o1, Humar32 o2) {
                return o2.getDate().compareToIgnoreCase(o1.getDate());
            }
        });
        System.out.println(list.get(0));
        System.out.println(list.get(list.size() - 1));
    }
}

class Humar32 {
    private String name;
    private String date;

    Humar32() {
    }

    Humar32(String name, String date) {
        this.name = name;
        this.date = date;
    }

    Humar32(String in) {
        String[] i = in.split("\\s+");
        this.name = i[0];
        this.date = i[1];
    }

    public String getDate() {
        String[] d = date.split("/");
        return d[2] + d[1] + d[0];
    }

    @Override
    public String toString() {
        return name;
    }
}