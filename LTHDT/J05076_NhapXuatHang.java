import java.util.*;
import java.io.*;

public class J05076_NhapXuatHang {
    public static void main(String[] args) throws IOException {
        J05076_Reader sc = new J05076_Reader();
        int T = Integer.parseInt(sc.nextLine());
        ArrayList<HangHoa76> list = new ArrayList<>();
        String id, name, code;
        while (T-- > 0) {
            id = sc.nextLine();
            name = sc.nextLine();
            code = sc.nextLine();
            list.add(new HangHoa76(id, name, code.charAt(0)));
        }
        T = Integer.parseInt(sc.nextLine());
        String[] inp;
        HangHoa76 hh;
        final float[] tax = {0.08f, 0.05f, 0.02f};
        int j;
        while(T-- > 0) {
            inp = sc.nextLine().split("\\s+");
            j = 0;
            do {
                hh = list.get(j);
            } while(!hh.getId().equals(inp[0]) && j++ < list.size());
            hh.calc(Integer.parseInt(inp[1]), Integer.parseInt(inp[2]), Integer.parseInt(inp[3]), tax[hh.getCode() - 'A']);
            System.out.println(hh);
        }
    }
}

class HangHoa76{
    private String id;
    private String name;
    private char code;
    private int in, out;

    public HangHoa76(String id, String name, char code) {
        this.id = id;
        this.name = name;
        this.code = code;
    }

    public void calc(int countIn, int price, int countOut, float exp){
        in = countIn * price;
        out = Math.round(countOut * price * (1.0f + exp));
    }

    public char getCode() {
        return code;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + in + " " + out;
    }    
}

class J05076_Reader {
    BufferedReader br;
    StringTokenizer st;

    public J05076_Reader() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}
