package J07036;

/**
 *
 * @author Donald
 */
public class SinhVien {
    private String msv, name, lop, email;

    public SinhVien(String msv, String name, String lop, String email) {
        this.msv = msv;
        this.name = name;
        this.lop = lop;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getLop() {
        return lop;
    }

    public String getMsv() {
        return msv;
    }
    
    @Override
    public String toString() {
        return msv + " "+ name + "" + lop + " " + email;
    }
}
