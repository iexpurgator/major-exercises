import java.util.*;
import java.io.*;

/**
 *
 * @author Donald
 */
public class J07027_QuanLyBaiTapNhom {

    static Scanner sc;

    public static void main(String[] args) throws Exception {
        int n, m;
        String id, name, phoneNumber;
        int stt;

        sc = new Scanner(new FileInputStream("SINHVIEN.in"));
        n = Integer.parseInt(sc.nextLine());
        HashMap<String, SinhVien> map = new HashMap<>();
        for (int i = 0; i < n; i++) {
            id = sc.nextLine();
            name = sc.nextLine();
            phoneNumber = sc.nextLine();
            map.put(id, new SinhVien(id, name, phoneNumber));
        }

        sc = new Scanner(new FileInputStream("BAITAP.in"));
        m = Integer.parseInt(sc.nextLine());
        String[] list = new String[m];
        for (int i = 0; i < m; i++) {
            list[i] = sc.nextLine();
        }

        sc = new Scanner(new FileInputStream("NHOM.in"));
        ArrayList<Nhom> nhom = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            name = sc.next();
            stt = Integer.parseInt(sc.next());
            nhom.add(new Nhom(stt, name, map.get(name), list[stt - 1]));
        }
        Collections.sort(nhom);
        nhom.forEach(System.out::println);
    }
}

class SinhVien {

    private String id;
    private String name;
    private String phoneNumber;

    public SinhVien(String id, String name, String phoneNumber) {
        this.id = id;
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + phoneNumber;
    }
}

class Nhom implements Comparable<Nhom> {

    private int id;
    private String svId;
    private SinhVien sv;
    private String nameProject;

    public Nhom(int id, String svId, SinhVien sv, String nameProject) {
        this.id = id;
        this.svId = svId;
        this.sv = sv;
        this.nameProject = nameProject;
    }

    @Override
    public String toString() {
        return sv + " " + id + " " + nameProject;
    }

    @Override
    public int compareTo(Nhom o) {
        return svId.compareTo(o.svId);
    }
}