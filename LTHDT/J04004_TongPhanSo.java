import java.util.Scanner;

public class J04004_TongPhanSo {
    public static void main(String[] args) {
        long tu, mau;
        Scanner sc = new Scanner(System.in);
        tu = sc.nextLong();
        mau = sc.nextLong();
        PhanSo1 p = new PhanSo1(tu, mau);
        tu = sc.nextLong();
        mau = sc.nextLong();
        PhanSo1 p2 = new PhanSo1(tu, mau);
        p.Add(p2);
        System.out.println(p);
        sc.close();
    }
}

class PhanSo1 {
    private long tu;
    private long mau;

    PhanSo1() {
    }

    PhanSo1(long tu, long mau) {
        this.tu = tu;
        this.mau = mau;
    }

    public void nhap() {
        Scanner sc = new Scanner(System.in);
        tu = sc.nextLong();
        mau = sc.nextLong();
        sc.close();
    }

    private long GCD(long a, long b) {
        return a == 0 ? b : GCD(b % a, a);
    }

    public void rutGon() {
        long k = GCD(this.tu, this.mau);
        this.tu /= k;
        this.mau /= k;
    }

    public void Add(PhanSo1 p){
        this.tu = this.tu * p.mau + p.tu * this.mau;
        this.mau = p.mau * this.mau;
        rutGon();
    }

    @Override
    public String toString() {
        return (this.tu + "/" + this.mau);
    }
}
