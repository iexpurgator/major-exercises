import java.util.*;

public class J05022_LietKeSinhVienTheoLop {
    static Scanner sc;
    public static void main(String[] args) {
        List<SV22> list = new ArrayList<>();
        sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            String id, name, iclass, email;
            id = sc.nextLine();
            name = sc.nextLine();
            iclass = sc.nextLine();
            email = sc.nextLine();
            list.add(new SV22(id, name, iclass, email));
        }
        T = Integer.parseInt(sc.nextLine());
        while (T-- > 0) {
            String iclass = sc.nextLine();
            System.out.println("DANH SACH SINH VIEN LOP " + iclass + ':');
            list.stream().filter(e -> e.getIclass().equals(iclass)).forEach(System.out::println);
        }
    }
}

class SV22 {

    private String id;
    private String name;
    private String iclass;
    private String email;

    public SV22() {
    }

    public SV22(String id, String name, String iclass, String email) {
        this.id = id;
        this.name = name;
        this.iclass = iclass;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public String getIclass() {
        return iclass;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + iclass + " " + email;
    }
}
