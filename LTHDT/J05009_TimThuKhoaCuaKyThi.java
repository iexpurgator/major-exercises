import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

public class J05009_TimThuKhoaCuaKyThi {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        int id = 0;
        List<ThiSinh> list = new ArrayList<>();
        while (id++ < T) {
            String name, date;
            double s1, s2, s3;
            name = sc.nextLine();
            date = sc.nextLine();
            s1 = Double.parseDouble(sc.nextLine());
            s2 = Double.parseDouble(sc.nextLine());
            s3 = Double.parseDouble(sc.nextLine());
            list.add(new ThiSinh(id, name, date, s1, s2, s3));
        }
        list.sort((a, b) -> (int) ((b.getSum() * 100) - (a.getSum() * 100)));
        double tk = list.get(0).getSum();
        list.stream().filter(o -> o.getSum() == tk).forEach(System.out::println);
    }
}

class ThiSinh {
    private int id;
    private String name;
    private String date;
    private double sum;

    public ThiSinh(int id, String name, String date, double s1, double s2, double s3) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.sum = s1 + s2 + s3;
    }

    public double getSum() {
        return sum;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + date + " " + sum;
    }
}