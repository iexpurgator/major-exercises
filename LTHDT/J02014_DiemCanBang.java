import java.util.Scanner;

public class J02014_DiemCanBang {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        while (T > 0) {
            int n = Integer.parseInt(sc.nextLine());
            String[] in = sc.nextLine().split("\\s");
            int[] a = new int[n];
            int[] s = new int[n];
            for (int i = 0; i < n; ++i) {
                a[i] = Integer.parseInt(in[i]);
                s[i] = (i == 0) ? a[i] : a[i] + s[i - 1];
            }
            System.out.println(equ(s, n));
            T -= 1;
        }
    }

    private static int equ(int s[], int n) {
        for (int i = 1; i < n-1; ++i)
            if (s[i-1] == s[n-1] - s[i])
                return i+1;
        return -1;
    }

    static int equilibrium(int arr[], int n) {
        for (int i = 0; i < n; ++i) {
            int leftsum = 0;
            for (int j = 0; j < i; j++)
                leftsum += arr[j];
            int rightsum = 0;
            for (int j = i + 1; j < n; j++)
                rightsum += arr[j];
            if (leftsum == rightsum)
                return i + 1;
        }
        return -1;
    }
}
