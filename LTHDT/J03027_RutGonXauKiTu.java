import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class J03027_RutGonXauKiTu {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        List<Character> list = sc.nextLine().chars().mapToObj(c -> (char) c).collect(Collectors.toList());
        int i = 0;
        while (i < list.size()) {
            if (i + 1 < list.size() && list.get(i).equals(list.get(i + 1))) {
                list.remove(i);
                list.remove(i);
                i = 0;
            } else {
                i++;
            }
        }
        if (list.isEmpty())
            System.out.print("Empty String");
        else
            list.forEach(System.out::print);
        System.out.println();
    }
}
