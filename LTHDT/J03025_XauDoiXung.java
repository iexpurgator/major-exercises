import java.util.Scanner;

public class J03025_XauDoiXung {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        while (T > 0) {
            String s = sc.nextLine();
            System.out.println(isPari(s) ? "YES" : "NO");
            T -= 1;
        }
    }

    private static boolean isPari(String s) {
        char[] cs = s.toCharArray();
        int cnt = 0;
        for (int i = 0; i < s.length(); i++) {
            if (i == s.length() - i - 1)
                break;
            if (cs[i] != cs[s.length() - i - 1]) {
                cnt++;
            }
            if (cnt > 1 || i + 1 == s.length() - i - 1)
                break;
        }
        return cnt == 1 || (s.length() % 2 == 1 && cnt == 0);
    }
}
