import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

public class J05062_HocBongSinhVien {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = sc.nextInt();
        int n = sc.nextInt();
        int id = 0;
        List<SinhVien62> list = new ArrayList<>();
        List<Double> listTBC = new ArrayList<>();
        while (id++ < T) {
            sc.nextLine();
            String name;
            double tbc;
            int rl;
            name = sc.nextLine();
            tbc = sc.nextDouble();
            rl = sc.nextInt();
            listTBC.add(tbc);
            list.add(new SinhVien62(name, tbc, rl));
        }
        listTBC.sort((o1, o2) -> (int) ((o2 * 100) - (o1 * 100)));
        double tbcBase = listTBC.get(n - 1);
        list.forEach((o) -> {
            if (o.getTbc() >= tbcBase) {
                System.out.println(o + o.getXh());
            } else {
                System.out.println(o + "KHONG");
            }
        });
    }
}

class SinhVien62 {
    private String name;
    private String xh;
    private double tbc;
    private int rl;

    public SinhVien62(String name, double tbc, int rl) {
        this.name = name;
        this.tbc = tbc;
        this.rl = rl;
        calc();
    }

    private void calc() {
        if (tbc >= 3.6 && rl >= 90) {
            xh = "XUATSAC";
        } else if (tbc >= 3.2 && rl >= 80) {
            xh = "GIOI";
        } else if (tbc >= 2.5 && rl >= 70) {
            xh = "KHA";
        } else {
            xh = "KHONG";
        }
    }

    public double getTbc() {
        return tbc;
    }

    public String getXh() {
        return xh;
    }

    @Override
    public String toString() {
        return name + ": ";
    }
}