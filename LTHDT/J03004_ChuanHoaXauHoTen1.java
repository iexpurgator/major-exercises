import java.util.Scanner;

public class J03004_ChuanHoaXauHoTen1 {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int T = Integer.parseInt(sc.nextLine());
        while (T > 0) {
            args = sc.nextLine().toLowerCase().trim().split("\\s+");
            for (String s : args) {
                System.out.print(s.substring(0, 1).toUpperCase() + s.substring(1) + " ");
            }
            System.out.println();
            T -= 1;
        }
    }
}
