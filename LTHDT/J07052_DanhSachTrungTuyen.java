import java.io.*;
import java.util.*;
import java.text.*;

/**
 * @author Donald
 */
public class DanhSachTrungTuyen {

    static Scanner sc;

    public static void main(String[] args) throws Exception {
        sc = new Scanner(new FileInputStream("THISINH.in"));
        int T = Integer.parseInt(sc.nextLine());
        ArrayList<ThiSinh> listThiSinh = new ArrayList<>();
        String ma, ten;
        float dToan, dLy, dHoa;
        while (T-- > 0) {
            ma = sc.nextLine();
            ten = sc.nextLine();
            dToan = Float.parseFloat(sc.nextLine());
            dLy = Float.parseFloat(sc.nextLine());
            dHoa = Float.parseFloat(sc.nextLine());
            listThiSinh.add(new ThiSinh(ma, ten, dToan, dLy, dHoa));
        }
        Collections.sort(listThiSinh);
        int n = Integer.parseInt(sc.nextLine());
        float diemSan = 0;
        for (ThiSinh ts : listThiSinh) {
            if (n > 0) {
                diemSan = ts.getDiemXetTuyen();
                n--;
            }
            if (n == 0) break;
        }
        float finalDiemSan = diemSan;
        System.out.printf("%.01f\n", diemSan);
        listThiSinh.forEach((ts) -> {
            if (ts.getDiemXetTuyen() >= finalDiemSan)
                System.out.println(ts + " TRUNG TUYEN");
            else System.out.println(ts + " TRUOT");
        });
    }
}

class ThiSinh implements Comparable<ThiSinh> {
    private final HashMap<String, Float> diemUuTien = new HashMap<String, Float>() {{
        put("KV1", 0.5F);
        put("KV2", 1.0F);
        put("KV3", 2.5F);
    }};
    private String ma;
    private String ten;
    private float diemToan, diemLy, diemHoa;

    public ThiSinh(String ma, String ten, float diemToan, float diemLy, float diemHoa) {
        this.ma = ma;
        this.ten = capitalize(ten);
        this.diemToan = diemToan;
        this.diemLy = diemLy;
        this.diemHoa = diemHoa;
    }

    private String capitalize(String string) {
        String[] split = string.toLowerCase().trim().split("\\s+");
        String capitalized = "";
        for (String s : split) {
            capitalized += Character.toUpperCase(s.charAt(0)) + s.substring(1) + " ";
        }
        return capitalized.trim();
    }

    public float getDiemUuTien() {
        return diemUuTien.get(ma.substring(0, 3));
    }

    public float getDiemXetTuyen() {
        return diemToan*2 + diemLy + diemHoa + getDiemUuTien();
    }

    @Override
    public String toString() {
        DecimalFormat df = new DecimalFormat("#.#");
        return ma + " " + ten + " " + df.format(getDiemUuTien()) + " " + df.format(getDiemXetTuyen());
    }

    @Override
    public int compareTo(ThiSinh o) {
        return (int) ((o.getDiemXetTuyen() * 10) - (getDiemXetTuyen() * 10));
    }
}