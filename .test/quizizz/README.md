# Tạo nhiều câu hỏi trên quizizz

1. Chạy file [main.py](./main.py), sửa file input tương tự như file [example.txt](./example.txt)

2. Mở excel > tạo file mới > Data > import from text chọn file [example.csv](). Chọn UTF-8 và Delimited > (next) > Delimiters other `,` và Treat consecutive `"` > (finish)

3. Điền đáp án > Lưu lại file thành `.xls`, mở Quizizz chọn import từ `.xls`
