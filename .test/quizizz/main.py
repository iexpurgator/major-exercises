import re
import csv

file = open("example.txt", "r", encoding='utf-8')
outfile = open("example.csv", "w", encoding='utf-8', newline='')
line = 0
writer = csv.writer(outfile)

question = ["Question Text","Question Type","Option 1","Option 2","Option 3","Option 4","Option 5","Correct Answer","Time in seconds","Image Link"]
writer.writerow(question)
question = []
bEOF = False

while True:
    file_line = file.readline()
    if not file_line:
        bEOF = True
    if re.match("Câu \d+. ", file_line) or bEOF:
        # print(question)
        if question:
            # print(question)
            while len(question) < 7:
                question.append("\0")
            question.append("0")
            question.append("20")
            question.append("")
            writer.writerow(question)
            if bEOF:
                break
        question = []
        file_line = re.split("Câu \d+. ", file_line)[1].strip()
        question.append(file_line)
        question.append("Multiple Choice")
    else:
        # print(re.split(".*\t", file_line))
        file_line = re.split(".*\t", file_line)[1].strip()
        question.append(file_line)

file.close()
outfile.close()