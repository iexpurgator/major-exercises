Câu 1. Miền bảo vệ của ma trận kiểm soát truy cập bao gồm gồm tập các ... mà tiến trình có thể truy cập và tập các ... mà tiến trình có thể sử dụng.
A.	đối tượng và chủ thể
B.	chủ thể và thao tác
C.	đối tượng và dữ liệu
D.	đối tượng và thao tác
Câu 2. Đâu không phải là một phiên bản của hệ điều hành Microsoft Windows?
A.	Windows 98
B.	Windows 3.0
C.	Windows Mee
D.	Windows 3.1
Câu 3. Các nhãn trong hệ thống bảo vệ bắt buộc chống lại việc xâm nhập nhờ chúng được xây dựng bởi người quản trị tin cậy sử dụng phần mềm tin cậy và chúng cũng không bị thay đổi bởi ...
A.	các tiến trình không tin cậy của hệ thống
B.	các tiến trình tin cậy của hệ thống
C.	các tiến trình tin cậy của người dùng
D.	các tiến trình không tin cậy của người dùng
Câu 4. Hệ thống bảo vệ bắt buộc là hệ thống chỉ có thể được sửa đổi bởi ... thông qua phần mềm tin cậy.
A.	người quản trị tin cậy
B.	người dùng tin cậy
C.	người quản trị hệ thống
D.	chủ thể tin cậy
Câu 5. Các thành phần của bộ giám sát tham chiếu bao gồm: ..., mô đun xác thực và ...
A.	truyền thông và chính sách
B.	tập luật và giao tiếp
C.	chính sách và kho giao tiếp
D.	giao tiếp và kho chính sách
Câu 6. Trong an toàn hệ điều hành, tính bí mật giới hạn ...
A.	các thao tác có thể được thực hiện
B.	các chủ thể có thể được truy cập
C.	các chủ thể và đối tượng có thể được truy cập
D.	các đối tượng có thể được truy cập
Câu 7. Kho chính sách là cơ sở dữ liệu gồm: các trạng thái bảo vệ, ..., và các trạng thái dịch chuyển.
A.	trạng thái tự do
B.	các tên trạng thái
C.	các nhãn trạng thái
D.	các ID của trạng thái
Câu 8. Chính sách an toàn (Security policy) mô tả các ..., hành động và ... cần được thực hiện cho hệ thống thông tin.
A.	kiểm soát và quy định
B.	kiểm tra và quy trình
C.	kiểm soát và quy trình
D.	kiểm tra và quy định
Câu 9. Đâu không phải là một hệ điều hành cho máy tính?
A.	Cisco iOS
B.	MacOS
C.	Windows 10
D.	Ubuntu
Câu 10. Tìm phát biểu đúng về vấn đề an toàn:
A.	Coi trọng vấn đề an toàn ngang bằng như các tính năng vận hành của hệ thống
B.	Coi vấn đề an toàn ít quan trọng hơn các tính năng vận hành của hệ thống
C.	Coi vấn đề an toàn là một phần việc có thể thực hiện riêng
D.	Coi vấn đề an toàn quan trọng hơn các tính năng vận hành của hệ thống
Câu 11. Trong hệ điều hành an toàn, việc thực thi của các phần mềm không bị phá vỡ bởi...
A.	các chương trình không nằm trong danh sách các phần mềm tin cậy
B.	các chương trình người dùng nằm trong danh sách các phần mềm tin cậy
C.	các phần mềm độc hại
D.	các phần mềm ứng dụng
Câu 12. Đâu là một cơ chế, hay biện pháp bảo vệ?
A.	Ma trận kiểm soát truy cập
B.	Ma trận truy cập có kiểm soát
C.	Hệ thống bắt buộc bảo vệ
D.	Ma trận giám sát truy cập
Câu 13. Kiến trúc an toàn tránh phụ thuộc vào tính bí mật để đảm bảo an toàn, ngoại trừ việc ...
A.	quản lý mật khẩu và tên người dùng
B.	quản lý mật khẩu
C.	quản lý tài nguyên
D.	quản lý tài khoản
Câu 14. Mục tiêu an toàn (security goals) xác định các thao tác có thể được thực hiện bởi hệ thống trong khi ngăn chặn ...
A.	các dạng xâm nhập
B.	các dạng tấn công
C.	các truy cập từ người dùng ác tính
D.	các truy cập trái phép
Câu 15. Phần nhân an toàn cần phải nhỏ nhất có thể được để có thể ... của nó một cách dễ dàng.
A.	xác minh tính đúng đắn
B.	trắc nghiệm tính đúng đắn
C.	xác định khả năng bảo vệ
D.	xác định khả năng chống xâm nhập
Câu 16: Trong hệ điều hành an toàn, một phần mềm không được coi là tin cậy khi nó
A.	Bị gây nhiễu
B.	Bị phá hủy
C.	Bị xâm nhập
Câu 17: Các yêu cầu đảm bảo an toàn hệ điều hành bao gồm:
A.	Tính chống chối bỏ và xác thực được
B.	Tính bí mật, sẵn dùng
C.	Tính toàn vẹn và sẵn dùng
D.	Tính bí mật, toàn vẹn và sẵn dùng
Câu 18: Đâu không phải là tính năng/công cụ do hệ điều hành cung cấp
A.	Trình điều khiển thiết bị
B.	Tính năng an toàn và bảo mật
C.	Giao diện đồ họa
D.	Thành phần quản trị mạng
Câu 19: Truy cập hệ thống được mô tả bằng ... có thể thực hiện ... lên ...
A.	Chủ thể, Thao tác, Đối tượng
B.	Chủ thể, Đối tượng, Thao tác
C.	Đối tượng, Thao tác, Chủ thể
D.	Đối tượng, Chủ thể, Thao tác
Câu 20: Kiến trúc an toàn và quá trình thiết kế - xây dựng hệ thống có thể thực hiện theo cách ...
A.	Có thể thực hiện song song các việc trên
B.	Kiến trúc an toàn phải cần được thực hiện trước một bước
C.	Kiến trúc an toàn phải được thực hiện trước toàn bộ
D.	Kiến trức an toàn có thể được thực hiện sau
Câu 21: Đâu không phải là một yếu tố giúp xây dựng hệ điều hành an toàn?
A.	Mô hình an toàn
B.	Cơ chế bảo vệ
C.	Mô hình tin cậy
D.	Mô hình đe dọa
E.	Mục tiêu an toàn
Câu 22: Một trong số các quy tắc đảm bảo an toàn liên quan đến cấp quyền cho chủ thể là:
A.	Quyền tối đa
B.	Quyền tối thiểu
C.	Quyền liên tục
D.	Không cấp quyền
Câu 23: Các loại chính sách an toàn thông tin bao gồm:
A.	Chính sách chung cho từng tổ chức và chính sách cho từng đơn vị cụ thể
B.	Chính sách toàn cục và chính sách cụ thể
C.	Chính sách chung và chính sách cho ứng dụng cụ thể
D.	Chính sách chung và chính sách cho đơn vị cụ thể
Câu 24: Hệ điều hành không cung cấp gì
A.	Một số các dịch vụ và ứng dụng cơ bản cho người dùng
B.	Giao diện giữa người dùng và phần cứng máy tính/ thiết bị tính toán
C.	Một hệ thống toàn diện
D.	Môi trường cho các chương trình ứng dụng hoạt động
Câu 25: Để đạt được độ tin cậy cao về an toàn của hệ thống, người thiết kế cần ... của các phần liên quan tới an toàn của thiết kế.
A.	Giảm thiểu kích cỡ mã thực hiện và dữ liệu
B.	Giảm thiểu độ phức tạp tính toán và mã
C.	Giảm thiểu kích cỡ và độ phức tạp
D.	Giảm thiểu kích cỡ mã nguồn và mã thực hiện
Câu 26: Các chức năng cơ bản của HĐH bao gồm:
A.	Quản lý tiến trình, bộ nhớ và giao diện người dùng
B.	Quản lý tiến trình, bộ nhớ và người dùng
C.	Quản lý tiến trình, bộ nhớ, đĩa và hệ thống file
D.	Quản lý người dùng, đĩa và hệ thống file
Câu 27: Đâu không phải là một phiên bản của hệ điều hành Microsoft Windows:
A.	Windows NT 3, 4
B.	Windows XP
C.	MS-DOS, PC-DOS
D.	Windows Me
Câu 28: Mô hình đe dọa (Threat model) xây dựng tập các thao tác mà người tấn công có thể dùng để ...
A.	Vô hiệu hóa hệ thống
B.	Đánh cắp dữ liệu từ hệ thống
C.	Truy cập trái phép vào hệ thống
Câu 29: Danh sách kiểm soát truy cập được sử dụng thay thế ma trận kiểm soát truy cập do ma trận kiểm soát truy cập ...
A.	Có hiệu quả sử dụng bộ nhớ kém
B.	Là ma trận thưa
C.	Có tốc độ truy cập chậm
D.	Kích thước quá lớn
Câu 30: Mô hình tin cậy (Trust model) của hệ thống định nghĩa tập ... mà hệ thống sử dụng để đảm bảo thực hiện chính xác các mục tiêu an toàn của hệ thống
A.	Chủ thể và đối tượng
B.	Thực thể và đối tượng
C.	Phần mềm và dữ liệu
D.	Thực thể và thao tác
Câu 31: CSDL về kiểm soát truy cập thể hiện trạng thái an toàn của hệ thống và chứa các thông tin như ...
A.	Các thao tác truy cập và các thuộc tính an ninh
B.	Quyền truy cập và các thao tác
C.	Quyền truy cập và các thuộc tính an ninh
Câu 32: Nhân an toàn là phần cơ sở nền tảng có thể ... của hệ điều hành để đảm bảo an toàn cho hệ thống
A.	Thống kê được
B.	Phân tích được
C.	Xác minh được
D.	Kiểm chứng được
Câu 33: Các phần mềm tin cậy trong mô hình tin cậy bao gồm: ...
A.	Các phần mềm xác định và thực hiện các yêu cầu an toàn của người dùng
B.	Các phần mềm xác thực, cấp quyền và quản trị người dùng
C.	Các phần mềm xác định và thực hiện các yêu cầu an toàn của hệ thống
D.	Các phần mềm xác thực và cấp quyền người dùng
Câu 34: Hệ điều hành – OS là:
A.	Một chương trình quản lý máy tính
B.	Một chương trình quản lý các tài nguyên phần cứng và phần mềm của thiết bị tính toán
C.	Một chương trình giữa phần cứng và phần mềm
D.	Một chương trình chính cung cấp giao diện cho người dùng sử dụng hệ thống máy tính
Câu 35: Các cơ chế an toàn không được ảnh hưởng tới người dùng theo nghĩa chúng phải:
A.	Đơn giản với người dùng quản trị
B.	Trong suốt với người dùng quản trị
C.	Trong suốt với người dùng bình thường
D.	Đơn giản với người dùng bình thường
Câu 36: Trong ATHĐH, Tính sẵn dùng hạn chế các tài nguyên mà các chủ thể có thể sử dụng do các chủ thể này có thể ...
A.	Làm cạn kiệt các tài nguyên hệ thống
B.	Chiếm quyền kiểm soát hệ thống
C.	Làm cạn kiệt các tài nguyên đó
D.	Làm cạn kiệt các tài nguyên bộ nhớ
Câu 37: Các thuộc tính của bộ giám sát tham chiếu đảm bảo yêu cầu an toàn:
A.	Ngăn chặn một phần, Chống phá hại, Xác minh được
B.	Ngăn chặn hoàn toàn, Chống xâm nhập, Xác minh được
C.	Ngăn chặn một phần, Xác minh được
D.	Chống xâm nhập, Xác minh được
