package Oxyz;

class Vector3D extends Oxyz {

    Vector3D() { }

    public Vector3D(int x, int y, int z) {
        super(x, y, z);
    }

    public static int mul(Vector3D v1, Vector3D v2) {
        return v1.mul(v2);
    }

    public static Vector3D cross(Vector3D v1, Vector3D v2) {
        return v1.cross(v2);
    }

    public Vector3D cross(Vector3D v) {
        int x = (getY() * v.getZ()) - (getZ() * v.getY());
        int y = (getZ() * v.getX()) - (getX() * v.getZ());
        int z = (getX() * v.getY()) - (getY() * v.getX());
        return new Vector3D(x, y, z);
    }

    public int mul(Vector3D v) {
        return (getX() * v.getX()) + (getY() * v.getY()) + (getZ() * v.getZ());
    }
}
