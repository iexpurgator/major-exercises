package Oxyz;

class Point3D extends Oxyz {

    Point3D() { }

    public Point3D(int x, int y, int z) {
        super(x, y, z);
    }

    public static Vector3D toVector3D(Point3D p1, Point3D p2) {
        return p1.to(p2);
    }

    public static boolean check(Point3D p1, Point3D p2, Point3D p3, Point3D p4) {
        Vector3D v12 = p1.to(p2);
        Vector3D v13 = p1.to(p3);
        Vector3D c123 = Vector3D.cross(v12, v13);
        return toVector3D(p1, p4).mul(c123) == 0;
    }

    public Vector3D to(Point3D p) {
        return new Vector3D(getX() - p.getX(), getY() - p.getY(), getZ() - p.getZ());
    }
}
